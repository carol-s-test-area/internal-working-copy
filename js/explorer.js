window['explorer'] = (function() {
var w = new Object();
fil = new Array();
fil["0"]= "AddColumnBreaks.html@@@Add a column break@@@In addition to adding row breaks to group your data, you can add a column break...";
fil["1"]= "AddMetricModifier.html@@@Add a metric modifier@@@In addition to the adding simple metrics to your report, you can add a metric modifier...";
fil["2"]= "AddSubtotals.html@@@Add subtotals@@@You can add several layers of subtotals to your Explorer report...";
fil["3"]= "ArrangeBy.html@@@Use Arrange By to rearrange columns@@@In certain situations, you can use Arrange By to rearrange columns in your report...";
fil["4"]= "CreateReport.html@@@Create and run a report@@@Select date ranges, metrics, dimensions, and filters for a basic report...";
fil["5"]= "DeleteJob.html@@@Delete a job@@@You can delete a scheduled job from the Your Scheduled Jobs list...";
fil["6"]= "DeleteReport.html@@@Delete a saved report@@@You can delete a saved report from Explorer...";
fil["7"]= "DownloadJob.html@@@Download a scheduled report@@@After a job runs (scheduled or manually run), Explorer automatically sends an email to the recipients specified in the job...";
fil["8"]= "EditJob.html@@@Edit a job@@@You can edit an existing report job...";
fil["9"]= "ExplorerFeatures.html@@@Explorer features@@@The following section describes the Explorer features beyond the basics...";
fil["10"]= "ExplorerWorkspace.html@@@The Explorer workspace@@@The Explorer workspace consists of the following elements...";
fil["11"]= "ExportReport.html@@@Export a report@@@Once you are satisfied with your report, you can export it to an Excel (XLSX) or comma-separated values (CSV) file...";
fil["12"]= "GettingStarted.html@@@Getting started@@@The following section provides basic information about Explorer...";
fil["13"]= "HierarchicalFilters.html@@@Add hierarchical filters@@@You can add hierarchical filters to your Explorer report...";
fil["14"]= "Introduction.html@@@Introduction@@@Explorer is a point-and-click data analysis and exploration tool...";
fil["15"]= "Login.html@@@Sign in@@@You must sign in to Explorer with appropriate credentials in order to create reports...";
fil["16"]= "Logout.html@@@Sign out@@@Sign out to close your active Explorer session...";
fil["17"]= "OpenReport.html@@@Open a saved report@@@You can open a report you saved or a report that has been shared with your user ID...";
fil["18"]= "ReSortOutput.html@@@Re-sort report output@@@You can re-sort the rows in the default report output...";
fil["19"]= "RearrangeColumns.html@@@Use drag and drop to rearrange columns@@@Rearranging columns is a simple as dragging and dropping the columns...";
fil["20"]= "RearrangeRowBreaks.html@@@Use drag-and-drop to rearrange row breaks@@@You can rearrange row breaks by dragging and dropping dimensions...";
fil["21"]= "ReportBasics.html@@@Report basics@@@This section describes the basic actions of running a report, as well as saving a report and opening a saved report...";
fil["22"]= "ReportOutput.html@@@Rearranging the report output@@@You can rearrange the report output after you run a report...";
fil["23"]= "ReportScheduling.html@@@Report scheduling@@@If you need the same report on a certain day of the week or at a certain time of day,\n        schedule the report...";
fil["24"]= "SaveReport.html@@@Save and share a report@@@You can save reports and share reports with your team...";
fil["25"]= "ScheduleNewJob.html@@@Schedule a new job@@@If you need the same report on a certain day of the week or at a certain time of day,\n        schedule the report...";
fil["26"]= "SendReportNow.html@@@Run a job@@@You can manually run an existing scheduled job and have a report sent immediately...";
var doStem = false;searchLoaded = true;// Porter stemmer in Javascript. Few comments, but it's easy to follow against the rules in the original
// paper, in
//
//  Porter, 1980, An algorithm for suffix stripping, Program, Vol. 14,
//  no. 3, pp 130-137,
//
// see also http://www.tartarus.org/~martin/PorterStemmer

// Release 1 be 'andargor', Jul 2004
// Release 2 (substantially revised) by Christopher McKenzie, Aug 2009


var stemmer = (function(){
	var step2list = {
			"ational" : "ate",
			"tional" : "tion",
			"enci" : "ence",
			"anci" : "ance",
			"izer" : "ize",
			"bli" : "ble",
			"alli" : "al",
			"entli" : "ent",
			"eli" : "e",
			"ousli" : "ous",
			"ization" : "ize",
			"ation" : "ate",
			"ator" : "ate",
			"alism" : "al",
			"iveness" : "ive",
			"fulness" : "ful",
			"ousness" : "ous",
			"aliti" : "al",
			"iviti" : "ive",
			"biliti" : "ble",
			"logi" : "log"
		},

		step3list = {
			"icate" : "ic",
			"ative" : "",
			"alize" : "al",
			"iciti" : "ic",
			"ical" : "ic",
			"ful" : "",
			"ness" : ""
		},

		c = "[^aeiou]",          // consonant
		v = "[aeiouy]",          // vowel
		C = c + "[^aeiouy]*",    // consonant sequence
		V = v + "[aeiou]*",      // vowel sequence

		mgr0 = "^(" + C + ")?" + V + C,               // [C]VC... is m>0
		meq1 = "^(" + C + ")?" + V + C + "(" + V + ")?$",  // [C]VC[V] is m=1
		mgr1 = "^(" + C + ")?" + V + C + V + C,       // [C]VCVC... is m>1
		s_v = "^(" + C + ")?" + v;                   // vowel in stem

	return function (w) {
		var 	stem,
			suffix,
			firstch,
			re,
			re2,
			re3,
			re4,
			origword = w;

		if (w.length < 3) { return w; }

		firstch = w.substr(0,1);
		if (firstch == "y") {
			w = firstch.toUpperCase() + w.substr(1);
		}

		// Step 1a
		re = /^(.+?)(ss|i)es$/;
		re2 = /^(.+?)([^s])s$/;

		if (re.test(w)) { w = w.replace(re,"$1$2"); }
		else if (re2.test(w)) {	w = w.replace(re2,"$1$2"); }

		// Step 1b
		re = /^(.+?)eed$/;
		re2 = /^(.+?)(ed|ing)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			re = new RegExp(mgr0);
			if (re.test(fp[1])) {
				re = /.$/;
				w = w.replace(re,"");
			}
		} else if (re2.test(w)) {
			var fp = re2.exec(w);
			stem = fp[1];
			re2 = new RegExp(s_v);
			if (re2.test(stem)) {
				w = stem;
				re2 = /(at|bl|iz)$/;
				re3 = new RegExp("([^aeiouylsz])\\1$");
				re4 = new RegExp("^" + C + v + "[^aeiouwxy]$");
				if (re2.test(w)) {	w = w + "e"; }
				else if (re3.test(w)) { re = /.$/; w = w.replace(re,""); }
				else if (re4.test(w)) { w = w + "e"; }
			}
		}

		// Step 1c
		re = /^(.+?)y$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(s_v);
			if (re.test(stem)) { w = stem + "i"; }
		}

		// Step 2
		re = /^(.+?)(ational|tional|enci|anci|izer|bli|alli|entli|eli|ousli|ization|ation|ator|alism|iveness|fulness|ousness|aliti|iviti|biliti|logi)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			suffix = fp[2];
			re = new RegExp(mgr0);
			if (re.test(stem)) {
				w = stem + step2list[suffix];
			}
		}

		// Step 3
		re = /^(.+?)(icate|ative|alize|iciti|ical|ful|ness)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			suffix = fp[2];
			re = new RegExp(mgr0);
			if (re.test(stem)) {
				w = stem + step3list[suffix];
			}
		}

		// Step 4
		re = /^(.+?)(al|ance|ence|er|ic|able|ible|ant|ement|ment|ent|ou|ism|ate|iti|ous|ive|ize)$/;
		re2 = /^(.+?)(s|t)(ion)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(mgr1);
			if (re.test(stem)) {
				w = stem;
			}
		} else if (re2.test(w)) {
			var fp = re2.exec(w);
			stem = fp[1] + fp[2];
			re2 = new RegExp(mgr1);
			if (re2.test(stem)) {
				w = stem;
			}
		}

		// Step 5
		re = /^(.+?)e$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(mgr1);
			re2 = new RegExp(meq1);
			re3 = new RegExp("^" + C + v + "[^aeiouwxy]$");
			if (re.test(stem) || (re2.test(stem) && !(re3.test(stem)))) {
				w = stem;
			}
		}

		re = /ll$/;
		re2 = new RegExp(mgr1);
		if (re.test(w) && re2.test(w)) {
			re = /.$/;
			w = w.replace(re,"");
		}

		// and turn initial Y back to y

		if (firstch == "y") {
			w = firstch.toLowerCase() + w.substr(1);
		}

		return w;
	}
})();// Auto generated list of analyzer stop words that must be ignored by search.
stopWords = new Array();
stopWords[0]= "but";
stopWords[1]= "be";
stopWords[2]= "with";
stopWords[3]= "such";
stopWords[4]= "then";
stopWords[5]= "for";
stopWords[6]= "no";
stopWords[7]= "will";
stopWords[8]= "not";
stopWords[9]= "are";
stopWords[10]= "and";
stopWords[11]= "their";
stopWords[12]= "if";
stopWords[13]= "this";
stopWords[14]= "on";
stopWords[15]= "into";
stopWords[16]= "a";
stopWords[17]= "or";
stopWords[18]= "there";
stopWords[19]= "in";
stopWords[20]= "that";
stopWords[21]= "they";
stopWords[22]= "was";
stopWords[23]= "is";
stopWords[24]= "it";
stopWords[25]= "an";
stopWords[26]= "the";
stopWords[27]= "as";
stopWords[28]= "at";
stopWords[29]= "these";
stopWords[30]= "by";
stopWords[31]= "to";
stopWords[32]= "of";

var indexerLanguage="en";

// Auto generated index for searching.
w["-"]="10*2*57$61$6o";
w["."]="0*2*2j$3c$3h,1*2*20$4a$6h,2*2*v$34$3i,3*3*2l$3l$3o$4f,4*2*4u$52$5d,5*1*10$25,6*1*1d$1m,8*1*k$1p,10*0*3s,15*2*r$35$38,16*0*11,17*0*12,25*1*20$26,26*1*1m$2s";
w["1010data"]="1*0*6f,7*1*14$1u,10*0*3q,14*0*h,15*4*1t$2v$47";
w["16"]="15*0*3l";
w["2"]="1*0*3m";
w["26"]="4*0*35";
w["45"]="15*0*3m";
w["50"]="24*0*2h";
w[":"]="3*0*25";
w[">"]="4*0*1t,5*0*u,6*1*17$19,8*0*i,11*3*1d$1f$1o$1q,17*2*s$u$29,24*1*25$3q,25*0*1u,26*0*1k";
w["a."]="10*0*a";
w["about"]="5*1*1g$1k,8*1*14$18,12*10*-1$9,25*0*2u,26*1*26$2a";
w["above"]="24*0*20";
w["access"]="17*0*1r";
w["account"]="15*1*n$15";
w["account."]="15*0*14";
w["action"]="7*0*3j";
w["actions"]="21*10*-1$7";
w["active"]="15*1*3e$46,16*10*-1$8";
w["add"]="0*44*-1$0$3$f$1n$21$2k$2t,1*43*-1$0$3$e$1e$23$54,2*46*-1$0$2$6$e$10$19$1b$1l$28,4*1*3t$3u,10*4*8l$98$ae$al$an,13*42*-1$0$3$8$q$21,14*0*4p,19*0*22,25*1*28$44";
w["added"]="25*0*4d";
w["adding"]="0*11*-1$7$j,1*11*-1$7$5p,13*0*4n";
w["addition"]="0*10*-1$6,1*10*-1$6,19*0*1v";
w["address"]="15*0*h";
w["adds"]="2*0*2a";
w["affect"]="17*0*1n";
w["after"]="7*10*-1$6,18*0*k,19*0*p,20*0*u,22*10*-1$b";
w["again"]="15*0*4c,20*0*1v";
w["again."]="15*0*4b,20*0*1u";
w["against"]="26*0*16";
w["allowing"]="24*0*3b";
w["allows"]="0*0*m";
w["already"]="4*0*1m,15*0*3c,25*0*39";
w["also"]="0*0*1b,1*1*r$3k,6*0*k,7*0*2b,10*0*4e,22*0*18";
w["alternatively"]="7*0*3c";
w["always"]="24*0*14";
w["analysis"]="14*10*-1$7";
w["analyst"]="1*0*6c,10*0*3n";
w["analyze"]="0*1*p$1d";
w["any"]="13*0*4r,15*0*44,23*0*18,24*0*t";
w["appear"]="10*1*8f$a8,19*0*1f";
w["appears"]="1*0*2l,5*2*15$1m$2c,6*0*1f,8*3*p$1a$1u$2j,10*1*84$92,11*0*22,17*1*16$19,18*0*16,24*0*2d,25*2*25$2l$68,26*2*1r$2c$35";
w["appears."]="1*0*2k,5*0*2b,18*0*15,24*0*2c,25*0*2k,26*0*34";
w["applicable"]="10*0*1h";
w["applying"]="1*0*60,10*0*3b";
w["appropriate"]="15*10*-1$6";
w["area"]="4*2*5m$6i$7p,13*3*10$2c$3k$43";
w["area."]="4*1*5l$6h,13*2*v$2b$42";
w["arrange"]="3*44*-1$1$5$d$s$24$2g$3c,10*1*1f$1l,22*0*t";
w["arranged"]="3*3*2n$2v$3r$42";
w["arrow"]="5*0*1d,8*0*11,26*0*23";
w["ascending"]="22*0*1b";
w["attachment"]="23*0*q";
w["attempt"]="15*0*3n";
w["automatically"]="7*11*-1$d$28,25*0*4f";
w["automatically."]="7*0*27,25*0*4e";
w["averages"]="14*0*1u";
w["b"]="10*0*1e";
w["b."]="10*0*1d";
w["bar"]="10*0*36,15*0*i";
w["bar."]="10*0*35";
w["base"]="1*0*59";
w["basic"]="4*11*-1$c$t,12*10*-1$7,21*10*-1$6";
w["basics"]="9*10*-1$b,21*30*1$3";
w["basics."]="9*10*-1$a";
w["baton"]="0*0*3v";
w["been"]="17*10*-1$e";
w["before"]="2*0*1s,10*0*9s,11*0*r,19*0*2g,24*1*17$41";
w["being"]="2*0*2p,10*4*4o$52$5a$64$6r";
w["below"]="4*0*v,14*0*42";
w["beyond"]="9*10*-1$9";
w["body"]="10*0*bn";
w["both"]="14*1*2k$2m";
w["both."]="14*0*2j";
w["bottom"]="7*0*31";
w["break"]="0*48*-1$2$5$i$l$1r$20$23$30$37$3a$4e,2*0*2n,3*0*r,4*0*5t,10*7*5e$5j$6v$74$8t$8v$9b$ar,19*0*1s,20*1*16$1d";
w["break."]="0*11*-1$h$1v,4*0*5s,10*2*5d$6u$9a";
w["breaks"]="0*12*-1$9$12$2p,2*2*14$1g$31,4*0*5k,10*2*a3$a7$aj,14*8*2g$2i$2o$2q$3d$3p$44$4e$4s,19*0*25,20*43*-1$6$d$i$p$1p$23,22*0*13";
w["breaks."]="0*0*11,2*0*1f,10*0*ai,20*0*1o";
w["breaks:"]="2*0*30";
w["browser"]="7*0*37,15*1*g$17";
w["buttons"]="10*0*9g";
w["c"]="10*0*22";
w["c."]="10*0*21";
w["calculations"]="14*0*20";
w["can"]="0*10*-1$e,1*12*-1$d$53$5e,2*11*-1$5$1u,3*10*-1$b,4*0*77,5*10*-1$5,6*10*-1$7,7*1*2a$3e,8*10*-1$5,10*4*q$1j$2k$46$4d,11*10*-1$a,13*14*-1$7$p$1q$32$4u,14*5*1s$22$2d$3a$3m$4o,15*0*2d,17*10*-1$7,18*10*-1$b,19*0*2i,20*10*-1$f,22*11*-1$7$17,23*0*11,24*11*-1$7$k,25*1*33$77,26*10*-1$5";
w["cannot"]="15*0*28";
w["careful"]="17*0*1i";
w["categories"]="10*0*7a";
w["categories."]="10*0*79";
w["category"]="0*4*19$1k$1o$38$3t,2*2*33$36$3a,4*3*1f$5i$5q$63,10*2*2g$5p$71,13*0*1j,14*3*3h$3t$49$58";
w["category."]="0*0*1j,4*0*1e,13*0*1i";
w["category:"]="0*0*3s";
w["caution"]="6*0*u";
w["caution!"]="6*0*t";
w["certain"]="3*10*-1$8,23*20*-1$-1$8$b,25*20*-1$-1$a$d";
w["change"]="26*0*l";
w["changes"]="17*0*1m";
w["characters"]="24*0*2i";
w["choose"]="4*0*3l,10*0*r,18*0*1m";
w["city"]="0*2*1h$1s$3b,14*0*4j";
w["click"]="1*0*2c,2*0*18,3*1*2j$3m,4*3*3k$3s$4q$80,5*2*1c$1t$22,6*0*1k,7*2*1k$2u$38,8*3*10$1h$1m$27,10*1*9h$ba,14*10*-1$5,15*1*2g$36,16*0*u,18*0*17,20*0*1q,24*1*1u$44,25*2*27$5s$78,26*2*22$2j$2o";
w["close"]="5*0*2d,8*0*2t,16*10*-1$6,25*0*6i,26*0*3m";
w["column"]="0*47*-1$1$4$g$k$1u$22$2v$39$49$4d,2*1*2d$2j,3*1*q$3k,4*0*4n,10*8*30$49$55$5c$5i$8s$8u$99$b7,14*3*2h$2p$3o$4d,18*2*r$t$1r,19*5*10$12$18$1e$1n$1r";
w["column."]="4*0*4m";
w["columns"]="0*0*3p,2*0*24,3*43*-1$3$7$f$12$19$20,4*0*59,10*2*1n$1u$5n,19*52*-1$-1$4$9$b$g$k$2o,22*1*m$p";
w["columns."]="2*0*23,19*11*-1$f$2n";
w["columns:"]="4*0*58,22*0*l";
w["com"]="1*0*6g,7*0*15,10*0*3r";
w["comma"]="4*0*7f,11*11*-1$f$1j,13*0*3a,25*0*5k";
w["comma-separated"]="4*0*7e,11*11*-1$e$1i,13*0*39,25*0*5j";
w["commonly"]="10*0*28";
w["comp"]="1*4*30$38$3l$43$47";
w["comparable"]="1*3*33$3d$4k$4t";
w["compare"]="1*0*4i";
w["comparing"]="4*0*13";
w["complete"]="15*0*1d";
w["completed"]="7*0*s";
w["concept"]="14*0*16";
w["configured"]="10*0*27,15*0*11";
w["confirm"]="6*0*1h,7*0*39";
w["consider"]="17*0*26,24*0*3n";
w["consists"]="10*10*-1$6";
w["contact"]="1*0*6a,10*0*3l,15*0*1s";
w["containing"]="7*0*q";
w["contains"]="4*0*4k,7*0*1b,10*1*25$7h";
w["contents"]="19*0*1d";
w["continue"]="13*0*4m";
w["copy"]="4*0*78,13*0*33";
w["cost"]="10*0*33";
w["count"]="26*0*15";
w["create"]="0*1*27$2g,1*1*1k$1t,2*1*j$s,4*32*0$3$11$1g,8*0*2f,15*10*-1$9,25*0*64";
w["created"]="25*0*6h";
w["created."]="25*0*6g";
w["creating"]="4*0*k";
w["credentials"]="7*0*22,15*10*-1$7,25*1*12$7d";
w["credentials."]="7*0*21";
w["csv"]="11*11*-1$i$1g,23*0*s,25*1*16$5n";
w["ct"]="13*0*2m";
w["cumbersome"]="3*0*1h";
w["cumbersome."]="3*0*1g";
w["current"]="8*0*1v";
w["cursor"]="18*0*q,19*0*v,20*0*14";
w["d"]="10*0*3u";
w["d."]="10*0*3t";
w["data"]="0*11*-1$c$r,1*0*63,3*0*1u,10*1*3e$44,13*0*k,14*17*-1$6$f$1c$1k$1p$1r$35$52";
w["data."]="3*0*1t,10*0*43,14*1*1b$1o";
w["data;"]="14*0*51";
w["date"]="0*0*2b,1*0*1o,2*0*n,3*7*m$1m$28$2r$32$38$3i$3t,4*17*-1$7$29$2p$33$3d$3m$3p$40,10*4*b$j$t$15$1a,19*0*29";
w["date."]="3*0*2q";
w["dates"]="4*0*2j,10*0*18";
w["day"]="23*20*-1$-1$9$d,25*21*-1$-1$b$f$6q";
w["dd"]="4*0*3i";
w["default"]="3*0*2h,18*10*-1$g";
w["delete"]="5*43*-1$0$2$6$n$23$28,6*44*-1$0$3$8$g$v$18$1l,7*1*3f$3q";
w["deletes"]="5*0*h,7*0*3k";
w["deleting"]="5*0*f,6*0*l";
w["deletion"]="5*0*26,6*1*1j$1q";
w["deletion."]="6*0*1i";
w["department"]="2*2*32$3d$3g,14*0*54";
w["descending"]="22*0*1c";
w["described"]="0*0*2f,1*0*1s,2*0*r";
w["describes"]="9*10*-1$6,21*10*-1$5";
w["description"]="4*0*2g";
w["designated"]="11*0*24,26*0*37";
w["desired"]="4*0*7r,7*0*2l,8*0*26,10*0*2t,13*1*3m$4q,19*0*1a,20*0*1e";
w["desired."]="7*0*2k,8*0*25,13*0*4p";
w["detailed"]="15*0*2p";
w["details"]="5*0*1f,8*0*13,26*1*25$3l";
w["details."]="26*0*3k";
w["dialog"]="5*0*14,6*0*1e,7*0*1j,8*2*o$1t$2n,24*0*2b,25*3*24$2j$2r$6c,26*0*1q";
w["dialog."]="7*0*1i";
w["different"]="3*0*v,10*0*5r,17*0*2e,24*0*3v";
w["dimension"]="0*3*1p$1t$2u$3e,10*8*4g$4m$51$59$63$6q$8k$97$ad";
w["dimensions"]="0*1*2e$2n,1*0*1r,2*0*q,3*0*2c,4*12*-1$a$5f$6a,10*2*3v$40$48,13*1*26$3s,14*0*u,20*10*-1$m,22*0*11";
w["dimensions."]="20*10*-1$l";
w["directly"]="14*0*i";
w["disabled"]="0*0*4b,19*0*1p";
w["display"]="4*0*60,14*1*45$4f";
w["displaying"]="10*1*6d$b6";
w["displays"]="0*0*3n,1*0*3v,2*0*2k,4*4*4e$56$5p$7q$87,6*0*1o,10*1*5l$bm,13*0*3l,15*0*18,16*0*13,20*0*21";
w["divided"]="14*0*57";
w["do"]="4*0*2b,15*0*1p";
w["document"]="25*0*5p";
w["does"]="26*0*k";
w["down"]="1*1*2i$5m,3*0*3f,4*1*2u$71,10*0*12,13*1*2s$4a,18*1*1c$1k,24*0*2m,25*2*3n$4p$57";
w["downl"]="25*0*5d";
w["download"]="7*32*0$3$k$2i,11*1*1h$1s,25*2*13$7e$7i,26*0*3h";
w["downloaded"]="11*0*20";
w["downloads"]="7*0*26,11*0*25";
w["drag"]="2*0*20,3*0*1c,4*2*45$5g$6b,10*2*8i$95$ab,13*2*27$3t$4v,14*1*q$24,19*33*1$6$m$17$2k,20*32*2$9$s$1b";
w["drag-and-drop"]="3*0*1b,20*30*1$8";
w["drag-and-drop:"]="20*0*r";
w["dragging"]="0*0*47,4*0*n,10*0*1s,19*11*-1$d$1l,20*10*-1$j,22*1*n$v";
w["drink"]="13*0*1n";
w["drop"]="1*1*2h$5l,2*0*21,3*1*1d$3e,4*4*2t$46$5h$6c$70,10*3*11$8j$96$ac,13*4*28$2r$3u$49$50,14*1*r$25,18*1*1b$1j,19*32*2$7$o$2l,20*31*3$a$t,24*0*2l,25*2*3m$4o$56";
w["drop-down"]="1*1*2g$5k,3*0*3d,4*1*2s$6v,10*0*10,13*1*2q$48,18*1*1a$1i,24*0*2k,25*2*3l$4n$55";
w["drop:"]="19*0*n";
w["dropping"]="0*0*48,4*0*o,10*0*1t,19*11*-1$e$1m,20*10*-1$k,22*1*o$10";
w["e"]="10*0*7c,19*0*1k";
w["e."]="10*0*7b";
w["e:"]="19*0*1j";
w["each"]="0*1*1i$3r,2*0*35,3*1*31$37,10*0*ap,13*0*11,14*4*3g$3s$3u$48$4i,16*0*g";
w["easy"]="14*0*l";
w["edit"]="8*43*-1$0$2$6$b$1n$23,23*0*12,24*0*3d";
w["edited"]="8*0*2s";
w["edited."]="8*0*2r";
w["elements"]="10*10*-1$9";
w["elements."]="10*10*-1$8";
w["email"]="7*14*-1$f$p$10$1a$32,23*0*o,25*4*q$3j$4h$71$7a,26*0*3c";
w["email."]="25*0*3i";
w["emails"]="7*0*2q";
w["enables"]="14*0*1f";
w["energy"]="13*0*1m";
w["enter"]="4*0*3b,7*0*1s,10*1*14$31,15*3*j$q$1i$21,24*0*2e,25*3*10$3d$4g$7b";
w["environment"]="7*0*18";
w["environment-specific"]="7*0*17";
w["error"]="15*0*3u";
w["every"]="17*0*1o";
w["everyone"]="5*0*k,6*0*n,7*0*3m,24*0*3g";
w["example"]="0*1*13$35,1*1*i$5c,2*0*2t,3*2*1j$2s$40,4*0*30,10*3*2c$5f$68$70,13*1*18$1k,14*2*38$41$53";
w["excel"]="4*0*7c,11*11*-1$c$1t,13*0*37,23*0*r,25*1*15$5g";
w["exclude"]="4*0*6o,13*0*2i";
w["existing"]="8*10*-1$7,26*11*-1$8$i";
w["exit"]="5*0*2h,8*0*31,25*0*6m,26*0*3q";
w["exploration"]="14*10*-1$8";
w["explorer"]="2*10*-1$b,4*1*f$1q,6*11*-1$d$1n,7*10*-1$c,9*40*-1$0$2$7,10*42*-1$0$2$4$24$b2,12*10*-1$b,13*11*-1$c$t,14*12*-1$2$b$14,15*14*-1$5$19$3p$3q$4a,16*13*-1$9$m$r$12,17*0*1a,20*0*1h,25*0*6s";


// Auto generated index for searching.
w["explorer."]="6*10*-1$c,12*10*-1$a,16*0*l";
w["export"]="11*44*-1$0$2$b$t$v$1e$1p,25*0*54";
w["exporting"]="10*0*7l";
w["f"]="10*0*7q";
w["f."]="10*0*7p";
w["feature"]="22*0*u";
w["features"]="0*0*v,9*40*-1$1$3$8";
w["february"]="3*1*1r$49";
w["field"]="4*0*7n,13*0*3i";
w["field."]="4*0*7m,13*0*3h";
w["fields"]="10*0*41,15*0*1g";
w["fields:"]="15*0*1f";
w["file"]="4*0*1s,5*0*t,6*1*16$1c,7*0*25,8*0*h,10*1*7d$7f,11*15*-1$k$1c$1m$1n$1v$21,17*1*r$28,24*1*24$3p,25*2*1t$5c$5h,26*0*1j";
w["file."]="11*11*-1$j$1u";
w["filter"]="4*3*6p$6s$7k$7s,10*3*4f$5v$67$6b,13*14*l$12$17$20$24$29$2j$2n$3f$3n$40$45$4f$4l$53";
w["filter."]="10*0*66,13*1*16$4k";
w["filter:"]="13*1*1v$23";
w["filtered"]="0*0*3f";
w["filters"]="4*13*-1$b$6f$6g$7o,10*2*89$8c$8p,13*47*-1$2$5$a$s$u$2a$3j$41$4o$51,14*0*v";
w["filters."]="10*0*8o";
w["find"]="0*0*16,10*0*2s";
w["finish"]="16*0*j";
w["first"]="2*0*1p,3*2*2o$2u$3s,10*0*f,13*0*4j,19*0*2d,25*0*1c";
w["flexibility"]="14*0*g";
w["folder"]="11*0*27";
w["folder."]="11*0*26";
w["follow"]="15*0*2j,25*0*u";
w["following"]="0*0*34,2*0*2s,3*0*1i,4*0*2e,9*10*-1$4,10*10*-1$7,12*10*-1$4,13*0*1t,15*0*1e";
w["following:"]="4*0*2d";
w["format"]="4*0*3j,5*0*1p,8*0*1d,23*0*v,25*2*19$5a$5q,26*0*2f";
w["format."]="23*0*u,25*0*18";
w["from"]="1*0*5i,3*0*3b,4*10*1v$28$2r$3o$43$4o$5e$69$6u$7b$7d,5*11*-1$9$q,6*11*-1$b$13,8*0*e,10*1*v$19,13*5*25$2p$36$38$3r$47,14*1*26$2v,16*0*q,17*0*o,18*0*1h,24*0*27,25*3*1q$3k$4k$53,26*0*1g";
w["further"]="0*0*o,13*0*13,14*1*4t$56";
w["g"]="10*0*88";
w["g."]="10*0*87";
w["gain"]="14*0*2t";
w["garden"]="2*1*38$3c";
w["generated"]="7*0*2g";
w["getting"]="12*30*0$2";
w["given"]="1*0*36";
w["gives"]="3*0*t";
w["goods"]="14*0*4a";
w["granula"]="14*0*e";
w["granularity"]="14*0*2u";
w["grid"]="1*2*26$3u$5t,4*4*4b$4d$54$5o$8a,10*2*4s$b1$bl,20*0*1j,22*0*r,24*0*22";
w["grid."]="1*0*5s,4*1*4a$89,10*0*4r";
w["group"]="0*10*-1$a,10*1*2f$47";
w["grouped"]="4*0*1d,10*0*77";
w["grouping"]="2*0*2o,14*0*55";
w["groupings"]="14*0*5a";
w["groupings."]="14*0*59";
w["groups"]="14*0*1n";
w["guide"]="15*3*34";
w["h"]="10*0*8r";
w["h."]="10*0*8q";
w["hands"]="14*0*j";
w["has"]="2*1*39$3f,17*10*-1$d";
w["have"]="11*0*14,14*1*3b$3n,15*0*1q,24*0*1n,25*0*1g,26*10*-1$b";
w["header"]="18*0*s,19*1*11$19";
w["helps"]="14*0*2r";
w["here"]="1*0*68,10*4*3j$86$8h$94$aa";
w["here."]="10*3*85$8g$93$a9";
w["hierarchical"]="13*42*-1$1$4$9$1u$22";
w["hierarchy"]="13*1*r$55";
w["hierarchy."]="13*0*54";
w["highest"]="14*0*33";
w["highlighted"]="1*0*29,10*4*4n$4t$58$62$6p";
w["highlighted."]="1*0*28";
w["history"]="5*0*1s,8*0*1g,26*0*2i";
w["history."]="5*0*1r,8*0*1f,26*0*2h";
w["how"]="10*0*50,13*0*4e,25*0*4s";
w["i"]="10*0*9d";
w["i."]="10*0*9c";
w["icon"]="10*3*4u$56$60$6n,18*1*14$19";
w["id"]="17*11*-1$j$1q";
w["id."]="17*10*-1$i";
w["immediately"]="26*11*-1$f$3a";
w["immediately."]="26*10*-1$e";
w["in:"]="15*0*d";
w["include"]="4*0*6n,13*0*2h,14*0*2e";
w["includes"]="8*0*2o,25*0*6d";
w["including"]="1*0*58";
w["information"]="4*0*p,5*0*1j,7*0*3u,8*1*17$22,12*10*-1$8,25*1*2t$7n,26*0*29";
w["information."]="7*0*3t,8*0*21,25*0*7m";
w["insights"]="7*0*1v,15*4*1k$23$30";
w["instead"]="10*0*1r";
w["instructions"]="15*1*2k$2q";
w["interested"]="1*0*5v,10*0*3a";
w["interface"]="14*0*o";
w["interface."]="14*0*n";
w["internal"]="15*0*3s";
w["internet"]="15*0*f";
w["introduction"]="14*30*0$1";
w["intuitive"]="14*0*m";
w["it."]="5*0*m,7*0*3o,10*0*9v,11*0*u,15*0*2f,24*1*3i$43,25*0*1m";
w["its"]="26*0*u";
w["j"]="10*0*a1";
w["j."]="10*0*a0";
w["january"]="3*1*1q$43";
w["job"]="5*47*-1$1$3$8$g$j$p$1i$1l$1q$24,7*26*-1$-1$7$j$t$1h$2t$3i$3l$3r,8*52*-1$1$3$a$d$16$19$1e$1o$1s$20$24$2b$2c$2g$2p,25*38*2$5$2a$2f$2i$2q$60$61$65$6e,26*47*-1$1$3$a$j$n$r$1f$28$2b$2g";
w["job."]="5*0*1h,7*11*-1$i$3h,8*12*-1$9$15$2a,25*1*2e$5v,26*0*27";
w["job:"]="5*0*o,8*0*c,26*0*1e";
w["jobs"]="5*13*-1$c$13$1b$2g,8*3*n$v$2m$30,25*4*23$31$38$6b$6l,26*3*14$1p$21$3p";
w["jobs."]="5*0*1a,8*0*u,26*0*20";
w["just"]="14*0*p";
w["k"]="10*0*av";
w["k."]="10*0*au";
w["know"]="1*0*l";
w["la"]="0*0*3g";
w["large"]="14*0*1i";
w["larger"]="13*0*1h";
w["last"]="1*5*t$17$39$4m$4q$5g,4*0*50";
w["layers"]="2*10*-1$8";
w["layout"]="10*0*b9";
w["layout."]="10*0*b8";
w["les"]="10*0*2i";
w["lets"]="1*0*4g";
w["level"]="2*1*2c$2i,14*0*34";
w["limit"]="26*0*19";
w["limited"]="13*0*4h";
w["limits"]="13*0*14";
w["line"]="7*0*1d,25*0*3f";
w["link"]="7*2*r$1l$30,25*2*t$v$79";
w["link."]="25*0*s";
w["linked"]="25*0*5b";
w["links"]="26*0*3d";
w["list"]="1*1*2j$5n,3*0*3g,4*3*2v$73$79$7h,5*11*-1$e$16,7*0*2d,8*0*q,10*1*13$2q,13*3*2u$34$3c$4c,14*0*27,18*1*1d$1l,24*0*2n,25*4*3o$47$4q$58$75,26*0*1s";
w["list."]="4*0*72,5*10*-1$d,13*1*2t$4b,25*0*74";
w["load"]="17*0*13";
w["location"]="19*1*1c$1i";
w["location."]="19*1*1b$1h";
w["log"]="15*0*42";
w["logged"]="15*0*3d";
w["login"]="25*1*11$7c";
w["losing"]="0*0*t,14*0*32";
w["louisiana"]="4*0*18";
w["ly"]="1*1*45$48,4*0*5b";
w["main"]="22*0*h";
w["make"]="4*0*j";
w["manual"]="3*0*1l,4*1*37$3c";
w["manually"]="7*10*-1$a,10*1*16$20,24*0*s,26*13*-1$6$g$12$1c";
w["manually."]="10*0*1v";
w["many"]="3*0*18";
w["march"]="3*0*1s";
w["maximum"]="24*0*2j";
w["may"]="1*0*3j,13*0*1a,17*0*25,24*0*3m";
w["member"]="24*0*2s";
w["members"]="24*1*39$3k";
w["menu"]="4*0*21,5*0*r,6*0*14,8*0*f,10*1*7e$7g,16*0*t,17*0*p,24*0*29,25*0*1r,26*0*1h";
w["menu."]="4*0*20,24*0*28";
w["message"]="6*0*1p,15*0*40,16*0*15,17*0*15,25*0*4j,26*0*33";
w["message."]="15*0*3v,25*0*4i";
w["method"]="3*0*1e";
w["metric"]="1*51*-1$1$4$f$16$1b$1f$24$27$2b$2e$2t$3o$55$5b,3*2*2p$3j$3v,4*0*48,10*2*2o$2u$32";
w["metric."]="1*1*2s$5a,3*0*3u,10*0*2n";
w["metrics"]="0*0*2d,1*12*-1$9$1q$61,2*0*p,3*0*2b,4*13*-1$9$i$44$4p,10*3*23$2b$2r$3c,14*1*t$2a";
w["metrics."]="10*0*2a,14*0*29";
w["mm"]="4*0*3h";
w["mode"]="20*0*1l";
w["modified"]="17*0*2c";
w["modifier"]="1*45*-1$2$5$h$1d$1g$2f$2p$56";
w["modifier."]="1*11*-1$g$1c";
w["modifiers"]="1*0*42";
w["modify"]="17*0*21";
w["modifying"]="17*0*1j,24*0*42";
w["month"]="25*1*36$3c,26*0*1b";
w["month."]="25*0*3b,26*0*1a";
w["more"]="0*0*2m,1*1*2d$2o,2*0*12,4*3*2n$3v$4r$6e,5*1*18$1u,7*0*3s,8*1*s$1i,10*5*i$6g$8b$8n$a5$ag,13*0*h,18*1*13$18,24*0*2q,25*1*3r$7l,26*1*1u$2k";
w["move"]="18*0*o,19*1*t$16,20*1*12$1a";
w["move."]="19*0*15,20*0*19";
w["msa"]="10*0*5g";
w["msas"]="10*0*5s";
w["multiple"]="3*0*l,14*0*4q,19*0*28";
w["must"]="2*0*1o,10*0*e,11*0*o,15*10*-1$3,19*0*2c,25*1*1b$43";
w["name"]="6*0*1a,17*1*v$2g,24*0*40";
w["name."]="17*0*2f";
w["need"]="4*0*s,13*0*g,23*10*-1$5,25*10*-1$7";
w["need."]="4*0*r";
w["new"]="0*0*42,4*2*1h$1u$23,7*0*1g,8*0*1r,19*0*1g,25*34*1$4$29$2c$2h$2p";
w["next"]="4*0*4s";
w["noreply"]="7*0*13";
w["noreply@1010data.com"]="7*0*12";
w["note"]="0*0*46,1*1*2v$51,2*0*1i,4*0*75,7*0*2n,10*0*38,11*0*m,13*1*30$4d,15*0*3a,17*0*1h,24*1*11$32,25*1*2n$41";
w["note:"]="0*0*45,1*1*2u$50,2*0*1h,4*0*74,7*0*2m,10*0*37,11*0*l,13*0*2v,15*0*39,17*0*1g,24*1*10$31,25*1*2m$40";
w["notification"]="5*0*2a,8*0*2i,16*0*14,25*1*67$70";
w["now"]="3*0*3q,4*2*4j$55$5v,18*0*1s,26*0*2r";
w["number"]="25*0*30";
w["oad"]="25*0*5f";
w["oad:"]="25*0*5e";
w["often"]="25*0*4t";
w["oklahoma"]="14*0*4m";
w["oklahoma:"]="14*0*4l";
w["once"]="10*1*7t$bg,11*10*-1$4";
w["one"]="0*0*2l,1*0*2n,2*0*11,4*3*14$2c$2m$6d,5*0*17,8*0*r,10*6*h$4h$6f$8a$8m$a4$af,24*0*2p,25*0*3q,26*0*1t";
w["ones"]="1*0*66,10*0*3h";
w["only"]="1*1*35$3f,10*1*4k$6l";
w["only."]="10*1*4j$6k";
w["open"]="4*0*1p,7*0*o,17*42*-1$0$3$8$k$t,25*0*1j";
w["opened"]="7*0*35";
w["opening"]="10*0*7k,21*10*-1$d";
w["opens"]="4*0*22";
w["option"]="4*1*2f$3a,18*0*1p";
w["option."]="18*0*1o";
w["optional"]="4*0*1l";
w["optional:"]="4*0*1k";
w["options"]="3*0*10,4*0*2q,5*0*21,8*0*1l,10*0*7i,18*0*1g,26*0*2n";
w["options."]="5*0*20,8*0*1k,18*0*1f,26*0*2m";
w["order"]="0*0*4a,3*0*2i,15*10*-1$8,19*0*1o,20*1*1g$26,22*0*1e,25*0*1k";
w["order."]="20*1*1f$25,22*0*1d";
w["organize"]="14*0*19";
w["orleans"]="0*0*44";
w["orleans."]="0*0*43";
w["other"]="1*0*64,10*0*3f,14*0*1v,15*1*3h$45,24*1*37$3j";
w["out"]="15*0*43,16*44*-1$1$3$5$f$p$10$18";
w["out."]="16*0*17";
w["out:"]="16*0*o";
w["output"]="5*0*1o,8*0*1c,10*0*1q,11*0*1a,18*40*-1$4$9$j,22*40*-1$2$5$a,24*0*1t,26*0*2e";
w["output."]="18*10*-1$i";
w["page"]="7*0*34,15*1*1c$2o";
w["page."]="15*1*1b$2n";
w["particular"]="10*0*6h,24*0*o";
w["password"]="15*5*20$26$2b$2i$2l$2u";
w["password."]="15*0*25";
w["past"]="4*0*34";
w["paste"]="4*0*7i,13*0*3d";
w["pdf"]="23*0*t,25*1*17$5r";
w["per"]="0*0*1g,4*0*62,25*0*35";


// Auto generated index for searching.
w["percentage"]="25*0*37";
w["period"]="1*1*p$14";
w["period."]="1*0*13";
w["picker"]="4*0*3r,10*0*1c";
w["picker."]="4*0*3q,10*0*1b";
w["pivot"]="14*3*10$17$1d$2b";
w["platform"]="7*0*20,15*5*10$1l$24$31";
w["please"]="1*0*69,10*0*3k,15*0*41";
w["plus"]="1*0*41";
w["point"]="14*10*-1$4";
w["point-and-click"]="14*10*-1$3";
w["portable"]="25*0*5o";
w["power"]="14*0*d";
w["pre"]="10*0*26";
w["predefined"]="14*0*28";
w["press"]="15*0*p";
w["preview"]="4*0*4f,10*0*b5,20*0*1k";
w["previous"]="1*0*3g,4*1*19$66,13*0*15";
w["previously"]="7*0*2h";
w["prime"]="15*0*3k";
w["prime-16.45"]="15*0*3j";
w["prior"]="1*0*3s";
w["prior."]="1*0*3r";
w["product"]="13*0*1e";
w["prompted"]="7*0*1r";
w["provided"]="15*0*l";
w["provides"]="4*0*g,12*10*-1$6,25*0*2s";
w["puts"]="14*0*c";
w["quantity"]="3*2*36$48$4e,14*0*1j";
w["quick"]="4*1*2i$2o,10*0*s";
w["range"]="0*0*2c,1*0*1p,2*0*o,3*2*29$33$3a,4*2*2a$3e$3n,10*2*c$k$u";
w["range."]="3*0*39";
w["ranges"]="3*1*n$1n,4*12*-1$8$38$42,19*0*2a";
w["ranges."]="4*0*41";
w["re"]="18*42*-1$1$6$d$11$1u";
w["re-sort"]="18*40*-1$0$5$c";
w["re-sort."]="18*0*10";
w["re-sorted."]="18*0*1t";
w["rearrange"]="3*41*-1$2$6$e$1v,10*0*1m,19*31*3$8$h,20*41*-1$4$b$g$n,22*11*-1$8$j";
w["rearranging"]="3*0*11,19*10*-1$a,22*30*0$3";
w["receive"]="23*0*n,25*1*p$4a,26*0*3b";
w["receiving"]="7*0*2p";
w["recipient"]="25*1*46$73";
w["recipients"]="5*0*1n,7*10*-1$g,8*0*1b,23*0*l,25*3*n$3h$3s$76,26*1*2d$39";
w["recipients'"]="25*0*3g";
w["refine"]="14*0*4u";
w["regarding"]="7*0*2r";
w["region"]="14*0*40";
w["region."]="14*0*3v";
w["remember"]="15*0*29";
w["reorder"]="2*0*22,13*0*52,19*0*2m";
w["report"]="0*6*26$29$2i$2s$33$3k$3m,1*18*-1$b$1j$1m$1v$25$3t$4d$4f$5r,2*17*-1$d$i$l$u$17$1r$27$2g,3*17*-1$i$15$22$2f$2m$2t$3p$41,4*58*-1$2$5$e$l$u$12$1j$1o$27$49$4c$4h$4i$53$5n$5u$6l$7v$85$86$88,6*44*-1$2$5$a$i$m$q$12,7*34*2$5$n$v$1p$24,8*10*-1$8,10*17*o$1p$5k$6c$75$7r$81$82$8e$91$9m$9r$b0$b4$bd$bj$bk$bq,11*44*-1$1$3$8$q$11$16$19,13*13*-1$e$n$2f$3q,17*58*-1$-1$2$5$9$c$n$11$18$1f$1l$1u$23$2d,18*41*-1$3$8$h$n,19*2*j$s$2f,20*2*11$1t$20,21*60*-1$-1$-1$0$2$9$c$g,22*52*-1$-1$1$4$9$f$k$q,23*55*-1$-1$0$2$7$g$k$m$p$14$17,24*42*2$5$16$1d$1k$1p$1s$21$2f$2v$36$3f$3u$49,25*34*-1$-1$9$i$m$o$r$14$1f$1i$1p$2d$3v$4b$50$6u$7g$7k,26*16*-1$c$2q$2t$31$38$3f$3j";
w["report."]="0*2*2r$32$3j,1*0*4c,2*13*-1$c$16$26$2f,3*12*-1$h$14$2e,4*14*-1$d$26$4g$7u$84,7*1*u$1o,10*2*n$9l$bp,13*11*-1$d$3p,17*1*1e$1t,21*10*-1$f,22*10*-1$e,23*10*-1$f,24*0*48,25*12*-1$h$3u$7f,26*0*3e";
w["report:"]="0*0*25,1*0*1i,2*0*h,4*0*1i,6*0*11,7*0*m,11*0*10,17*0*m,24*0*1j,25*0*1o";
w["reports"]="3*0*17,7*0*2e,10*0*7o,14*0*13,15*10*-1$b,24*21*-1$-1$9$b$h";
w["reports."]="10*0*7n,14*0*12,15*10*-1$a";
w["representing"]="3*0*1p";
w["reset"]="15*3*2e$2h$2m$2s";
w["result"]="24*0*1g";
w["result."]="24*0*1f";
w["resulting"]="0*0*3l,1*0*4e";
w["results"]="10*2*6e$76$bf";
w["results."]="10*0*be";
w["returns"]="15*0*3r,20*0*1i";
w["revised"]="20*0*24";
w["rouge"]="0*0*40";
w["row"]="0*14*-1$8$10$1q$2o$36,2*3*13$1e$2m$2v,4*1*5j$5r,10*7*4b$6m$6t$73$a2$a6$ah$aq,14*4*2f$2n$3c$43$4r,19*0*24,20*45*-1$5$c$h$o$15$1c$1n$22,22*0*12";
w["row."]="10*0*4a";
w["rows"]="10*0*78,18*10*-1$f,22*0*1a";
w["run"]="0*1*2h$3i,1*1*1u$4b,2*2*t$1q$25,3*1*2k$3n,4*32*1$4$81$82,7*10*-1$b,10*4*9f$9j$9p$bb$bi,11*1*p$15,17*0*1d,18*0*m,19*1*r$2e,20*2*10$1r$1s,22*10*-1$d,24*3*n$q$15$1o,25*3*34$3a$4m$52,26*45*-1$0$2$7$q$t$13$18$1d";
w["run."]="25*0*51,26*0*p";
w["running"]="21*10*-1$8,26*0*h";
w["runs"]="7*10*-1$8,25*0*6t,26*0*2u";
w["s"]="1*0*2q,3*0*2a,4*0*2k,10*1*l$6j,24*0*2t";
w["sa"]="10*0*2h";
w["sales"]="0*4*18$1f$3q$3u$41,1*15*n$10$15$1a$32$3c$40$44$46$49$4j$4l$4p$4v$5j$5q,3*5*30$34$45$46$4b$4c,4*7*17$1c$47$4l$4t$5a$5c$61,10*1*2m$5q,13*0*1o,14*3*3f$3r$47$4g";
w["sales."]="1*0*4u";
w["same"]="1*0*11,23*10*-1$6,25*10*-1$8";
w["satisfied"]="11*11*-1$6$17,24*0*1q";
w["save"]="10*3*7v$9e$9i$9u,17*1*2a$2b,24*50*-1$0$3$8$f$19$1h$1v$26$2a$3r$3s$45$46,25*0*1d";
w["saved"]="4*0*1n,6*43*-1$1$4$9$h$10$1b,17*43*-1$1$4$b$l$10$17,21*10*-1$e,24*0*3e,25*0*1h";
w["saving"]="10*0*7j,21*10*-1$b,24*0*1c";
w["schedule"]="5*1*v$1v,7*0*1f,8*5*j$1j$1q$28$29$2d,23*12*-1$e$j$13,24*0*l,25*51*-1$0$3$g$l$1l$1n$1v$2b$2g$2o$4l$5t$5u$62,26*1*1l$2l";
w["scheduled"]="5*24*-1$-1$7$b$i$12$19$2f,7*43*-1$1$4$9$l$2s$3g,8*3*m$t$2l$2v,23*0*16,25*5*22$3t$6a$6k$6p$7j,26*16*-1$9$o$v$1o$1v$3i$3o";
w["scheduler"]="5*0*2j,8*0*33,25*0*6o,26*0*3s";
w["scheduler."]="5*0*2i,8*0*32,25*0*6n,26*0*3r";
w["scheduling"]="10*0*7m,23*30*1$3";
w["scope"]="13*0*4i";
w["scroll"]="10*0*2p";
w["search"]="10*0*34,13*1*1c$1l";
w["second"]="13*0*3v";
w["section"]="9*10*-1$5,12*10*-1$5,21*10*-1$4,22*0*15";
w["section."]="22*0*14";
w["see"]="7*0*3p,15*0*2r,25*0*7h,26*0*3g";
w["sele"]="13*0*2l";
w["select"]="1*1*2m$5f,2*0*1k,3*2*k$26$3h,4*17*-1$6$1r$2h$2l$39$4v$6j$6r,5*0*s,6*0*15,8*0*g,10*2*g$17$ak,11*0*1b,13*1*2d$44,17*0*q,19*0*21,24*1*23$2o,25*3*1s$3p$4r$59,26*0*1i";
w["selected"]="0*0*4g,1*0*2a,3*0*1o,4*0*64,18*0*1q,19*0*1u";
w["selected."]="0*0*4f,19*0*1t";
w["selecting"]="0*0*2a,1*0*1n,2*0*m";
w["sell"]="14*0*4c";
w["send"]="23*0*15,26*1*2p$30";
w["sends"]="7*10*-1$e,25*0*6v";
w["sent"]="7*0*11,26*10*-1$d";
w["separate"]="10*0*5m";
w["separated"]="4*0*7g,11*11*-1$g$1k,13*0*3b,25*0*5l";
w["server"]="15*0*3t";
w["session"]="15*0*3f,16*10*-1$b";
w["session."]="16*10*-1$a";
w["sessions"]="15*0*48";
w["set"]="4*0*h,13*0*1r";
w["several"]="2*10*-1$7,13*0*1d";
w["share"]="24*44*-1$1$4$a$1i$2u$35$47";
w["shared"]="6*0*p,17*13*-1$f$1k$1s$22,24*0*3t";
w["should"]="4*0*6m,10*0*9o,13*0*2g,16*0*d,24*0*13";
w["showing"]="10*0*5o";
w["shows"]="1*1*31$3b,10*1*4v$b3";
w["sign"]="15*45*-1$0$1$4$c$1a$37$3o$49,16*43*-1$0$2$4$e$n$v";
w["signs"]="15*0*t,16*0*16";
w["similar"]="7*0*16";
w["simple"]="1*10*-1$8,4*0*m,13*0*j,19*10*-1$c";
w["simply"]="14*0*23";
w["situations"]="3*10*-1$9";
w["so"]="24*1*i$1a";
w["sort"]="18*41*-1$2$7$e$12,22*0*19";
w["sorted"]="18*0*1v";
w["sorting"]="18*1*1e$1n";
w["specific"]="7*0*19";
w["specified"]="7*11*-1$h$1e";
w["started"]="12*30*1$3";
w["state"]="0*0*3d,10*3*4i$5u$69$6i,14*0*4k";
w["state."]="10*0*5t";
w["still"]="26*0*s";
w["stop"]="7*0*2o";
w["store"]="14*0*3k";
w["store."]="14*0*3j";
w["stores"]="1*1*34$3e";
w["subcategories"]="13*0*1f";
w["subject"]="7*0*1c,25*0*3e";
w["subscribed"]="5*0*l,7*0*3n";
w["subtotals"]="2*45*-1$1$3$9$f$1a$1c$1m$29,10*1*am$ao,19*0*23";
w["successful"]="5*1*27$29,6*0*1s,8*1*2e$2h,17*0*14,25*1*63$66,26*0*32";
w["successful."]="6*0*1r";
w["successfully"]="26*0*2v";
w["summaries"]="14*0*1m";
w["summarization"]="2*1*2b$2h,14*0*37";
w["summarization."]="14*0*36";
w["summarize"]="14*2*3e$3q$4v";
w["summarizing"]="0*0*u";
w["summary"]="14*3*1q$30$46$4h";
w["sums"]="14*0*1t";
w["supplied"]="1*0*67,10*0*3i";
w["support"]="1*0*6e,10*0*3p,15*0*1v";
w["support."]="15*0*1u";
w["support@1010data.com"]="1*0*6d,10*0*3o";
w["tab"]="4*0*24";
w["table"]="14*2*18$1e$2c";
w["team"]="15*0*o,24*13*-1$e$2r$38$3l";
w["team."]="24*10*-1$d";
w["than"]="1*0*65,10*0*3g,13*0*i,15*0*3i";
w["them"]="4*0*7j,7*0*2j,13*0*3e,24*2*m$r$3c";
w["three"]="3*0*1k,22*0*g";
w["time"]="1*1*o$12,13*0*4s,16*0*h,23*11*-1$c$1a,24*1*p$v,25*11*-1$e$6r,26*0*11";
w["time."]="23*0*19,24*0*u,26*0*10";
w["title"]="10*1*7s$83,24*0*2g";
w["tool"]="14*10*-1$a";
w["tool."]="14*10*-1$9";
w["total"]="0*1*17$1e,1*0*m,2*1*3b$3h,25*0*2v";
w["totalled"]="2*0*2r";
w["totalled."]="2*0*2q";
w["totals"]="3*1*44$4a";
w["two"]="0*0*3o,1*0*3p,2*0*2u,4*0*57";
w["type"]="10*0*at";
w["type."]="10*0*as";
w["unexpected"]="24*0*1e";
w["unit"]="3*2*35$47$4d";
w["unsubscribe"]="7*1*2v$3b";
w["unsubscribe."]="7*0*3a";
w["up"]="13*0*1s";
w["updated"]="20*0*1m";
w["url"]="15*1*k$s";
w["use"]="2*0*1v,3*41*-1$0$4$c$p,6*0*s,10*1*1k$2l,19*32*0$5$27$2j,20*30*0$7";
w["used"]="10*8*29$4p$54$5b$5h$65$6a$6s$72";
w["used."]="10*0*53";
w["useful"]="3*0*16,14*0*1l";
w["user"]="1*0*5d,15*3*13$33,17*11*-1$h$1p";
w["user's"]="15*3*32";
w["username"]="15*2*1h$1n$1r";
w["username."]="15*0*1m";
w["users"]="14*0*k";
w["uses"]="14*0*15";
w["using"]="3*0*23,14*0*2l,16*0*k,17*0*27,19*0*l,20*0*q,22*0*s,24*1*3h$3o";
w["value"]="10*0*4q";
w["values"]="4*3*6q$6t$7a$7l,11*11*-1$h$1l,13*5*2k$2o$35$3g$46$4g,25*0*5m";
w["version"]="15*1*v$3g";
w["view"]="5*0*1e,7*1*1m$2c,8*0*12,10*0*bc,14*0*1h,26*0*24";
w["want"]="0*1*15$1c,1*1*k$s,10*0*2e,13*0*1b,17*0*20,25*0*4v";
w["ways"]="22*0*i";
w["we"]="4*0*10";
w["web"]="7*0*33";
w["week"]="23*10*-1$a,25*10*-1$c";
w["weeks"]="4*0*36";
w["well"]="21*10*-1$a";
w["were"]="7*0*2f";
w["when"]="0*0*4c,1*0*21,6*0*e,7*0*1q,10*0*4l,11*0*12,19*0*1q,23*0*h,24*1*1l$33,25*0*j,26*0*m";
w["where"]="10*0*1g";
w["whether"]="4*0*6k,13*0*2e";
w["which"]="2*0*2l,3*0*1a";
w["whole"]="2*0*3e";
w["wish"]="18*0*v,19*0*14,20*0*18,25*0*49";
w["with."]="6*0*r,24*0*30";
w["within"]="2*0*37,13*0*1g";
w["without"]="0*0*s,1*1*57$5o,14*0*31";
w["workspace"]="10*40*-1$1$3$5,16*0*s,17*0*1c";
w["workspace."]="17*0*1b";
w["would"]="0*0*1m,3*0*1f";
w["xlsx"]="11*11*-1$d$1r,25*0*5i";
w["year"]="1*8*v$19$37$3a$3i$3n$4o$4s$5h,4*5*16$1b$32$51$65$68";
w["year's"]="1*3*u$18$4n$4r,4*1*15$1a";
w["year."]="1*0*3h,4*0*67";
w["years"]="1*0*3q";
w["yesterday"]="4*0*31";
w["you"]="0*14*-1$d$n$14$1a$1l,1*16*-1$c$j$q$22$4h$52$5u,2*13*-1$4$1j$1n$1t,3*13*-1$a$j$o$u,4*1*q$76,5*10*-1$4,6*14*-1$6$f$j$o$1g,7*1*29$3d,8*11*-1$4$2q,10*11*d$p$1i$2d$2j$39$45$4c$7u$9n$9t$bh,11*23*-1$-1$5$9$n$s$13,13*16*-1$6$f$o$19$1p$31$4t,14*6*1g$21$2s$39$3l$4b$4n,15*15*-1$2$u$1o$27$2c$3b,16*1*c$i,17*22*-1$-1$6$a$1v$24,18*12*-1$a$l$u,19*5*q$13$20$26$2b$2h,20*12*-1$e$v$17,22*21*-1$-1$6$c$16,23*12*-1$4$i$10,24*17*-1$6$j$12$18$1b$1m$34$3a,25*18*-1$6$k$1a$32$42$48$4c$4u$6f,26*10*-1$4";
w["your"]="0*15*-1$b$q$24$28$2q$31,1*15*-1$a$1h$1l$2r$62$6b,2*15*-1$a$g$k$15$1d$2e,3*14*-1$g$13$21$27$2d,4*2*25$7t$83,5*12*-1$a$11$2e,7*3*1n$1t$23$36,8*2*l$2k$2u,10*11*m$1o$2v$3d$3m$42$80$8d$90$9k$9q$bo,11*12*-1$7$18$23,13*12*-1$b$m$3o,14*4*s$11$1a$3i$50,15*7*e$m$12$16$1j$22$2a$2t,16*10*-1$7,17*10*-1$g,18*0*p,19*1*i$u,20*0*13,24*12*-1$c$g$1r,25*4*1e$21$69$6j$72,26*3*17$1n$36$3n";
w["yourself"]="25*0*45";
w["yyyy"]="4*0*3g";
w["yyyy-mm-dd"]="4*0*3f";

return {'fil': fil, 'w': w}})();
