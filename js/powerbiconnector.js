window['powerbiconnector'] = (function() {
var w = new Object();
fil = new Array();
fil["0"]= "pg_addmacrocode.html@@@Add 1010data Macro Language to a query@@@In the course of creating visualizations for your data in Power BI, you may need to use specific 1010data operations...";
fil["1"]= "pg_adjustsecuritysettings.html@@@Adjust security settings@@@You need to first adjust the security settings in Power BI in order to use the 1010data Power BI Connector.  Watch the video, or follow the steps below to adjust the security settings in Power BI...";
fil["2"]= "pg_connectMT.html@@@Connect to 1010data magic tables@@@1010data&apos;s magic table feature allows you to perform Power BI queries on tables, but take advantage of the underlying power of the 1010data Macro Language...";
fil["3"]= "pg_connectPBISSO.html@@@Configure single sign-on (SSO)@@@Use the SSO options in the ODBC driver to authenticate to 1010data with single sign-on...";
fil["4"]= "pg_connectQQ.html@@@Connect to 1010data Quick Queries@@@Another way to connect to 1010data is to connect to a 1010data saved Quick Query...";
fil["5"]= "pg_connectorsetup.html@@@Power BI Connector setup@@@To set up the Power BI Connector, install and configure the ODBC driver, and then install the 1010data connector...";
fil["6"]= "pg_connectpbibasic.html@@@Connect to 1010data from Power BI@@@Once you install the ODBC Driver and 1010data Power BI Connector and adjust the security settings, you are ready to connect to 1010data from Power BI...";
fil["7"]= "pg_connectsam.html@@@Configure Shared Access Management (SAM) pools@@@SAM pools enable a single set of credentials to be shared among client-side threads...";
fil["8"]= "pg_contactsupport.html@@@Contact support@@@If you checked the Troubleshooting section but are still having difficulties, you will need to create a log and send it to 1010data support...";
fil["9"]= "pg_enabletracing.html@@@Enable tracing@@@Tracing is useful for troubleshooting connectivity issues with the 1010data Power BI Connector, such as finding out why a 1010data query was not supported in DirectQuery mode...";
fil["10"]= "pg_external_manuals.html@@@External Reference Manuals@@@Refer to the CDATA reference manuals for technical specifications for the ODBC Driver and Power BI Connector...";
fil["11"]= "pg_introduction.html@@@Introduction@@@The 1010data Power BI Connector is a utility that offers self-service integration with Microsoft Power BI...";
fil["12"]= "pg_powerbiqueries.html@@@Connection dialog@@@The 1010data connection dialog is where you enter DSN information and advanced connection properties...";
fil["13"]= "pg_powerbivisualization.html@@@Visualize data@@@After connecting to your data source, you can filter and create visualizations from the data...";
fil["14"]= "pg_startinstallpowerbi.html@@@Install the connector@@@Watch the video, or follow the instructions below to install the Power BI Connector...";
fil["15"]= "pg_troubleshooting.html@@@Troubleshooting and support@@@The following are common issues when connecting 1010data to Power BI. You may be able to troubleshoot them without needing to contact 1010data support...";
fil["16"]= "pg_usagepowerbi.html@@@Using Power BI with 1010data@@@After you connect to 1010data, you can use all the features of Power BI on your 1010data tables and Quick Queries...";
var doStem = false;searchLoaded = true;// Porter stemmer in Javascript. Few comments, but it's easy to follow against the rules in the original
// paper, in
//
//  Porter, 1980, An algorithm for suffix stripping, Program, Vol. 14,
//  no. 3, pp 130-137,
//
// see also http://www.tartarus.org/~martin/PorterStemmer

// Release 1 be 'andargor', Jul 2004
// Release 2 (substantially revised) by Christopher McKenzie, Aug 2009


var stemmer = (function(){
	var step2list = {
			"ational" : "ate",
			"tional" : "tion",
			"enci" : "ence",
			"anci" : "ance",
			"izer" : "ize",
			"bli" : "ble",
			"alli" : "al",
			"entli" : "ent",
			"eli" : "e",
			"ousli" : "ous",
			"ization" : "ize",
			"ation" : "ate",
			"ator" : "ate",
			"alism" : "al",
			"iveness" : "ive",
			"fulness" : "ful",
			"ousness" : "ous",
			"aliti" : "al",
			"iviti" : "ive",
			"biliti" : "ble",
			"logi" : "log"
		},

		step3list = {
			"icate" : "ic",
			"ative" : "",
			"alize" : "al",
			"iciti" : "ic",
			"ical" : "ic",
			"ful" : "",
			"ness" : ""
		},

		c = "[^aeiou]",          // consonant
		v = "[aeiouy]",          // vowel
		C = c + "[^aeiouy]*",    // consonant sequence
		V = v + "[aeiou]*",      // vowel sequence

		mgr0 = "^(" + C + ")?" + V + C,               // [C]VC... is m>0
		meq1 = "^(" + C + ")?" + V + C + "(" + V + ")?$",  // [C]VC[V] is m=1
		mgr1 = "^(" + C + ")?" + V + C + V + C,       // [C]VCVC... is m>1
		s_v = "^(" + C + ")?" + v;                   // vowel in stem

	return function (w) {
		var 	stem,
			suffix,
			firstch,
			re,
			re2,
			re3,
			re4,
			origword = w;

		if (w.length < 3) { return w; }

		firstch = w.substr(0,1);
		if (firstch == "y") {
			w = firstch.toUpperCase() + w.substr(1);
		}

		// Step 1a
		re = /^(.+?)(ss|i)es$/;
		re2 = /^(.+?)([^s])s$/;

		if (re.test(w)) { w = w.replace(re,"$1$2"); }
		else if (re2.test(w)) {	w = w.replace(re2,"$1$2"); }

		// Step 1b
		re = /^(.+?)eed$/;
		re2 = /^(.+?)(ed|ing)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			re = new RegExp(mgr0);
			if (re.test(fp[1])) {
				re = /.$/;
				w = w.replace(re,"");
			}
		} else if (re2.test(w)) {
			var fp = re2.exec(w);
			stem = fp[1];
			re2 = new RegExp(s_v);
			if (re2.test(stem)) {
				w = stem;
				re2 = /(at|bl|iz)$/;
				re3 = new RegExp("([^aeiouylsz])\\1$");
				re4 = new RegExp("^" + C + v + "[^aeiouwxy]$");
				if (re2.test(w)) {	w = w + "e"; }
				else if (re3.test(w)) { re = /.$/; w = w.replace(re,""); }
				else if (re4.test(w)) { w = w + "e"; }
			}
		}

		// Step 1c
		re = /^(.+?)y$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(s_v);
			if (re.test(stem)) { w = stem + "i"; }
		}

		// Step 2
		re = /^(.+?)(ational|tional|enci|anci|izer|bli|alli|entli|eli|ousli|ization|ation|ator|alism|iveness|fulness|ousness|aliti|iviti|biliti|logi)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			suffix = fp[2];
			re = new RegExp(mgr0);
			if (re.test(stem)) {
				w = stem + step2list[suffix];
			}
		}

		// Step 3
		re = /^(.+?)(icate|ative|alize|iciti|ical|ful|ness)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			suffix = fp[2];
			re = new RegExp(mgr0);
			if (re.test(stem)) {
				w = stem + step3list[suffix];
			}
		}

		// Step 4
		re = /^(.+?)(al|ance|ence|er|ic|able|ible|ant|ement|ment|ent|ou|ism|ate|iti|ous|ive|ize)$/;
		re2 = /^(.+?)(s|t)(ion)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(mgr1);
			if (re.test(stem)) {
				w = stem;
			}
		} else if (re2.test(w)) {
			var fp = re2.exec(w);
			stem = fp[1] + fp[2];
			re2 = new RegExp(mgr1);
			if (re2.test(stem)) {
				w = stem;
			}
		}

		// Step 5
		re = /^(.+?)e$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(mgr1);
			re2 = new RegExp(meq1);
			re3 = new RegExp("^" + C + v + "[^aeiouwxy]$");
			if (re.test(stem) || (re2.test(stem) && !(re3.test(stem)))) {
				w = stem;
			}
		}

		re = /ll$/;
		re2 = new RegExp(mgr1);
		if (re.test(w) && re2.test(w)) {
			re = /.$/;
			w = w.replace(re,"");
		}

		// and turn initial Y back to y

		if (firstch == "y") {
			w = firstch.toLowerCase() + w.substr(1);
		}

		return w;
	}
})();// Auto generated list of analyzer stop words that must be ignored by search.
stopWords = new Array();
stopWords[0]= "but";
stopWords[1]= "be";
stopWords[2]= "with";
stopWords[3]= "such";
stopWords[4]= "then";
stopWords[5]= "for";
stopWords[6]= "no";
stopWords[7]= "will";
stopWords[8]= "not";
stopWords[9]= "are";
stopWords[10]= "and";
stopWords[11]= "their";
stopWords[12]= "if";
stopWords[13]= "this";
stopWords[14]= "on";
stopWords[15]= "into";
stopWords[16]= "a";
stopWords[17]= "or";
stopWords[18]= "there";
stopWords[19]= "in";
stopWords[20]= "that";
stopWords[21]= "they";
stopWords[22]= "was";
stopWords[23]= "is";
stopWords[24]= "it";
stopWords[25]= "an";
stopWords[26]= "the";
stopWords[27]= "as";
stopWords[28]= "at";
stopWords[29]= "these";
stopWords[30]= "by";
stopWords[31]= "to";
stopWords[32]= "of";

var indexerLanguage="en";

// Auto generated index for searching.
w["'1/1/2018'"]="0*0*3l";
w["'1/31/2018'"]="0*0*3q";
w["';'"]="8*0*a1";
w["*"]="0*0*2p,2*0*5l,12*0*4i";
w["+attach"]="8*0*gd";
w["-"]="8*1*1p$d0,12*0*38,15*0*31";
w["."]="0*1*20$65,1*3*2h$2s$3j$3m,2*1*2i$47,3*3*2n$4f$7a$8r,6*9*1e$1o$2h$50$53$5a$6c$6j$6n$7l,7*2*27$4l$5a,8*10*1e$1k$2j$3t$54$5o$da$dm$ek$ep$gn,9*3*2e$2n$2u$3j,11*0*27,12*3*2i$32$5t$67,13*11*2g$39$3r$41$44$5u$6b$6k$75$79$7d$88,14*6*1d$36$6c$ae$bq$dt$eb,15*3*38$4t$57$5v,16*1*2j$40";
w[".pqx"]="14*0*db";
w["/prime-17.36"]="14*0*6o";
w["1"]="0*2*3m$3n$3r,8*1*76$7p,12*0*5d,13*1*68$69";
w["1/1/2019"]="13*0*67";
w["10"]="2*0*6f,3*0*8o,7*0*57,8*0*am,13*2*4u$78$8e,14*0*47";
w["10."]="14*0*46";
w["100"]="8*0*bl";
w["1010"]="13*0*2v";
w["1010data"]="0*51*-1$1$6$m$s$13$1h$1o$1s$21$4o$5n$5t,1*11*-1$g$10,2*53*-1$-1$1$5$9$o$2f,3*15*-1$g$1m$23$2m$2r$85,4*58*-1$-1$1$5$b$d$p$1p$2d$2q,5*10*-1$i,6*57*-1$-1$1$6$f$p$13$1l$1t$2i$4e$6v$9f,7*3*1c$26$2b$4s,8*17*-1$g$1o$39$3s$cv$ec$ei$go,9*20*-1$-1$9$g,11*10*-1$2,12*14*-1$4$l$s$5p$8f,13*2*k$2c$2m,14*7*26$3p$4l$6i$72$7c$bp$e7,15*26*-1$-1$9$l$q$1g$2j$30$5k$5t,16*57*-1$-1$3$7$b$k$p$16$20$25$2p$34$3t";
w["1010data's"]="2*10*-1$8";
w["1010data."]="8*0*38,12*0*8e,14*1*3o$4k";
w["1010data:"]="6*0*12";
w["10data"]="2*0*6g";
w["12"]="13*0*6h";
w["12/31/2019"]="13*0*6g";
w["15"]="15*0*3d";
w["17"]="14*0*6q";
w["2"]="8*0*7k,12*0*5a";
w["2018"]="0*1*3o$3t";
w["2019"]="13*4*25$51$6a$6j$8i";
w["2019."]="13*2*24$50$8h";
w["3"]="8*0*85,12*0*5h";
w["31"]="0*0*3s,13*0*6i";
w["36"]="14*0*6r";
w["3;"]="12*0*5g";
w["4"]="8*0*8d";
w["400"]="15*0*1n";
w["403"]="15*0*3j";
w["5"]="8*0*8s";
w["728"]="12*0*4q";
w["92"]="2*0*5u";
w[":"]="7*2*30$3b$41,8*4*4c$6t$9m$af$bs,14*3*5f$6f$7v$98";
w["<"]="0*2*3u$4q$54";
w[">"]="0*2*4j$4v$59,1*1*2n$2q,6*1*1c$9k,9*1*29$2c,15*1*4m$4p";
w["@first"]="12*0*5b";
w["@second"]="12*0*58";
w["@third"]="12*0*5e";
w["\\appdata\\local\\microsoft\\power"]="9*1*1i$36";
w["\\appdata\\roaming\\cdata\\c1010data"]="8*0*5g";
w["\\documents"]="14*0*c1";
w["\\documents\\power"]="14*0*dl";
w["able"]="15*10*-1$f";
w["about"]="1*0*2b,2*0*2b,3*0*1e,6*0*4q,7*0*14,8*0*7u,11*0*1v";
w["above"]="8*0*1g,14*0*77,15*0*3f";
w["above."]="14*0*76,15*0*3e";
w["access"]="3*0*22,7*31*2$8$1j,14*1*4f$4q,15*3*14$1j$1t$2s";
w["access."]="15*1*1i$2r";
w["accessing"]="3*0*3s,8*0*1i,14*0*6l";
w["account"]="3*0*1n,7*0*1d,8*0*fd,14*1*j$r,15*0*1e,16*0*26";
w["account."]="8*0*fc,14*0*q";
w["accuracy"]="13*0*8q";
w["active"]="3*0*8e";
w["ad"]="2*0*6o";
w["add"]="0*32*0$5$r$1b,4*1*22$29,12*0*5o,16*3*1g$29$2g$2n";
w["additional"]="3*0*6c,8*1*7s$92,12*0*1q";
w["additionally"]="8*2*86$8e$8t";
w["address"]="8*1*f7$fk";
w["address."]="8*0*fj";
w["adfs"]="3*3*42$48$6n$8g";
w["adjust"]="1*52*-1$-1$0$3$9$q$1g$1o,6*10*-1$j";
w["administrator"]="3*0*5c";
w["administrators"]="3*0*5i";
w["advanced"]="0*3*1f$25$72$7a,12*13*-1$c$1i$33$3o,13*0*5s";
w["advantage"]="2*10*-1$l";
w["affi"]="0*0*42";
w["affinity"]="0*2*4u$58$5b";
w["after"]="4*1*2m$4b,6*0*8j,13*11*-1$4$65,14*0*7f,16*10*-1$8";
w["again"]="14*0*aq,15*0*5n";
w["again."]="15*0*5m";
w["aggregate"]="12*0*86";
w["all"]="6*0*6q,7*2*3n$47$4a,8*1*2q$a5,13*0*5l,16*10*-1$f";
w["allow"]="1*0*3c,4*0*l,16*0*39";
w["allows"]="2*10*-1$d,12*1*1n$3d";
w["already"]="3*0*28,4*0*1d,7*0*1o,13*0*2j,14*1*cp$d7";
w["also"]="6*0*1r,13*0*5g,14*0*62,16*0*2m";
w["always"]="12*0*8o";
w["among"]="7*10*-1$j";
w["analysis"]="4*0*1m";
w["analyze"]="6*0*91";
w["analyzes"]="0*0*5a";
w["another"]="4*10*-1$8";
w["any"]="1*0*3d,2*0*4s,3*0*6b,4*0*4v,8*0*7g,12*0*77,14*0*7a";
w["api"]="3*0*7e";
w["apitoken"]="3*0*7b";
w["appdata"]="8*0*5h,9*1*1j$37";
w["appear"]="9*0*31";
w["appears"]="0*0*6b,1*0*30,2*0*42,3*0*2u,4*0*44,6*6*1j$29$2l$56$5v$6f$8c,7*0*2e,8*1*42$eu,9*0*2i,12*0*10,13*0*7r,14*1*2a$3b";
w["appears."]="0*0*6a,1*0*2v,2*0*41,3*0*2t,6*2*1i$2k$5u,7*0*2d,8*0*41,9*0*2h,14*1*29$3a";
w["appears:"]="8*0*et,12*0*v";
w["appended"]="8*1*b3$ce";
w["appending"]="8*0*dt";
w["applicable"]="8*0*80";
w["application"]="3*1*4m$51,6*0*8t";
w["application's"]="3*0*50";
w["application."]="6*0*8s";
w["applications"]="14*0*5p";
w["apply"]="0*0*7k,13*1*5h$7b";
w["appropriate"]="15*0*1h";
w["apps"]="3*2*61$62$63";
w["asks"]="12*0*9e";
w["associated"]="8*0*f8";
w["assume"]="3*0*11,7*0*r";
w["attach"]="8*0*ge";
w["attached"]="7*0*4f";
w["authenticate"]="3*11*-1$f$q,14*0*3n";
w["authentication"]="3*0*2v";
w["authscheme"]="3*0*33";
w["automatically"]="0*0*84";
w["automatically."]="0*0*83";
w["available"]="3*0*4r,6*0*6s";
w["average"]="2*0*62";
w["avgcol"]="0*0*4g";
w["bad"]="15*0*1o";
w["bar"]="13*0*1r";
w["base"]="3*0*45";
w["basic"]="13*0*73";
w["basis"]="2*0*6q";
w["basket"]="0*3*41$44$4t$57";
w["before"]="8*0*10,9*0*1r,11*0*13,13*1*6e$8t,16*0*3e";
w["beginning"]="9*0*3h";
w["behind"]="2*0*6d";
w["below"]="1*10*-1$p,12*0*14,14*10*-1$8";
w["below."]="12*0*13";
w["best"]="14*0*7k";
w["between"]="0*6*3j$5c";
w["bi"]="0*13*-1$g$10$1m$1v,1*38*-1$-1$-1$d$i$v$12$18$1i$1t$20$2j$3s$41,2*20*-1$h$1s$2o$2q$3c$3t$49$4i$4k$67$78,3*3*25$7p,4*7*t$14$2s$40$47$4f$4m$54,5*40*-1$1$5$b,6*58*-1$-1$4$9$h$t$10$18$3d$75$88$8f$8n$98,7*0*1l,9*14*-1$b$14$1n$1v$3b,10*10*-1$f,11*23*-1$-1$4$f$m$18$1q,12*7*q$2m$2u$6r$7h$8i,13*5*s$1d$1i$2f$46$7t,14*19*-1$b$u$1a$1j$6v$cm$cu$do$e3$ea,15*12*-1$c$s$4i,16*44*-1$2$6$i$r$14$3j$3p";
w["bi."]="1*10*-1$u,2*2*1r$3s$4h,4*3*s$3v$4l$53,6*13*-1$s$17$87$97,9*0*13,11*10*-1$e,15*10*-1$b,16*0*3i";
w["bi:"]="1*0*1s,2*0*2n,4*0*13";
w["blank"]="13*0*4b";
w["body"]="8*0*88";
w["both"]="14*1*1f$1n";
w["bound"]="15*0*4c";
w["box"]="2*0*3l,4*0*3p,6*1*1v$81,13*0*3k";
w["build"]="0*0*6g";
w["c"]="3*0*2e,7*0*1u,8*1*3k$5d,9*1*1f$33,14*2*bh$bu$di,15*0*2b";
w["c1010"]="6*0*9m,14*0*6a";
w["c1010data"]="8*0*5k";
w["c:\\program"]="3*0*2d,7*0*1t,8*0*3j,14*0*bg,15*0*2a";
w["c:\\users\\"]="8*0*5c,9*1*1e$32,14*1*bt$dh";
w["ca"]="2*0*60";
w["cache"]="8*0*7q,12*0*70";
w["cached"]="15*0*3v";
w["caches"]="6*0*3e";
w["call"]="12*0*4t";
w["called"]="14*1*ck$d3";
w["can"]="0*0*q,2*1*1e$51,3*1*5j$8k,4*0*4o,6*1*1q$8v,7*0*53,8*4*16$28$5s$al$ff,11*0*15,12*0*41,13*12*-1$a$n$5f,14*1*61$dv,15*0*25,16*12*-1$d$1o$2l";
w["cannot"]="2*0*3d,15*0*5q";
w["canvas"]="13*1*4d$7v";
w["canvas."]="13*1*4c$7u";
w["capacity"]="13*0*q";
w["captured"]="8*0*2p";
w["case"]="14*1*3q$4m";
w["cases"]="3*0*7i";
w["cdata"]="3*3*2i$2j$2o$82,6*0*9l,7*3*22$23$28$4p,8*4*2g$3o$3p$5j$cs,10*10*-1$7,14*2*69$bl$bm,15*1*2f$2g";
w["certain"]="6*0*3k";
w["certified"]="1*1*15$19";
w["change"]="8*1*2h$fg,13*0*9d,15*0*3n";
w["changes"]="0*0*7m,12*1*6n$78,14*0*b6";
w["changes."]="0*0*7l";
w["chart"]="13*1*97$9e";
w["check"]="2*0*3k,4*0*3o,6*0*80,13*1*3j$8p,15*0*35";
w["checked"]="8*10*-1$5";
w["checking"]="8*0*2f";
w["choose"]="0*0*2f";
w["clear"]="15*0*49";
w["click"]="0*1*63$6c,1*2*2f$3k$3n,2*2*3j$3m$45,4*1*3n$3q,6*5*1m$2f$51$6a$7v$82,8*5*1t$6f$d8$el$gc$gl,12*0*7b,13*7*2n$2u$3i$3p$42$7a$7l$94,14*3*34$ab$al$ao,15*1*54$5b";
w["client"]="7*10*-1$l";
w["client-side"]="7*10*-1$k";
w["close"]="1*0*3o";
w["code"]="2*0*74";
w["col1"]="0*0*49";
w["col2"]="0*0*4d";
w["column"]="13*0*96";
w["com"]="3*1*4e$5s,8*0*ej";
w["combining"]="0*0*5k";
w["commands"]="8*0*9c";
w["commands."]="8*0*9b";
w["common"]="15*10*-1$5";
w["communication"]="8*1*8j$8v";
w["compatible"]="14*0*71";
w["complete"]="8*0*ev,12*1*25$2j";
w["completed"]="14*0*2q";
w["complex"]="12*0*5k";
w["compliance"]="12*0*62";
w["configuration"]="6*1*2u$5n,8*3*1r$3v$6c$d2,14*3*38$56$at$b9,15*0*33";
w["configure"]="3*30*0$5,5*10*-1$e,7*31*0$6$1g,14*3*2i$2v$3s$4o";
w["configured"]="12*0*1e";
w["configureodbc"]="3*0*2c,7*0*1s,8*2*1d$1j$3h,14*0*be,15*0*28";
w["conne"]="13*0*2k";
w["connect"]="0*1*1n$1r,2*30*0$4,3*0*6f,4*50*-1$-1$0$4$a$c,6*45*-1$0$5$o$11$1n$2e$6b$9d,12*0*91,13*1*j$2b,14*2*4j$e1$e6,15*1*p$5j,16*14*-1$a$15$2v$3k$3s";
w["connect."]="6*0*2d";
w["connected"]="4*0*2p";
w["connecting"]="6*1*23$4d,13*10*-1$5,15*10*-1$8,16*0*3f";
w["connection"]="0*1*1i$22,3*0*31,6*0*4u,8*0*43,12*59*-1$-1$0$2$5$d$k$t$1j$1t$28$2b$2n$2q$9a,13*0*3s,14*3*n$5i$5s$ad,15*0*40";
w["connection."]="12*0*1s";
w["connections"]="2*0*3u,4*0*42,6*0*8a";
w["connectivity"]="6*0*33,9*10*-1$7,12*1*69$9g";
w["connector"]="1*12*-1$k$13$1a,3*4*27$7q$8q,5*50*-1$-1$2$6$c$k,6*10*-1$i,7*1*1n$59,9*10*-1$c,10*10*-1$h,11*14*-1$5$n$s$19$1s,12*5*2v$73$8a,14*44*-1$1$3$d$g$1b$1k$70";
w["connector."]="1*10*-1$j,5*10*-1$j,10*10*-1$g,11*0*1r,12*0*72,14*10*-1$c";
w["connector:"]="3*0*26,7*0*1m,14*0*f";
w["connectors"]="1*0*2e,14*1*d5$ds";
w["connectors:"]="1*0*2d";
w["consider"]="8*0*dn";
w["contact"]="3*1*1k$59,7*0*1a,8*33*0$2$12$3c$gr,14*0*h,15*12*-1$k$1c$5s,16*0*23";
w["contacting"]="8*0*37";
w["containing"]="6*1*57$6g";
w["content"]="3*0*5v";
w["contents"]="9*0*1b";
w["continue"]="3*0*8m,6*0*2g,7*0*55,14*1*44$51";
w["copy"]="12*1*6g$82";
w["copy."]="12*0*81";
w["core"]="8*0*9o";
w["count"]="8*1*bg$br";
w["course"]="0*10*-1$a";
w["create"]="0*0*85,2*1*1k$76,4*1*1e$41,6*1*89$94,8*12*-1$d$3a$em,12*0*j,13*13*-1$c$1p$26$8v,14*1*ci$d1,16*0*1d";
w["created"]="4*1*1c$36,8*1*au$c9";
w["creates"]="6*0*3g";
w["creating"]="0*10*-1$b,13*0*8l";
w["credententials"]="15*0*4b";
w["credential"]="6*0*5s";
w["credentials"]="7*10*-1$h,15*1*3q$5e";
w["cross"]="2*0*5r";
w["cted"]="13*0*2l";
w["currently"]="7*0*3s";
w["custom"]="14*1*d4$dr,16*0*2o";
w["data"]="0*13*-1$e$6f$6j$80,1*0*38,2*1*65$6k,4*1*4a$4c,6*14*1b$1g$2o$32$3f$3j$3s$44$4b$5e$66$8i$8k$93$9s,8*1*8k$90,11*0*10,12*11*n$15$3k$4b$68$6h$7a$7r$8l$8t$93$9f,13*56*-1$-1$1$3$7$g$12$28$30$4s$8s$9c,14*3*31$5c$5t$65,15*1*4q$4u,16*0*3d";
w["data."]="2*0*64,4*0*49,6*2*43$4a$8h,11*0*v,12*0*8s,13*12*-1$f$11$9b";
w["database"]="2*0*2u,4*0*30,6*1*58$6h,12*1*4e$51,15*0*1a,16*0*10";
w["database."]="12*1*4d$50,15*0*19,16*0*v";
w["date"]="0*15*3i$3k$3p,2*0*5p,6*0*42,8*1*b1$cc,13*1*53$84,14*0*12";
w["date."]="14*0*11";
w["day"]="13*1*20$8g";
w["default"]="7*1*3e$4i,8*4*55$a4$bi$cn$fh,14*0*64";
w["define"]="12*0*84";
w["delete"]="15*1*5c$5d";
w["deleted"]="8*1*bb$cm";
w["deleted."]="8*0*cl";
w["deleting"]="8*0*do";
w["demo"]="13*0*17";
w["department"]="0*3*30$32$4b$4f";
w["departments"]="0*0*5g";
w["departments."]="0*0*5f";
w["depending"]="2*0*6r,8*0*bc";
w["desc"]="0*5*31";
w["described"]="0*0*1q,12*0*12";
w["description"]="6*0*3a,8*0*g4";
w["desired"]="13*0*9h";
w["desired."]="13*0*9g";
w["desktop"]="1*3*21$2k$3u$42,9*2*1p$21$3d,13*0*47,14*2*cn$cv$dq,15*0*4j";
w["desktop."]="1*0*3t,9*0*20";
w["desktop\\custom"]="14*0*dp";
w["desktop\\traces"]="9*1*1o$3c";
w["detail"]="0*0*3a,12*0*4n,13*3*38$3e$3o$4l";
w["detailed"]="8*0*g6,11*0*1t";
w["details"]="3*0*7v,8*0*93";
w["details."]="3*0*7u";
w["diagnostic"]="9*0*2p";
w["diagnostics"]="9*0*2m";
w["dialog"]="0*1*1k$23,1*0*2u,2*0*40,4*0*43,6*5*1h$2j$4s$4v$5t$8b,9*1*2g$2k,12*42*-1$1$3$6$u$9c,13*0*3u,14*2*2s$ak$an,15*1*51$5a";
w["dialog."]="0*0*1j";
w["dialog:"]="14*0*aj";
w["different"]="0*0*5e,3*1*r$75,6*0*5h,8*0*61,16*0*t";
w["difficult"]="2*0*16";
w["difficulties"]="8*10*-1$a";
w["direc"]="8*0*5a";
w["direct"]="0*0*7s";
w["directions"]="8*0*1h,16*1*2f$3r";
w["directory"]="3*0*8f,8*2*3i$63$69,9*0*1d,14*2*1t$bf$c4,15*0*29";
w["directory."]="8*0*62,14*1*1s$c3";
w["directquery"]="2*0*44,6*2*3o$46$4l,9*10*-1$j,12*2*3a$7k$7n,13*0*40";
w["directquery."]="6*0*4k";
w["discover"]="13*0*t";
w["display"]="2*0*3e";
w["displaying"]="15*0*t";
w["displays"]="2*1*4d$4l,6*0*6p,13*2*4a$4h$8a";
w["do"]="0*0*81,15*1*12$4e,16*0*1m";
w["documentation"]="11*0*1u,13*0*1e";
w["documents"]="14*3*c2$c7$cf$dm";
w["does"]="14*1*co$d6";
w["domain"]="3*1*70$72";
w["don"]="4*0*17";
w["don't"]="4*0*16";
w["down"]="3*1*3c$3g,7*0*2g";
w["download"]="14*0*1e";
w["downloaded"]="14*0*df";
w["downloads"]="14*0*1r";
w["drag"]="13*1*52$6l";
w["drive"]="14*0*ce";
w["driver"]="3*15*-1$e$1t$2l$2q$3l$84,5*10*-1$g,6*10*-1$e,7*3*25$2a$2m$4r,8*6*s$1n$26$2e$3r$cu$dg,10*10*-1$d,11*3*1h$1o$21$24,12*6*1h$2f$64,14*4*20$25$2e$2l$bo,15*1*2i$2v";
w["driver."]="3*0*3k,7*0*2l,8*0*25,11*0*1g,12*0*1g,14*0*2k";
w["drop"]="3*0*3b";
w["drop-down"]="3*0*3a";
w["dsn"]="6*1*2t$5m,8*4*1q$3u$4l$6b$d1,12*11*-1$a$19,14*5*37$55$5n$68$as$b8,15*2*32$4d$53";
w["during"]="6*0*5p";
w["e"]="14*0*18";


// Auto generated index for searching.
w["each"]="9*0*u,13*0*8f";
w["easily"]="6*0*22";
w["easily."]="6*0*21";
w["edit"]="0*1*76$7g,15*1*55$58";
w["editor"]="0*2*6p$74$7c";
w["editor."]="0*2*6o$73$7b";
w["ellipsis"]="8*0*6g";
w["email"]="8*1*f6$fi";
w["embed"]="3*1*4n$4s";
w["en"]="3*0*5t";
w["enable"]="7*10*-1$e,9*33*0$2$o$22$2s,14*0*l";
w["enabling"]="9*0*1s";
w["end"]="8*1*b4$cf";
w["ensure"]="14*0*s";
w["enter"]="0*0*27,3*2*3q$5e$6a,6*3*1s$2m$5d$60,7*1*2q$31,8*7*47$4d$6u$9n$ag$bt$fm$g5,12*12*-1$9$18$3r,13*1*66$6f,14*3*58$6g$80$99";
w["entered"]="3*0*4i,6*1*5l$9r";
w["environment"]="6*0*71,14*0*6j";
w["environment."]="6*0*70";
w["equivalent"]="2*0*5h";
w["error"]="15*1*1m$3i";
w["error:"]="15*1*1l$3h";
w["errors"]="8*0*7i";
w["errors."]="8*0*7h";
w["etc"]="12*3*24";
w["etc."]="12*3*23";
w["everything"]="8*0*7m";
w["example"]="0*1*2j$6r,2*0*5d,3*1*8d$8i,8*0*4s,12*3*1u$4g$53$5i,13*0*15";
w["example:"]="3*1*8c$8h,12*1*4f$52";
w["executable"]="14*0*1h";
w["execute"]="12*1*4r$54";
w["executes"]="12*1*6s$8b";
w["execution"]="8*1*7d$9h";
w["exist"]="14*1*cr$d9";
w["exist."]="14*1*cq$d8";
w["existing"]="8*1*5u$du,12*0*92,15*0*4a";
w["exit"]="1*0*3p,14*0*ar";
w["expand"]="6*0*6k,12*0*3n";
w["experiencing"]="8*1*df$ea";
w["experiencing."]="8*0*e9";
w["experiencing.  the"]="8*0*de";
w["explanation"]="8*0*g7";
w["express"]="2*0*17";
w["extended"]="0*0*33";
w["extension"]="1*0*3e";
w["extensions"]="1*0*39";
w["external"]="10*30*0$3";
w["extract"]="12*0*4a";
w["false"]="7*0*4k";
w["familiarity"]="3*0*13,7*0*t";
w["feature"]="2*10*-1$c";
w["features"]="13*0*1k,14*0*o,16*10*-1$g";
w["features."]="13*0*1j";
w["field"]="3*3*34$3p$4u$69,8*0*f4,12*1*1m$3c,13*1*54$6n";
w["fields"]="2*0*4p,6*0*4r,8*0*f2,12*1*11$87,13*2*4e$4i$87";
w["fields:"]="8*0*f1";
w["file"]="1*0*2m,8*20*4i$4r$50$58$60$6k$6r$72$9t$aa$ad$ak$b5$bf$bj$bq$cg$di$e1$gf$gk,9*1*28$3f,14*1*1m$dd,15*0*4l";
w["file."]="8*6*4h$4q$6q$9s$a9$e0$gj,14*0*1l";
w["files"]="3*0*2h,7*0*21,8*1*3n$c1,14*1*1o$bk,15*0*2e";
w["files\\cdata\\cdata"]="3*0*2g,7*0*20,8*0*3m,14*0*bj,15*0*2d";
w["filter"]="13*14*-1$b$5p$70$7c$7e";
w["filtering"]="13*1*5t$74";
w["filters"]="2*0*6s,12*0*85,13*4*57$59$5i$6q$6s";
w["find"]="6*0*20,8*1*17$29,14*0*13";
w["finding"]="9*10*-1$d";
w["finish"]="14*0*35";
w["first"]="1*10*-1$8,6*0*2a,11*0*1c,12*1*i$5c,16*0*1c";
w["folder"]="8*1*b0$cb,14*5*c8$ch$cj$d0$d2$dg";
w["folder."]="14*0*cg";
w["follow"]="1*10*-1$n,11*0*1i,14*11*-1$6$2b,16*1*2e$3q";
w["following"]="1*0*28,2*1*5c$6c,3*2*v$5l$8a,6*0*38,7*2*p$2r$51,8*3*34$48$d7$f0,12*1*r$43,13*3*13$1l$4o$7k,14*1*59$ai,15*11*-1$4$4g";
w["following:"]="2*0*6b,3*0*89,6*0*37,7*0*50,8*0*d6,13*0*7j,15*0*4f";
w["follows"]="0*0*2n,8*0*74";
w["follows:"]="0*0*2m,8*0*73";
w["formatting"]="13*0*9f";
w["from"]="0*10*1t$2q$36,2*2*34$5m$6l,3*1*39$76,4*0*38,6*44*-1$2$7$q$2r$5i$7a$7j,9*0*25,12*2*4c$4j$7d,13*14*-1$e$10$2d$2q$86,14*0*e8,15*0*46,16*0*3n";
w["fs"]="3*0*4c";
w["full"]="13*1*p$1f";
w["function"]="0*1*15$5i";
w["further"]="14*0*b5";
w["future"]="14*0*bb";
w["general"]="6*0*45";
w["get"]="6*1*1a$1f,11*1*h$o,12*0*m,14*0*2o";
w["getting"]="3*0*1f,6*0*3v,7*0*15";
w["global"]="1*0*34";
w["good"]="13*0*8n";
w["graph"]="13*0*1s";
w["group"]="7*1*2u$33";
w["guide"]="4*6*1u$2i,11*0*g";
w["have"]="3*0*1c,4*1*19$2o,7*0*12,8*2*n$2o$4n,13*0*2i,15*1*13$3u";
w["having"]="8*10*-1$9";
w["headers"]="8*0*83";
w["headers."]="8*0*82";
w["help"]="3*0*5q,8*0*gu";
w["helpful"]="8*0*95";
w["here"]="3*1*4j$5g";
w["here."]="3*0*5f";
w["higher"]="8*0*9e";
w["highlighted"]="13*1*82$93";
w["hit"]="8*1*ar$c6";
w["hoc"]="2*0*6p";
w["home"]="12*0*7e,13*0*2r";
w["hour"]="2*0*5q";
w["hourly"]="2*0*61";
w["how"]="2*0*1j,13*0*1o";
w["however"]="8*0*64,14*0*7j";
w["htm"]="3*0*65";
w["http"]="8*0*81";
w["https"]="3*1*4b$5p,8*0*eg";
w["https://fs.myorg.com"]="3*0*4a";
w["https://help.okta.com/en/prod/content/topics/apps/apps_apps_page.htm#show"]="3*0*5o";
w["https://support.1010data.com"]="8*0*ef";
w["icon"]="8*0*6h";
w["id"]="0*1*2u$45,7*1*2v$34";
w["identity"]="3*0*6g";
w["ids"]="7*1*3r$4c";
w["import"]="0*1*2g$7p,2*0*2j,4*1*n$11,6*0*3b,12*1*6b$6d";
w["in."]="15*0*48";
w["include"]="8*0*gg";
w["included"]="8*3*7n$9q$a3$a7";
w["included."]="8*0*a2";
w["includes"]="8*1*8n$99";
w["including"]="14*0*6m";
w["independent"]="12*0*71";
w["information"]="0*0*62,3*0*4q,4*1*21$2l,6*0*4p,7*0*2t,8*4*2r$4a$7t$dr$e5,12*10*-1$b,14*2*43$50$5b";
w["information."]="0*0*61,4*1*20$2k,14*1*42$4v";
w["information:"]="7*0*2s,8*0*49,14*0*5a";
w["insights"]="4*6*1q$2e,6*0*7b,8*0*fa,13*0*v,14*1*73$7d,15*1*1v$2n,16*0*21";
w["install"]="3*0*8p,5*20*-1$-1$d$h,6*10*-1$c,7*0*58,11*1*1d$1m,14*42*-1$0$2$9$e$2h";
w["installation"]="6*0*5q,14*0*2p";
w["installed"]="8*0*t,14*0*v";
w["instance"]="3*0*49";
w["instructions"]="11*0*1j,14*11*-1$7$e5";
w["integration"]="11*10*-1$b";
w["interface"]="8*0*9a";
w["introduction"]="11*30*0$1";
w["issue"]="8*1*dc$e7";
w["issues"]="9*10*-1$8,15*10*-1$6";
w["it."]="1*0*1n";
w["items"]="13*0*61";
w["its"]="2*0*24";
w["join"]="0*5*3b,12*0*88";
w["just"]="2*1*1u$4q,12*0*9d,16*0*31";
w["keep"]="8*0*c3";
w["keep."]="8*0*c2";
w["know"]="2*2*1i$20$70";
w["language"]="0*31*3$8$5p,2*12*-1$r$6i$73,12*0*5r";
w["language."]="2*10*-1$q";
w["later"]="14*0*5m";
w["latest"]="8*0*2c,11*0*1e,14*0*7r";
w["launch"]="1*0*1u";
w["learn"]="2*0*29";
w["level"]="8*1*6v$8i,13*2*56$5k$6p";
w["license"]="8*0*1u";
w["like"]="0*0*6v,2*2*v$4r$6a,13*0*7i";
w["likely"]="3*0*56";
w["limit"]="8*2*aq$c5$cq,13*0*4q";
w["limit."]="8*0*cp";
w["link"]="3*1*4p$4t,14*0*1c";
w["link."]="3*0*4o";
w["list"]="2*0*35,3*1*3e$6m,4*0*39,12*1*26$2k";
w["list."]="3*1*3d$6l";
w["lists"]="8*0*f5";
w["load"]="0*0*6d,1*0*3f,2*1*3n$3o,4*1*3r$3s,6*1*83$84,12*0*95,13*0*3q";
w["loaded"]="4*0*4d,6*0*8l";
w["loads"]="2*0*4a,4*0*48,6*0*8g";
w["local"]="1*0*23,9*1*1k$38,12*1*6v$80,14*0*cb";
w["location"]="8*0*6o";
w["log"]="8*39*-1$e$2i$3b$4g$4v$57$5v$6j$6p$71$77$7l$87$8f$8u$9k$9r$a8$ac$aj$at$b9$be$bp$c0$c8$ck$dv$eb$gi,14*3*8a$8r$9i$a1,15*1*3l$5r";
w["logfile"]="8*0*4b";
w["logging"]="8*1*46$dq,15*0*47";
w["logic"]="2*0*15";
w["login"]="3*1*3n$78,7*0*3l,14*0*6d,15*0*36";
w["longer"]="15*1*42$5g";
w["look"]="2*0*u,13*0*7h";
w["looks"]="0*0*6u,2*0*69,3*0*87,7*0*4u,8*0*d4";
w["machine"]="1*0*25,14*0*cc";
w["machine."]="1*0*24";
w["macro"]="0*34*2$7$14$2r$5h$5o,2*12*-1$p$6h$72,12*0*5q";
w["magic"]="2*52*-1$2$6$a$s$1a$1l$21$2c$2k$32$3g$3p$4b$4m$56,6*0*4f";
w["make"]="8*2*k$2l$30,12*0*6m,14*1*2t$b4";
w["management"]="7*30*3$9,14*1*4g$4r";
w["manager"]="3*0*1p,7*0*1f,15*0*1f,16*0*28";
w["manager."]="3*0*1o,7*0*1e,16*0*27";
w["manual"]="0*3*5v,2*3*2h,3*3*7s,11*0*26,12*9*2h$31$66";
w["manuals"]="10*40*-1$2$5$9";
w["many"]="3*0*k";
w["max"]="8*2*ab$bd$bo";
w["maximum"]="8*1*ah$bu";
w["may"]="0*10*-1$i,8*1*94$ba,9*0*18,14*1*3g$4c,15*12*-1$e$1r$3t";
w["mb"]="8*1*ao$bn";
w["mb."]="8*1*an$bm";
w["menu"]="9*0*26,12*0*7f";
w["message"]="1*0*2a";
w["metadata"]="16*0*1u";
w["method"]="6*0*48";
w["microsoft"]="1*0*16,6*0*15,9*1*1l$39,11*10*-1$c,13*0*1b";
w["mode"]="0*2*2i$7q$7u,6*0*34,9*10*-1$l,12*3*6a$6e$7o$9i";
w["mode."]="0*0*2h,9*10*-1$k";
w["mode:"]="12*0*9h";
w["modules"]="8*3*9l$9p$9v$a6";
w["more"]="0*0*60,2*0*2a,3*0*7t,4*1*1v$2j,6*1*1d$4o,12*0*5j,14*1*41$4u";
w["most"]="3*1*55$7h,6*0*40,8*0*o,12*0*8q";
w["move"]="8*0*5t,9*0*1a,14*0*da";
w["mtdemo"]="2*0*5o";
w["mtdemo.date_hour_cross"]="2*0*5n";
w["must"]="0*1*2e$7j,3*1*44$4k,6*0*4i,8*1*4m$66,11*0*1b,14*0*c9,16*0*1b";
w["my"]="12*0*56";
w["my_proc"]="12*0*55";
w["mylog"]="8*1*52$dk";
w["mylog.txt"]="8*1*51$dj";
w["myorg"]="3*0*4d";
w["name"]="2*0*23,6*5*2q$5g$63$69$9o$9u,8*4*4e$4u$6l$b7$ci,9*0*3g,12*0*17,14*9*5e$5g$5o$67$84$8c$8g$8t$8u$96";
w["name."]="6*0*68,8*1*b6$ch,14*1*83$95";
w["names"]="6*3*79";
w["navigate"]="14*0*br";
w["navigator"]="2*0*2r,4*0*2t,6*4*54$6d$6m$6o$7k,13*0*33,15*0*10";
w["nce"]="15*0*3b";
w["necessary"]="0*1*5j$75,2*1*1h$6v,8*0*2s,12*0*3q";
w["need"]="0*10*-1$j,1*11*-1$7$1f,2*0*1v,3*0*58,8*10*-1$c,9*0*r,14*1*5k$b3,15*1*17$5i";
w["need."]="15*0*5h";
w["needing"]="15*10*-1$j";
w["negotiation"]="8*0*8q";
w["negotiation."]="8*0*8p";
w["new"]="1*0*43,8*5*56$68$6n$as$c7$en";
w["next"]="6*0*99,11*0*1k,12*0*8u,13*0*3l,14*0*52";
w["nity"]="0*0*43";
w["note"]="0*1*2c$7o,2*1*28$3a,6*1*4n$73,8*0*5q,9*0*16,11*0*12,13*1*5d$8k,14*6*79$86$8m$9e$9s$b1$c6";
w["note:"]="0*1*2b$7n,2*1*27$39,6*1*4m$72,8*0*5p,9*0*15,11*0*11,13*1*5c$8j,14*6*78$85$8l$9d$9r$b0$c5";
w["now"]="2*0*52,3*1*86$8l,4*0*4p,6*0*90,7*1*4t$54,8*0*d3,14*1*1p$e0";
w["number"]="8*3*19$23$79$bv";
w["obtaining"]="6*0*49";
w["occur"]="0*0*82";
w["odbc"]="3*15*-1$d$1s$2k$2p$3j$83,5*10*-1$f,6*10*-1$d,7*3*24$29$2k$4q,8*4*r$1m$24$3q$ct,10*10*-1$c,11*3*1f$1n$20$23,12*7*1f$27$2e$63,14*7*15$1g$1v$24$2d$2j$30$bn,15*3*1k$2h$2u$3g";
w["offers"]="11*10*-1$7";
w["ok"]="0*0*64,1*1*2g$3l,2*0*46,6*0*52,8*0*d9,13*0*43,14*1*am$ap";
w["okta"]="3*9*4g$4l$54$5b$5h$5r$6r$71$7d$8b";
w["okta."]="3*0*53";
w["old"]="8*1*b8$dp";
w["oldest"]="8*0*cj";
w["once"]="6*10*-1$a,8*0*1l,13*0*h,15*0*2t";
w["one"]="6*1*36$5j";
w["only"]="8*1*9u$e3";
w["open"]="3*0*29,6*0*14,7*0*1p";
w["operation"]="0*0*4p";
w["operations"]="0*12*-1$o$t$5r";
w["operations."]="0*11*-1$n$5q";
w["option"]="6*0*39,9*0*t";
w["optional"]="0*0*7e,3*0*6t,6*0*5b,8*0*ft,12*2*1l$35$3b,14*1*3d$49";
w["optional:"]="0*0*7d,14*1*3c$48";
w["options"]="0*1*1g$26,1*3*2o$2r$2t$36,3*10*-1$c,9*4*2a$2d$2f$2j$2q,12*3*2d$2s$34$3p,15*0*4n";
w["options."]="1*0*35";
w["order"]="1*11*-1$e$1l,2*1*1n$75,8*0*2k,16*0*12";
w["organization"]="3*0*1a,14*1*3f$4b";
w["organization."]="3*0*19";
w["organizations"]="3*0*l";
w["other"]="2*0*4t,4*0*50";
w["our"]="8*0*2t";
w["out"]="1*0*3q,8*0*2a,9*10*-1$e";
w["own"]="14*1*93$a8";
w["owner"]="14*6*8v$a4";
w["package"]="14*0*16";
w["page"]="3*0*64,13*1*55$6o";
w["pages"]="13*0*5m";
w["pane"]="4*0*3m,6*0*7u,13*4*3h$4f$5b$6u$7f";
w["pane."]="4*0*3l,6*0*7t,13*2*3g$5a$6t";
w["parameterized"]="2*0*1g";
w["parameterized."]="2*0*1f";
w["party"]="6*0*26";
w["password"]="6*0*64,14*6*97$9c$9j$9m$a2$a3$aa";
w["password."]="14*1*9b$a9";
w["paste"]="12*0*3s";
w["path"]="6*0*77";
w["pbidesktop"]="9*0*3i";
w["perform"]="2*12*-1$f$53$5f";
w["performs"]="0*0*4n";
w["permission"]="8*0*4o";
w["permissions"]="15*1*56$59";
w["pertaining"]="8*0*e6";
w["placed"]="8*0*59";
w["platform"]="4*6*1r$2f,6*0*7c,8*0*fb,14*1*74$7e,15*1*20$2o,16*0*22";
w["please"]="3*0*1j,7*0*19,8*0*j,15*0*1b";
w["point"]="6*0*3l,8*0*67";
w["pool"]="7*6*32$38$3a$3k$3p$40$4e,14*3*8q$91$a0$a6";
w["pool."]="7*0*37";
w["pools"]="7*46*-1$5$b$d$10$18$1i$2i$2o$4n,14*1*4i$4t";
w["pools."]="7*0*v";
w["portal"]="3*0*3u,8*0*ee";
w["power"]="0*15*-1$f$v$1l$1u$6m$70,1*38*-1$-1$-1$c$h$t$11$17$1h$1r$1v$2i$3r$40,2*30*-1$-1$g$n$1q$2m$2p$3b$3r$48$4g$4j$66$77,3*3*24$7o,4*7*r$12$2r$3u$46$4e$4k$52,5*40*-1$0$4$a,6*58*-1$-1$3$8$g$r$v$16$3c$74$86$8e$8m$96,7*0*1k,9*14*-1$a$12$1m$1u$3a,10*10*-1$e,11*23*-1$-1$3$d$l$17$1p,12*7*p$2l$2t$6q$7g$8h,13*5*r$1c$1h$2e$45$7s,14*19*-1$a$t$19$1i$6u$cl$ct$dn$e2$e9,15*12*-1$a$r$4h,16*44*-1$1$5$h$q$13$3h$3o";
w["pqx"]="14*0*dc";
w["practice"]="14*0*7l";
w["preferable"]="6*0*47";
w["preventing"]="15*0*44";
w["preview"]="0*0*66,2*0*3f,4*1*3f$3k,6*1*7o$7s,13*0*3f";
w["prime"]="14*1*6p$7q";
w["prime-latest"]="14*0*7p";
w["prior"]="8*0*36,15*0*21";
w["priority"]="8*1*fs$fv";
w["problem"]="8*1*g9$h3";
w["problem."]="8*0*h2";
w["problems"]="8*0*98";
w["problems."]="8*0*97";
w["proc"]="12*0*57";


// Auto generated index for searching.
w["procedures"]="12*0*4u";
w["process"]="6*0*5r";
w["prod"]="0*1*48$4c,3*0*5u";
w["produce"]="8*0*dh";
w["products"]="0*0*3e";
w["program"]="1*0*1c,3*1*2b$2f,7*1*1r$1v,8*1*3g$3l,14*1*bd$bi,15*1*27$2c";
w["program."]="1*0*1b";
w["prompts"]="14*0*2c";
w["properties"]="3*3*68$6d$6v$7n,12*14*-1$f$1k$1r$29$2o";
w["properties."]="3*0*6u,12*10*-1$e";
w["property"]="3*0*6o";
w["propertya"]="12*3*1v";
w["propertyb"]="12*3*21";
w["provide"]="12*0*3f";
w["provider"]="3*3*17$38$41$6h,8*0*5m,14*1*8k$9q";
w["provider."]="3*0*40,14*1*8j$9p";
w["provider\\schema"]="8*0*5l";
w["pulling"]="2*0*6j";
w["queries"]="0*0*5m,2*11*-1$i$54,4*31*3$7$k,8*0*7r,12*2*6u$8d$8j,16*16*-1$o$19$1j$2r$38$3m$3v";
w["queries."]="16*10*-1$n";
w["query"]="0*41*4$9$11$53$68$6l$6n$6t$71$79$7h$7t$8a,2*0*5g,4*19*-1$h$10$1b$1h$1o$24$2b$35$3h$4s,8*0*78,9*10*-1$h,11*0*t,12*1*5m$5s";
w["query."]="0*1*52$89,4*10*-1$g";
w["querying"]="6*0*3q";
w["question"]="8*0*g8";
w["questions"]="3*0*1d,7*0*13";
w["quick"]="4*50*-1$2$6$f$j$v$1a$1g$1n$23$2a$34$3g$4r,16*16*-1$m$18$1i$2q$37$3l$3u";
w["rather"]="12*0*7u";
w["ready"]="6*10*-1$n";
w["real"]="6*0*3t,12*0*7s";
w["receive"]="1*0*27";
w["recent"]="6*0*9i,8*0*p,12*0*8r,13*0*2o";
w["recommend"]="14*0*7n";
w["recommended"]="1*0*3b,12*0*7l";
w["refer"]="3*0*5k,10*10*-1$6,13*0*1a";
w["refere"]="15*0*3a";
w["reference"]="0*3*5u,2*3*2g,3*3*5n$7r,10*40*-1$1$4$8,11*0*25,12*10*2g$30$5v$65";
w["reference:"]="3*0*5m";
w["refine"]="16*0*3b";
w["refresh"]="12*0*7c";
w["regular"]="2*1*10$59";
w["release"]="7*0*49";
w["remember"]="8*0*9d,14*0*5l";
w["remote"]="12*1*79$7q";
w["report"]="2*0*4e,4*0*4i,6*0*8q,12*2*6k$6p$76,13*2*48$5j$5n";
w["report."]="12*0*6j";
w["representation"]="13*0*9a";
w["representative"]="14*0*k";
w["reproduce"]="8*0*db";
w["request"]="8*4*7v$89$fr$g3$gb,14*0*5r,15*0*1p";
w["request."]="8*2*fq$g2$ga";
w["requester"]="8*0*f3";
w["require"]="14*0*3h";
w["required"]="3*1*6e$6q";
w["required."]="3*0*6p";
w["reset"]="7*0*3v";
w["resolve"]="8*0*h0";
w["response"]="8*0*8b";
w["response."]="8*0*8a";
w["restart"]="1*0*3v";
w["results"]="0*2*50$69$87";
w["retail"]="0*4*38$3d$40$4s$56";
w["retail:basket_affi"]="0*0*3v";
w["retail:basket_affinity"]="0*1*4r$55";
w["retaildemo"]="0*1*37$3c,12*0*4l,13*0*36";
w["retaildemo.sales_detail"]="12*0*4k,13*0*35";
w["retry"]="7*1*39$3i";
w["return"]="12*0*3m";
w["return."]="12*0*3l";
w["returned"]="8*0*7b";
w["ribbon"]="12*0*7j,13*0*2t";
w["ribbon."]="12*0*7i,13*0*2s";
w["right"]="8*0*6i,13*0*4g";
w["roaming"]="8*0*5i";
w["rows"]="8*0*7a";
w["run"]="3*0*2a,7*0*1q,8*0*3f,14*1*1u$bc,15*0*26";
w["running"]="8*0*1c";
w["s"]="6*0*7i";
w["sales"]="0*3*34$35$39$4i,12*0*4m,13*6*1v$37$3d$3n$4k$4r$8c";
w["sales_detail"]="13*2*3c$3m$4j";
w["sam"]="7*50*-1$4$a$c$u$17$1h$2h$2n$36$3j$3o$4d$4m,14*5*4h$4s$8p$90$9v$a5";
w["same"]="6*0*9e,8*1*av$ca";
w["save"]="4*2*u$1f$1l,12*0*6f";
w["saved"]="4*11*-1$e$i";
w["scenes"]="2*0*6e";
w["schema"]="2*2*26$30$38,4*3*27$2c$32$3c,8*0*5n,15*0*15,16*4*1f$1l$2d$2i$2u";
w["schema."]="2*2*25$2v$37,4*2*26$31$3b,16*0*1k";
w["screen"]="3*0*2s,6*1*30$5o,7*0*2c,8*3*1s$40$6e$es,14*4*28$39$57$av$ba,15*0*34";
w["screen."]="6*0*2v,8*0*6d,14*0*au";
w["scroll"]="3*0*3f,7*0*2f";
w["search"]="6*0*1u";
w["second"]="12*0*59";
w["section"]="3*2*30$3i$81,7*2*2j$2p$4o,8*10*-1$7,11*0*1l,13*1*58$6r";
w["security"]="1*53*-1$-1$1$4$a$r$1j$1p$32,6*10*-1$k";
w["see"]="0*0*5s,2*0*2e,3*0*7l,4*2*1k$28$3e,6*1*4t$7n,8*1*1f$21,11*0*22,12*4*2a$2p$5n$60$98,13*1*3b$98,14*2*3r$4n$ah,15*0*2k";
w["select"]="0*10*2o$2s,1*2*2l$31$3a,2*3*2s$31$43$5k,3*0*35,4*1*2u$33,6*4*19$1k$35$7f$9h,7*1*3c$42,8*1*6m$fu,9*3*s$27$2l$2r,12*1*48$4h,13*7*34$3v$5r$64$6d$72$76$83,15*1*4k$52";
w["selected"]="2*0*6u,4*0*3j,6*0*7r,14*0*33";
w["selected."]="2*0*6t";
w["self"]="11*10*-1$9,16*0*1r";
w["self-service"]="11*10*-1$8,16*0*1q";
w["semicolon"]="3*0*6j";
w["semicolon-separated"]="3*0*6i";
w["send"]="8*10*-1$f";
w["separated"]="3*0*6k,8*0*a0";
w["service"]="6*0*27,11*10*-1$a,16*0*1s";
w["session"]="15*0*3r";
w["sessions"]="7*0*48";
w["set"]="3*0*1q,5*10*-1$8,7*10*-1$g,11*0*j,13*0*1g";
w["settings"]="1*54*-1$-1$2$5$b$s$1k$1q$2p$45,2*0*3v,3*0*52,6*10*-1$l,9*0*2b,12*0*9b,13*0*3t,15*2*4o$4s$50";
w["settings."]="1*0*44";
w["setup"]="5*30*3$7,14*2*21$27$2f";
w["shared"]="7*40*-1$1$7$i,14*2*4e$4p$cd";
w["should"]="13*0*7g,14*0*ag,15*0*39";
w["show"]="3*0*66,13*1*1n$60";
w["showing"]="13*0*1t";
w["shown"]="14*0*6t";
w["shown."]="14*0*6s";
w["side"]="7*10*-1$m";
w["sign"]="3*44*-1$3$8$j$p$16$1i$20,14*1*3l$3v";
w["sign-on"]="3*33*2$7$o$15$1v,14*1*3k$3u";
w["sign-on."]="3*10*-1$i";
w["signing"]="3*0*74";
w["similar"]="3*0*88,7*0*4v,8*0*d5";
w["simple"]="13*2*14$1q$27";
w["simpler"]="12*0*99";
w["since"]="12*0*8g";
w["single"]="3*44*-1$1$6$h$n$14$1h$1u,7*10*-1$f,13*0*21,14*1*3j$3t";
w["size"]="8*2*ae$ai$bk";
w["sku"]="0*0*3g";
w["slower"]="8*0*9g";
w["snapshot"]="6*0*3h";
w["software"]="3*0*s";
w["some"]="3*0*12,7*0*s";
w["source"]="0*0*1p,6*6*2p$5f$67$9g$9n$9p$9t,8*1*8m$91,12*2*16$8m$94,13*11*-1$8$32,14*4*32$5d$5v$66$6b,15*2*4r$4v$5l";
w["source."]="8*0*8l,13*0*31,14*0*5u";
w["sources"]="6*0*9j,13*0*2p";
w["specific"]="0*10*-1$l";
w["specifications"]="10*10*-1$b";
w["specified"]="6*0*65,12*0*1b";
w["specifies"]="12*0*3i";
w["specify"]="12*0*1p";
w["sql"]="0*8*17$1c$28$2k$4l$51$5l$6s$78,2*3*12$19$1c$5i,12*6*36$3g$3t$45$5l$5u$61,14*0*m,16*3*1e$1t$2c$2t";
w["sql."]="2*0*18";
w["sqlmap"]="0*3*46$4a$4e$4h";
w["ssl"]="8*0*8o";
w["sso"]="3*49*-1$4$9$b$21$37$3h$3m$3t$67$77$7m$80,14*7*3m$40$89$8e$8i$9h$9l$9o";
w["stacked"]="13*0*95";
w["staff"]="8*0*2v";
w["start"]="6*0*u,8*0*7c,9*0*11";
w["started"]="3*0*1g,7*0*16,11*0*q";
w["state"]="2*0*5v";
w["statement"]="0*4*19$1d$2a$2l$4m,2*0*5j,12*2*37$3h$3v";
w["statement."]="0*1*18$29,12*0*3u";
w["statements"]="12*2*47$49$4s";
w["statements:"]="12*0*46";
w["step"]="3*0*8n,7*0*56,14*1*45$54";
w["step."]="14*0*53";
w["steps"]="1*10*-1$o,3*0*10,7*0*q,8*0*35,13*1*1m$4p";
w["still"]="8*10*-1$8,15*0*5p";
w["store"]="12*0*4p,13*4*22$4t$6m$77$8d";
w["stored"]="12*0*4v";
w["string"]="12*1*2c$2r";
w["subject"]="8*1*fl$fn";
w["submit"]="8*1*eq$gm";
w["subset"]="4*0*o";
w["successfully"]="14*0*2r";
w["support"]="8*48*-1$1$3$i$14$2u$3e$ed$eh$fp$g1$gp,15*41*-1$1$3$n$5u";
w["support."]="8*11*-1$h$13,15*10*-1$m";
w["support:"]="8*0*3d";
w["supported"]="9*10*-1$i,12*0*39";
w["sure"]="8*2*l$2m$31,14*0*2u";
w["synchronize"]="12*0*74";
w["system"]="8*0*v";
w["systems"]="3*0*u";
w["systems."]="3*0*t";
w["t"]="4*0*18";
w["tab"]="3*0*32,8*1*1v$44";
w["table"]="2*22*-1$b$1b$1m$22$2l$33$3q$4c$4o$4v$57$5b$68,4*2*q$3t$51,6*5*4g$78$7d$7h$7p$85,12*0*96,13*8*l$19$4n$7m$7p$80$89$8m$92,16*1*2h$36";
w["table's"]="2*0*4n";
w["table."]="2*1*4u$5a,13*1*18$4m,16*0*35";
w["tables"]="2*46*-1$3$7$j$t$11$2d$36$3i$6n,4*0*3a,6*0*6r,12*0*89,15*0*v,16*13*-1$l$17$1h$2a";
w["tables."]="2*0*3h";
w["take"]="2*10*-1$k,8*0*33";
w["taken"]="8*0*7f";
w["takes"]="4*0*4g,6*0*8o";
w["team"]="8*0*gq";
w["technical"]="10*10*-1$a";
w["tenten"]="6*2*59$6i$6l,15*0*18";
w["terminate"]="7*0*46";
w["test"]="14*0*ac";
w["th"]="14*0*17";
w["than"]="12*0*7v";
w["them"]="2*0*1p,15*10*-1$h,16*0*30";
w["therefore"]="1*0*1d,16*0*11";
w["third"]="6*0*25,12*0*5f";
w["third-party"]="6*0*24";
w["threads"]="7*10*-1$o";
w["threads."]="7*10*-1$n";
w["through"]="16*0*3g";
w["ticket"]="8*1*eo$er";
w["time"]="6*3*2b$3n$3u$9a,8*2*7e$b2$cd,9*0*v,12*1*7t$8v";
w["time."]="6*0*3m";
w["times"]="8*0*9j";
w["times."]="8*0*9i";
w["titles"]="6*0*7e";
w["token"]="3*0*7g";
w["token."]="3*0*7f";
w["tool"]="16*0*1v";
w["tools"]="2*0*13";
w["topics"]="3*0*60";
w["tory"]="8*0*5b";
w["total"]="13*1*1u$8b";
w["traces"]="9*3*1c$1q$2v$3e";
w["tracing"]="9*44*-1$1$3$4$p$1t$24$2t";
w["tracing:"]="9*0*23";
w["trans"]="0*0*2t";
w["transform"]="0*0*6i";
w["transid"]="0*1*2v$47";
w["transport"]="8*0*8h";
w["transport-level"]="8*0*8g";
w["troubleshoot"]="15*10*-1$g";
w["troubleshooting"]="8*11*-1$6$96,9*10*-1$6,15*30*0$2";
w["trs"]="4*1*1j$37";
w["trs."]="4*0*1i";
w["true"]="0*5*4k,7*1*3d$43";
w["trying"]="15*1*1s$2q";
w["two"]="0*0*5d,3*0*6s";
w["txt"]="8*1*53$dl";
w["type"]="13*1*5q$71";
w["types"]="12*0*44";
w["u"]="8*0*gt";
w["unable"]="15*0*3k";
w["uncertified"]="1*0*2c";
w["under"]="0*1*1e$24,1*1*33$37,6*0*31,8*0*45,9*0*2o,13*3*5o$5v$6c$6v";
w["underlying"]="2*12*-1$m$6m$71,6*0*3r,12*2*6t$8c$8k,16*0*u";
w["unlike"]="0*0*7r";
w["unnecessary"]="3*0*7k";
w["unnecessary."]="3*0*7j";
w["up"]="3*0*1r,5*10*-1$9,6*0*41,11*0*k,14*0*10";
w["updates"]="0*0*7v";
w["url"]="3*6*3o$3r$43$46$4h$5d$79,14*1*6e$6h,15*0*37";
w["use"]="0*10*-1$k,1*11*-1$f$1m,2*1*14$1o,3*11*-1$a$m,4*0*4q,6*0*4j,7*0*3u,11*0*16,12*2*42$6c$7m,13*0*o,14*3*3i$4d$5q$63,16*12*-1$e$s$1p";
w["use."]="7*0*3t";
w["useful"]="9*10*-1$5";
w["user"]="4*6*1t$2h,6*0*62,7*2*3q$4b$4h,8*0*4j,14*5*7u$82$8b$8f$8s$94,15*0*3p";
w["user's"]="4*6*1s$2g";
w["user."]="7*0*4g";
w["username"]="8*0*5f,9*1*1h$35,14*1*c0$dk";
w["users"]="8*0*5e,9*1*1g$34,14*1*bv$dj";
w["uses"]="6*0*76";
w["using"]="0*5*12$3f,8*1*1b$4k,11*0*r,13*0*16,14*5*7o$88$8o$9g$9u$e4,16*30*0$4";
w["usually"]="9*0*30";
w["utility"]="11*10*-1$6";
w["v15"]="14*1*75$7g,15*0*23";
w["v15."]="15*0*22";
w["validation"]="1*0*3h";
w["value"]="7*1*3f$4j,8*1*bh$co,13*0*63";
w["value1"]="12*3*20";
w["value2"]="12*3*22";
w["verbosity"]="8*8*6s$70$75$7j$7o$84$8c$8r$9f";
w["version"]="8*3*q$18$22$2d,14*2*6n$7b$7t,15*2*1u$2m$3c";
w["version."]="14*0*7s";
w["video"]="1*10*-1$m,14*10*-1$5";
w["view"]="0*0*6k,2*1*1d$4f,4*0*4j,6*0*8r,13*0*49";
w["viewing"]="12*0*8p";
w["visual"]="13*0*99";
w["visualization"]="13*4*2a$7o$7q$81$91";
w["visualization."]="13*1*7n$90";
w["visualization:"]="13*0*29";
w["visualizations"]="0*12*-1$c$6h$86,2*1*55$7a,6*0*95,13*10*-1$d";
w["visualizations."]="2*0*79";
w["visualize"]="11*0*u,13*31*0$2$u";
w["want"]="6*0*9c,7*1*3h$45,8*0*e4,9*0*19,15*0*43";
w["warning"]="1*1*29$3i,6*0*28";
w["watch"]="1*10*-1$l,14*10*-1$4";
w["way"]="4*10*-1$9,13*0*8o";
w["we"]="2*0*5e,8*1*4t$e2,14*0*7m";
w["weather"]="2*0*63";
w["were"]="2*0*58";
w["what"]="0*0*6q,8*0*2b,12*0*3j";
w["when"]="7*0*3m,8*1*ap$c4,12*1*g$1c,13*0*62,14*0*2m,15*10*-1$7";
w["where"]="0*5*3h,2*0*5s,12*11*-1$7$4o";
w["which"]="15*0*2l";
w["while"]="4*0*45,6*0*8d";
w["why"]="9*10*-1$f";
w["window"]="6*1*55$6e,12*0*o";
w["windows"]="14*0*14";
w["wish"]="9*0*n";
w["within"]="0*0*16,3*0*4v,8*0*6a,14*0*cs";
w["without"]="1*0*3g,15*10*-1$i";
w["wizard"]="14*1*23$2g";
w["wizard."]="14*0*22";
w["work"]="0*0*6e,12*0*7p,14*0*7i";
w["work."]="14*0*7h";
w["would"]="4*0*4u,16*0*33";
w["write"]="8*0*4p";
w["xsales"]="13*0*85";
w["year"]="2*0*5t,13*1*23$4v";
w["yet"]="1*0*14";
w["yo"]="8*0*gs";
w["you"]="0*15*-1$h$p$1a$2d$7f$7i,1*12*-1$6$1e$26,2*12*-1$e$1t$50,3*3*1b$57$73$8j,4*7*m$15$2n$3d$3i$4h$4n$4t,6*34*-1$-1$b$m$1p$2c$3p$4c$4h$5c$5k$6t$7m$7q$8p$8u$9b$9q,7*3*11$3g$44$52,8*35*-1$-1$4$b$m$11$15$1a$20$27$2n$32$5r$65$dd$ds$e8$fe$gv,9*3*m$q$10$17,11*3*i$p$14$1a,12*21*-1$8$h$1a$1d$1o$3e$40$6l$83$8n$90$97,13*16*-1$9$i$m$2h$3a$5e$8u,14*11*2n$5j$60$6k$87$8n$9f$9t$af$b2$de$du,15*22*-1$d$o$11$16$1q$24$2p$3m$3s$41$45$5f$5o,16*25*-1$-1$9$c$1a$1n$2k$32$3a";
w["your"]="0*14*-1$d$u$67$77$88,1*0*22,2*0*2t,3*6*18$1l$36$3v$47$5a$7c,4*1*25$2v,6*6*2n$2s$3i$61$6u$7g$92,7*1*1b$35,8*7*u$4f$cr$f9$fo$g0$gh$h1,12*2*6i$6o$75,13*11*-1$6$8r,14*16*i$p$1q$3e$4a$5h$81$8d$8h$92$9a$9k$9n$a7$b7$bs$ca,15*2*u$1d$3o,16*14*-1$j$24$2b$2s$3c";

return {'fil': fil, 'w': w}})();
