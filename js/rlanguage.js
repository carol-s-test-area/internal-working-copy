window['rlanguage'] = (function() {
var w = new Object();
fil = new Array();
fil["0"]= "ColNamesFunction.html@@@ColNames(metadataobj)/ColKTypes(metadataobj)/ColLabels(metadataobj)@@@Get/set a vector of names/Ktypes/labels from a metadata object...";
fil["1"]= "ColsMetaDataFunction.html@@@ColsMetaData(ops,table)@@@Return the metadata for table with ops ops...";
fil["2"]= "DataFunction.html@@@Data(ops,table,rows,cols)@@@Get data for table with ops ops, and return the data as a data.frame...";
fil["3"]= "DistApply.html@@@DistApply(using,fun,args)@@@Run a function fun with the argument args on each segment in a sub-process and return a list of segment results...";
fil["4"]= "DistReduce.html@@@DistReduce(using,mapfun,args,reducefun)@@@Is only run on sub-processes and is a way to shrink the amount of data being passed to the accum...";
fil["5"]= "Introduction.html@@@Introduction@@@This guide contains the information you need to start developing with the R language in the 1010data Insights Platform...";
fil["6"]= "MakeMetaDataFunction.html@@@MakeMetaData(data,names,Ktypes,labels)@@@Sets a new temporary table (worksheet) and returns a base op...";
fil["7"]= "MakeTable.html@@@MakeTable(data,labels)@@@Takes a data.frame and returns a tabu object...";
fil["8"]= "RTabulationFunctions.html@@@R tabulation functions@@@The following section contains the 1010data-supplied functions that can be placed within a tabulation (&lt;tabu&gt;) in the Macro Language code...";
fil["9"]= "ReBaseFunction.html@@@Rebase(data,colsobj)@@@Set a new temporary table (worksheet) and returns a base op...";
fil["10"]= "RinTRS.html@@@Using the R language in TRS@@@The Trillion-Row Spreadsheet (TRS) timeline makes it easy to insert R code into your analysis...";
fil["11"]= "RowsFunction.html@@@Rows(ops,table)@@@Return the number of records after a query with base table table and query operations ops...";
fil["12"]= "SliceMetaDataFunction.html@@@SliceMetaData(colsobj,columnindices)@@@Get a functional subset of a column&apos;s metadata object...";
fil["13"]= "TabFunctionsExamples.html@@@Tabulation function examples@@@The following examples show how to implement various R applications within Macro Language code with the tabulation functions DistApply() and DistReduce()...";
fil["14"]= "codeLanguageR.html@@@Using &lt;code language_=&quot;r&quot;&gt; in Macro Language@@@You can use the &lt;code&gt; tag to add R code to your Macro Language queries...";
fil["15"]= "examplesLanguageR.html@@@Examples for &lt;code language_=&quot;r&quot;&gt;@@@Usage examples to leverage the new language_=&quot;r&quot; functionality in the\n      &lt;code&gt; tag...";
fil["16"]= "suppliedRFunctions.html@@@Macro Language R functions@@@The following section contains 1010data-supplied functions for interfacing with the Insights Platform...";
var doStem = false;searchLoaded = true;// Porter stemmer in Javascript. Few comments, but it's easy to follow against the rules in the original
// paper, in
//
//  Porter, 1980, An algorithm for suffix stripping, Program, Vol. 14,
//  no. 3, pp 130-137,
//
// see also http://www.tartarus.org/~martin/PorterStemmer

// Release 1 be 'andargor', Jul 2004
// Release 2 (substantially revised) by Christopher McKenzie, Aug 2009


var stemmer = (function(){
	var step2list = {
			"ational" : "ate",
			"tional" : "tion",
			"enci" : "ence",
			"anci" : "ance",
			"izer" : "ize",
			"bli" : "ble",
			"alli" : "al",
			"entli" : "ent",
			"eli" : "e",
			"ousli" : "ous",
			"ization" : "ize",
			"ation" : "ate",
			"ator" : "ate",
			"alism" : "al",
			"iveness" : "ive",
			"fulness" : "ful",
			"ousness" : "ous",
			"aliti" : "al",
			"iviti" : "ive",
			"biliti" : "ble",
			"logi" : "log"
		},

		step3list = {
			"icate" : "ic",
			"ative" : "",
			"alize" : "al",
			"iciti" : "ic",
			"ical" : "ic",
			"ful" : "",
			"ness" : ""
		},

		c = "[^aeiou]",          // consonant
		v = "[aeiouy]",          // vowel
		C = c + "[^aeiouy]*",    // consonant sequence
		V = v + "[aeiou]*",      // vowel sequence

		mgr0 = "^(" + C + ")?" + V + C,               // [C]VC... is m>0
		meq1 = "^(" + C + ")?" + V + C + "(" + V + ")?$",  // [C]VC[V] is m=1
		mgr1 = "^(" + C + ")?" + V + C + V + C,       // [C]VCVC... is m>1
		s_v = "^(" + C + ")?" + v;                   // vowel in stem

	return function (w) {
		var 	stem,
			suffix,
			firstch,
			re,
			re2,
			re3,
			re4,
			origword = w;

		if (w.length < 3) { return w; }

		firstch = w.substr(0,1);
		if (firstch == "y") {
			w = firstch.toUpperCase() + w.substr(1);
		}

		// Step 1a
		re = /^(.+?)(ss|i)es$/;
		re2 = /^(.+?)([^s])s$/;

		if (re.test(w)) { w = w.replace(re,"$1$2"); }
		else if (re2.test(w)) {	w = w.replace(re2,"$1$2"); }

		// Step 1b
		re = /^(.+?)eed$/;
		re2 = /^(.+?)(ed|ing)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			re = new RegExp(mgr0);
			if (re.test(fp[1])) {
				re = /.$/;
				w = w.replace(re,"");
			}
		} else if (re2.test(w)) {
			var fp = re2.exec(w);
			stem = fp[1];
			re2 = new RegExp(s_v);
			if (re2.test(stem)) {
				w = stem;
				re2 = /(at|bl|iz)$/;
				re3 = new RegExp("([^aeiouylsz])\\1$");
				re4 = new RegExp("^" + C + v + "[^aeiouwxy]$");
				if (re2.test(w)) {	w = w + "e"; }
				else if (re3.test(w)) { re = /.$/; w = w.replace(re,""); }
				else if (re4.test(w)) { w = w + "e"; }
			}
		}

		// Step 1c
		re = /^(.+?)y$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(s_v);
			if (re.test(stem)) { w = stem + "i"; }
		}

		// Step 2
		re = /^(.+?)(ational|tional|enci|anci|izer|bli|alli|entli|eli|ousli|ization|ation|ator|alism|iveness|fulness|ousness|aliti|iviti|biliti|logi)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			suffix = fp[2];
			re = new RegExp(mgr0);
			if (re.test(stem)) {
				w = stem + step2list[suffix];
			}
		}

		// Step 3
		re = /^(.+?)(icate|ative|alize|iciti|ical|ful|ness)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			suffix = fp[2];
			re = new RegExp(mgr0);
			if (re.test(stem)) {
				w = stem + step3list[suffix];
			}
		}

		// Step 4
		re = /^(.+?)(al|ance|ence|er|ic|able|ible|ant|ement|ment|ent|ou|ism|ate|iti|ous|ive|ize)$/;
		re2 = /^(.+?)(s|t)(ion)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(mgr1);
			if (re.test(stem)) {
				w = stem;
			}
		} else if (re2.test(w)) {
			var fp = re2.exec(w);
			stem = fp[1] + fp[2];
			re2 = new RegExp(mgr1);
			if (re2.test(stem)) {
				w = stem;
			}
		}

		// Step 5
		re = /^(.+?)e$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(mgr1);
			re2 = new RegExp(meq1);
			re3 = new RegExp("^" + C + v + "[^aeiouwxy]$");
			if (re.test(stem) || (re2.test(stem) && !(re3.test(stem)))) {
				w = stem;
			}
		}

		re = /ll$/;
		re2 = new RegExp(mgr1);
		if (re.test(w) && re2.test(w)) {
			re = /.$/;
			w = w.replace(re,"");
		}

		// and turn initial Y back to y

		if (firstch == "y") {
			w = firstch.toLowerCase() + w.substr(1);
		}

		return w;
	}
})();// Auto generated list of analyzer stop words that must be ignored by search.
stopWords = new Array();
stopWords[0]= "but";
stopWords[1]= "be";
stopWords[2]= "with";
stopWords[3]= "such";
stopWords[4]= "then";
stopWords[5]= "for";
stopWords[6]= "no";
stopWords[7]= "will";
stopWords[8]= "not";
stopWords[9]= "are";
stopWords[10]= "and";
stopWords[11]= "their";
stopWords[12]= "if";
stopWords[13]= "this";
stopWords[14]= "on";
stopWords[15]= "into";
stopWords[16]= "a";
stopWords[17]= "or";
stopWords[18]= "there";
stopWords[19]= "in";
stopWords[20]= "that";
stopWords[21]= "they";
stopWords[22]= "was";
stopWords[23]= "is";
stopWords[24]= "it";
stopWords[25]= "an";
stopWords[26]= "the";
stopWords[27]= "as";
stopWords[28]= "at";
stopWords[29]= "these";
stopWords[30]= "by";
stopWords[31]= "to";
stopWords[32]= "of";

var indexerLanguage="en";

// Auto generated index for searching.
w["#"]="4*4*40$65$75$7f$85,9*1*2k$30,13*5*1t$25$2u$3q$44$4p";
w["#create"]="15*0*3j";
w["#example"]="6*1*2q$3o,9*1*1e$2g";
w["#get"]="0*0*1s,15*2*4m$50$6a";
w["#rbind"]="15*0*bs";
w["#re-base"]="15*0*6j";
w["#run"]="15*1*8o$aa";
w["#set"]="0*0*24";
w["#subset"]="15*0*5s";
w["#the"]="15*1*3s$44";
w["#to"]="4*0*5m";
w["%in%"]="15*0*5j";
w["'elev'."]="15*1*2s$3q";
w["'ops'"]="15*0*49";
w["'pet"]="6*0*48";
w["'primes'"]="3*0*13,4*0*1v";
w["'species'#"]="13*0*1h";
w["'state'"]="15*1*2q$3o";
w["'table'"]="15*0*47";
w["'tabu'"]="13*1*4b$4s";
w["-"]="0*1*21$2a,1*0*1s,2*0*2h,4*6*48$52$5d$62$6j$6v$7r,6*3*30$39$3i$3u,9*3*1k$1u$2c$36,11*0*11,13*6*1g$1q$2h$2r$3c$3l$4f,15*18*4i$4s$57$5f$5n$63$6f$7o$7u$8a$8i$95$9h$ak$b0$bn$c3$cb$cg";
w["- 'species'"]="4*0*3u";
w["."]="0*0*15,1*0*b,2*0*1t,3*0*1i,4*1*1n$2e,8*0*18,9*1*q$10,10*0*3p,11*0*h,13*0*m,14*2*2a$2t$41,15*0*33,16*0*u";
w["/code"]="4*0*8m,13*0*57,14*0*5l,15*1*6t$cl";
w["/defop"]="4*0*8v,13*0*5f";
w["/tabu"]="4*0*8r,13*0*5b";
w["0"]="0*0*1m,15*0*d0";
w["05"]="15*0*d1";
w["1"]="0*0*1c,6*4*1v$2t$3b$3c$3d,9*1*1h$27";
w["10"]="15*0*67";
w["1000000"]="15*0*83";
w["1010"]="8*0*q,16*0*l";
w["1010data"]="5*12*-1$b$1d$2l,8*10*-1$a,14*4*13$1m$55,15*3*12$1h,16*10*-1$c";
w["1010data's"]="5*0*2k";
w["1010data-supplied"]="8*10*-1$9,16*10*-1$b";
w["1:"]="6*0*2s,9*0*1g";
w["2"]="0*0*1f,6*2*22$3f$3r,9*1*28$2j";
w["20986"]="2*0*2n";
w["2:"]="6*0*3q,9*0*2i";
w["34"]="2*0*2m";
w["4"]="0*0*1i,2*0*19,6*1*26$3e,9*0*29,15*0*73";
w["4."]="6*0*25";
w["8"]="2*0*1a";
w["<"]="0*1*20$29,1*0*1r,2*0*2g,4*16*36$3f$3j$3q$3t$47$51$5c$61$6i$6u$7q$8l$8q$8u$92$9b,5*1*1s$28,6*3*2v$38$3h$3t,8*2*h$v$15,9*3*1j$1t$2b$35,11*0*10,13*15*t$14$17$1c$1f$1p$2g$2q$3b$3k$4e$56$5a$5e$5i$5o,14*40*1$9$j$v$1j$20$2j$2v$44$5a$5f$5k,15*63*1$7$j$p$1e$1p$34$3c$3h$4h$4r$56$5e$5m$62$6e$6s$77$7g$7l$7n$7t$89$8h$94$9g$a5$aj$av$bm$c2$ca$cf$ck$co,16*0*p";
w["<code>"]="14*9*-1,15*9*-1";
w[">"]="0*3*1d$1g$1j$1n,4*8*3d$3h$3n$8j$8o$8t$91$9a$9h,5*1*20$2c,6*2*1u$21$24,8*2*j$13$17,13*8*13$16$1b$55$59$5d$5h$5n$5t,14*40*5$d$l$11$1l$22$2l$31$48$5e$5j$5n,15*45*5$b$l$r$1g$1r$3b$3g$6r$6v$7f$7k$82$cj$cn$cv$d2,16*0*t";
w["about"]="1*0*1k";
w["access"]="5*0*2j,8*0*1b";
w["account"]="5*0*2g,10*0*1u,14*0*4n,15*0*2a";
w["accum"]="4*12*-1$m$1i$2n";
w["accum."]="4*11*-1$l$2m";
w["add"]="10*1*1a$1f,14*10*-1$n";
w["adding"]="10*0*u";
w["after"]="10*0*3c,11*10*-1$9";
w["aggregation"]="13*9*s";
w["ai"]="5*0*t";
w["all"]="2*0*2a,8*0*1f,16*1*v$1d";
w["allowing"]="14*0*1b,15*0*11";
w["allows"]="14*0*3k";
w["alpha"]="9*0*22";
w["already"]="4*0*5r";
w["also"]="5*0*v";
w["amount"]="4*12*-1$h$1e$2i";
w["analysis"]="10*11*-1$l$15,14*0*1d";
w["analysis."]="10*11*-1$k$14";
w["anht"]="15*0*8n";
w["appears"]="10*1*3s$4e";
w["appears:"]="10*0*4d";
w["applications"]="13*10*-1$d";
w["applied"]="3*0*1b,4*0*27";
w["apply"]="8*0*1c";
w["args"]="3*42*-1$3$7$c$r$1f,4*35*3$8$r$2b$43$60$6p,13*2*1l$2p$3i";
w["argument"]="3*11*-1$b$1g,4*1*2c$7e,13*0*43";
w["argument."]="4*0*7d,13*0*42";
w["arguments"]="0*9*v,1*9*g,2*9*q,3*9*s,4*9*1o,6*9*p,7*9*i,9*9*j,11*9*m,12*9*i";
w["array"]="5*0*o";
w["assign"]="15*0*6m";
w["atomic"]="6*0*1p";
w["attempt"]="10*0*2c";
w["attribute"]="14*0*18";
w["attributes"]="1*0*11";
w["attributes."]="1*0*10";
w["automatically"]="14*0*25,15*0*4b";
w["available"]="5*0*1h";
w["b"]="9*0*1q";
w["base"]="4*0*93,6*10*-1$g,9*10*-1$c,11*10*-1$b,13*0*5j,15*2*35$6l$78";
w["been"]="4*0*5s";
w["before"]="4*0*8c,13*0*50";
w["being"]="4*12*-1$j$1g$2k";
w["beta"]="9*0*23";
w["better"]="4*0*1k";
w["c"]="0*0*2b,2*0*2o,4*0*5j,6*2*31$3a$43,9*2*1p$21$26,15*1*58$8j";
w["call"]="9*0*14,15*1*97$am";
w["called"]="4*1*7n$88,13*1*4a$4r";
w["called 'tabu'"]="4*0*7m";
w["called 'tabu' must"]="4*0*87";
w["can"]="8*11*-1$d$t,14*10*-1$h,16*0*n";
w["cdata"]="4*0*3r,10*0*1g,13*0*1d,14*1*4h$5g,15*2*24$3i$7m,16*0*18";
w["character"]="1*0*p,2*1*15$1n,6*1*1d$2i,7*0*16";
w["character."]="2*0*14";
w["click"]="10*3*2o$32$3n$4i";
w["cloud"]="5*0*2n,16*0*1k";
w["cmd"]="9*1*1s$2f,15*2*4q$5i$5p";
w["cmd2"]="15*1*5l$6i";
w["code"]="4*2*3k$8e$8n,5*1*1t$29,8*12*-1$n$10$1p,10*22*-1$i$m$12$18$1d$1n$1o$2m$2v$3o$3q$4c$4h,13*13*-1$h$18$52$58,14*59*-1$-1$2$a$k$p$10$1h$1k$21$2k$30$45$5b$5m,15*48*-1$2$8$k$q$1c$1f$1q$3d$6u$7h$cm,16*2*q$11$1f";
w["code."]="8*10*-1$m,14*0*1g,15*0*1b";
w["code:"]="10*0*2l";
w["col"]="4*3*3s$55$64$6m,13*3*1e$2k$2t$3f";
w["colktypes"]="0*35*2$8$r";
w["collabels"]="0*35*4$a$t";
w["colnames"]="0*37*0$6$p$22$27,6*1*2u$3l,15*0*5h";
w["cols"]="2*34*4$9$p$1m$28$2d,9*0*1o,15*7*55$5c$5k$5q$69$8g$9c$ar";
w["cols."]="2*0*2c";
w["cols.ind"]="15*0*5b";
w["colsmetadata"]="0*0*14,1*36*0$3$d$1t,9*0*v,12*0*n,15*0*4t";
w["colsobj"]="0*1*23$28,1*0*1q,6*1*3g$3s,9*33*2$5$i$p$r,12*32*1$4$g$j";
w["column"]="0*0*1q,2*0*1p,3*0*u,4*1*1q$4g,6*1*1b$1t,12*10*-1$a,13*0*21";
w["column's"]="12*10*-1$9";
w["column."]="0*0*1p";
w["columnindices"]="12*32*2$5$h$o";
w["columns"]="15*4*2p$32$3n$43$74";
w["columns."]="15*0*42";
w["compatible"]="9*0*o";
w["completely"]="14*0*3t";
w["complex"]="1*0*u";
w["computing"]="5*0*l";
w["computing."]="5*0*k";
w["contain"]="14*0*1e";
w["contained"]="2*0*1r";
w["contains"]="1*0*1i,5*11*-1$3$m,8*10*-1$8,16*10*-1$a";
w["counts"]="4*1*80$84,13*11*r$4k$4o";
w["create"]="9*0*2s,15*0*3k";
w["creates"]="15*0*2m";
w["creating"]="9*0*19";
w["current"]="14*1*2d$3g";
w["currently"]="10*0*3d";
w["d"]="15*2*88$9a$ap";
w["dat"]="4*1*50$59,9*1*1i$2e,13*1*2f$2o,15*1*61$6h";
w["data"]="2*66*-1$-1$-1$0$5$b$g$i$l$2i,3*0*15,4*16*-1$i$1f$21$2j$4f$79$7t,5*0*q,6*37*1$6$l$q$s$17$1l$2c$40,7*44*-1$1$4$8$g$j$l$10,9*35*1$4$h$k$m$1l$2o,13*2*20$3u$4h,15*8*54$60$64$6c$87$8b$8v$9l$b6";
w["data.frame"]="4*0*78,7*10*-1$7,9*1*l$2n,13*0*3t";
w["data.frame."]="2*10*-1$h,6*0*r,7*0*k";
w["data.frames"]="15*0*8u";
w["data for the"]="4*0*4e";
w["default"]="2*1*1i$1u,6*3*u$1g$27$2l,7*1*n$19,14*0*2r";
w["default.lonely"]="14*0*2q";
w["defop"]="4*1*37$90,13*1*u$5g";
w["demo"]="15*0*38";
w["description"]="4*9*t,8*9*o,15*9*o,16*9*j";
w["designed"]="5*0*h";
w["determine"]="9*0*31";
w["developing"]="5*10*-1$8";
w["df"]="2*0*2f";
w["directly"]="1*0*1f";
w["directly."]="1*0*1e";
w["display"]="6*0*2g,7*0*14";
w["distapply"]="3*35*0$4$o,4*3*10$1m$45$4u,8*0*1l,13*14*-1$k$1n$2e$2v$3d";
w["distapply."]="4*0*4t,13*0*2d";
w["distreduce"]="4*39*0$5$o$u$1c$66$6k,8*0*1m,13*10*-1$l";
w["do"]="15*1*96$al";
w["down"]="10*0*46";
w["drop"]="10*0*45";
w["drop-down"]="10*0*44";
w["dummy"]="4*1*39$9d,13*1*10$5q";
w["dummy_reduce_ucnt_tabu/"]="4*0*9c";
w["dummy_ucnt_tabu/"]="13*0*5p";
w["e"]="4*0*4i";
w["each"]="3*12*-1$d$10$1c,4*2*1s$28$6b,9*0*13,13*0*34";
w["easier"]="1*0*13";
w["easy"]="5*0*12,10*10*-1$f";
w["edit"]="10*0*4f";
w["effect"]="9*0*18,14*0*2h";
w["either"]="2*0*26";
w["element"]="4*0*6d,13*0*36";


// Auto generated index for searching.
w["elev"]="15*3*2t$3r$5a$8m";
w["enabled"]="10*0*21,14*0*4q,15*0*2d,16*0*1o";
w["entering"]="14*0*1s";
w["entirely"]="14*0*3s";
w["environments"]="5*0*2o,16*0*1n";
w["equal"]="4*0*30";
w["error"]="10*0*28";
w["examine"]="14*0*33";
w["example"]="0*9*1r,1*9*1p,2*9*2e,4*9*35,6*11*2p$2r$3p,9*12*1d$1f$2h$2l,11*9*u,13*9*o,15*1*2l$71";
w["example:"]="13*9*n";
w["examples"]="5*0*27,13*40*-1$2$5$7,15*49*-1$0$6$d$2j";
w["exist"]="4*0*8b,13*0*4v,15*0*4c";
w["exiting"]="14*0*2u";
w["exits"]="4*0*8g,13*0*54";
w["exits."]="4*0*8f,13*0*53";
w["extension"]="5*0*1k";
w["extension."]="5*0*1j";
w["extract"]="14*0*39";
w["f"]="13*1*1o$3h";
w["float"]="0*0*1h,6*0*20";
w["folder"]="4*0*98,15*0*7d";
w["follow"]="10*0*31";
w["follow."]="10*0*30";
w["following"]="5*0*1l,8*10*-1$6,10*0*49,13*10*-1$6,15*1*2k$70,16*11*-1$8$k";
w["follows"]="14*0*59";
w["follows:"]="14*0*58";
w["frame"]="2*10*-1$j,4*1*7a$7u,6*0*t,7*11*-1$9$m,9*1*n$2p,13*1*3v$4i,15*1*9m$b7";
w["frames"]="15*0*90";
w["from"]="0*10*-1$k,10*0*42,14*0*1v,15*0*17";
w["fun"]="3*43*-1$2$6$a$q$19$1h,4*0*2d,13*0*3g";
w["function"]="3*11*-1$9$1a,4*3*26$2g$49$5e,8*0*1e,13*31*1$4$1r,15*2*7p$9d$as";
w["functional"]="12*10*-1$7";
w["functionality"]="5*0*p,15*10*-1$i,16*0*1g";
w["functions"]="1*0*18,5*0*26,8*43*-1$2$5$c$r$19$1k,13*10*-1$j,14*1*4d$5i,15*0*20,16*41*-1$3$7$e$m";
w["g"]="9*0*1r";
w["gamma"]="9*0*24";
w["general"]="1*0*12";
w["get"]="0*11*-1$d$1t,1*0*16,2*10*-1$a,4*2*4c$4o$53,10*0*27,12*10*-1$6,13*2*1u$28$2i,14*0*26,15*2*4n$51$6b";
w["get/set"]="0*10*-1$c,1*0*15";
w["guide"]="5*10*-1$2";
w["has"]="5*0*10,9*0*16";
w["have"]="4*0*5q,5*0*2i,8*0*1a";
w["helpful"]="5*0*1q";
w["helpful:"]="5*0*1p";
w["how"]="13*10*-1$9";
w["however"]="6*0*12,7*0*r";
w["icon"]="10*1*36$4m";
w["icon."]="10*1*35$4l";
w["id"]="0*0*2d";
w["implement"]="13*10*-1$a";
w["ind"]="15*1*5d$5r";
w["indices"]="12*0*p";
w["information"]="1*0*1j,3*0*v,4*0*1r,5*10*-1$4,14*0*1r,15*0*1m";
w["information."]="14*0*1q,15*0*1l";
w["insert"]="10*12*-1$g$2j$33";
w["inserts"]="10*0*38";
w["inside"]="14*0*4g,15*0*23,16*0*17";
w["insights"]="5*11*-1$c$1e,10*0*q,16*10*-1$g";
w["int"]="0*0*1e";
w["integer"]="0*0*19,6*0*1s";
w["integers"]="2*0*1c";
w["interface"]="15*0*13";
w["interfacing"]="16*10*-1$f";
w["introduction"]="5*30*0$1";
w["iris"]="4*0*99,6*0*41,9*1*2m$38,13*0*5m";
w["it."]="14*0*3a";
w["item"]="2*0*2r,6*0*34";
w["itemname"]="6*0*35";
w["ks"]="15*6*ac$ah$b1$bj$bq$bv$c7";
w["ks.results"]="15*0*ag";
w["kt"]="6*1*37$3n";
w["ktype"]="6*0*1r";
w["ktype:"]="6*0*1q";
w["ktypes"]="0*11*-1$i$18,6*34*3$8$n$14$1o$3m,7*0*t,9*1*25$33";
w["ktypes."]="9*0*32";
w["label"]="1*0*1m";
w["labels"]="0*10*-1$j,4*1*7c$82,6*34*4$9$o$2f$2h$42,7*33*2$5$h$13$15,9*0*2v,13*1*41$4m";
w["language"]="4*0*3l,5*17*-1$a$g$1i$1u$22$24$2a$2t,8*11*-1$l$11,10*34*2$6$11$1c$25$43,13*11*-1$g$19,14*77*-1$3$7$b$f$s$15$19$46$4v$53$57$5c,15*44*-1$3$9$g$v$2i$3e$7i,16*33*1$5$r$14$1t";
w["language."]="5*0*2s,14*0*4u,15*0*2h,16*0*1s";
w["lapply"]="15*1*99$ao";
w["large"]="4*0*16";
w["lat"]="15*0*8k";
w["latest"]="5*0*2m";
w["learning"]="5*0*17";
w["len"]="6*1*45$4a";
w["length"]="4*1*2v$70,13*0*3m";
w["leverage"]="15*10*-1$e";
w["libraries"]="5*0*19";
w["libraries."]="5*0*18";
w["linux"]="16*0*1m";
w["list"]="0*0*11,1*0*i,2*0*s,3*11*-1$j$1l,4*4*2q$2u$63$68$6c,9*0*s,10*0*48,11*0*o,12*0*k,13*2*2s$31$35";
w["list."]="10*0*47";
w["lively"]="5*0*11";
w["lon"]="15*0*8l";
w["lonely"]="14*0*2s";
w["lower"]="4*0*5n";
w["lowercased"]="4*0*5u";
w["lowercased."]="4*0*5t";
w["machine"]="5*0*16";
w["macro"]="5*1*21$23,8*10*-1$k,13*10*-1$f,14*42*-1$6$e$r$14$56,15*0*1a,16*31*0$4$13";
w["make"]="10*0*22,14*0*4r,15*0*2e,16*0*1p";
w["makemetadata"]="6*37*0$5$k$3j$3v,9*0*1v";
w["makes"]="10*10*-1$e";
w["maketable"]="4*2*76$7l$7s,7*35*0$3$f,13*2*3r$49$4g";
w["manual"]="14*3*1o,15*3*1j";
w["mapfun"]="4*33*2$7$q$25$6n";
w["may"]="5*0*1n,16*0*1h";
w["mdb"]="4*0*95,15*0*7a";
w["message"]="10*0*29";
w["meta"]="15*1*53$5v";
w["meta-data"]="15*1*52$5u";
w["metadata"]="0*10*-1$l,1*12*-1$7$s$1a,12*11*-1$b$s";
w["metadataobj"]="0*94*1$3$5$7$9$b$q$s$u$10";
w["method"]="15*3*9v$a1$bg$bi";
w["mf"]="4*1*46$6o";
w["model"]="0*0*1o";
w["modern"]="5*0*s";
w["modify"]="1*1*19$1d,14*0*3m";
w["modifying"]="14*0*3o";
w["more"]="14*0*1p,15*0*1k";
w["much"]="15*0*86";
w["must"]="4*1*4p$8a,5*0*2h,6*0*15,7*0*u,10*0*1v,13*1*29$4u,14*0*4o,15*0*2b,16*0*15";
w["n"]="11*0*v,15*2*4g$68$7s";
w["nam"]="4*0*4h";
w["name"]="1*0*1l,4*2*38$4m$4q,13*3*v$22$26$2a,15*2*a4$bl$cq";
w["named"]="14*0*37";
w["names"]="0*12*-1$h$1u$26,2*0*1q,6*35*2$7$m$13$1a$1c$3k,7*0*s,9*2*20$2q$2u,15*1*a7$bp";
w["names/ktypes/labels"]="0*10*-1$g";
w["names/labels"]="9*0*2t";
w["natural"]="5*0*1a";
w["necessary"]="10*0*1e,14*0*4a,15*0*1t";
w["need"]="5*10*-1$6";
w["nested"]="16*0*16";
w["new"]="6*10*-1$b,9*10*-1$7,10*1*39$3i,14*1*3b$3u,15*15*-1$f$2n$2u$3l$3t$6n";
w["nms"]="0*0*1v";
w["normal"]="15*0*cr";
w["normality"]="15*0*76";
w["normality:"]="15*0*75";
w["normally"]="14*0*2p";
w["note"]="0*0*17,1*0*r,2*0*23,4*0*7h,5*0*2e,9*0*12,10*0*1s,13*0*46,14*0*4l,15*0*28,16*0*1c";
w["note:"]="0*0*16,1*0*q,2*0*22,4*0*7g,5*0*2d,9*0*11,10*0*1r,13*0*45,14*0*4k,15*0*27,16*0*1b";
w["now"]="5*0*1g,10*0*s,14*1*16$3e,15*0*t";
w["null"]="2*2*1l$21$24,6*6*11$19$1i$1n$29$2e$2o,7*2*q$12$1c";
w["null."]="2*1*1k$20,6*3*18$1m$2d$2n,7*1*11$1b";
w["null;"]="6*0*10,7*0*p";
w["number"]="4*1*17$31,11*10*-1$7,15*0*4o";
w["numeric"]="2*0*1f";
w["object"]="0*10*-1$n,1*3*n$t$1b$1h,2*0*12,4*1*42$7k,7*10*-1$d,11*0*t,12*10*-1$d,13*1*1k$48";
w["object."]="0*10*-1$m,7*10*-1$c,12*10*-1$c";
w["objects"]="15*0*46";
w["only"]="4*11*-1$a$12,16*0*1i";
w["op"]="6*10*-1$i,9*10*-1$e";
w["op."]="6*10*-1$h,9*10*-1$d";
w["operation"]="10*8*n$19$2f$2p$34$3a$3f$3j$4k";
w["operation."]="10*0*2e";
w["operations"]="11*10*-1$f,14*1*2g$4f,15*0*22";
w["ops"]="1*54*-1$-1$1$4$9$a$e$h$j$1u,2*54*-1$-1$1$6$d$e$m$r$u$2j,9*2*1m$2a$34,11*44*-1$1$4$g$k$n$p$13,14*5*28$2b$38$3c$3p$40,15*11*4a$4k$4u$65$6d$6q$7q$80$8c$c9$ce$ch";
w["ops."]="1*9*-1,2*0*t,11*9*-1,15*0*6p";
w["optionally"]="4*0*7b,13*0*40";
w["order"]="5*0*2p";
w["other"]="16*0*1l";
w["output"]="0*0*13,9*0*u,12*0*m";
w["p"]="15*4*9q$9t$bb$be$ct";
w["package"]="15*0*16";
w["pairing"]="5*0*1b";
w["panel"]="10*3*1q$3b$3k$3r";
w["panel."]="10*0*1p";
w["particularly"]="5*0*1o";
w["passed"]="4*13*-1$k$1h$2l$44,13*0*1m";
w["path"]="1*1*l$1v,2*1*10$2k,11*1*r$14,13*0*5l";
w["per"]="4*0*19";
w["perform"]="4*0*1j";
w["pet"]="6*1*49$4b";


// Auto generated index for searching.
w["placed"]="8*10*-1$e";
w["platform"]="5*11*-1$e$1f,10*0*r,14*0*32,16*10*-1$i";
w["platform."]="5*10*-1$d,16*10*-1$h";
w["pnorm"]="15*0*b5";
w["popular"]="15*0*14";
w["price"]="0*0*2e,2*0*2s,6*0*36";
w["primes"]="3*0*14,4*0*20";
w["process"]="3*10*-1$h,4*0*1b";
w["processes"]="3*0*18,4*13*-1$e$15$24$34";
w["pub"]="15*0*37";
w["put"]="14*0*4b,15*0*1u";
w["queries"]="14*10*-1$u";
w["queries."]="14*10*-1$t";
w["query"]="11*20*-1$-1$a$e,14*3*2f$3j$3n$3r";
w["query."]="14*0*3i";
w["r"]="2*0*17,4*1*3m$8d,5*15*-1$9$f$1v$25$2b$2r,8*33*0$3$12$1d$1j,10*49*-1$1$5$h$10$1b$1m$24$2k$2u$41$4b$4g,13*12*-1$c$1a$51,14*51*-1$4$c$o$1a$1f$1t$23$34$47$4c$4t$52$5d$5h,15*55*-1$4$a$h$10$1v$2g$3f$45$4d$7j$9f$9o$9s$a0$au$b9$bd$bh,16*34*2$6$s$10$1e$1r";
w["rbind"]="15*5*8s$98$ae$an$bt$c4";
w["re"]="15*0*6k";
w["rebase"]="9*38*0$3$g$15$2d$37,15*1*6g$cc";
w["records"]="11*10*-1$8";
w["reduce"]="4*1*3a$9e";
w["reduced"]="4*0*2r";
w["reducefun"]="4*33*4$9$s$2f$6q";
w["reduces"]="4*0*2h";
w["reference"]="14*3*1n,15*3*1i";
w["regression"]="4*0*96,15*0*7b";
w["related"]="14*0*4e,15*0*21";
w["replace"]="14*0*3q";
w["repository"]="5*0*14";
w["represent"]="14*0*3f";
w["representation"]="0*0*1b";
w["representation:"]="0*0*1a";
w["represents"]="14*0*2c";
w["required"]="6*1*1k$2b";
w["res"]="15*1*c1$cd";
w["research"]="5*0*u";
w["result"]="4*0*6e,13*0*37";
w["results"]="2*0*29,3*11*-1$m$1o,4*3*2t$6a$6h$73,13*2*33$3a$3p,15*9*93$a3$a9$af$ai$bk$br$c0$c6$c8";
w["results."]="3*11*-1$l$1n,4*1*2s$69,13*0*32";
w["retrieves"]="4*0*4d,13*0*1v";
w["return"]="1*10*-1$6,2*10*-1$f,3*19*-1$i$1j,4*11*2o$56$7j,11*10*-1$6,13*1*2l$47";
w["returned"]="1*0*1g";
w["returns"]="4*0*67,6*10*-1$f,7*10*-1$a,9*10*-1$b,13*0*30";
w["rf"]="4*1*5b$6r";
w["rounded"]="2*0*1e";
w["row"]="10*10*-1$a,15*2*40$a6$bo";
w["rows"]="2*34*3$8$o$16$27$2b,11*36*0$3$j$12,15*3*31$4j$4p$7v";
w["run"]="3*10*-1$8,4*11*-1$b$11,8*0*1o,15*1*8p$ab";
w["s"]="15*0*41";
w["sample"]="10*0*4a";
w["scale"]="15*0*b3";
w["science"]="5*0*r";
w["section"]="8*11*-1$7$s,16*10*-1$9";
w["sections"]="5*0*1m";
w["see"]="14*0*1i,15*0*1d";
w["segment"]="3*23*-1$-1$e$k$12$1e$1m,4*2*1u$2a$6g,13*0*39";
w["segment."]="3*1*11$1d,4*2*1t$29$6f,13*0*38";
w["segments"]="4*0*18,8*0*1g";
w["select"]="10*0*40";
w["selected"]="10*0*3e";
w["sep"]="6*1*44$46";
w["session"]="14*2*1u$24$35,15*0*4f";
w["session."]="15*0*4e";
w["set"]="0*11*-1$e$25,1*1*v$17,2*1*1b$1d,9*10*-1$6,14*0*3v";
w["sets"]="6*10*-1$a";
w["shapiro"]="15*6*8q$92$9i$a2$a8$bu$c5";
w["shapiro.results"]="15*0*91";
w["should"]="4*0*5p";
w["show"]="13*10*-1$8";
w["shrink"]="4*11*-1$g$1d";
w["side"]="9*0*17";
w["similar"]="0*0*12,4*0*v,9*0*t,12*0*l";
w["simple"]="13*9*p";
w["simply"]="10*0*1j";
w["slice"]="2*0*18,12*0*r";
w["slicemetadata"]="12*35*0$3$f,15*0*5o";
w["snippet"]="10*0*v";
w["species"]="4*0*3v,13*0*1i";
w["specifically"]="5*0*i,10*0*20,14*0*4p,15*0*2c";
w["spreadsheet"]="10*10*-1$b";
w["start"]="5*10*-1$7";
w["stat"]="15*1*9n$b8";
w["state"]="14*1*2e$3h,15*2*2r$3p$59";
w["stations"]="15*1*3a$7e";
w["statistic"]="15*1*9p$ba";
w["statistical"]="5*1*j$15";
w["statistics"]="15*0*15";
w["stop"]="15*0*84";
w["store"]="0*0*2c,2*0*2q,6*0*33";
w["strengths"]="5*0*1c";
w["sub"]="3*11*-1$g$17,4*14*-1$d$14$1a$23$33";
w["sub-process"]="3*10*-1$f";
w["sub-processes"]="4*10*-1$c";
w["sub-processes."]="3*0*16,4*2*13$22$32";
w["submit"]="10*1*2d$4j";
w["subprocesses"]="8*0*1r";
w["subprocesses."]="8*0*1q";
w["subset"]="12*10*-1$8,15*2*30$3v$5t";
w["supplementary"]="8*0*p";
w["supplied"]="4*2*4k$4n$4r,6*0*16,7*0*v,8*10*-1$b,13*2*24$27$2b,16*10*-1$d";
w["supplied."]="4*0*4j,13*0*23";
w["supports"]="10*0*t,14*0*17,15*0*u";
w["symbol"]="0*0*1l";
w["syntax"]="0*9*o,1*9*c,2*9*k,3*9*n,4*9*n,6*9*j,7*9*e,9*9*f,10*0*1i,11*9*i,12*9*e,14*10*50$51";
w["syntax."]="10*0*1h";
w["table"]="1*44*-1$2$5$8$f$k$m$20,2*45*-1$2$7$c$n$v$11$1s$2l,4*0*94,6*10*-1$d,8*0*1i,9*12*-1$9$1c$1n,11*54*-1$-1$2$5$c$d$l$q$s$15,13*0*5k,14*1*29$2o,15*9*36$48$4l$4v$66$79$7r$81$8d$ci";
w["table."]="8*0*1h,9*0*1b";
w["tabu"]="4*6*3c$3g$7o$7p$89$8s$9g,7*10*-1$b,8*11*-1$i$16,13*6*12$15$4c$4d$4t$5c$5s";
w["tabulation"]="8*40*-1$1$4$g,13*40*-1$0$3$i";
w["tag"]="14*14*-1$m$12$2n$49$4j,15*13*-1$n$s$1s$26,16*0*1a";
w["tag."]="14*1*2m$4i,15*11*-1$m$25,16*0*19";
w["takes"]="4*0*77,7*10*-1$6,13*0*3s";
w["temporary"]="6*10*-1$c,9*11*-1$8$1a";
w["test"]="4*0*97,15*5*7c$8f$9b$9j$aq$b2";
w["test.cols"]="15*0*8e";
w["tests"]="15*2*72$8r$ad";
w["text"]="0*0*1k,6*0*23";
w["than"]="1*0*1c,4*0*1l";
w["the return object"]="4*0*7i";
w["timeline"]="10*16*-1$d$p$2i$2q$3h$3m$3v";
w["timeline."]="10*1*3g$3u";
w["together"]="15*0*8t";
w["tolower"]="4*0*58,13*0*2n";
w["too"]="15*0*85";
w["transid"]="2*0*2p,6*0*32";
w["trillion"]="10*10*-1$9";
w["trillion-row"]="10*10*-1$8";
w["trs"]="10*46*-1$3$7$c$o$2h$2n$37$3l$3t";
w["type"]="1*1*o$1o,2*0*13";
w["type."]="1*0*1n";
w["ucnt"]="4*1*3b$9f,13*1*11$5r";
w["unique"]="4*4*57$5h$71$7v$83,13*13*q$2m$3n$4j$4n";
w["unlist"]="4*1*5i$72,13*0*3o";
w["unneeded"]="4*0*5o";
w["unqcnts"]="4*1*6t$81,13*1*3j$4l";
w["up"]="14*0*2i";
w["usage"]="15*10*-1$c";
w["use"]="1*0*14,5*1*13$2q,10*2*17$23$2g,14*11*-1$i$4s,15*0*2f,16*0*1q";
w["used"]="8*1*u$1n,9*0*2r,16*1*o$1j";
w["user"]="14*0*3l";
w["using"]="3*32*1$5$p$t,4*34*1$6$p$1p$4s$6l,5*0*1r,10*30*0$4,13*1*2c$3e,14*31*0$8$43,15*0*1o";
w["val"]="15*2*9r$bc$cu";
w["value"]="2*1*1j$1v,3*9*1k,4*10*2p$86,6*5*v$1h$1j$28$2a$2m,7*1*o$1a,13*0*4q,15*3*6o$9u$bf$cs";
w["values"]="2*1*1h$25";
w["values."]="2*0*1g";
w["variable"]="14*1*36$3d";
w["variables"]="14*0*27";
w["various"]="13*10*-1$b";
w["vector"]="0*10*-1$f,2*0*1o,6*1*1f$2k,7*0*18";
w["vector."]="6*1*1e$2j,7*0*17";
w["want"]="10*0*2t";
w["way"]="4*10*-1$f";
w["weather"]="15*0*39";
w["when"]="10*0*2a,14*0*42,15*0*1n";
w["where"]="10*0*2r";
w["which"]="12*0*q,15*0*5g";
w["wid"]="6*1*47$4c";
w["wide"]="5*0*n";
w["willbe"]="15*0*cp";
w["within"]="8*11*-1$f$14,13*10*-1$e,14*0*54,15*0*18,16*0*12";
w["worksheet"]="6*10*-1$e,9*10*-1$a,15*3*2o$2v$3m$3u";
w["write"]="10*0*1k";
w["x"]="4*4*41$4a$54$5f$5k,13*2*1j$1s$2j,15*3*9e$9k$at$b4";
w["y"]="4*1*5g$5l";
w["you"]="5*10*-1$5,10*3*16$26$2b$2s,14*10*-1$g";
w["your"]="5*0*2f,10*13*-1$j$13$1l$1t,14*12*-1$q$1c$4m,15*1*19$29";
w[" "]="4*3*5a$5v$6s$74";
w["  "]="4*3*3e$3p$8i$8p";
w["  #"]="4*1*4b$4l";
w["  dat"]="4*0*4v";
w["   "]="4*1*3o$8h";
w["    "]="4*1*3i$8k";

return {'fil': fil, 'w': w}})();
