window['yoyanalysis'] = (function() {
var w = new Object();
fil = new Array();
fil["0"]= "QuarterSalesGrowth.html@@@Quarter with Highest Sales and Highest Growth@@@The next step in our YOY analysis of sales data is to determine which quarter had the highest sales and greatest percent growth. We&apos;ll start with the tabulation we have already produced, which gives...";
fil["1"]= "YOY.html@@@Retail Year-Over-Year (YOY) Analysis@@@Retailers frequently measure their performance by comparing current year numbers to prior year numbers. This type of analysis is referred to as a year-over-year (YOY) analysis and can be done many...";
var doStem = false;searchLoaded = true;// Porter stemmer in Javascript. Few comments, but it's easy to follow against the rules in the original
// paper, in
//
//  Porter, 1980, An algorithm for suffix stripping, Program, Vol. 14,
//  no. 3, pp 130-137,
//
// see also http://www.tartarus.org/~martin/PorterStemmer

// Release 1 be 'andargor', Jul 2004
// Release 2 (substantially revised) by Christopher McKenzie, Aug 2009


var stemmer = (function(){
	var step2list = {
			"ational" : "ate",
			"tional" : "tion",
			"enci" : "ence",
			"anci" : "ance",
			"izer" : "ize",
			"bli" : "ble",
			"alli" : "al",
			"entli" : "ent",
			"eli" : "e",
			"ousli" : "ous",
			"ization" : "ize",
			"ation" : "ate",
			"ator" : "ate",
			"alism" : "al",
			"iveness" : "ive",
			"fulness" : "ful",
			"ousness" : "ous",
			"aliti" : "al",
			"iviti" : "ive",
			"biliti" : "ble",
			"logi" : "log"
		},

		step3list = {
			"icate" : "ic",
			"ative" : "",
			"alize" : "al",
			"iciti" : "ic",
			"ical" : "ic",
			"ful" : "",
			"ness" : ""
		},

		c = "[^aeiou]",          // consonant
		v = "[aeiouy]",          // vowel
		C = c + "[^aeiouy]*",    // consonant sequence
		V = v + "[aeiou]*",      // vowel sequence

		mgr0 = "^(" + C + ")?" + V + C,               // [C]VC... is m>0
		meq1 = "^(" + C + ")?" + V + C + "(" + V + ")?$",  // [C]VC[V] is m=1
		mgr1 = "^(" + C + ")?" + V + C + V + C,       // [C]VCVC... is m>1
		s_v = "^(" + C + ")?" + v;                   // vowel in stem

	return function (w) {
		var 	stem,
			suffix,
			firstch,
			re,
			re2,
			re3,
			re4,
			origword = w;

		if (w.length < 3) { return w; }

		firstch = w.substr(0,1);
		if (firstch == "y") {
			w = firstch.toUpperCase() + w.substr(1);
		}

		// Step 1a
		re = /^(.+?)(ss|i)es$/;
		re2 = /^(.+?)([^s])s$/;

		if (re.test(w)) { w = w.replace(re,"$1$2"); }
		else if (re2.test(w)) {	w = w.replace(re2,"$1$2"); }

		// Step 1b
		re = /^(.+?)eed$/;
		re2 = /^(.+?)(ed|ing)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			re = new RegExp(mgr0);
			if (re.test(fp[1])) {
				re = /.$/;
				w = w.replace(re,"");
			}
		} else if (re2.test(w)) {
			var fp = re2.exec(w);
			stem = fp[1];
			re2 = new RegExp(s_v);
			if (re2.test(stem)) {
				w = stem;
				re2 = /(at|bl|iz)$/;
				re3 = new RegExp("([^aeiouylsz])\\1$");
				re4 = new RegExp("^" + C + v + "[^aeiouwxy]$");
				if (re2.test(w)) {	w = w + "e"; }
				else if (re3.test(w)) { re = /.$/; w = w.replace(re,""); }
				else if (re4.test(w)) { w = w + "e"; }
			}
		}

		// Step 1c
		re = /^(.+?)y$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(s_v);
			if (re.test(stem)) { w = stem + "i"; }
		}

		// Step 2
		re = /^(.+?)(ational|tional|enci|anci|izer|bli|alli|entli|eli|ousli|ization|ation|ator|alism|iveness|fulness|ousness|aliti|iviti|biliti|logi)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			suffix = fp[2];
			re = new RegExp(mgr0);
			if (re.test(stem)) {
				w = stem + step2list[suffix];
			}
		}

		// Step 3
		re = /^(.+?)(icate|ative|alize|iciti|ical|ful|ness)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			suffix = fp[2];
			re = new RegExp(mgr0);
			if (re.test(stem)) {
				w = stem + step3list[suffix];
			}
		}

		// Step 4
		re = /^(.+?)(al|ance|ence|er|ic|able|ible|ant|ement|ment|ent|ou|ism|ate|iti|ous|ive|ize)$/;
		re2 = /^(.+?)(s|t)(ion)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(mgr1);
			if (re.test(stem)) {
				w = stem;
			}
		} else if (re2.test(w)) {
			var fp = re2.exec(w);
			stem = fp[1] + fp[2];
			re2 = new RegExp(mgr1);
			if (re2.test(stem)) {
				w = stem;
			}
		}

		// Step 5
		re = /^(.+?)e$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(mgr1);
			re2 = new RegExp(meq1);
			re3 = new RegExp("^" + C + v + "[^aeiouwxy]$");
			if (re.test(stem) || (re2.test(stem) && !(re3.test(stem)))) {
				w = stem;
			}
		}

		re = /ll$/;
		re2 = new RegExp(mgr1);
		if (re.test(w) && re2.test(w)) {
			re = /.$/;
			w = w.replace(re,"");
		}

		// and turn initial Y back to y

		if (firstch == "y") {
			w = firstch.toLowerCase() + w.substr(1);
		}

		return w;
	}
})();// Auto generated list of analyzer stop words that must be ignored by search.
stopWords = new Array();
stopWords[0]= "but";
stopWords[1]= "be";
stopWords[2]= "with";
stopWords[3]= "such";
stopWords[4]= "then";
stopWords[5]= "for";
stopWords[6]= "no";
stopWords[7]= "will";
stopWords[8]= "not";
stopWords[9]= "are";
stopWords[10]= "and";
stopWords[11]= "their";
stopWords[12]= "if";
stopWords[13]= "this";
stopWords[14]= "on";
stopWords[15]= "into";
stopWords[16]= "a";
stopWords[17]= "or";
stopWords[18]= "there";
stopWords[19]= "in";
stopWords[20]= "that";
stopWords[21]= "they";
stopWords[22]= "was";
stopWords[23]= "is";
stopWords[24]= "it";
stopWords[25]= "an";
stopWords[26]= "the";
stopWords[27]= "as";
stopWords[28]= "at";
stopWords[29]= "these";
stopWords[30]= "by";
stopWords[31]= "to";
stopWords[32]= "of";

var indexerLanguage="en";

// Auto generated index for searching.
w["+"]="0*0*cn";
w["-1"]="1*0*vh";
w["."]="0*2*bc$i7$1c8,1*1*pb$11r";
w["..."]="0*9*-1,1*9*-1";
w["/note"]="0*4*25$79$dt$mh$tm,1*4*ge$jt$q5$13q$1cq";
w["/tabu"]="0*11*42$96$fq$hh$of$q6$vj$11a,1*9*i8$lq$s2$15o$1eg";
w["1"]="0*25*5n$a7$qm$qs$11q$120$13e$141$19t,1*8*sg$te$vi$166";
w["10"]="0*5*1ah";
w["100"]="0*11*9g$ba$c6$cm$g4$op$vt";
w["1010data"]="1*2*19$fs$mg";
w["11"]="0*5*co$1aj";
w["12"]="1*0*10b";
w["13"]="0*0*13m";
w["13."]="0*0*13l";
w["1:"]="0*5*5m";
w["2"]="0*16*a8$d3$r1$125$19v,1*18*sj$169$16q$17c$1et$1fc";
w["2008"]="0*0*cl";
w["200811"]="0*0*cs";
w["2009"]="0*0*14e";
w["2:"]="0*5*d2";
w["3"]="0*10*a9$j6$1a1";
w["3:"]="0*5*j5";
w["4"]="0*10*aa$sn$1a4";
w["4:"]="0*5*sm";
w["4;'apr';5;'may';6;'jun';"]="0*5*1a3";
w["5"]="0*5*1a6";
w["6"]="0*5*1a8";
w["7"]="0*5*1ab";
w["7;'jul';8;'aug';9;'sep';"]="0*5*1aa";
w["8"]="0*5*1ad";
w["9"]="0*5*1af";
w[";"]="0*1*189$18e";
w["<"]="0*108*1p$24$28$2g$2r$35$3c$3j$41$6t$78$7c$7k$7v$89$8g$8n$95$99$dh$ds$e0$e8$ej$et$f4$fb$fp$ft$g9$gj$gq$h1$hg$m5$mg$mk$ms$n7$nh$no$nv$oe$oi$ou$p8$pf$pm$q5$q9$ta$tl$tp$u1$uc$um$ut$v4$vi$vm$102$10c$10j$10q$119$11d$127$12l$19l,1*85*70$78$7h$g2$gd$gh$gp$h4$he$hl$hs$i7$jh$js$k0$k8$kj$kt$l4$lb$lp$pp$q4$q8$qg$qr$r5$rc$rj$s1$s5$13e$13p$13t$145$14g$14r$152$159$15n$15r$16f$16s$1cd$1cp$1ct$1d5$1dg$1dt$1e4$1ef$1ej$1ev$1fe";
w[">"]="0*109*1t$27$2f$2q$34$3b$3i$40$44$63$71$7b$7j$7u$88$8f$8m$94$98$9k$dl$dv$e7$ei$es$f3$fa$fo$fs$g8$gi$gp$h0$hf$hj$m9$mj$mr$n6$ng$nn$nu$od$oh$ot$p7$pe$pl$q4$q8$r2$te$to$u0$ub$ul$us$v3$vh$vl$101$10b$10i$10p$118$11c$126$12k$132$1am,1*87*6p$77$7g$7s$d5$g6$gg$go$h3$hd$hk$hr$i6$ia$jl$jv$k7$ki$ks$l3$la$lo$ls$pt$q7$qf$qq$r4$rb$ri$s0$s4$so$13i$13s$144$14f$14q$151$158$15m$15q$16e$16r$17d$1ch$1cs$1d4$1df$1ds$1e3$1ee$1ei$1eu$1fd$1fk";
w["abbreviations"]="0*0*1b0";
w["abo"]="1*0*f6";
w["above"]="0*3*1i$9m$r3$1an,1*4*4m$af$iv$sq$19o";
w["above."]="1*0*ae";
w["absolute"]="1*0*17v";
w["acceptable"]="0*0*ao";
w["account"]="0*1*15p$16a";
w["across"]="0*1*ig$ik,1*0*1a8";
w["actions"]="1*1*j8$pd";
w["actual"]="1*0*121";
w["actually"]="0*1*6b$l3";
w["add"]="0*2*6o$c7$sq,1*0*jb";
w["adding"]="1*0*136";
w["addition"]="1*0*4l";
w["additional"]="0*0*k2,1*0*137";
w["adjustments"]="1*0*1bh";
w["after"]="1*0*dn";
w["aggregate"]="0*0*ie,1*1*bh$bs";
w["aggregated"]="1*2*ev$ob$19g";
w["aggregations"]="1*0*dh";
w["all"]="0*1*ih$il,1*4*b9$u6$10a$12j$1a9";
w["allows"]="0*0*17v";
w["already"]="0*10*-1$12,1*1*uf$112";
w["also"]="1*0*19l";
w["among"]="1*0*44";
w["analyses"]="1*0*58";
w["analyses."]="1*0*57";
w["analysis"]="0*12*-1$e$jh$14r,1*59*-1$-1$6$d$r$12$3u$9v$d4$11v$190$19p$1br$1cc$1gl";
w["analysis."]="1*1*3t$1cb";
w["another"]="0*0*l1";
w["answer"]="1*0*32";
w["any"]="1*0*2d";
w["appear"]="1*0*1c2";
w["appearance"]="1*0*n9";
w["appears"]="0*1*dd$ss,1*0*je";
w["applied"]="0*4*1u$72$dm$ma$tf,1*4*g7$jm$pu$13j$1ci";
w["apr"]="0*5*1a5";
w["argument"]="1*2*tk$ur$vg";
w["arguments"]="1*0*un";
w["around"]="1*0*m5";
w["around."]="1*0*m4";
w["ascending"]="1*1*mt$n2";
w["ask"]="0*0*15k";
w["ask:"]="0*0*15j";
w["assign"]="0*0*5o";
w["assigns"]="0*0*4q";
w["associated"]="0*0*a1";
w["aug"]="0*5*1ae";
w["base"]="0*4*1s$70$dk$m8$td,1*5*71$g5$jk$ps$13h$1cg";
w["basic"]="0*0*4j,1*2*18$1t$23";
w["basis"]="1*0*3r";
w["because"]="1*3*c1$vj$vt$1g5";
w["before"]="1*0*fi";
w["begin"]="1*0*bg";
w["below"]="0*2*1o$dg$su,1*3*pl$139$18o$1bm";
w["below:"]="0*1*1n$df,1*0*18n";
w["better"]="0*1*15v$16g";
w["between"]="0*1*iu$13d,1*0*2i";
w["blank"]="1*0*up";
w["blank."]="1*0*uo";
w["bold"]="0*2*6s$de$st,1*2*jg$pk$1c3";
w["bold:"]="0*0*6r,1*0*jf";
w["both"]="0*1*bi$14f,1*4*7t$8b$c9$129$13b";
w["break"]="0*24*36$3d$8a$8h$eu$f5$gk$gr$i2$ni$np$p9$pg$un$uu$10d$10k,1*19*hf$hm$ku$l5$mc$nh$r6$rd$14s$153$1du";
w["breaks"]="0*12*31$85$ep$gf$ir$nd$p4$ui$108,1*9*ha$kp$r1$14n$1dm";
w["calculate"]="0*1*ja$jt,1*1*co$fh";
w["calculate."]="1*0*fg";
w["calculated"]="1*0*ti";
w["calculated."]="1*0*th";
w["calculates"]="0*0*58";
w["calculating"]="1*0*120";
w["calculation"]="0*1*ka$l7,1*3*1j$u8$12d$132";
w["calculation."]="0*0*l6";
w["calculation:"]="1*0*12c";
w["calculations"]="1*1*ar$u1";
w["calculations."]="1*0*u0";
w["calendar"]="0*0*a3";
w["can"]="0*9*1g$aq$bm$jj$k8$s6$18o$1b2$1bo$1c3,1*22*-1$13$69$9h$a3$bf$cs$in$m8$n7$uj$12r$19k$1a2";
w["case"]="0*17*13k$17g$17n$194$19r$1br,1*0*9j";
w["case."]="0*0*193";
w["cbreaks"]="1*5*1do";
w["central"]="1*0*5b";
w["challenge"]="0*0*k3";
w["change"]="1*0*122";
w["changed"]="0*0*16v";
w["changing"]="0*0*s1";
w["chose"]="0*0*1aq,1*0*19m";
w["clabels"]="1*5*1dq";
w["cleaner"]="1*0*ff";
w["clearly"]="0*0*14a";
w["click"]="1*0*el";
w["closer"]="1*0*fq";
w["code"]="0*2*6j$r4$sr,1*4*e0$pj$st$138$1bl";
w["code."]="1*0*1bk";
w["col"]="0*23*37$3e$8b$8i$ev$f6$gl$gs$nj$nq$pa$ph$uo$uv$10e$10l,1*18*hg$hn$kv$l6$r7$re$14t$154$1dv$1fg";
w["column"]="0*18*4p$57$67$da$iq$j9$ke$kk$l2$lr$m4$t3$t7$183$199$19c$1bf$1bj$1cf,1*20*4s$53$6l$8a$b5$ij$it$ms$n1$oo$po$tn$ut$vd$10s$117$11g$11n$17g$182$1gb";
w["column."]="0*1*kd$m3,1*4*6k$89$b4$pn$vc";
w["column..."]="0*0*66";
w["columns"]="0*7*5d$62$al$bh$i4$rm$sb$139,1*4*cb$md$mi$ni$18k";
w["columns:"]="0*0*i3";
w["combination"]="0*0*1d";
w["combination:"]="0*0*1c";
w["combine"]="0*1*ah$lb";
w["common"]="1*1*43$135";
w["common."]="1*0*134";
w["compare"]="1*0*o7";
w["compared"]="1*6*3e$5k";
w["comparing"]="1*10*-1$i";
w["comparisons"]="1*0*fd";
w["completed"]="0*0*14p,1*0*18u";
w["compute"]="0*0*4n";
w["computed"]="0*6*56$5c$65$j8$kc$lq$m2,1*0*18j";
w["concerned"]="1*0*c3";
w["congratulations"]="1*0*18q";
w["congratulations!"]="1*0*18p";
w["containing"]="0*1*13a$19d";
w["contains"]="1*2*4a$us$128";
w["convert"]="0*10*17b$195";
w["converts"]="1*0*186";
w["copies"]="1*0*11j";
w["copy"]="0*0*kl";
w["correct"]="1*0*b2";
w["correctly"]="1*1*at$uh";
w["corresponds"]="0*0*1bg";
w["could"]="0*1*5v$rj";
w["create"]="0*5*4m$4v$55$5b$64$j7,1*2*dg$om$pm";
w["created"]="0*2*d8$kj$l0,1*0*id";
w["creates"]="1*0*1g9";
w["creating"]="0*1*s8$1c9";
w["cross"]="1*4*19r$19v$1b7$1bt$1g6";
w["current"]="0*0*kr,1*13*-1$j$2j$12e$1b2";
w["d"]="0*12*4o$17m$17t$18l$1c1,1*0*9p";
w["data"]="0*11*-1$g$149,1*6*41$bj$bt$eu$uc$10m$19i";
w["date"]="0*12*2e$2m$7i$7q$a4$ak$bg$e6$ee$mq$n2$tv$u7,1*16*4e$6j$7f$7o$88$8t$a1$gn$gv$k6$ke$qe$qm$143$14b$1d3$1db";
w["dd"]="1*0*aa";
w["dec"]="0*11*r0$124$1al,1*18*si$168$16p$17b$1es$1fb";
w["decrease"]="1*0*18c";
w["default"]="1*0*ih";
w["defined"]="1*0*ec";
w["department"]="1*0*2b";
w["descending"]="0*1*rn$146";
w["detail"]="0*11*30$84$eo$ge$nc$p3$uh$107,1*10*3p$48$h9$ko$r0$14m$1dl";
w["determine"]="0*11*-1$h$45,1*1*5v$1h1";
w["dialog"]="1*4*dd$e7$ig$ja$pf";
w["dialog:"]="1*0*e6";
w["did"]="1*0*1ag";
w["differ"]="1*0*1c4";
w["difference"]="1*3*2h$12p$12q$181";
w["difference."]="1*1*12o$180";
w["different"]="0*1*j1$14t";
w["digit"]="1*1*8p$94";
w["dimension"]="1*0*2f";
w["dir"]="1*0*1fi";
w["discover"]="0*0*15a";
w["distinguishing"]="0*0*it";
w["distribution"]="0*0*16q";
w["do"]="0*5*60$6d$k9$l4$s7$15l,1*5*67$u9$12m$18m$19a$19n";
w["doc"]="1*0*74";
w["does"]="0*1*167$173";
w["dollars"]="1*3*2s$3l";
w["dollars?"]="1*3*3k";
w["done"]="1*10*-1$14";
w["drives"]="0*0*15c";
w["during"]="0*1*15u$16f";
w["e"]="0*0*a5,1*3*5g";
w["each"]="0*4*18$4s$5q$a2$144,1*10*4v$5i$63$bk$f0$o8$vu$10f$1gc";
w["easily"]="1*1*1g$ma";
w["edit"]="1*1*j7$pc";
w["either"]="0*0*174";
w["end"]="1*0*9u";
w["enter"]="1*0*6t";
w["entering"]="0*0*db";
w["essentially"]="0*0*lg";
w["etc"]="1*0*9f";
w["every"]="1*2*4g$106$125";
w["everything"]="0*0*6e";
w["examine"]="1*0*1e";
w["example"]="0*2*cb$ic$1ao";
w["extended"]="0*4*3u$92$fm$ob$vf,1*11*4p$i4$ir$lm$ru$v7$15k$1ec";
w["extract"]="1*1*6f$84";
w["feb"]="0*5*1a0";
w["few"]="1*0*1bf";


// Auto generated index for searching.
w["final"]="0*1*bt$so";
w["find"]="0*1*5e$ra,1*0*12n";
w["finding"]="1*1*2g$vm";
w["first"]="0*2*cf$ki$1ci,1*2*31$5t$ml";
w["first!"]="0*0*1ch";
w["first."]="1*0*mk";
w["following"]="0*2*9o$r7$1bk,1*3*6v$jc$nn$t1";
w["following:"]="0*0*r6,1*0*6u";
w["format"]="0*14*2n$7r$ap$bk$c0$d0$ef$n3$qt$u8$121,1*25*7p$ac$ak$h0$kf$qn$sh$14c$167$16o$178$1dc$1er$1f8";
w["format!"]="0*0*cv";
w["formats"]="0*0*b1,1*0*a2";
w["formula"]="0*0*ci";
w["four"]="0*0*4i,1*0*93";
w["four-digit"]="1*0*92";
w["fourth"]="0*0*14c";
w["frame"]="0*0*150";
w["frame!"]="0*0*14v";
w["frequently"]="1*10*-1$f";
w["from"]="1*4*6i$87$os$17p$1c5";
w["fun"]="0*11*3q$8u$fi$h6$o6$pr$vb$10v,1*9*i0$li$rq$15g$1e8";
w["function"]="0*8*9t$ad$av$js$sk$13t$17u$18t$1c2,1*7*8j$90$9q$p3$tf$v1$19e$1g4";
w["function."]="0*1*au$sj";
w["functionality"]="1*0*1b";
w["functionality."]="1*0*1a";
w["functions"]="0*0*bp,1*6*1p$1r$20$6e$82$8c$a5";
w["functions."]="1*0*1v";
w["further"]="0*0*159,1*0*1gu";
w["further."]="1*0*1gt";
w["g"]="0*28*a6$jl$jn$qh$qn$sd$sf$11l$11r$12d$12r$13n$13p,1*16*1q$p2$p4$p6$sb$ta$tj$161$19d$1ak$1am$1ft$1fv";
w["g_function"]="1*1*p1$19c";
w["generated"]="0*0*6l,1*0*e1";
w["get"]="0*0*bs,1*3*en$n8$1a3$1b9";
w["give"]="1*2*j2$sv$17u";
w["given"]="1*4*3c$1a6";
w["gives"]="0*10*-1$15";
w["go"]="1*0*d3";
w["going"]="0*3*61$6c$la$li";
w["got"]="1*0*nv";
w["greatest"]="0*13*-1$n$4b$5i$rg";
w["group"]="0*0*d6,1*2*1o$c8$cl";
w["grouped"]="0*0*ia,1*0*lv";
w["grouping"]="1*2*bp$ee$f4";
w["grouping."]="1*0*bo";
w["groups"]="0*0*51";
w["growth"]="0*52*-1$4$9$q$4d$5a$5k$jd$jv$rh$130$14k,1*0*1hd";
w["growth."]="0*11*-1$p$jc,1*0*1hc";
w["guess"]="0*0*1bq";
w["had"]="0*14*-1$k$48$af$c3$rd,1*0*1h4";
w["has"]="0*0*16p,1*1*101$108";
w["have"]="0*12*-1$11$hm$1bb,1*4*b8$es$116$12l$18s";
w["here"]="0*0*15e,1*1*cg$e5";
w["here's"]="1*0*e4";
w["higher"]="0*2*15m$15q$16b";
w["highest"]="0*75*-1$1$3$6$8$l$49$5f$re$14g$14j,1*0*1h5";
w["how"]="0*10*16o$17a$18r,1*10*1f$35$5e$df$n5$nt$1gi";
w["important"]="1*0*54";
w["in."]="1*1*66$dm";
w["include"]="1*0*u5";
w["included"]="0*0*b6";
w["includes"]="1*0*4o";
w["including"]="0*0*io,1*0*a6";
w["increase"]="1*0*18b";
w["increase/decrease"]="1*0*18a";
w["incredibly"]="0*0*1c4";
w["incrementally"]="0*0*142";
w["indicating"]="1*0*4t";
w["information"]="1*1*86$bb";
w["input"]="1*2*a4$mb$ne";
w["insert"]="1*0*pg";
w["instead"]="0*0*ht";
w["integer"]="0*0*bj,1*2*8e$8q$96";
w["integer."]="1*0*95";
w["integers"]="0*10*13b$17c$19a";
w["interested"]="0*0*154,1*3*dl$ek$ts$vl";
w["is:"]="0*0*19k";
w["isn"]="1*0*ao";
w["isn't"]="1*0*an";
w["it's"]="1*0*ue";
w["item"]="0*0*16r";
w["items"]="0*0*15o";
w["jan"]="0*5*19u,1*0*9d";
w["january"]="1*0*9e";
w["jul"]="0*5*1ac";
w["jun"]="0*5*1a9";
w["just"]="0*3*c2$rk$14o$1cg,1*3*d2$m9$18t$1af";
w["keep"]="1*0*ai";
w["know"]="1*0*1gh";
w["knowing"]="0*0*18m";
w["knowledge"]="1*0*1gr";
w["label"]="0*41*2t$3s$81$90$9i$el$fk$g6$gb$hb$n9$o8$or$p0$q0$ue$vd$vv$104$114$12h$12v,1*24*h6$i2$kl$lk$qt$rs$sk$14i$15i$16a$1di$1ea";
w["language"]="0*1*1m$6h,1*7*6r$dv$fu$pi$ss$1bj$1bo$1c8";
w["language."]="0*0*6g";
w["largest"]="0*0*13v,1*0*1ha";
w["last"]="0*1*6m$6p,1*11*mj$sl$118$11s$16b$17k$18d$1as";
w["lastly"]="1*0*ve";
w["leave"]="1*0*uk";
w["less"]="1*3*39";
w["let"]="0*1*k5$rq,1*6*30$d9$fo$j1$ol$t6$1gp";
w["let's"]="0*1*k4$rp,1*6*2v$d8$fn$j0$ok$t5$1go";
w["lettered"]="0*0*1au";
w["line"]="0*2*6q$9l$176";
w["ll"]="0*10*-1$t,1*0*dq";
w["logic"]="0*1*b7$19h";
w["longer"]="1*0*1fq";
w["look"]="1*7*b1$db$fr$t9$18l";
w["looking"]="0*0*ce";
w["ly"]="0*0*lo,1*22*s8$11p$15u$16m$174$176";
w["ly_sales"]="0*0*ln,1*0*11o";
w["m1"]="1*15*1eq$1f6$1f7";
w["m2"]="1*10*1ep$1f5";
w["m2-m1"]="1*5*1eo";
w["macro"]="0*1*1l$6f,1*7*6q$du$ft$ph$sr$1bi$1bn$1c7";
w["make"]="1*0*av";
w["makes"]="1*0*f9";
w["manipulate"]="0*0*ag";
w["manipulations"]="1*0*19f";
w["manner"]="0*0*1bm";
w["manner:"]="0*0*1bl";
w["many"]="1*10*-1$15";
w["mar"]="0*5*1a2";
w["mathematical"]="0*0*bo";
w["may"]="0*5*1a7";
w["mean"]="1*0*o4";
w["meaningful"]="1*0*j4";
w["measure"]="1*11*-1$g$25";
w["menu"]="1*0*6m";
w["method"]="1*0*195";
w["methods"]="1*0*13d";
w["methods."]="1*0*13c";
w["metric"]="1*0*ef";
w["might"]="0*0*15i";
w["mind"]="1*0*aj";
w["minor"]="1*0*1bg";
w["mm"]="1*0*a9";
w["mm/dd/yy"]="1*0*a8";
w["mo"]="1*0*1cn";
w["mo.salesdetail"]="1*0*1cm";
w["month"]="0*61*1a$2b$2d$33$3f$47$4t$5s$7f$7h$87$8j$9h$an$bb$c8$e3$e5$er$f7$g5$hv$mn$mp$nf$nr$oq$ts$tu$uk$v0$vu$17d$190$198$19e$19o$19s$1av$1bd$1bi$1cd,1*73*3d$3g$5j$5n$61$6g$7b$7d$7u$8h$8s$9a$bm$c5$cm$f3$fb$fc$gk$gm$hc$ho$k3$k5$kr$l7$m1$mr$oa$qb$qd$r3$rf$sd$tc$tm$vq$vv$10g$140$142$14p$155$163$1a7$1d0$1d2$1dn$1fh";
w["month's"]="1*0*o9";
w["month-naming"]="0*0*18v";
w["month-to-month"]="1*0*fa";
w["month."]="0*1*5r$hu";
w["month/year"]="0*0*19,1*0*bl";
w["month_name"]="0*1*1bc$1cc";
w["months"]="1*0*10c";
w["more"]="1*5*38$j3$1bc";
w["more/less"]="1*3*37";
w["most"]="1*1*22$133";
w["move"]="0*0*175,1*0*fk";
w["much"]="1*5*36$fe$1bb";
w["multiple"]="1*0*a0";
w["multiply"]="0*0*c4";
w["my"]="1*3*3a";
w["n"]="0*0*jr,1*5*pa$vf$10u$14k$1aq$1g3";
w["n/a."]="1*0*10t";
w["name"]="0*57*2a$2i$3l$7e$7m$8p$9b$e2$ea$fd$fv$h8$mm$mu$o1$ok$pt$qb$tr$u3$v6$vo$111$11f$129$12n$19n$19p$1be$1ce,1*39*7a$7j$9b$gj$gr$ii$j6$k2$ka$ld$qa$qi$rl$s7$13v$147$15b$15t$16h$16u$1cv$1d7$1el$1f1";
w["name."]="1*0*j5";
w["names"]="0*10*17e$19f";
w["naming"]="0*0*191";
w["necessary"]="1*1*aq$12b";
w["need"]="0*1*4g$19j,1*3*bd$o6$ua$1fr";
w["new"]="0*3*sa$138$19b$1cb,1*4*ca$on$v5$11m$17f";
w["newly"]="0*0*d7";
w["next"]="0*10*-1$a,1*1*fl$1gv";
w["nicely"]="1*0*1g8";
w["nocommas"]="0*4*2p$7t$eh$n5$ua,1*6*7r$am$h2$kh$qp$14e$1de";
w["note"]="0*10*1q$26$6u$7a$di$du$i0$m6$mi$tb$tn,1*12*ah$g3$gf$ji$ju$lt$pq$q6$13f$13r$1ce$1cr$1fm";
w["note:"]="1*1*ag$1fl";
w["notice"]="1*0*1fo";
w["nov"]="0*5*1ak";
w["now"]="0*3*hk$133$18p$1b8,1*7*b6$e8$er$nq$t4$115$127$1gf";
w["number"]="0*3*a0$bu$13f$143,1*2*4c$od$of";
w["numbers"]="1*20*-1$-1$l$p";
w["numbers."]="1*10*-1$o";
w["o"]="0*2*jp$sh$13r,1*3*p8$um$1ao$1g1";
w["obtain"]="0*0*1h";
w["occurred"]="1*0*65";
w["oct"]="0*5*1ai";
w["of`extended`"]="0*0*oa";
w["one"]="0*2*b0$lf$157,1*4*21$8m$og$ot$102";
w["one-"]="1*0*8l";
w["one."]="0*0*le";
w["only"]="0*2*4f$i9$19g,1*2*100$1be$1c0";
w["opposed"]="0*1*162$16j";
w["order"]="0*3*ab$ro$s2$148,1*4*mf$mu$n4$ng$ub";
w["order."]="0*0*147,1*0*n3";
w["ordered"]="1*0*nm";
w["original"]="0*1*jg$197";
w["originally"]="0*0*kg";
w["other"]="1*3*2e$m2$mm$19b";
w["otherwise"]="0*0*18j";
w["our"]="0*19*-1$c$ai$be$ch$ct$jf$lm$lu$18u$196,1*13*3s$4i$55$5a$ch$ed$mo$o0$tu$v4$11l$11u$17e$1c6";
w["outputs"]="0*0*9u";
w["over"]="0*0*170,1*40*-1$3$a$v";
w["pct"]="0*6*qv$123,1*6*17a$1fa";
w["per"]="1*0*104";
w["perc"]="0*17*m1$qd$t6$11h$12p$12u,1*7*170$185$1f3";
w["percent"]="0*15*-1$o$4c$59$5j$jb$ju,1*0*131";
w["percentage"]="0*2*l5$15r$16c,1*0*189";
w["percentages"]="1*0*2u";
w["percentages."]="1*0*2t";
w["perform"]="0*2*4h$bn$d4,1*2*1h$cv$1gj";
w["performance"]="1*12*-1$h$2m$2q";
w["performing"]="0*3*160$164$16h$16l,1*0*1bp";
w["period"]="1*0*tq";
w["poorly"]="0*1*163$16k";
w["portion"]="0*1*ll$lt";
w["portions"]="1*0*1c1";
w["prefer"]="0*0*1b7,1*0*98";
w["prefer."]="0*0*1b6";
w["previous"]="1*5*5o$10i$1c9";
w["price"]="0*0*16s,1*0*4u";
w["priced"]="0*0*15n";
w["prior"]="0*0*km,1*21*-1$m$2n$3h$oj$ov$vn$10d$10n$11h$12g";
w["prior."]="1*0*oi";
w["probably"]="0*0*1bp";


// Auto generated index for searching.
w["process"]="0*0*156";
w["produce"]="1*0*8d";
w["produced"]="0*10*-1$13,1*0*fv";
w["produces"]="0*0*9n,1*1*8k$91";
w["programmatically"]="0*0*181";
w["pub"]="1*0*73";
w["pull"]="1*1*op$1ar";
w["pulls"]="1*0*11b";
w["qtr"]="0*73*9c$g0$gh$gt$h9$hd$i5$ol$p6$pi$pu$q2$qf$qj$qk$qp$qq$t1$vp$10a$10m$112$116$11j$11n$11o$11t$11u$12f";
w["qtr_sales"]="0*0*t0";
w["quarter"]="0*72*-1$0$5$j$4r$52$5p$9e$9j$9r$9v$as$b8$d9$g2$g7$hs$ib$j0$k1$on$os$rc$vr$100$14d$14i$14m,1*1*1h3$1h8";
w["quarter."]="0*1*k0$14l";
w["quarters"]="0*4*ij$161$166$16i$16n";
w["quarters?"]="0*1*165$16m";
w["question"]="1*1*34$5c";
w["question:"]="1*0*33";
w["questions"]="0*0*15g";
w["quickly"]="1*2*dr$t7$1bd";
w["quite"]="1*0*42";
w["r1"]="0*12*17j$17q$188$1bu,1*0*9m";
w["r2"]="0*12*17l$17s$18d$1c0,1*0*9o";
w["r3"]="0*0*18i";
w["ranging"]="0*0*13c";
w["rank"]="0*34*ru$se$sv$12a$12e$12j$12o$12s$131$13o$13u";
w["re"]="0*1*6a$153,1*1*dk$ej";
w["recognized"]="0*0*ar";
w["referred"]="1*10*-1$s";
w["regrouping"]="0*0*14s";
w["relevant"]="1*0*85";
w["remember"]="0*0*kf";
w["reminder"]="0*0*1e";
w["represented"]="1*0*12s";
w["representing"]="1*0*8r";
w["requirements"]="1*0*ci";
w["result"]="0*1*cr$r5,1*1*8g$10r";
w["result:"]="1*0*8f";
w["resulting"]="1*1*b3$nj";
w["results"]="0*1*9q$15d,1*2*ep$o2$t3";
w["results:"]="0*0*9p,1*1*eo$t2";
w["retail"]="1*31*0$7$75";
w["retailde"]="1*0*1cl";
w["retaildemo"]="0*4*22$76$dq$me$tj,1*3*gb$jq$q2$13n";
w["retaildemo.salesdetail"]="0*4*21$75$dp$md$ti,1*3*ga$jp$q1$13m";
w["retailer"]="1*1*27$4k";
w["retailer's"]="1*0*26";
w["retailer."]="1*0*4j";
w["retailers"]="1*11*-1$e$46";
w["retailers."]="1*0*45";
w["reverse"]="1*1*me$nf";
w["revisiting"]="1*0*59";
w["row"]="0*2*cg$ku$145,1*6*vo$103$109$10e$126$1ad$1b1";
w["row."]="1*0*1ac";
w["rows"]="0*0*13h,1*0*u7";
w["rshift"]="0*12*jm$qi$qo$11m$11s,1*9*p5$sc$tb$162$1al$1fu";
w["run"]="1*0*ds";
w["running"]="1*0*sp";
w["s"]="0*2*jo$sg$13q,1*3*p7$ul$1an$1g0";
w["sales"]="0*139*-1$-1$2$7$f$m$16$2v$3n$3v$4a$54$5g$83$8r$93$en$ff$fn$gd$h5$ha$he$hr$if$kp$lp$nb$o3$oc$p2$pq$pv$q3$qg$ql$qr$rf$t2$ug$v8$vg$106$10u$113$117$11k$11p$11v$12b$12g$12i$15t$16e$177,1*104*3b$3o$47$4q$5h$5l$bi$c4$cf$cq$et$h8$i5$is$kn$lf$ln$oc$or$qv$rn$rv$s9$sf$sn$v8$vb$11a$11f$11q$124$12f$12i$14l$15d$15l$15v$165$16d$16l$16n$173$175$177$17n$17s$18g$19h$1a4$1av$1b6$1dk$1ed$1h6";
w["sales."]="1*3*ce$123$12h$1b5";
w["salesdetail"]="0*4*23$77$dr$mf$tk,1*5*76$gc$jr$q3$13o$1co";
w["same"]="1*11*3f$5m$oe$vp$187$1ab$1b0";
w["satisfy"]="1*0*13a";
w["saw"]="1*0*1h9";
w["say"]="0*0*rr";
w["scenarios"]="0*0*1c7";
w["screenshot"]="1*0*iu";
w["second"]="0*2*ii$iv$14q";
w["see"]="0*2*k6$136$18q,1*0*io";
w["seen"]="1*0*113";
w["select"]="1*0*6n";
w["selling"]="0*0*14h";
w["sep"]="0*5*1ag";
w["separate"]="1*0*1ga";
w["separately"]="0*0*t9";
w["separately."]="0*0*t8";
w["several"]="1*0*12t";
w["short"]="1*5*1dr";
w["should"]="0*1*135$1ba,1*0*su";
w["shown"]="1*0*f5";
w["shows"]="0*0*14b";
w["similar"]="0*1*ho$je,1*0*1ai";
w["simple"]="1*2*1l$6c$12v";
w["simplest"]="1*0*bq";
w["simply"]="1*2*au$tl$17i";
w["since"]="0*0*bd,1*2*to$u2$ud";
w["single"]="0*0*kb";
w["sku"]="1*2*2c$4f$50";
w["sold"]="0*1*169$16u,1*0*52";
w["sold."]="1*0*51";
w["some"]="0*1*s9$15f,1*1*1n$1s";
w["something"]="1*0*1ah";
w["sort"]="0*24*39$3g$8d$8k$f1$f8$gn$gu$nl$ns$pc$pj$rl$uq$v1$10g$10n,1*19*hi$hp$l1$l8$mh$r9$rg$14v$156$1e1$1ff";
w["sorted"]="1*2*mq$mv$ug";
w["source"]="0*11*3o$8s$fg$h3$o4$po$v9$10s,1*9*hu$lg$ro$15e$1e6";
w["standard"]="1*0*a7";
w["start"]="0*11*-1$u$6i,1*0*1c";
w["step"]="0*32*-1$b$5l$d1$j4$sl$sp$158,1*2*fm$11t$1gs";
w["steps"]="0*1*4l$ld";
w["steps:"]="0*0*4k";
w["store"]="1*0*2a";
w["string"]="1*1*1u$6d";
w["submit"]="1*0*em";
w["substitute"]="0*0*lj";
w["subtraction"]="1*1*130$188";
w["subtracts"]="1*0*17j";
w["sum"]="0*16*3r$3t$8v$91$fj$fl$h7$o7$o9$ps$vc$ve$110,1*21*cc$cp$i1$i3$iq$lj$ll$rr$rt$v6$15h$15j$1e9$1eb";
w["summarization"]="0*0*hp";
w["summarizations"]="1*0*eg";
w["surprisingly"]="1*0*10l";
w["t"]="1*0*ap";
w["t0"]="1*1*ik$td";
w["table"]="0*7*20$4u$74$do$mc$s4$th$13i,1*9*3q$49$4n$72$ad$g9$jo$q0$13l$1ck";
w["table."]="0*0*s3";
w["table:"]="0*4*1v$73$dn$mb$tg,1*4*g8$jn$pv$13k$1cj";
w["tabu"]="0*23*2s$43$80$97$ek$fr$ga$hi$n8$og$ov$q7$ud$vk$103$11b,1*18*h5$i9$kk$lr$qs$s3$14h$15p$1dh$1eh";
w["tabulatio"]="1*0*14j";
w["tabulation"]="0*25*-1$v$1j$2u$50$82$d5$em$gc$na$p1$uf$105,1*26*1m$c0$ck$d1$d7$dc$e3$g1$h7$ic$if$km$mp$nk$o1$qu$193$19t$1a0$1b8$1bv$1dj$1g7";
w["tabulation."]="1*6*bv$d0$e2$g0$192$19s$1bu";
w["tabulation..."]="1*0*d6";
w["tabulation:"]="1*0*cj";
w["take"]="1*3*da$fp$t8$1gq";
w["taking"]="0*0*155";
w["tcol"]="0*11*3k$8o$fc$h2$o0$pn$v5$10r,1*9*ht$lc$rk$15a$1e5";
w["terms"]="1*3*2r$3j";
w["text"]="0*0*dc,1*0*jd";
w["th"]="1*3*5f";
w["three"]="0*0*1at";
w["three-lettered"]="0*0*1as";
w["through"]="1*0*dt";
w["time"]="0*3*6n$l8$14u$172,1*1*ba$tp";
w["time?"]="0*0*171";
w["tools"]="1*0*6o";
w["top"]="1*0*ip";
w["tot"]="0*11*3m$8q$fe$h4$o2$pp$v7$10t,1*25*le$rm$se$va$11e$15c$164$16k$172";
w["tot_sales"]="1*1*v9$11d";
w["total"]="0*11*5h$hc$hq$q1$115$13g$15s$16d,1*3*oq$17o$17t$18i";
w["total."]="1*0*18h";
w["totals"]="0*1*17$53,1*0*1a5";
w["touch"]="1*0*9t";
w["trans"]="1*1*7e$7n";
w["transaction"]="1*3*4b$4d$4h$64";
w["transactional"]="1*0*40";
w["translate"]="0*0*ck";
w["trends"]="0*0*179,1*0*28";
w["trends?"]="0*0*178";
w["trs"]="1*0*cu";
w["two"]="0*4*aj$bf$i1$lc$137,1*2*6b$8o$1gm";
w["two-digit"]="1*0*8n";
w["type"]="0*15*1r$2o$6v$7s$dj$eg$m7$n4$qu$tc$u9$122,1*29*-1$q$3v$7q$al$g4$h1$jj$kg$pr$qo$13g$14d$179$1cf$1dd$1f9";
w["understand"]="1*1*de$ns";
w["up"]="0*23*3a$3h$8e$8l$f2$f9$go$gv$nm$nt$pd$pk$ur$v2$10h$10o,1*18*cd$hj$hq$l2$l9$ra$rh$150$157$1e2$1fj";
w["us"]="0*0*180";
w["use"]="0*2*jk$192$1ar,1*5*3n$6a$9i$ct$v0$1fs";
w["useful"]="0*1*18s$1c5,1*0*197";
w["using"]="0*10*1k$sc$17f,1*11*17$1k$99$bu$ie$p0$tt$191$19q$19u$1aj$1bs";
w["utilize"]="0*0*ac";
w["v1"]="0*12*17i$17p$186$1bt,1*0*9l";
w["v2"]="0*12*17k$17r$18b$1bv,1*0*9n";
w["v3"]="0*0*18g";
w["value"]="0*46*2c$2k$7g$7o$9d$ca$e4$ec$g1$kq$lk$ls$mo$n0$om$qe$tt$u5$vq$11i$12c$12q$140$185$18a$18f$19q,1*33*7c$7l$gl$gt$k4$kc$qc$qk$sa$vs$10q$11c$11k$141$149$160$16j$171$1d1$1d9$1en$1f4";
w["value."]="0*0*c9,1*0*vr";
w["values"]="0*1*br$rv,1*0*12a";
w["values."]="0*0*bq";
w["various"]="0*0*1c6";
w["ve"]="1*2*eb$f8$111";
w["ve."]="1*0*f7";
w["very"]="0*0*hn,1*0*196";
w["volume"]="0*1*168$16t";
w["want"]="0*0*rt,1*3*5u$nc$u4$199";
w["want."]="1*0*nb";
w["wanted"]="0*0*r9";
w["way"]="1*2*br$m3$np";
w["way:"]="1*0*no";
w["ways"]="1*3*16$24$12u$1gn";
w["we"]="0*45*-1$-1$s$10$1f$4e$5u$69$6k$ae$b5$bl$c1$cc$hl$i8$is$ji$k7$kh$kv$l9$lh$r8$ri$rs$s5$19i$1ap,1*33*1d$3m$5s$68$9s$b7$bc$be$c2$c7$dj$dp$ea$ei$eq$fj$lu$n6$na$nd$nr$nu$o5$tr$u3$ui$uu$vk$110$114$12k$1ae$1fp$1h0";
w["we'll"]="0*10*-1$r,1*0*do";
w["we're"]="0*0*68,1*1*di$eh";
w["we've"]="1*1*e9$10v";
w["were"]="0*0*cd";
w["what"]="0*0*15b,1*2*60$o3$tg";
w["whatever"]="0*0*1b4";
w["when"]="1*0*ib";
w["whether"]="1*1*29$9c";
w["which"]="0*25*-1$-1$i$14$46$b3$cp$rb$13j,1*4*9r$il$v2$1h2$1h7";
w["while"]="0*0*5t,1*1*m6$194";
w["why"]="0*0*b4";
w["willbe"]="0*38*29$2h$7d$7l$9a$e1$e9$fu$ml$mt$oj$qa$tq$u2$vn$11e$128$12m$19m,1*29*79$7i$gi$gq$k1$k9$q9$qh$s6$13u$146$15s$16g$16t$1cu$1d6$1ek$1f0";
w["wish"]="1*0*uv";
w["without"]="0*0*s0";
w["words"]="1*0*mn";
w["work"]="1*1*83$as";
w["workshop"]="1*0*6s";
w["would"]="0*2*cj$cq$id,1*2*nl$v3$10h";
w["write"]="0*5*182$187$18c$18h$18k$1b3";
w["x"]="0*16*9s$at$jq$si$13s$17h$17o$184$1bs,1*8*7v$81$8i$8v$9k$p9$uq$1ap$1g2";
w["xml"]="1*1*j9$pe";
w["xsales"]="0*4*3p$8t$fh$o5$va,1*9*4r$hv$lh$rp$15f$1e7";
w["year"]="0*49*1b$2j$2l$32$38$7n$7p$86$8c$9f$am$b9$c5$eb$ed$eq$f0$g3$gg$gm$i6$ip$ko$kt$mv$n1$ne$nk$oo$p5$pb$u4$u6$uj$up$vs$109$10f,1*173*-1$-1$-1$-1$2$4$9$b$k$n$u$10$2l$2p$3i$5q$62$6h$7k$7m$80$8u$b0$bn$c6$cn$f2$gs$gu$hb$hh$kb$kd$kq$l0$m0$n0$oh$ou$qj$ql$r2$r8$sm$105$107$10k$10p$119$11i$148$14a$14o$14u$16c$17m$17r$18f$1au$1b4$1d8$1da$1dp$1e0$1ge";
w["year's"]="0*1*kn$ks,1*7*2k$2o$10o$17l$17q$18e$1at$1b3";
w["year-over-year"]="1*40*-1$1$8$t";
w["year."]="1*1*10j$1gd";
w["year/month"]="1*0*f1";
w["year?"]="1*3*5p";
w["years"]="0*1*in$j3,1*0*1aa";
w["years."]="0*1*im$j2";
w["you"]="0*8*134$14n$152$15h$18n$1b1$1b5$1b9$1bn,1*12*97$9g$cr$im$m7$t0$18r$198$19j$1a1$1ba$1fn$1gg";
w["you're"]="0*0*151";
w["your"]="0*1*1bh$1ca";
w["yoy"]="0*23*-1$d$m0$qc$t5$11g$12t,1*62*-1$5$c$11$1i$56$tv$16i$16v$17h$184$18v$1bq$1ca$1em$1f2$1gk$1hb";
w["yoy_perc"]="0*1*lv$t4,1*0*183";
w["yy"]="1*0*ab";
w["yyyymm"]="0*2*b2$bv$cu";
w["–"]="1*1*5d$5r";

return {'fil': fil, 'w': w}})();
