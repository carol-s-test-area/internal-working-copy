window['tableauconnector'] = (function() {
var w = new Object();
fil = new Array();
fil["0"]= "pg_connectSSO.html@@@Configure single sign-on (SSO)@@@Use the SSO options in the JDBC driver to authenticate to 1010data with single sign-on...";
fil["1"]= "pg_connectbasic.html@@@Connect to 1010data from Tableau@@@Once you install the 1010data Tableau Connector, you are ready to access and visualize 1010data tables in Tableau...";
fil["2"]= "pg_connectsam.html@@@Configure Shared Access Management (SAM) pools@@@SAM pools enable a single set of credentials to be shared among client-side threads...";
fil["3"]= "pg_contactsupport.html@@@Contact support@@@If you checked the Troubleshooting section but are still having difficulties, you will need to create a log and send it to 1010data support...";
fil["4"]= "pg_external_manuals.html@@@External Reference Manuals@@@Refer to the CDATA reference manuals for technical specifications for the JDBC Driver and Tableau Connector...";
fil["5"]= "pg_installing.html@@@Install the connector@@@Watch the video, or follow the instructions below to install the 1010data Tableau Connector...";
fil["6"]= "pg_introduction.html@@@Introduction@@@The 1010data Tableau Connector allows you to access 1010data tables seamlessly and view them in Tableau...";
fil["7"]= "pg_tableauQQ.html@@@Connect to 1010data Quick Queries@@@Another way to connect to 1010data is to connect to a 1010data saved Quick Query...";
fil["8"]= "pg_tableauqueries.html@@@Connect to SQL queries@@@Once you have established a connection to your data source, you can specify a custom SQL query in Tableau...";
fil["9"]= "pg_tableausetup.html@@@Tableau setup@@@The 1010data Tableau Connector setup consists of running the setup.jar file and copying the 1010data connector file\n            (1010data.taco) into the appropiate directory. You will then be able to connect to your 1010data tables within Tableau...";
fil["10"]= "pg_tableauvisualization.html@@@Visualize data@@@You can use Tableau visualizations to display insights that have been discovered in the data...";
fil["11"]= "pg_troubleshooting.html@@@Troubleshooting and support@@@The following are common issues when connecting 1010data to Tableau. You may be able to troubleshoot them without needing to contact 1010data support...";
fil["12"]= "pg_usagetableau.html@@@Using Tableau with the 1010data connector@@@The 1010data Tableau Connector integrates seamlessly into Tableau, offering full support for live data connections to 1010data tables and Quick Queries...";
var doStem = false;searchLoaded = true;// Porter stemmer in Javascript. Few comments, but it's easy to follow against the rules in the original
// paper, in
//
//  Porter, 1980, An algorithm for suffix stripping, Program, Vol. 14,
//  no. 3, pp 130-137,
//
// see also http://www.tartarus.org/~martin/PorterStemmer

// Release 1 be 'andargor', Jul 2004
// Release 2 (substantially revised) by Christopher McKenzie, Aug 2009


var stemmer = (function(){
	var step2list = {
			"ational" : "ate",
			"tional" : "tion",
			"enci" : "ence",
			"anci" : "ance",
			"izer" : "ize",
			"bli" : "ble",
			"alli" : "al",
			"entli" : "ent",
			"eli" : "e",
			"ousli" : "ous",
			"ization" : "ize",
			"ation" : "ate",
			"ator" : "ate",
			"alism" : "al",
			"iveness" : "ive",
			"fulness" : "ful",
			"ousness" : "ous",
			"aliti" : "al",
			"iviti" : "ive",
			"biliti" : "ble",
			"logi" : "log"
		},

		step3list = {
			"icate" : "ic",
			"ative" : "",
			"alize" : "al",
			"iciti" : "ic",
			"ical" : "ic",
			"ful" : "",
			"ness" : ""
		},

		c = "[^aeiou]",          // consonant
		v = "[aeiouy]",          // vowel
		C = c + "[^aeiouy]*",    // consonant sequence
		V = v + "[aeiou]*",      // vowel sequence

		mgr0 = "^(" + C + ")?" + V + C,               // [C]VC... is m>0
		meq1 = "^(" + C + ")?" + V + C + "(" + V + ")?$",  // [C]VC[V] is m=1
		mgr1 = "^(" + C + ")?" + V + C + V + C,       // [C]VCVC... is m>1
		s_v = "^(" + C + ")?" + v;                   // vowel in stem

	return function (w) {
		var 	stem,
			suffix,
			firstch,
			re,
			re2,
			re3,
			re4,
			origword = w;

		if (w.length < 3) { return w; }

		firstch = w.substr(0,1);
		if (firstch == "y") {
			w = firstch.toUpperCase() + w.substr(1);
		}

		// Step 1a
		re = /^(.+?)(ss|i)es$/;
		re2 = /^(.+?)([^s])s$/;

		if (re.test(w)) { w = w.replace(re,"$1$2"); }
		else if (re2.test(w)) {	w = w.replace(re2,"$1$2"); }

		// Step 1b
		re = /^(.+?)eed$/;
		re2 = /^(.+?)(ed|ing)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			re = new RegExp(mgr0);
			if (re.test(fp[1])) {
				re = /.$/;
				w = w.replace(re,"");
			}
		} else if (re2.test(w)) {
			var fp = re2.exec(w);
			stem = fp[1];
			re2 = new RegExp(s_v);
			if (re2.test(stem)) {
				w = stem;
				re2 = /(at|bl|iz)$/;
				re3 = new RegExp("([^aeiouylsz])\\1$");
				re4 = new RegExp("^" + C + v + "[^aeiouwxy]$");
				if (re2.test(w)) {	w = w + "e"; }
				else if (re3.test(w)) { re = /.$/; w = w.replace(re,""); }
				else if (re4.test(w)) { w = w + "e"; }
			}
		}

		// Step 1c
		re = /^(.+?)y$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(s_v);
			if (re.test(stem)) { w = stem + "i"; }
		}

		// Step 2
		re = /^(.+?)(ational|tional|enci|anci|izer|bli|alli|entli|eli|ousli|ization|ation|ator|alism|iveness|fulness|ousness|aliti|iviti|biliti|logi)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			suffix = fp[2];
			re = new RegExp(mgr0);
			if (re.test(stem)) {
				w = stem + step2list[suffix];
			}
		}

		// Step 3
		re = /^(.+?)(icate|ative|alize|iciti|ical|ful|ness)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			suffix = fp[2];
			re = new RegExp(mgr0);
			if (re.test(stem)) {
				w = stem + step3list[suffix];
			}
		}

		// Step 4
		re = /^(.+?)(al|ance|ence|er|ic|able|ible|ant|ement|ment|ent|ou|ism|ate|iti|ous|ive|ize)$/;
		re2 = /^(.+?)(s|t)(ion)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(mgr1);
			if (re.test(stem)) {
				w = stem;
			}
		} else if (re2.test(w)) {
			var fp = re2.exec(w);
			stem = fp[1] + fp[2];
			re2 = new RegExp(mgr1);
			if (re2.test(stem)) {
				w = stem;
			}
		}

		// Step 5
		re = /^(.+?)e$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(mgr1);
			re2 = new RegExp(meq1);
			re3 = new RegExp("^" + C + v + "[^aeiouwxy]$");
			if (re.test(stem) || (re2.test(stem) && !(re3.test(stem)))) {
				w = stem;
			}
		}

		re = /ll$/;
		re2 = new RegExp(mgr1);
		if (re.test(w) && re2.test(w)) {
			re = /.$/;
			w = w.replace(re,"");
		}

		// and turn initial Y back to y

		if (firstch == "y") {
			w = firstch.toLowerCase() + w.substr(1);
		}

		return w;
	}
})();// Auto generated list of analyzer stop words that must be ignored by search.
stopWords = new Array();
stopWords[0]= "but";
stopWords[1]= "be";
stopWords[2]= "with";
stopWords[3]= "such";
stopWords[4]= "then";
stopWords[5]= "for";
stopWords[6]= "no";
stopWords[7]= "will";
stopWords[8]= "not";
stopWords[9]= "are";
stopWords[10]= "and";
stopWords[11]= "their";
stopWords[12]= "if";
stopWords[13]= "this";
stopWords[14]= "on";
stopWords[15]= "into";
stopWords[16]= "a";
stopWords[17]= "or";
stopWords[18]= "there";
stopWords[19]= "in";
stopWords[20]= "that";
stopWords[21]= "they";
stopWords[22]= "was";
stopWords[23]= "is";
stopWords[24]= "it";
stopWords[25]= "an";
stopWords[26]= "the";
stopWords[27]= "as";
stopWords[28]= "at";
stopWords[29]= "these";
stopWords[30]= "by";
stopWords[31]= "to";
stopWords[32]= "of";

var indexerLanguage="en";

// Auto generated index for searching.
w["+attach"]="3*0*ik";
w["-jar"]="5*0*3q";
w["."]="0*4*39$56$81$a2$a9,1*5*19$1e$2m$7u$89$93,2*1*21$4i,3*7*3c$72$9l$dq$e1$gr$h0$iu,5*6*1r$2m$5i$60$6n$6u$80,7*0*2u,8*3*2u$3a$4o$5h,10*8*3n$4n$50$58$6u$77$7a$82$85,11*2*2v$36$4g,12*2*2h$3s$4m";
w["/prime-latest"]="1*0*27";
w["021"]="0*0*2j";
w["021\\lib"]="0*0*2i";
w["1"]="1*0*bf,3*3*87$a0$aj$ca,7*0*3f,10*2*26$29$5b";
w["1."]="3*0*c9";
w["10"]="10*0*81";
w["100mb"]="3*0*9k";
w["1010data"]="0*26*-1$g$1m$23$2g$2s$3d$8m$9j$9l$a6$a8$aa$ac$br$bt$d6$db,1*70*-1$-1$1$5$b$i$q$16$18$1b$1d$1f$1h$1l$1n$21$2i$2q$55$6g$7v$81$90$ab$al$c1,2*2*19$1k$25,3*29*-1$g$1t$29$2i$3n$4d$51$5d$5l$cm$du$e0$e2$e4$ej$fh$fj$gj$gp$iv,5*16*-1$a$i$29$42$4c$51$7t,6*21*-1$-1$2$8$18,7*58*-1$-1$1$5$b$d$p$1n$2b$2o,8*2*20$28$2o,9*40*-1$-1$-1$-1$4$f$j$s,10*2*j$3k$3s,11*25*-1$-1$9$k$r$1c$2t$35$3d,12*58*-1$-1$2$6$8$k$p$14$1u$23$2n$32$3p$57";
w["1010data.taco"]="5*1*28$50,9*0*i";
w["1010data:"]="1*0*p";
w["2"]="0*0*2h,3*1*3k$ae,5*0*t,10*0*2a";
w["2019"]="10*1*3d$76";
w["2019."]="10*0*3c";
w["2021"]="0*2*2u$3e$8n,2*2*1b$1m$26,3*8*1v$2b$2j$3j$53$5f$5m$cn$ek,5*0*s,11*1*2u$3e";
w["2021.2"]="3*0*3i,5*0*r";
w["2021/lib"]="0*0*2t,2*0*1l,3*1*2a$5e";
w["2021\\lib"]="2*0*1a,3*1*1u$52";
w["3"]="3*0*av";
w["4"]="3*0*b7";
w["5"]="0*0*d4,3*0*bm";
w["6"]="5*0*10";
w["92"]="6*0*12";
w[":"]="1*3*1u$4v$6b$9f,2*2*2t$38$3u";
w[">"]="1*0*11,3*0*41";
w["\\documents\\my"]="5*0*5b";
w["ab"]="3*0*60";
w["able"]="9*10*-1$p,11*10*-1$e";
w["about"]="0*1*1e$9d,1*1*4c$7j,3*3*2m$42$ao$d5";
w["above"]="1*0*2v,5*0*11";
w["above."]="1*0*2u";
w["access"]="0*0*22,1*11*-1$g$7q,2*31*2$8$s,6*11*-1$7$n";
w["accessing"]="0*0*4h,1*0*24";
w["account"]="0*0*1n,1*0*am,3*0*hk,12*0*24";
w["account."]="3*0*hj";
w["active"]="0*1*4t$90";
w["add"]="1*0*ae,7*1*20$27,8*0*21,12*3*1e$27$2e$2l";
w["added"]="1*0*9u";
w["additional"]="0*0*73,3*1*am$bs";
w["additionally"]="3*2*b0$b8$bn";
w["address"]="3*1*he$hr";
w["address."]="3*0*hq";
w["adfs"]="0*3*4n$4v$7e$92";
w["administrator"]="0*0*63";
w["administrators"]="0*0*69";
w["advanced"]="1*0*37";
w["after"]="7*0*2k,8*0*2k";
w["aggregates"]="8*0*3s";
w["all"]="2*2*3k$44$47,11*0*3g";
w["allow"]="7*0*l,12*0*37";
w["allows"]="6*10*-1$5";
w["already"]="7*0*1b,10*0*3q";
w["also"]="1*0*4g,10*0*2f,12*1*2k$49";
w["alternatively"]="5*0*3c";
w["among"]="2*10*-1$j";
w["amount"]="8*1*t$1p";
w["analysis"]="1*0*bj,7*1*1k$3j";
w["another"]="7*10*-1$8,8*0*1m";
w["any"]="0*0*72,3*0*aa,7*0*40,8*0*3q,11*0*2d";
w["api"]="0*0*85";
w["apitoken"]="0*0*82";
w["appear"]="7*0*3a";
w["appears"]="0*2*3h$af$b1,1*0*1k,2*0*29,3*3*5p$e7$er$h5,5*0*46,8*0*3g,10*1*6n$74";
w["appears."]="0*1*3g$ae,1*0*1j,2*0*28,3*1*5o$e6,5*0*45,8*0*3f,10*1*6m$73";
w["appears:"]="3*0*h4";
w["appended"]="3*1*7p$90";
w["applicable"]="3*0*aq";
w["application"]="0*1*5d$5o";
w["application's"]="0*0*5n";
w["applications"]="0*0*2n,2*0*1f,3*1*24$58";
w["applications/cdata/cdata"]="0*0*2m,2*0*1e,3*1*23$57";
w["appropiate"]="9*10*-1$l";
w["appropriate"]="1*0*9v,10*0*2q";
w["apps"]="0*2*6o$6p$6q";
w["area"]="10*3*5s$8b$8v$97";
w["area."]="10*2*8a$8u$96";
w["arrow"]="10*0*8e";
w["assistance"]="11*0*46";
w["associated"]="3*0*hf,11*0*1q";
w["assume"]="0*0*11";
w["attach"]="3*0*il";
w["attached"]="2*0*4c";
w["authenticate"]="0*11*-1$f$q";
w["authentication"]="0*0*3i";
w["authscheme"]="0*0*3m";
w["automatically"]="10*0*91";
w["available"]="0*0*5i,8*0*32";
w["bar"]="10*1*33$5u";
w["base"]="0*0*4q";
w["be."]="3*0*8l";
w["been"]="1*0*9t,10*10*-1$c";
w["before"]="3*0*10,5*0*e,12*0*3c";
w["begin"]="10*0*5g";
w["below"]="5*10*-1$8,7*0*3c";
w["below."]="7*0*3b";
w["between"]="10*0*5n";
w["body"]="3*0*b2";
w["both"]="5*1*1t$2b";
w["bottom"]="10*0*5d";
w["box"]="0*0*9a,3*0*d2";
w["building"]="1*0*bt";
w["built"]="0*0*bd";
w["c"]="0*0*28,2*0*11,3*3*1l$4p$6u$ge,5*1*58$6h,12*0*41";
w["c1010"]="0*1*37$b0,1*0*3t,2*0*1v,3*2*1h$4l$eq,5*2*5t$68$7f";
w["c:\\logs"]="3*0*gd";
w["c:\\logs\\tableaulog.txt"]="3*0*6t";
w["c:\\program"]="0*0*27,2*0*10,3*1*1k$4o,5*0*6g";
w["c:\\users\\"]="5*0*57";
w["cache"]="3*0*ak";
w["can"]="0*1*6a$d0,1*3*3k$a3$ai$bp,3*3*16$30$8k$hm,5*3*g$3e$4j$7m,6*1*1f$2c,7*0*3p,8*13*-1$f$n$48$5o,10*12*-1$5$19$2e,11*1*1k$48,12*2*1m$2j$54";
w["canvas"]="1*0*9d";
w["canvas."]="1*0*9c";
w["card"]="10*0*5m";
w["cases"]="0*0*89";
w["categories"]="10*0*s";
w["cdata"]="0*6*2c$2d$2o$2p$35$3a$8j,1*0*3r,2*5*15$16$1g$1h$1t$22,3*13*1f$1p$1q$25$26$2f$4j$4t$4u$59$5a$5i$cj$eg,4*10*-1$7,5*4*3v$49$5r$66$7d,11*2*2q$32$3a";
w["cdata.jdbc.c1010.jar"]="0*0*34,1*0*3q,2*0*1s,3*1*1e$4i,5*2*5q$65$7c";
w["change"]="3*1*3a$hn,10*0*98,11*0*30";
w["changing"]="3*0*6e";
w["chart"]="10*1*23$99";
w["check"]="3*0*44";
w["checked"]="3*10*-1$5";
w["checking"]="3*0*37";
w["choose"]="5*0*34,10*0*1i";
w["choosing"]="10*0*7v";
w["click"]="0*2*33$9p$a0,1*2*1a$87$b2,2*0*1r,3*5*2l$dh$do$gs$ij$is,5*2*2r$32$4p,8*3*36$49$4m$4v,10*4*59$6s$78$83$8c";
w["client"]="2*10*-1$l";
w["client-side"]="2*10*-1$k";
w["clipboard"]="0*0*9r,3*0*dj";
w["close"]="0*0*a1,3*0*dp";
w["column"]="10*1*88$8s";
w["columns"]="10*0*89";
w["com"]="0*1*55$6j,1*0*2j,3*0*gq,5*0*1d";
w["command"]="5*0*3k";
w["commands"]="3*0*c6";
w["commands."]="3*0*c5";
w["common"]="11*10*-1$5";
w["communication"]="3*1*bd$bp";
w["compatible"]="1*0*2p,3*0*3q";
w["complete"]="1*1*2b$4o,3*0*h6,5*0*4u";
w["complete."]="5*0*4t";
w["comprehensive"]="6*0*i";
w["configuration"]="3*0*6h";
w["configure"]="0*30*0$5,1*2*36$45$7o,2*31*0$6$p";
w["configuring"]="1*0*7k";
w["connect"]="0*2*76$d5$d9,1*32*0$4$o$v,5*1*7o$7s,7*50*-1$-1$0$4$a$c,8*32*0$3$27$2f,9*10*-1$q,10*1*i$3j,12*4*13$2t$3h$3o$4j";
w["connected"]="6*0*17,7*0*2n,8*0*2n,10*0*3r";
w["connecting"]="11*10*-1$8,12*0*3d";
w["connection"]="0*12*3k$46$97$9f$9t$a5$ah$ak$b4$ba$c0$cm$cr,1*8*30$38$3i$3m$4h$4q$8i$8n$au,2*0*2f,3*10*5u$cv$d7$dd$dl$dt$e9$ec$eu$f2$fm,8*10*-1$a,10*1*43$48";
w["connection."]="0*0*9e,1*0*3h,3*0*d6";
w["connections"]="12*10*-1$j";
w["connector"]="0*0*26,1*12*-1$d$2o$8c,2*0*v,3*9*s$2h$2u$39$3p$5k$63$6g$cl$ei,4*10*-1$g,5*48*-1$1$3$d$k$1j$1o$26$41$4h$4o$5o,6*14*-1$4$g$1d$21$26,9*20*-1$-1$6$g,11*4*u$1e$2s$34$3c,12*41*-1$3$7$a$4p";
w["connector."]="3*1*2t$62,4*10*-1$f,5*12*-1$c$4g$4n,11*0*t";
w["connector:"]="0*0*25,2*0*u,5*0*1i";
w["connectors"]="5*0*5h,11*0*1s";
w["consists"]="9*10*-1$8";
w["contact"]="0*1*1k$60,1*0*aj,3*33*0$2$12$4c$j2,11*11*-1$j$4e,12*0*21";
w["containing"]="0*0*bh";
w["contains"]="0*0*9b,3*1*d3$f6";
w["content"]="0*0*6m";
w["context"]="8*0*56";
w["copied"]="0*0*9h,3*0*da";
w["copy"]="0*1*9q$9s,1*0*3v,3*1*di$dk,5*1*64$77";
w["copying"]="9*10*-1$e";
w["could"]="1*0*2d";
w["count"]="3*2*77$86$9d";
w["cp"]="5*0*7b";
w["create"]="3*12*-1$d$49$gt,7*0*1c,8*2*11$1t$2e,10*2*1a$31$3e,12*0*1b";
w["created"]="0*0*an,3*2*7k$8r$ef,7*1*1a$34";
w["credentials"]="0*0*c8,2*10*-1$h,3*0*fu";
w["currently"]="2*0*3p";
w["custom"]="8*19*-1$h$p$2g$38$3c$3i$4r$50$5c$5j,10*0*l,12*0*2m";
w["data"]="1*5*8g$8o$av$ba$bn$c5,3*1*be$bq,6*3*m$1i$2a$2m,7*0*3n,8*13*-1$c$u$1q$5s,10*51*-1$1$3$f$t$11$1b$1f$2i$2n$3g$49$5o$6a$7g,12*15*-1$i$3b$47$4o$4v$5a";
w["data."]="1*2*8f$bm$c4,6*1*29$2l,7*0*3m,10*11*-1$e$1e";
w["database"]="1*0*91,7*0*2s,8*0*2s,10*0*4g,12*0*v";
w["database."]="12*0*u";
w["date"]="3*1*7n$8u,10*6*66$6e$6k$71$7u$87$8h";
w["date."]="10*0*8g";
w["day"]="10*1*38$8j";
w["default"]="1*0*b1,2*1*3b$4f,3*3*82$9f$c7$ho,5*0*4l";
w["default."]="1*0*b0";
w["delete"]="0*0*at,3*0*en";
w["deleted"]="3*1*81$98";
w["deleted."]="3*0*80";
w["depending"]="3*0*99";
w["description"]="3*0*ib";
w["desired"]="1*0*9j,10*0*9c";
w["desired."]="10*0*9b";
w["detail"]="10*0*54";
w["detailed"]="3*0*id";
w["details"]="0*0*8h,3*0*bt";
w["details."]="0*0*8g";
w["dialog"]="0*1*9m$ad,1*2*1i$1o$82,3*1*de$e5,8*0*3e,10*1*6l$72";
w["different"]="0*1*r$7s,12*0*s";
w["difficulties"]="3*10*-1$a";
w["dimensions"]="10*3*q$1s$28$68";
w["directions"]="12*1*2d$3n";
w["directory"]="0*1*4u$91,3*4*1j$22$4n$56$gh,5*3*2h$4m$5j$6d,9*10*-1$n";
w["directory."]="3*0*gg,5*0*2g,9*10*-1$m";
w["directory:"]="5*0*6c";
w["discovered"]="10*11*-1$d$13";
w["discovery"]="6*0*u";
w["display"]="10*11*-1$9$5r";
w["do"]="1*0*9h,11*0*25,12*0*1k";
w["document"]="11*0*2l";
w["document."]="11*0*2k";
w["documents"]="5*0*5c";
w["does"]="12*0*4s";
w["domain"]="0*1*7n$7p,1*0*2h";
w["don"]="7*0*15";
w["don't"]="7*0*14";
w["double"]="0*0*32,2*0*1q,5*0*2q,8*0*35";
w["double-click"]="0*0*31,2*0*1p,5*0*2p,8*0*34";
w["down"]="0*1*3v$43,1*0*9o,2*0*2b,3*0*5r,10*4*4j$4s$62$8d$8n";
w["download"]="5*1*12$1s";
w["downloaded"]="5*0*55";
w["downloads"]="5*1*1p$2f";
w["drag"]="1*0*94,10*6*2g$51$55$6d$7j$86$8q";
w["driver"]="0*18*-1$e$1t$2f$2r$3c$4a$8l$aq$bf,1*3*3p$4l,2*4*18$1j$24$2j$4n,3*5*1s$28$36$50$5c$g7,4*10*-1$d,5*2*1m$1v$4b";
w["driver."]="0*1*49$ap,2*0*2i";
w["drivers"]="5*2*6m$6t$7k";
w["drop"]="0*0*3u,1*0*9n,10*4*2h$4i$4r$61$8m";
w["drop-down"]="0*0*3t,1*0*9m,10*3*4h$4q$60$8l";
w["dynamically"]="10*0*12";
w["each"]="11*0*38";
w["earlier"]="3*0*3r";
w["easily"]="12*0*55";
w["edit"]="8*1*3b$3h,11*0*1l";
w["editor"]="11*1*2g$2n";
w["editor."]="11*0*2f";
w["eliminated"]="11*0*1f";
w["email"]="3*1*hd$hp";
w["embed"]="0*1*5e$5j";
w["en"]="0*0*6k";
w["enable"]="2*10*-1$e";
w["end"]="3*1*7q$91";
w["ensure"]="8*0*1b";
w["enter"]="0*3*4f$65$71$c5,1*3*1p$1v$50$6c,2*1*2n$2u,3*6*66$78$8f$9o$fr$ht$ic,8*2*3k$59$5g";
w["entered"]="0*0*59";
w["environment"]="1*0*22";
w["error"]="11*0*18";
w["errors"]="3*0*ac";
w["errors."]="3*0*ab";
w["established"]="8*10*-1$9";
w["everything"]="3*0*ag";
w["example"]="0*1*8v$94,1*0*2a,3*0*6o,10*1*21$2u";
w["example:"]="0*1*8u$93";
w["execution"]="3*1*a7$cf";
w["experience"]="8*0*1d";
w["experiencing"]="3*0*g6";
w["experiencing.  the"]="3*0*g5";


// Auto generated index for searching.
w["explanation"]="3*0*ie";
w["extensive"]="6*0*s";
w["external"]="4*30*0$3";
w["false"]="2*0*4h";
w["familiar"]="12*0*3v";
w["familiarity"]="0*0*13";
w["ff"]="0*0*53";
w["field"]="0*3*3n$4e$5l$70,1*0*43,3*2*65$f4$hb,10*0*6j";
w["field."]="1*0*42";
w["fields"]="3*0*h9";
w["fields:"]="3*0*h8";
w["file"]="3*15*4b$6c$6n$6s$76$7r$85$8d$8j$92$9c$9i$9s$g9$im$ir,5*8*21$27$2v$33$3b$53$5v$6a$78,9*20*-1$-1$d$h,11*4*15$2c$2i$3k$3o";
w["file."]="3*2*6b$6m$iq,5*0*3a,11*0*3j";
w["files"]="0*0*2b,2*0*14,3*2*1o$4s$7c,5*2*2c$6k$73,11*0*1p";
w["files\\cdata\\cdata"]="0*0*2a,2*0*13,3*1*1n$4r";
w["files\\tableau\\drivers"]="5*0*6j";
w["filter"]="10*2*6i$6r$6v";
w["filter."]="10*0*6q";
w["filtering"]="10*0*7q";
w["filters"]="8*0*3r,10*1*6f$7l";
w["find"]="3*1*17$31,5*1*1k$5p,10*0*5k,11*0*2o";
w["finder"]="5*0*74";
w["first"]="12*0*1a";
w["folder"]="3*1*7m$8t,5*0*56";
w["follow"]="5*11*-1$6$47,12*1*2c$3m";
w["following"]="0*2*v$6c$8s,1*1*1q$86,2*1*2o$4s,3*1*cs$h7,5*1*p$6b,10*0*2t,11*12*-1$4$17$27";
w["following:"]="0*0*8r,1*0*85,2*0*4r,3*0*cr,5*0*o,11*0*26";
w["follows"]="3*0*9u,5*1*3o$7a";
w["follows:"]="3*0*9t,5*1*3n$79";
w["formatting"]="10*0*9a";
w["from"]="0*2*3s$7t$d7,1*30*2$6,5*2*19$3j$7u,8*0*55,10*9*14$1d$3l$3u$4f$4o$5v$67$7d$8k,12*0*3k";
w["full"]="6*1*1k$2e,12*11*-1$f$5e";
w["further"]="11*0*45";
w["get"]="1*0*br,6*1*1s$22,11*0*16";
w["getting"]="0*0*1f";
w["graph"]="10*0*34";
w["group"]="2*1*2r$30";
w["guide"]="6*0*1r,7*6*1s$2g";
w["has"]="1*0*9s";
w["have"]="0*0*1c,3*1*n$6j,5*0*n,7*1*17$2m,8*11*-1$8$2m,10*11*-1$b$3p,12*0*4a";
w["having"]="3*10*-1$9";
w["headers"]="3*0*at";
w["headers."]="3*0*as";
w["help"]="0*0*6h,3*0*j4";
w["helpful"]="3*0*bv";
w["here"]="0*1*5a$67,8*0*41,10*0*57";
w["here."]="0*0*66,8*0*40";
w["hidden"]="5*0*72";
w["high"]="6*0*k";
w["high-performance"]="6*0*j";
w["higher"]="3*0*cc";
w["hit"]="3*1*7h$8o";
w["how"]="10*1*1q$30";
w["htm"]="0*0*6s";
w["http"]="3*0*ar";
w["https"]="0*1*52$6g,1*0*2f,3*0*gn,5*0*1b";
w["https://ff.myorg.com"]="0*0*51";
w["https://help.okta.com/en/prod/content/topics/apps/apps_apps_page.htm#show"]="0*0*6f";
w["https://my.domain.1010data.com/prime-latest"]="1*0*2e";
w["https://oracle.com/java/"]="5*0*1a";
w["https://support.1010data.com"]="3*0*gm";
w["id"]="2*1*2s$31,11*0*3v";
w["identity"]="0*0*77";
w["ids"]="2*1*3o$49";
w["import"]="7*1*n$10";
w["important"]="8*0*15";
w["include"]="3*0*in";
w["included"]="3*0*ah";
w["includes"]="3*1*bh$c3,6*0*h";
w["including"]="1*0*25";
w["information"]="0*3*5h$9c$9g$bk,1*2*1s$4b$7i,2*0*2q,3*2*an$d4$d8,7*1*1v$2j,8*0*2d";
w["information."]="0*0*bj,7*1*1u$2i,8*0*2c";
w["information:"]="1*0*1r,2*0*2p";
w["insights"]="1*4*2r$56$6h$ac$c2,3*0*hh,6*0*19,7*6*1o$2c,10*10*-1$a,12*1*1v$58";
w["install"]="1*10*-1$a,5*43*-1$0$2$9$h$1g$4f";
w["installation"]="5*0*20";
w["installed"]="3*0*t,5*0*5m,11*0*o";
w["instance"]="0*0*50,11*0*39";
w["instead"]="8*0*2v";
w["instructions"]="5*11*-1$7$7r";
w["integrates"]="12*10*-1$b";
w["integration"]="6*0*r";
w["interested"]="10*0*2k";
w["interface"]="3*0*c4";
w["introduction"]="6*30*0$1";
w["issue"]="3*0*g3";
w["issues"]="11*10*-1$6";
w["its"]="10*0*15";
w["jar"]="0*0*38,1*0*3u,2*0*20,3*1*1i$4m,5*8*24$2l$2u$3i$3r$3u$5u$69$7g,9*10*-1$c";
w["java"]="5*4*v$15$1e$37$3p";
w["jdbc"]="0*24*-1$d$1s$2e$2q$36$3b$48$8k$9k$a7$ab$ao$av$be$bs,1*9*17$1c$1g$1m$3o$3s$4k$80,2*5*17$1i$1u$23$2h$4m,3*9*1g$1r$27$4k$4v$5b$dv$e3$ep$fi,4*10*-1$c,5*5*1l$1u$4a$5s$67$7e";
w["jdbc:c1010:"]="0*0*au,3*0*eo";
w["joins"]="8*0*3t";
w["just"]="12*0*2v";
w["keep"]="3*0*7e,5*0*4k";
w["keep."]="3*0*7d";
w["large"]="8*0*19";
w["later"]="3*0*3m,5*0*u";
w["later."]="3*0*3l";
w["latest"]="1*1*29$2l,3*0*34,5*0*13,11*0*1b";
w["left"]="10*0*5e";
w["level"]="3*1*9p$bc";
w["lib"]="0*1*2k$2v,2*1*1c$1n,3*3*20$2c$54$5g,5*0*61";
w["library"]="5*1*6r$7i";
w["likely"]="0*0*5t";
w["limit"]="3*1*7g$8n,8*1*s$1o";
w["link"]="0*1*5g$5k,5*0*1q";
w["link."]="0*0*5f";
w["list"]="0*1*41$7d,1*1*4p$9p,10*4*41$4k$4t$64$8p";
w["list."]="0*1*40$7c,10*2*40$63$8o";
w["lists"]="3*0*hc";
w["live"]="1*0*at,6*1*1h$28,12*10*-1$h";
w["log"]="0*0*ce,1*3*5e$5v$6p$78,3*33*-1$e$3b$4a$6a$6r$75$7b$7j$7v$84$8c$8i$8q$96$9b$9h$9r$a1$af$b1$b9$bo$gi$ip";
w["logfile"]="3*0*64";
w["logging"]="3*1*5s$f8";
w["login"]="0*1*4c$7v,2*0*3i";
w["loginurl"]="1*0*1t";
w["logs"]="3*1*6v$gf";
w["look"]="1*0*83";
w["looks"]="0*0*8p,2*0*4p,3*0*cp";
w["macos"]="0*0*30,2*0*1o,3*1*2d$5h,5*0*6p";
w["macos:"]="5*0*6o";
w["make"]="3*1*k$3d";
w["management"]="1*0*7r,2*30*3$9";
w["manager"]="0*0*1p,1*0*ao,12*0*26";
w["manager."]="0*0*1o,1*0*an,12*0*25";
w["manual"]="1*3*4n";
w["manuals"]="4*40*-1$2$5$9";
w["many"]="0*0*k,10*0*1r";
w["marks"]="10*0*5l";
w["max"]="3*4*74$83$8b$9a$9g";
w["maximum"]="3*1*79$8g";
w["may"]="1*0*34,3*1*97$bu,11*10*-1$d";
w["measures"]="10*3*u$1t$2c$7e";
w["measures."]="10*0*2b";
w["menu"]="8*0*58";
w["menu."]="8*0*57";
w["message"]="11*0*1a";
w["message:"]="11*0*19";
w["metadata"]="1*0*a9,6*0*t,10*0*17,12*0*1s";
w["metadata."]="10*0*16";
w["model"]="12*0*56";
w["more"]="0*0*8f,1*2*12$4a$7h,7*1*1t$2h,8*0*2b,10*0*27,12*0*3u";
w["most"]="0*1*5s$88,3*0*o,10*0*2p";
w["move"]="5*0*4v";
w["must"]="0*1*4p$5b,3*0*6i,5*0*m,12*0*19";
w["my"]="1*0*2g,5*0*5d";
w["myorg"]="0*0*54";
w["name"]="0*0*ci,1*5*52$5g$5k$61$62$69,3*3*68$6q$7t$94,8*0*5b";
w["name."]="1*0*68,3*1*7s$93";
w["necessary"]="5*0*1f";
w["need"]="0*0*5v,3*10*-1$c,5*0*70,11*0*44";
w["needing"]="11*10*-1$i";
w["negotiation"]="3*0*bk";
w["negotiation."]="3*0*bj";
w["new"]="3*3*7i$8p$dc$gu,8*1*37$5a";
w["next"]="0*0*bn,3*0*fd,10*0*6t";
w["normal"]="8*0*5v";
w["note"]="0*3*96$as$bm$cd,1*5*5a$5q$6l$73$9e$aq,3*1*cu$fc,8*0*1l";
w["note:"]="0*3*95$ar$bl$cc,1*4*59$5p$6k$72$ap,3*1*ct$fb,8*0*1k";
w["now"]="0*1*8o$d1,1*1*b4$bq,2*0*4o,3*1*co$f5,5*1*2d$7n,7*0*3q,8*0*5p";
w["number"]="3*4*19$2r$48$7a$a3";
w["number."]="3*0*47";
w["offering"]="12*10*-1$e";
w["ok"]="8*0*4n,10*1*79$84";
w["okta"]="0*9*57$5c$5r$62$68$6i$7i$7o$84$8t";
w["okta."]="0*0*5q";
w["old"]="3*0*95";
w["oldest"]="3*0*7u";
w["once"]="1*10*-1$8,3*0*2e,6*0*15,8*10*-1$6";
w["one"]="8*0*31";
w["onto"]="1*0*9a";
w["open"]="5*0*35,11*3*10$1t$28$3n";
w["operating"]="5*0*17";
w["option"]="1*0*15,12*0*4b";
w["optional"]="0*0*7k,1*0*32,3*0*i4";
w["options"]="0*10*-1$c,1*0*4j,3*0*fa";
w["options."]="3*0*f9";
w["oracle"]="5*0*1c";
w["order"]="12*0*11";
w["organization"]="0*0*1a";
w["organization."]="0*0*19";
w["organizations"]="0*0*l";
w["other"]="7*0*41,12*0*4t";
w["out"]="3*0*32";
w["owner"]="1*6*63$7b";
w["page"]="0*0*6r";
w["pane"]="10*2*5p$6c$7i";
w["pane."]="10*1*6b$7h";
w["particular"]="10*0*1j";
w["particularly"]="8*0*14";
w["password"]="0*0*ck,1*6*6a$6d$6q$6t$79$7a$7g,11*0*40";
w["password."]="0*0*cj,1*0*7f";
w["paste"]="0*0*aj,1*0*40,3*0*eb";
w["pasted"]="0*1*9i$b3,3*1*db$et";
w["path"]="3*0*67";
w["performance"]="6*0*l";
w["performed"]="10*0*7t";
w["permission"]="3*0*6k";
w["pie"]="10*0*22";
w["platform"]="1*4*2s$58$6j$ad$c3,3*0*hi,6*0*1a,7*6*1p$2d,12*1*20$59";
w["platform."]="1*1*57$6i";
w["please"]="0*0*1j,3*0*j";
w["pool"]="1*3*5u$65$77$7d,2*6*2v$35$37$3h$3m$3t$4b";
w["pool."]="2*0*34";
w["pools"]="1*1*7m$7t,2*44*-1$5$b$d$r$2d$2l$4k";
w["portal"]="0*0*4j,3*0*gl";
w["prefix"]="0*0*b2,3*0*es";
w["present"]="10*0*2m";
w["press"]="8*0*5f";
w["preview"]="8*1*4a$4d";
w["previous"]="11*0*1r";
w["prime"]="1*1*28$2k";
w["priority"]="3*1*i3$i6";
w["problem"]="3*1*ig$j9";
w["problem."]="3*0*j8";
w["problems"]="3*0*c2";
w["problems."]="3*0*c1";
w["proceed"]="8*0*5q";
w["prod"]="0*0*6l";
w["produce"]="3*0*g8";
w["program"]="0*0*29,2*0*12,3*3*1d$1m$4h$4q,5*1*38$6i";
w["prompt"]="5*0*3l";
w["prompted"]="11*0*3s";
w["prompts"]="5*0*48";
w["properies"]="1*0*31";
w["properties"]="0*6*6v$74$7m$8e$ai$b5$c1,1*2*39$3n$4t,3*3*ea$ev$f3$fn";
w["properties."]="0*0*7l,1*0*4s";
w["property"]="0*0*7f";
w["provide"]="8*0*3p";
w["provided"]="8*0*5m";
w["provider"]="0*3*17$3r$4m$78,1*1*5o$71";
w["provider."]="0*0*4l,1*1*5n$70";
w["provides"]="10*0*92";
w["quantitative"]="10*0*10";
w["queries"]="3*0*al,7*31*3$7$k,8*31*2$5$2a,12*18*-1$o$17$1h$2p$36$3j$3r$44$4l";
w["queries."]="12*10*-1$n";
w["query"]="3*0*a2,6*1*1g$27,7*19*-1$h$v$19$1f$1m$22$29$33$39$3t,8*20*-1$j$r$1v$2j$3o$4h$4l$4s$52$5e$5l,10*0*n,12*0*4e";
w["query."]="7*10*-1$g,8*1*3n$4g";
w["query:"]="8*0*2i";
w["question"]="3*0*if";
w["questions"]="0*0*1d";
w["quick"]="7*50*-1$2$6$f$j$u$18$1e$1l$21$28$32$38$3s,8*1*1u$29,12*16*-1$m$16$1g$2o$35$3i$3q";
w["quit"]="5*0*4q";
w["ready"]="1*10*-1$f";
w["real"]="6*0*p";
w["real-time"]="6*0*o";
w["recent"]="3*0*p";
w["refer"]="0*0*6b,4*10*-1$6";
w["reference"]="0*0*6e,1*3*4m,4*40*-1$1$4$8,11*0*1g";
w["reference:"]="0*0*6d";
w["refine"]="12*1*39$45";
w["release"]="2*0*46";


// Auto generated index for searching.
w["remains"]="8*0*1g";
w["remember"]="0*0*bv,3*1*cb$fl";
w["rename"]="8*1*4p$54";
w["repeat"]="10*0*7p,11*0*37";
w["replace"]="11*0*3f";
w["repository"]="5*0*5g";
w["repository\\connectors"]="5*0*5f";
w["reproduce"]="3*0*g2";
w["request"]="3*4*ap$b3$i2$ia$ii,11*0*4c";
w["request."]="3*2*i1$i9$ih,11*0*4b";
w["requester"]="3*0*ha";
w["required"]="0*1*75$7h,10*0*1u";
w["required."]="0*0*7g";
w["requires"]="10*0*25";
w["reset"]="2*0*3s";
w["resolve"]="3*0*j6";
w["response"]="3*0*b5";
w["response."]="3*0*b4";
w["responsive"]="8*0*1j";
w["responsive."]="8*0*1i";
w["result"]="12*0*52";
w["results"]="7*0*37,8*0*4b";
w["retaildemo"]="10*0*4v";
w["retrieves"]="1*0*8d";
w["retry"]="2*1*36$3f";
w["return"]="0*0*d2";
w["returned"]="3*0*a5";
w["right"]="5*0*31,8*0*4u,10*0*8f";
w["right-click"]="5*0*30,8*0*4t";
w["robust"]="6*0*v";
w["rows"]="3*0*a4,10*1*8t$95";
w["run"]="3*0*4g,5*2*2i$39$3f";
w["running"]="3*0*1c,9*10*-1$9";
w["s"]="1*1*96$b7";
w["sales"]="10*2*37$53$94";
w["sales_detail"]="10*0*52";
w["sam"]="1*5*5t$64$76$7c$7l$7s,2*48*-1$4$a$c$q$2c$2k$33$3g$3l$4a$4j";
w["same"]="3*1*7l$8s,12*0*4r";
w["satisfied"]="8*0*4j";
w["save"]="7*2*t$1d$1j,11*0*3h";
w["saved"]="7*11*-1$e$i,11*0*11";
w["schema"]="1*2*92$a1$ag,7*2*25$2a$2t,8*1*23$2t,10*0*4p,12*4*1d$1j$2b$2g$2s";
w["schema."]="1*0*a0,7*0*24,12*0*1i";
w["screen"]="0*2*3f$bg$cu,1*0*8t,2*0*27,3*3*2k$5n$em$h3,5*0*44,10*0*4e";
w["screen."]="0*0*ct,1*0*8s,3*0*el,10*0*4d";
w["scroll"]="0*0*42,2*0*2a,3*0*5q";
w["seamlessly"]="6*10*-1$a,12*10*-1$c";
w["section"]="0*2*3j$45$b7,2*2*2e$2m$4l,3*12*-1$7$5t$f1,10*1*69$7f";
w["section."]="0*0*b6,3*0*f0";
w["see"]="0*2*8c$b9$cp,1*5*14$44$4f$7n$8l$9i,3*0*2p,7*1*1i$26,8*2*26$42$4c,10*0*46,11*0*4d,12*0*4i";
w["select"]="0*0*3o,1*2*3l$8u$bd,2*1*39$3v,3*1*3v$i5,5*0*36,7*2*2q$31$3d,8*2*2q$43$53,10*8*3t$4l$4u$5t$65$6o$75$7b$8i";
w["selecting"]="8*0*30";
w["selection"]="8*0*5t";
w["selects"]="1*0*as";
w["self"]="1*0*a6,12*0*1p";
w["self-service"]="1*0*a5,12*0*1o";
w["semicolon"]="0*0*7a";
w["semicolon-separated"]="0*0*79";
w["send"]="3*10*-1$f";
w["separated"]="0*0*7b";
w["series"]="10*0*p";
w["server"]="1*0*10,10*0*3v";
w["service"]="1*0*a7,12*0*1q";
w["sessions"]="2*0*45";
w["set"]="0*1*1q$c4,2*10*-1$g,3*0*fq,6*2*1l$1u$2f";
w["set."]="0*0*c3,3*0*fp";
w["setting"]="1*0*3a";
w["settings"]="0*0*5p";
w["setup"]="5*7*23$2k$2t$3h$3t$43$4d$4s,9*50*-1$-1$1$3$7$b";
w["setup.jar"]="5*4*22$2j$2s$3g$3s,9*10*-1$a";
w["shared"]="1*0*7p,2*40*-1$1$7$i";
w["sheet"]="1*0*be,7*0*3e,10*1*5a$5q";
w["shelf"]="10*1*6h$7n";
w["shelf."]="10*1*6g$7m";
w["should"]="3*0*d9,11*0*3r";
w["show"]="0*0*6t,5*0*71";
w["showing"]="10*0*35";
w["shows"]="10*0*2v";
w["side"]="2*10*-1$m";
w["sign"]="0*46*-1$3$8$j$p$16$1i$20$bq$c9,1*2*3f$48$88,3*1*fg$fv";
w["sign-on"]="0*33*2$7$o$15$1v,1*1*3e$47";
w["sign-on."]="0*10*-1$i";
w["signing"]="0*0*7r";
w["similar"]="0*0*8q,1*0*84,2*0*4q,3*0*cq";
w["simple"]="10*1*32$3f";
w["single"]="0*44*-1$1$6$h$n$14$1h$1u,1*1*3d$46,2*10*-1$f,10*0*39";
w["size"]="3*2*8e$8h$9j";
w["slower"]="3*0*ce";
w["smooth"]="8*0*1h";
w["software"]="0*0*s";
w["some"]="0*0*12";
w["source"]="1*0*8p,3*1*bg$br,8*10*-1$d,10*0*4a";
w["source."]="3*0*bf";
w["sources"]="12*0*51";
w["sources."]="12*0*50";
w["specifications"]="4*10*-1$b";
w["specify"]="8*10*-1$g";
w["sql"]="1*2*9k$9q$a8,6*0*11,8*51*-1$1$4$i$q$22$2h$39$3d$3j$3m$4f$51$5d$5k,10*0*m,12*6*1c$1r$2a$2r$43$4d$4k";
w["sql-92"]="6*0*10";
w["ssl"]="3*0*bi";
w["sso"]="0*50*-1$4$9$b$21$3q$44$4b$4i$6u$7u$8d$bi$cg,1*8*3g$49$4e$5d$5i$5m$6o$6s$6v";
w["sso."]="1*0*4d";
w["start"]="0*0*a4,1*2*m$r$bh,3*1*a6$ds,7*0*3h";
w["started"]="0*0*1g,1*0*bs,6*0*24";
w["statements"]="8*0*44";
w["step"]="0*0*d3";
w["steps"]="0*0*10,10*0*7r";
w["still"]="3*10*-1$8";
w["store"]="10*4*3a$7c$7k$7o$80";
w["string"]="0*3*98$9v$al$bb,1*1*4i$4r,3*2*d0$dn$ed";
w["string."]="0*0*9u,3*0*dm";
w["subdirectory"]="5*0*63";
w["subdirectory."]="5*0*62";
w["subject"]="3*1*hs$hu";
w["submit"]="3*1*h1$it,11*0*49";
w["subset"]="7*0*o";
w["successful"]="0*0*cn,1*0*8j,10*0*44";
w["sum"]="10*0*93";
w["support"]="3*47*-1$1$3$i$14$4f$gk$go$i0$i8$j0,6*0*14,11*42*-1$1$3$m$4a$4f,12*10*-1$g";
w["support."]="3*11*-1$h$13,6*0*13,11*10*-1$l";
w["support:"]="3*0*4e";
w["sure"]="3*1*l$3e";
w["syntax"]="8*0*46";
w["syntax."]="8*0*45";
w["system"]="3*0*v,5*0*18";
w["systems"]="0*0*u";
w["systems."]="0*0*t";
w["t"]="3*0*5v,7*0*16";
w["tab"]="0*1*3l$47,1*1*8q$bg,2*0*2g,3*0*2n,7*0*3g,10*1*4b$5c";
w["table"]="0*0*dd,1*3*95$9r$b6$b9,7*2*q$30$42,8*1*25$61,10*0*k,12*1*2f$34";
w["table's"]="1*0*b8";
w["table."]="0*0*dc,8*1*24$60,12*0*33";
w["tableau"]="0*6*24$9o$a3$bu$cb$cs$d8,1*57*-1$-1$3$7$c$l$n$t$2n$8r$9b$ar$bv,2*0*t,3*17*r$2g$2s$38$3h$3o$3u$40$43$5j$61$6f$ck$dg$dr$eh$fk$g1,4*10*-1$e,5*23*-1$b$j$q$1h$1n$25$40$5e$5n$6l$6s$7j$7p$7v,6*25*-1$-1$3$e$f$1c$1n$20$2h,7*3*s$12$2p$43,8*14*-1$l$10$1f$1s$2p,9*50*-1$-1$0$2$5$10,10*17*-1$7$o$1m$1n$2l$3m$4c$90,11*21*-1$b$s$12$1d$1m$1v$23$29$2r$33$3b$3p,12*57*-1$-1$1$5$9$d$q$12$3g$3l$4h$4u$5d";
w["tableau's"]="6*1*1m$2g,12*0*5c";
w["tableau."]="0*1*9n$ca,1*11*-1$k$s,3*2*3t$df$g0,6*10*-1$d,7*0*r,8*10*-1$k,9*10*-1$v,11*10*-1$a,12*1*3f$4g";
w["tableau:"]="7*0*11";
w["tableaulog"]="3*1*70$gb";
w["tableaulog.txt"]="3*0*ga";
w["tables"]="1*12*-1$j$9l$af,6*10*-1$9,8*1*1a$33,9*10*-1$t,10*0*56,12*13*-1$l$15$1f$28";
w["taco"]="5*1*2a$52,9*10*-1$k";
w["taken"]="3*0*a9";
w["team"]="3*0*j1";
w["technical"]="4*10*-1$a";
w["tells"]="10*0*1o";
w["tenten"]="10*0*4m";
w["terminal"]="5*1*3m$76";
w["terminate"]="2*0*43";
w["text"]="0*0*99,3*0*d1,11*2*2e$2m$31";
w["them"]="1*0*41,6*10*-1$c,11*10*-1$g,12*0*2u";
w["therefore"]="12*0*10";
w["threads"]="2*10*-1$o";
w["threads."]="2*10*-1$n";
w["through"]="12*0*3e";
w["ticket"]="3*1*gv$h2";
w["time"]="0*0*bo,3*3*7o$8v$a8$fe,6*0*q";
w["times"]="3*0*ch";
w["times."]="3*0*cg";
w["token"]="0*0*87";
w["token."]="0*0*86";
w["tool"]="1*0*aa,12*0*1t";
w["tools"]="6*1*1q$2j";
w["tools."]="6*0*1p";
w["toolset"]="12*0*5h";
w["toolset."]="12*0*5g";
w["topics"]="0*0*6n";
w["total"]="10*0*36";
w["transport"]="3*0*bb";
w["transport-level"]="3*0*ba";
w["troubleshoot"]="11*10*-1$f";
w["troubleshooting"]="3*11*-1$6$c0,11*30*0$2";
w["trs"]="7*1*1h$36";
w["trs."]="7*1*1g$35";
w["true"]="2*1*3a$40";
w["twb"]="11*4*14$1o$2b$2h$3i";
w["two"]="0*0*7j";
w["txt"]="3*1*71$gc";
w["type"]="10*0*1l";
w["unable"]="5*0*2o";
w["under"]="0*0*ag,1*0*u,3*3*73$8a$9m$e8,7*0*2v";
w["underlying"]="12*0*t";
w["unlimited"]="3*0*89";
w["unlimited."]="3*0*88";
w["unnecessary"]="0*0*8b";
w["unnecessary."]="0*0*8a";
w["up"]="0*0*1r,1*0*3b,6*0*1v";
w["update"]="1*1*b3$b5,11*0*22";
w["updated"]="1*0*bc,11*0*p";
w["updated."]="1*0*bb";
w["url"]="0*6*4d$4g$4o$4r$58$64$80,1*1*20$2c";
w["use"]="0*11*-1$a$m,1*3*54$6f$99$a4,2*0*3r,5*0*75,6*1*1j$2d,7*0*3r,8*0*o,10*10*-1$6,12*1*r$1n";
w["use."]="2*0*3q";
w["used"]="8*1*v$1r";
w["user"]="0*1*c7$ch,1*4*51$5f$5j$60$67,2*2*3n$48$4e,3*1*6d$ft,7*6*1r$2f,11*0*3u";
w["user's"]="7*6*1q$2e";
w["user."]="2*0*4d";
w["username"]="1*0*4u,5*0*5a";
w["users"]="5*0*59";
w["using"]="1*3*5c$5s$6n$75,3*0*1b,5*0*7q,6*0*25,8*0*1e,10*0*2o,12*31*0$4$5b";
w["ustom"]="12*0*42";
w["usual"]="11*1*21$42";
w["usual."]="11*1*20$41";
w["usually"]="10*1*r$v";
w["v15"]="1*0*2t";
w["value"]="2*1*3c$4g,3*0*9e";
w["verbosity"]="3*9*9n$9q$9v$ad$ai$au$b6$bl$c8$cd";
w["version"]="1*0*26,3*5*q$18$2q$35$3g$46,5*0*14,11*0*q";
w["versions"]="3*0*3s";
w["very"]="8*0*18";
w["via"]="6*0*1b";
w["video"]="5*10*-1$5";
w["view"]="6*10*-1$b";
w["visualization"]="1*0*bk,6*1*1o$2i,7*0*3k,10*5*1k$20$24$2s$3i$5j,12*0*5f";
w["visualization."]="10*2*1v$2r$5i";
w["visualization:"]="10*0*3h";
w["visualizations"]="8*0*13,10*11*-1$8$1c";
w["visualizations."]="8*0*12";
w["visualize"]="1*10*-1$h,8*0*5r,10*30*0$2";
w["visualizing"]="12*0*4n";
w["wait"]="1*0*8a";
w["want"]="1*0*35,2*1*3e$42,8*0*3v";
w["watch"]="5*10*-1$4";
w["way"]="7*10*-1$9,8*0*1n";
w["we"]="3*0*6p";
w["were"]="8*0*5u";
w["what"]="3*0*33";
w["when"]="2*0*3j,3*1*7f$8m,5*0*4r,8*0*16,10*1*g$1g,11*11*-1$7$3l";
w["where"]="5*0*5k";
w["while"]="1*0*8b";
w["window"]="10*0*5f";
w["windows"]="0*0*2l,2*0*1d,3*1*21$55,5*0*6f";
w["windows:"]="5*0*6e";
w["wish"]="1*0*98";
w["within"]="0*0*5m,9*10*-1$u,12*0*4f";
w["without"]="11*10*-1$h";
w["wizard"]="5*0*4e";
w["words"]="11*0*2p";
w["workbook"]="1*0*c0,7*0*45,11*2*13$1n$2a";
w["workbook."]="7*0*44";
w["workbooks"]="11*1*1u$24";
w["working"]="8*0*17";
w["works"]="12*0*4q";
w["would"]="7*0*3v,12*0*31";
w["write"]="3*0*6l";
w["writing"]="12*1*40$4c";
w["xml"]="11*0*2j";
w["xsales"]="10*0*8r";
w["year"]="10*1*3b$70,11*0*1i";
w["year."]="11*0*1h";
w["years"]="10*0*6p";
w["you"]="0*9*1b$5u$7q$am$b8$bc$bp$c2$co$cv,1*36*-1$-1$9$e$13$23$33$3j$53$5b$5r$6e$6m$74$8k$97$9g$a2$ah$bo,2*1*3d$41,3*33*-1$-1$4$b$m$11$15$1a$2o$2v$ee$ff$fo$g4$hl$j3$j5,5*8*f$l$2n$3d$4i$54$5l$6v$7l,6*15*-1$6$16$1e$1t$23$2b,7*4*m$13$2l$3o$3u,8*26*-1$-1$7$e$m$2l$3u$47$4i$5n,9*10*-1$o,10*19*-1$4$h$18$1h$1p$2d$2j$3o$45$7s,11*17*-1$c$n$v$1j$3m$3q$43$47,12*7*18$1l$2i$30$38$3t$48$53";
w["your"]="0*12*18$1l$3p$4k$4s$61$83$8i$c6$cf$cl$cq$da,1*14*3c$5h$5l$66$6r$6u$7e$8e$8h$8m$8v$ak$bi$bl$bu,2*0*32,3*11*u$3f$45$69$ci$f7$fs$hg$hv$i7$io$j7,5*1*16$2e,6*0*2k,7*3*23$2r$3i$3l,8*17*-1$b$1c$2r$3l$4e$4k$4q$5i,9*10*-1$r,10*2*42$47$5h,11*0*3t,12*4*22$29$2q$3a$46";
w["~/library/tableau/drivers"]="5*1*6q$7h";

return {'fil': fil, 'w': w}})();
