window['java_sdk'] = (function() {
var w = new Object();
fil = new Array();
fil["0"]= "BasicUsage.html@@@Basic usage@@@Creating a new application with the 1010data Java SDK includes setting up your application framework, establishing a session, submitting a query, receiving results, and releasing the session...";
fil["1"]= "BestPractices.html@@@Best practices@@@There are several recommended best practices when using the 1010data Java SDK...";
fil["2"]= "ConnectingViaProxy.html@@@Connecting via proxy@@@If your application is connecting to the Insights Platform via a proxy server, your gateway string needs to be modified to include the proxy server&apos;s address\n    (MYCORPORATEPROXYADDRESS)...";
fil["3"]= "ExceptionHandling.html@@@Exception handling@@@The Java SDK contains the TenTenException class...";
fil["4"]= "FlushPool.html@@@flushPool (Flush a SAM pool)@@@Sets a flag for all busy IDs so that the next time you call reluid on one of those IDs, the ID also logs out...";
fil["5"]= "GettingStarted.html@@@Getting started@@@Before you start writing your first application that connects to the 1010data Insights Platform, you need to download the Java SDK files...";
fil["6"]= "Introduction.html@@@Introduction@@@The 1010data Java SDK enables developers to create applications that connect their applications to the 1010data Insights Platform for processing and retrieval of big data...";
fil["7"]= "LoginTypes.html@@@Login Types@@@The login type determines what happens when the application establishes a connection to the Insights Platform and another session is currently active...";
fil["8"]= "Multithreading.html@@@Shared Access Management (SAM)@@@Shared Access Management (SAM) pools enable a single set of credentials to be shared between client side threads to leverage multiple threads of parallelism on the 1010data Insights Platform...";
fil["9"]= "Query.html@@@Running a query@@@Your application should act as an intermediary between your users and the platform to pass in a query and receive the queried data...";
fil["10"]= "Reference.html@@@Reference@@@The 1010data Java SDK Reference contains the details about its classes and functions...";
fil["11"]= "Release.html@@@Cleaning up a session@@@Applications that use the 1010data Java SDK must manually clean up their 1010data Insights Platform sessions after use. You can do this using the logout method...";
fil["12"]= "ResetPool.html@@@resetPool (Reset a SAM pool)@@@You can release all of the user IDs in a specific SAM pool using the resetPool function...";
fil["13"]= "Results.html@@@Accessing query results@@@After querying the 1010data Insights Platform, your application receives a set of results, which are accessible through the ResultSet object...";
fil["14"]= "Session.html@@@Establishing a session@@@A 1010data Insights Platform session is an instance of a particular user ID logged into the platform within a certain environment. A session is comprised of a dedicated set of resources that can serve a single request at a time...";
fil["15"]= "Setup.html@@@Setting up an application@@@Your application setup includes importing any required libraries, including the 1010data Java SDK and creating a class that holds the functions that interact with the Insights Platform and the Main method...";
fil["16"]= "SupportRequests.html@@@Support requests@@@If you are using the 1010data SDKs, 1010data offers full support...";
fil["17"]= "Troubleshooting.html@@@Troubleshooting and support@@@You can try to troubleshoot issues with the Java SDK. If you cannot resolve the issue, file a support request...";
fil["18"]= "UpdatingExistingApplications.html@@@Updating existing applications@@@The latest 1010data Java SDK is designed for developing applications in a style that is more closely compatible with Java development, specifically object-oriented programming...";
fil["19"]= "WarmPool.html@@@warmPool (Warm a SAM pool)@@@One way to reduce the amount of time it takes to retrieve results from the 1010data Insights Platform is to &quot;warm&quot; the SAM pool by logging in IDs ahead of time...";
var doStem = false;searchLoaded = true;// Porter stemmer in Javascript. Few comments, but it's easy to follow against the rules in the original
// paper, in
//
//  Porter, 1980, An algorithm for suffix stripping, Program, Vol. 14,
//  no. 3, pp 130-137,
//
// see also http://www.tartarus.org/~martin/PorterStemmer

// Release 1 be 'andargor', Jul 2004
// Release 2 (substantially revised) by Christopher McKenzie, Aug 2009


var stemmer = (function(){
	var step2list = {
			"ational" : "ate",
			"tional" : "tion",
			"enci" : "ence",
			"anci" : "ance",
			"izer" : "ize",
			"bli" : "ble",
			"alli" : "al",
			"entli" : "ent",
			"eli" : "e",
			"ousli" : "ous",
			"ization" : "ize",
			"ation" : "ate",
			"ator" : "ate",
			"alism" : "al",
			"iveness" : "ive",
			"fulness" : "ful",
			"ousness" : "ous",
			"aliti" : "al",
			"iviti" : "ive",
			"biliti" : "ble",
			"logi" : "log"
		},

		step3list = {
			"icate" : "ic",
			"ative" : "",
			"alize" : "al",
			"iciti" : "ic",
			"ical" : "ic",
			"ful" : "",
			"ness" : ""
		},

		c = "[^aeiou]",          // consonant
		v = "[aeiouy]",          // vowel
		C = c + "[^aeiouy]*",    // consonant sequence
		V = v + "[aeiou]*",      // vowel sequence

		mgr0 = "^(" + C + ")?" + V + C,               // [C]VC... is m>0
		meq1 = "^(" + C + ")?" + V + C + "(" + V + ")?$",  // [C]VC[V] is m=1
		mgr1 = "^(" + C + ")?" + V + C + V + C,       // [C]VCVC... is m>1
		s_v = "^(" + C + ")?" + v;                   // vowel in stem

	return function (w) {
		var 	stem,
			suffix,
			firstch,
			re,
			re2,
			re3,
			re4,
			origword = w;

		if (w.length < 3) { return w; }

		firstch = w.substr(0,1);
		if (firstch == "y") {
			w = firstch.toUpperCase() + w.substr(1);
		}

		// Step 1a
		re = /^(.+?)(ss|i)es$/;
		re2 = /^(.+?)([^s])s$/;

		if (re.test(w)) { w = w.replace(re,"$1$2"); }
		else if (re2.test(w)) {	w = w.replace(re2,"$1$2"); }

		// Step 1b
		re = /^(.+?)eed$/;
		re2 = /^(.+?)(ed|ing)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			re = new RegExp(mgr0);
			if (re.test(fp[1])) {
				re = /.$/;
				w = w.replace(re,"");
			}
		} else if (re2.test(w)) {
			var fp = re2.exec(w);
			stem = fp[1];
			re2 = new RegExp(s_v);
			if (re2.test(stem)) {
				w = stem;
				re2 = /(at|bl|iz)$/;
				re3 = new RegExp("([^aeiouylsz])\\1$");
				re4 = new RegExp("^" + C + v + "[^aeiouwxy]$");
				if (re2.test(w)) {	w = w + "e"; }
				else if (re3.test(w)) { re = /.$/; w = w.replace(re,""); }
				else if (re4.test(w)) { w = w + "e"; }
			}
		}

		// Step 1c
		re = /^(.+?)y$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(s_v);
			if (re.test(stem)) { w = stem + "i"; }
		}

		// Step 2
		re = /^(.+?)(ational|tional|enci|anci|izer|bli|alli|entli|eli|ousli|ization|ation|ator|alism|iveness|fulness|ousness|aliti|iviti|biliti|logi)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			suffix = fp[2];
			re = new RegExp(mgr0);
			if (re.test(stem)) {
				w = stem + step2list[suffix];
			}
		}

		// Step 3
		re = /^(.+?)(icate|ative|alize|iciti|ical|ful|ness)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			suffix = fp[2];
			re = new RegExp(mgr0);
			if (re.test(stem)) {
				w = stem + step3list[suffix];
			}
		}

		// Step 4
		re = /^(.+?)(al|ance|ence|er|ic|able|ible|ant|ement|ment|ent|ou|ism|ate|iti|ous|ive|ize)$/;
		re2 = /^(.+?)(s|t)(ion)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(mgr1);
			if (re.test(stem)) {
				w = stem;
			}
		} else if (re2.test(w)) {
			var fp = re2.exec(w);
			stem = fp[1] + fp[2];
			re2 = new RegExp(mgr1);
			if (re2.test(stem)) {
				w = stem;
			}
		}

		// Step 5
		re = /^(.+?)e$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(mgr1);
			re2 = new RegExp(meq1);
			re3 = new RegExp("^" + C + v + "[^aeiouwxy]$");
			if (re.test(stem) || (re2.test(stem) && !(re3.test(stem)))) {
				w = stem;
			}
		}

		re = /ll$/;
		re2 = new RegExp(mgr1);
		if (re.test(w) && re2.test(w)) {
			re = /.$/;
			w = w.replace(re,"");
		}

		// and turn initial Y back to y

		if (firstch == "y") {
			w = firstch.toLowerCase() + w.substr(1);
		}

		return w;
	}
})();// Auto generated list of analyzer stop words that must be ignored by search.
stopWords = new Array();
stopWords[0]= "but";
stopWords[1]= "be";
stopWords[2]= "with";
stopWords[3]= "such";
stopWords[4]= "then";
stopWords[5]= "for";
stopWords[6]= "no";
stopWords[7]= "will";
stopWords[8]= "not";
stopWords[9]= "are";
stopWords[10]= "and";
stopWords[11]= "their";
stopWords[12]= "if";
stopWords[13]= "this";
stopWords[14]= "on";
stopWords[15]= "into";
stopWords[16]= "a";
stopWords[17]= "or";
stopWords[18]= "there";
stopWords[19]= "in";
stopWords[20]= "that";
stopWords[21]= "they";
stopWords[22]= "was";
stopWords[23]= "is";
stopWords[24]= "it";
stopWords[25]= "an";
stopWords[26]= "the";
stopWords[27]= "as";
stopWords[28]= "at";
stopWords[29]= "these";
stopWords[30]= "by";
stopWords[31]= "to";
stopWords[32]= "of";

var indexerLanguage="en";

// Auto generated index for searching.
w["%path%"]="5*0*40";
w["&"]="18*1*8n$ll";
w["'\\n'"]="3*0*3s,8*0*e0";
w["'\\t'"]="3*0*3h,8*0*dl,18*0*o6,19*0*cd";
w["+"]="3*1*6e$8f,8*5*bn$c8$ca$cc$cf$eg,18*19*87$ai$ak$al$bd$bh$bi$ct$d1$d2$dp$dt$du$fi$fv$g3$g4$hm$l5$p0,19*5*84$86$88$a0$a2$d7";
w["-"]="8*3*32$38$3c$3i,18*0*c4,19*1*2i$3d";
w["-2"]="18*3*br";
w["."]="1*1*12$84,3*0*h,4*1*1u$2r,5*1*1u$25,6*0*4b,7*0*12,9*0*1o,10*0*k,11*1*4n$5c,13*1*t$12,14*1*2l$4e,17*0*31,18*3*3k$4f$4r$5b,19*1*38$3h";
w[".group"]="8*0*a4";
w[".length;"]="18*0*hc";
w[".ops"]="8*0*ag";
w[".output"]="8*0*ak";
w[".owner"]="8*0*a0";
w[".password"]="8*0*a8";
w[".table"]="8*0*ac";
w[".url"]="8*0*9s";
w["/"]="3*1*f$10,11*1*56$5e";
w["//"]="18*39*9b$9s$a5$bq$cb$ea$ev$f6$gf$gv$ht$i2$mn,19*3*au";
w["0"]="3*3*2f$32$3d$61,8*3*cj$d6$dh$h5,11*0*8e,13*0*3j,18*10*7q$99$ac$b4$ck$gm$h6$ko$n0$nm$o1,19*4*7l$9c$b7$bt$c8";
w["1"]="1*3*26$3u$4g$5o,3*1*5t$65,5*0*14,8*1*h0$ha,9*0*56,11*0*7i,13*0*2p,18*9*7m$7u$9r$ar$bo$d8$e4$ga$kk$ks,19*2*7h$7p$99";
w["1."]="1*0*3t";
w["1.6"]="5*0*13";
w["10"]="1*1*2n$37,9*1*32$57,11*0*7j,13*0*2q";
w["10%"]="1*0*2m";
w["1000"]="18*1*eo$mi,19*0*ap";
w["10000"]="18*0*93";
w["1010"]="3*0*8c,12*0*t,18*0*os,19*1*1v$d3";
w["1010data"]="0*10*-1$7,1*11*-1$a$4a,2*3*2a$45$4s$5i,3*1*n$4d,4*0*3b,5*12*-1$c$n$1n,6*23*-1$-1$2$b$47,8*12*-1$q$33$fa,9*4*1f$1l$4g,10*15*-1$2$g,11*21*-1$-1$8$f$6s,13*11*-1$8$23,14*11*-1$4$5g,15*10*-1$f,16*21*-1$-1$6$8$p,18*20*-1$7$s$15$1l$25$2r$4v$5h$8c$is$la,19*12*-1$h$2o$8d";
w["1900"]="18*1*8p$ln";
w["2"]="3*1*5i$79,8*0*hf,18*5*7b$bs$c5$k9,19*0*7t";
w["3"]="8*0*gk,19*0*75";
w["33"]="1*0*36";
w["40000"]="18*0*96";
w["5000"]="18*8*eg$el$mq$n6,19*4*b1$bd";
w["5000."]="18*3*ef";
w["50000"]="8*0*ge";
w["6"]="5*0*15";
w["7"]="17*0*12";
w["7:"]="17*0*11";
w["8"]="19*0*a6";
w["8080"]="2*1*41$4o";
w[":"]="2*1*1v$25";
w[";"]="3*6*2g$33$4i$4p$4u$62$66,8*8*ck$d7$ff$fr$g6$gg$h6$hb$hg,9*1*4m$59,11*2*72$7l$8f,13*2*29$2s$3k,14*0*5m,18*17*7r$7v$8h$8r$91$94$97$9a$ep$gn$h7$kp$kt$lf$lp$lv$n1$nn,19*8*7m$7q$7u$8j$8u$95$9a$b8$bu";
w["<"]="1*0*32,3*3*2i$35$5h$74,8*4*cm$d9$g9$gd$gj,9*0*51,11*1*7d$8h,13*1*2k$3m,18*11*7a$8j$ab$b3$cj$es$gp$h9$k8$lh$n3$np,19*3*74$8l$ba$c0";
w[">"]="1*0*38,3*2*3c$78$7a,8*1*dg$gf,9*0*58,11*0*7k,13*0*2r,18*2*8q$lo$o0,19*1*8t$c7";
w["@"]="2*0*23";
w["about"]="1*0*5g,6*0*42,10*10*-1$8,11*0*4f,14*0*2f";
w["access"]="1*3*1t$22$2b$5q,6*0*33,8*41*-1$1$5$9$4v,11*0*4k,18*0*58";
w["access."]="1*0*1s";
w["accessible"]="13*10*-1$h";
w["accessing"]="1*0*18,13*30*0$3";
w["account"]="8*0*4k";
w["acquire"]="18*0*4m";
w["acs"]="8*2*fm$fo$g2";
w["act"]="9*10*-1$7";
w["action"]="18*3*a4";
w["active"]="7*13*-1$j$1c$25$2b,14*1*1c$1j";
w["active."]="7*10*-1$i";
w["activity"]="19*0*53";
w["actu"]="4*0*2u";
w["add"]="5*0*2j";
w["added"]="2*0*1k";
w["additional"]="16*9*2q";
w["additionally"]="11*0*3q,19*0*15";
w["address"]="2*10*-1$n,14*0*44,17*0*20";
w["admin"]="4*0*2k";
w["after"]="11*10*-1$j,13*10*-1$6,18*3*3q$a2";
w["afterward"]="11*0*54";
w["again"]="19*0*4t";
w["again."]="19*0*4s";
w["against"]="9*0*2h";
w["ahamilton"]="2*0*3s";
w["ahead"]="19*10*-1$p";
w["all"]="1*1*76$7q,3*0*m,4*11*-1$a$21,8*1*6m$fn,12*10*-1$b,18*3*fa";
w["allocated"]="4*0*2c";
w["alltypes"]="5*0*4i";
w["ally"]="4*0*2v";
w["already"]="19*0*4h";
w["also"]="2*0*11,4*11*-1$n$1b,5*0*3c,6*1*28$3m";
w["always"]="16*0*3i";
w["amount"]="19*10*-1$b";
w["analytical"]="9*0*10";
w["another"]="7*10*-1$f";
w["answer"]="6*0*3r";
w["any"]="2*0*55,4*0*2b,5*0*3j,12*0*1m,14*0*45,15*10*-1$b,16*0*31,18*0*2c";
w["any:"]="16*0*30";
w["appears"]="11*0*45";
w["application"]="0*20*-1$-1$6$e,1*0*6j,2*10*-1$7,5*10*-1$a,6*1*r$31,7*10*-1$a,8*0*7a,9*10*-1$5,13*11*-1$c$u,15*40*-1$2$5$7,18*0*1g";
w["application."]="6*0*30";
w["applications"]="6*22*-1$-1$8$a$1b$3c,11*10*-1$6,18*43*-1$2$5$c$p$1i$4h";
w["applications."]="6*0*3b";
w["applied"]="9*0*2s";
w["applied."]="9*0*2r";
w["appropriate"]="5*0*50";
w["args"]="3*3*45$5f$60$64,8*4*f2$gh$h4$h9$he,9*0*4a,11*0*6m,13*0*1t,14*0*5a,18*7*63$78$7p$7t$je$k6$kn$kr,19*4*64$72$7k$7o$7s";
w["array"]="19*0*2e";
w["asarray"]="3*0*2v,8*0*d3,18*0*nj,19*0*bq";
w["ask"]="1*0*3c";
w["associated"]="19*0*31";
w["attached"]="14*0*15";
w["attaches"]="7*0*16";
w["automatically"]="19*0*5a";
w["automatically."]="19*0*59";
w["available"]="5*0*q,19*0*v";
w["back"]="4*0*27,11*0*2r";
w["baseball"]="18*1*8v$lt";
w["based"]="18*0*47";
w["basequery"]="19*5*2f$2j$5i$6v$98$9e";
w["basic"]="0*30*0$2,6*1*2j$2o";
w["batting"]="18*1*90$lu";
w["because"]="19*0*30";
w["been"]="4*1*1m$25";
w["before"]="1*0*1n,5*10*-1$4";
w["beginning"]="2*0*1l";
w["being"]="9*0*2f";
w["below"]="1*0*2j,19*0*5c";
w["best"]="1*42*-1$0$2$6$27$3n,9*0*36,18*1*32$3g";
w["better"]="1*1*1d$7e";
w["between"]="2*0*1o,8*10*-1$i,9*11*-1$9$54,11*0*7g,13*0*2n";
w["big"]="6*10*-1$g";
w["bigint"]="5*0*4r";
w["bin"]="2*3*2d$48$4v$5l,3*0*4g,5*1*22$26,8*0*fd,9*0*4j,11*0*6v,13*0*26,14*0*5j,18*1*8f$ld,19*0*8g";
w["binary"]="18*3*bv";
w["block"]="18*10*6s$95$en$f3$fj$ge$gj";
w["block..."]="18*3*gi";
w["block;"]="18*0*gd";
w["both"]="6*0*16";
w["brackets"]="2*1*1q$3d";
w["browser"]="17*0*1b";
w["buffered"]="1*0*43";
w["busy"]="4*11*-1$b$2d";
w["call"]="4*11*-1$h$2p";
w["called"]="11*0*3l";
w["calls"]="18*0*4p";
w["can"]="1*1*67$7p,3*0*c,4*1*1r$2o,5*0*1d,11*10*-1$n,12*10*-1$9,14*11*-1$n$25,16*0*43,17*11*-1$5$2n,19*2*1a$1h$47";
w["cannot"]="17*11*-1$d$2i";
w["cases"]="4*0*1p,8*0*4l";
w["casts"]="5*0*4j";
w["catch"]="3*6*g$11$85,8*5*e6,18*5*ol,19*5*cs";
w["caught"]="3*0*d";
w["caution"]="12*0*22";
w["caution."]="12*0*21";
w["census"]="8*1*fl$g1";
w["certain"]="14*10*-1$f";
w["cgi"]="2*3*2c$47$4u$5k,3*0*4f,8*0*fc,9*0*4i,11*0*6u,13*0*25,14*0*5i,18*1*8e$lc,19*0*8f";
w["change"]="1*0*63";
w["check"]="5*0*3n,18*3*3n$9v";
w["class"]="3*18*-1$9$b$j$l$1v,6*0*44,8*5*8h,9*5*3m,11*5*u$62,13*5*19,14*5*4l,15*16*-1$j$u$1i,18*11*21$2g$5r$j6,19*5*5s";
w["class."]="3*10*-1$8,18*1*20$2f";
w["classes"]="10*10*-1$a";
w["classpath"]="5*0*2p";
w["clean"]="1*1*e$o,11*12*-1$d$3u$4s";
w["cleaned"]="11*1*1l$52";
w["cleaning"]="1*0*v,11*31*0$3$2u";
w["client"]="8*10*-1$j";
w["close"]="3*1*82$8o,8*1*e4$ep,18*0*pa,19*0*dh";
w["closely"]="18*10*-1$f";
w["code"]="16*12*l$3c$3h$3n$42,18*4*5e$a1$in,19*0*8r";
w["codes"]="18*0*3p";
w["colinfo"]="18*3*75$da$de$fk";
w["collector"]="11*0*3t";
w["column"]="8*0*g4,18*3*dm$h1";
w["columns"]="5*0*56,18*3*fc";
w["columns."]="18*3*fb";
w["columnwriter"]="5*0*54";
w["com"]="2*3*2b$46$4t$5j,3*1*1r$4e,5*2*1o$2c$2k,8*1*8d$fb,9*1*3i$4h,11*1*5u$6t,13*1*15$24,14*1*4h$5h,15*0*1e,18*3*5n$8d$j2$lb,19*1*5o$8e";
w["com.tentendata.javasdk1010.*;"]="18*0*5m";
w["com.tentendata.javasdk1010v2.*;"]="3*0*1q,8*0*8c,9*0*3h,11*0*5t,13*0*14,14*0*4g,15*0*1d,18*0*j1,19*0*5n";
w["command"]="4*0*2s,5*0*47";
w["company"]="4*0*2n,17*0*1o";
w["company's"]="17*0*1n";
w["compatible"]="18*10*-1$g";
w["completion"]="1*0*k";
w["complex"]="5*0*67";
w["compressed"]="18*3*bu";
w["compressed_binary"]="18*3*bt";
w["comprised"]="14*10*-1$j";
w["concurrency"]="8*0*6p";
w["configuration"]="2*0*u";
w["configured"]="8*0*55,17*0*1c";
w["connect"]="6*10*-1$9,7*0*22,14*0*20,17*0*16";
w["connect."]="14*0*1v";
w["connected"]="1*0*7r";
w["connecting"]="2*40*-1$0$3$8,14*1*3p$4b,17*0*2a";
w["connection"]="7*14*-1$c$18$1e$1r$27,12*0*u,19*0*20";
w["connects"]="5*10*-1$b";
w["construct"]="8*1*5r$71";
w["construction"]="8*0*5f";
w["constructor"]="8*0*2r,16*0*1l";
w["contact"]="2*0*2v,17*0*1l";
w["containing"]="9*0*2j,16*0*m";
w["contains"]="3*10*-1$6,5*2*1v$28$4e,6*1*k$1v,10*10*-1$6,11*0*v,14*0*40,15*0*v,18*1*5d$im";
w["continues"]="11*0*21";
w["copy"]="5*0*3e";
w["core"]="16*0*37";
w["corporate"]="2*3*31$3v$4m$5d,17*0*1f";
w["couldn"]="17*0*14";
w["couldn't"]="17*0*13";
w["create"]="1*1*6f$6n,5*0*5m,6*11*-1$7$2q,11*0*1c,18*4*37$9c$ba";
w["created"]="9*0*23,14*1*1g$2p";
w["created."]="14*0*1f";
w["creates"]="8*0*4p";
w["creating"]="0*10*-1$4,8*1*u$16,15*10*-1$i,16*0*3d";
w["credentials"]="2*2*2u$3o$56,8*11*-1$g$45,14*0*47";
w["credentials."]="14*0*46";
w["currently"]="7*10*-1$h,14*1*1i$21";
w["data"]="1*8*15$19$1m$2d$3f$42$4v$52$58,3*2*2t$37$3m,5*1*4m$60,6*10*-1$i,8*5*7c$d1$db$dq$fk$g0,9*12*-1$i$m$p,18*3*fu$nh$nr$oc,19*3*92$bo$c2$cj";
w["data."]="1*0*57,6*10*-1$h,9*10*-1$h";
w["data.length;"]="3*0*36,8*0*da,18*0*nq,19*0*c1";
w["data:"]="18*0*ft";
w["database"]="4*0*1h,8*0*6o";
w["date"]="5*1*4s$4u";
w["datum"]="3*0*2s,8*0*d0,18*0*ng,19*0*bn";
w["deallocated"]="11*0*5r";
w["deallocated."]="11*0*5q";
w["decimal"]="5*0*4q";
w["dedicated"]="14*10*-1$k";
w["default"]="19*1*35$3e";
w["demo"]="3*0*4m,18*1*8u$ls";
w["denote"]="2*0*3e";
w["department"]="2*0*36";
w["department."]="2*0*35";
w["depending"]="2*0*p,11*0*18";
w["designed"]="8*0*5a,18*10*-1$a";
w["destroyed"]="11*0*4c";
w["destroyed."]="11*0*4b";
w["destructors"]="11*0*3k";
w["details"]="10*10*-1$7";
w["determines"]="7*10*-1$6,14*0*1n";
w["developed"]="18*1*q$1j";
w["developers"]="6*11*-1$6$3d";
w["developing"]="6*1*p$3j,18*10*-1$b";
w["development"]="6*0*15,18*10*-1$i";
w["different"]="8*1*1t$7f,11*0*1f,19*0*2s";
w["directories"]="5*0*3t";
w["directory"]="5*2*2l$2t$3k";
w["directory."]="5*0*2s";
w["displays"]="5*0*66";
w["disrupt"]="12*0*1l";
w["do"]="1*1*3h$61,11*10*-1$o";
w["doc"]="5*0*23";
w["documentationexample"]="9*0*3n,11*0*63,13*0*1a,14*0*4m,15*1*t$1j";
w["does"]="2*0*3a,4*0*2t,11*0*3i,18*0*28";
w["done"]="3*0*8u,8*0*6g,9*0*12,11*0*5m";
w["down"]="18*3*f1";
w["download"]="5*11*-1$h$1e";
w["downloads"]="5*1*1p$5v,8*0*7h";
w["draw"]="1*0*35";
w["due"]="4*0*15";
w["dumps"]="16*0*38";
w["during"]="8*1*5c$5m";
w["e"]="3*1*87$8g,8*1*e8$eh";
w["each"]="1*3*4j$6d$6k$7g,14*0*22,15*0*18,18*7*34$3r$gg$h0";
w["echo"]="5*1*3v$43";
w["effectively"]="16*0*t";
w["either"]="6*0*1d";
w["elev"]="3*0*77";
w["enable"]="8*10*-1$d";
w["enables"]="6*11*-1$5$14,16*0*o";
w["end"]="19*0*52";
w["ended"]="14*0*1q";
w["ends"]="7*0*1k";
w["ensure"]="1*0*1q,8*0*65,11*0*50";
w["enter"]="17*0*k";
w["environment"]="5*0*3q,14*10*-1$h";
w["environment."]="14*10*-1$g";
w["err"]="3*1*5l$8a,8*0*eb,18*9*7e$81$af$b7$cn$di$fp$kc$kv$oq,19*2*78$80$d1";
w["error"]="3*0*8e,8*0*ed,16*0*33,17*1*t$10,18*0*3o";
w["error:"]="3*0*8d";
w["established"]="3*0*6u,7*2*1g$1t$29";
w["established."]="7*2*1f$1s$28";
w["establishes"]="7*10*-1$b";
w["establishing"]="0*10*-1$g,14*30*0$2,18*0*3i";
w["even"]="1*0*5l";
w["every"]="8*0*61,18*3*a3";
w["exactly"]="18*3*1s$9d";
w["example"]="1*0*2h,2*3*39$3q$4i$5a,3*0*t,5*10*4a$6a,8*0*6u,9*0*20,11*0*4o,13*0*m,14*1*2m$3n,15*0*s,18*1*5c$il,19*1*3i$5b";
w["example:"]="2*2*3p$4h$59";
w["examplequery"]="9*2*2a$5l$5t,11*1*82$8a,13*2*r$37$3f";
w["examples"]="2*9*3h,5*2*24$4c$4h,6*1*2e$3q";
w["examples:"]="5*0*4g";
w["exc"]="18*1*on$p1,19*1*cu$d8";
w["exception"]="3*31*0$2$12,18*2*46$4d$ot,19*0*d4";
w["exception-based"]="18*0*45";
w["exceptions"]="3*0*r,16*0*39";
w["exceptions."]="3*0*q";
w["existing"]="7*2*19$1l$23,18*31*1$4$1f";
w["exit"]="3*0*5s,8*0*gv,18*6*7l$aq$bn$d7$e3$g9$kj,19*1*7g$a5";
w["expedite"]="16*0*2s";
w["expedites"]="16*0*3p";
w["experience"]="1*0*7c";
w["explains"]="6*0*36";
w["extends"]="8*5*8j";
w["fail"]="18*0*b9";
w["failed"]="18*2*ah$cp$dk";
w["fails"]="7*0*21,14*0*1u";
w["failure"]="18*0*fr";
w["familiar"]="6*0*3g";
w["feasible"]="16*0*3j";
w["feature"]="5*10*33$38,16*0*1f";
w["few"]="11*0*1e";
w["fhfa"]="19*1*93$94";
w["file"]="5*2*1g$2i$3f,9*0*3d,16*12*g$13$18$1r$2o,17*10*-1$g,19*1*3b$3m";
w["file."]="16*0*2n";
w["files"]="5*12*-1$l$2b$64,6*0*2i,8*0*7m";
w["files."]="5*11*-1$k$63,6*0*2h,8*0*7l";
w["filewriter"]="3*2*1o$7k$7n,8*2*85$hs$ia";
w["filter"]="1*0*1k";
w["finally"]="3*10*7v$8i,8*5*ej,11*6*57$5f$8s,18*10*id$p4,19*5*db";
w["finished"]="18*3*i5";
w["finrows"]="18*7*6u$98$er$f8$fh$gc";
w["first"]="5*10*-1$9,6*0*t,9*0*31";
w["flag"]="4*10*-1$9";
w["flush"]="4*30*1$5";
w["flushed"]="4*0*3p";
w["flushpool"]="4*35*0$4$1t$1v$2q$34$3r";
w["folder"]="5*3*27$2e$2m$4d";
w["folder."]="5*0*2d";
w["folders"]="5*0*21";
w["follow"]="17*0*28";
w["following"]="3*0*s,4*0*37,5*0*4f,6*0*1n,8*0*2t,12*0*o,19*0*1q";
w["four"]="14*0*32";
w["framework"]="0*10*-1$f";
w["from"]="1*1*49$4q,5*1*1j$5s,8*0*7d,19*12*-1$g$2t$4c";
w["full"]="2*0*3m,8*0*6i,16*10*-1$a";


// Auto generated index for searching.
w["fully"]="8*1*5j$69,11*0*46";
w["function"]="4*0*35,6*0*45,9*0*26,12*12*-1$k$m$1k,15*1*14$16,19*1*t$1o";
w["function."]="9*0*25,12*10*-1$j,15*0*13";
w["functions"]="10*10*-1$c,15*10*-1$l";
w["functions."]="10*10*-1$b";
w["g"]="8*0*3d";
w["g3n3ricpwd"]="2*0*3t";
w["garbage"]="11*0*3s";
w["gateway"]="2*11*-1$f$1m,4*0*3c,9*1*4d$5e,11*1*6p$7s,12*0*s,13*1*20$31,14*3*36$3u$5d$63,16*0*2b,18*5*6d$89$9o$ji$l7$m5,19*5*1u$40$68$8a$9l$ac";
w["gateway;"]="18*1*6c$jh,19*0*67";
w["gathering"]="9*0*j";
w["general"]="1*0*59";
w["generated"]="16*0*h";
w["get"]="18*3*dl$f9";
w["getdatarm1010"]="18*0*ff";
w["gets"]="11*0*5o";
w["getting"]="5*30*0$2,6*0*1t,18*0*fs";
w["getuid1010"]="18*0*4q";
w["getusername"]="8*0*bp";
w["going"]="1*0*3b";
w["greatly"]="16*0*3o";
w["group"]="3*0*57,4*3*2i$3e$3j$3o,8*15*1k$2d$2j$2l$37$39$3j$96$9k$a5$a7$bf$gt$hd$ho$i6,12*4*11$16$1b$1d$1i,19*15*23$27$2b$2d$3u$43$4q$6n$7e$7r$82$85$9o$dp$dv$e2";
w["group;"]="3*0*56,8*1*95$a6,19*0*6m";
w["groupid"]="4*1*3q$3v";
w["gt"]="18*1*8o$lm";
w["guarantee"]="11*0*3j";
w["guarantees"]="11*0*5h";
w["guide"]="6*0*j,17*0*2f";
w["guide."]="17*0*2e";
w["gw"]="2*3*2e$49$50$5m,3*0*4h,8*0*fe,9*0*4k,11*0*70,13*0*27,14*0*5k,18*1*8g$le,19*0*8h";
w["handle"]="8*0*57";
w["handle."]="8*0*56";
w["handles"]="18*0*2u";
w["handling"]="3*31*1$3$14,18*0*4e";
w["handling:"]="3*0*13";
w["happens"]="7*10*-1$8";
w["has"]="1*0*7i,8*0*4b";
w["have"]="1*1*5e$6c,4*1*1l$24,5*0*11,8*1*3s$4j,11*0*38,14*1*11$27,19*0*4k";
w["helpful"]="6*0*1s";
w["helpful:"]="6*0*1r";
w["hgranger"]="2*0*4k";
w["higher"]="5*0*16";
w["holds"]="15*10*-1$k";
w["how"]="6*0*2b";
w["however"]="16*0*3k";
w["http"]="2*3*1r$3r$4j$5b";
w["https"]="2*3*28$43$4q$5g,3*0*4b,5*0*1l,8*0*f8,9*0*4e,11*0*6q,13*0*21,14*0*5e,18*1*8a$l8,19*0*8b";
w["https://www2.1010data.com/cgi-bin/gw"]="2*3*27$42$4p$5f";
w["https://www2.1010data.com/downloads/tools/java2/javasdk1010v2.zip"]="5*0*1k";
w["i"]="1*0*4l,3*3*2e$2h$2l$2r,8*4*ci$cl$cp$cv$gc,9*0*55,11*4*7h$8d$8g$8k$8p,13*4*2o$3i$3l$3p$3u,18*8*gl$go$gt$hb$hk$mv$n2$n9$nf,19*3*b6$b9$bg$bm";
w["id"]="4*12*-1$m$33$3h,8*2*2n$3a$3g,12*1*14$1t,14*11*-1$b$24,19*0*2c";
w["id."]="4*0*32,8*0*2m";
w["idiom"]="11*0*5g";
w["ids"]="4*22*-1$-1$c$l$11$1e,8*2*40$4d$6k,11*0*3d,12*10*-1$d,19*13*-1$o$14$1e$4v";
w["ids."]="8*0*3v,19*0*13";
w["implemented"]="11*0*59";
w["import"]="3*25*15$1a$1f$1k$1p,8*25*7n$7s$81$86$8b,9*5*3g,11*5*5s,13*5*13,14*5*4f,15*5*1c,18*10*5l$j0,19*5*5m";
w["important"]="11*0*36,18*3*hu";
w["importing"]="15*10*-1$a";
w["in."]="19*1*1g$4f";
w["include"]="2*11*-1$j$14,16*29*11$21$2p$2v$3a";
w["include:"]="2*0*13";
w["includes"]="0*10*-1$a,6*0*29,15*10*-1$9,16*1*1e$1m";
w["including"]="15*10*-1$e,16*0*d";
w["incrementally"]="1*0*3j";
w["individual"]="8*0*21";
w["info"]="18*0*do";
w["info:"]="18*0*dn";
w["information"]="1*0*t,2*0*1j,5*0*5r,6*2*l$20$41,9*0*1b,10*0*e,11*0*4e,14*1*2e$49,16*9*e$2r,17*0*2t,18*2*3e$4b$55,19*0*4n";
w["input"]="19*0*4l";
w["inputstream"]="3*0*1e";
w["insights"]="1*1*4b$7t,2*10*-1$9,5*11*-1$d$4k,6*12*-1$c$1h$3k,7*10*-1$d,8*11*-1$r$34,9*1*1g$69,11*12*-1$g$1h$9b,13*11*-1$9$4a,14*12*-1$5$38$6i,15*10*-1$n,19*11*-1$i$dt";
w["installation"]="5*9*1b";
w["installed"]="5*0*17";
w["instance"]="14*10*-1$8,18*1*1u$2d";
w["instantiate"]="18*1*1r$2b";
w["instead"]="8*0*20";
w["instructions"]="6*0*2a,17*0*29";
w["int"]="3*5*30,8*5*d4,18*25*68$6q$gk$h4$nk,19*10*9h$br";
w["integer"]="5*0*4p";
w["interact"]="1*0*73,15*10*-1$m";
w["intermediary"]="9*10*-1$8";
w["intersections"]="1*0*3g";
w["introduction"]="6*30*0$1";
w["invocation"]="18*0*3u";
w["invocation."]="18*0*3t";
w["invoked"]="11*0*17";
w["invoked."]="11*0*16";
w["io"]="3*3*18$1d$1i$1n,8*2*7q$7v$84";
w["ioexception"]="3*2*19$2b$47,8*2*7r$e7$f4";
w["ip"]="17*0*1v";
w["isolating"]="16*0*3e";
w["issue"]="16*0*1u,17*11*-1$f$2l";
w["issues"]="17*10*-1$8";
w["it."]="14*1*16$2c,15*0*1b,18*3*i7";
w["its"]="1*1*6o$7j,10*10*-1$9,18*0*38";
w["j"]="3*4*31$34$39$3b$3n,8*4*d5$d8$dd$df$dr,18*8*h5$h8$he$hl$nl$no$nt$nv$od,19*4*bs$bv$c4$c6$ck";
w["java"]="0*10*-1$8,1*11*-1$b$45,3*15*-1$4$o$17$1c$1h$1m,5*16*-1$i$o$12$1h$2o$39$51,6*15*-1$3$12$25$48,8*4*58$7p$7u$83$88,10*15*-1$3$h,11*11*-1$9$3g,15*10*-1$g,16*0*i,17*10*-1$9,18*29*-1$-1$8$h$t$16$1m$26$2s$42$50$5i$it";
w["java.io.filewriter;"]="3*0*1l,8*0*82";
w["java.io.inputstream;"]="3*0*1b";
w["java.io.ioexception;"]="3*0*16,8*0*7o";
w["java.io.writer;"]="3*0*1g,8*0*7t";
w["java.lang.thread;"]="8*0*87";
w["java2"]="5*0*1r";
w["javasdk1010"]="18*7*1v$2e$5p$65$9f$9k";
w["javasdk1010v2"]="3*0*1t,5*0*1s,8*0*8f,9*0*3k,11*0*60,13*0*17,14*0*4j,15*0*1g,18*0*j4,19*0*5q";
w["javasdkv2"]="5*0*2g";
w["join"]="8*1*ii$ik";
w["k"]="9*0*4l,11*0*71,13*0*28,14*0*5l,19*0*8i";
w["keep"]="18*0*1h";
w["kill"]="7*1*u$1j,11*0*2c";
w["kind"]="1*0*5p";
w["lang"]="8*0*89";
w["language"]="9*3*14$1j$2l$2u,19*0*2q";
w["larger"]="1*0*4s";
w["latest"]="18*10*-1$6";
w["length"]="3*1*38$5g,8*1*dc$gi,18*4*79$gs$hd$k7$ns,19*1*73$c3";
w["leverage"]="8*10*-1$m";
w["libraries"]="15*10*-1$d";
w["library"]="5*0*2a,6*0*2g";
w["likely"]="1*0*5s";
w["limit"]="18*6*ec$mo,19*3*av";
w["limitless"]="8*0*4g";
w["limitless."]="8*0*4f";
w["line"]="5*0*49";
w["line."]="5*0*48";
w["link"]="18*19*67$9i$9m$am$b0$be$bj$c0$c6$cf$cu$d3$db$dq$dv$e7$fe$g0$g5$if";
w["link;"]="18*0*66";
w["linux"]="5*1*u$46";
w["linux."]="5*0*t";
w["list"]="5*0*3s";
w["listens"]="17*0*23";
w["log"]="4*0*30,8*2*3r$46$76,16*11*f$12$17$2m,19*2*3a$3l$9r";
w["logfile"]="16*2*1n$2g$2h,19*1*3c$45";
w["logged"]="11*1*1r$29,14*10*-1$c,19*2*1f$4e$4i";
w["logging"]="16*0*1i,19*11*-1$n$4b";
w["logging."]="16*0*1h";
w["login"]="3*0*6c,5*0*3i,7*41*-1$0$2$4$m,11*0*19,14*3*1l$2g$2j$3h,18*16*6a$85$9l$aa$aj$ao$bg$bl$ch$d0$d5$ds$e1$g2$g7$ih$l3,19*0*83";
w["login1010"]="18*0*9n";
w["login;"]="18*0*69";
w["logins"]="6*0*39";
w["logintype"]="1*0*82,3*0*6o,7*5*q$t$10$14$1i$1v,8*0*1l,9*0*5h,11*3*1t$2b$2e$7v,13*0*34,14*1*3l$66,16*0*2e,18*0*m8,19*0*af";
w["logintype.kill"]="7*1*s$1h,11*0*2a";
w["logintype.nokill"]="7*1*v$1u,11*0*2d";
w["logintype.possess"]="1*0*81,7*1*p$13,11*0*1s,14*0*3k";
w["logout"]="1*0*m,11*14*-1$q$10$4p$5b$8v";
w["logout1010"]="18*0*ig";
w["logs"]="4*10*-1$o,14*0*19,16*1*20$22,19*0*u";
w["logs."]="16*0*1v";
w["long"]="3*5*2d,8*5*ch,11*5*8c,13*5*3h,18*10*71$mu,19*5*b5";
w["longer"]="9*0*38,11*0*33";
w["lots"]="6*0*3p";
w["macos"]="5*1*s$45";
w["macro"]="9*3*13$1i$2k$2t,19*0*2p";
w["main"]="3*0*43,8*0*f0,9*1*24$48,11*0*6k,13*0*1r,14*1*2q$58,15*12*-1$p$10$24,18*1*61$jc,19*0*62";
w["make"]="18*3*a6";
w["management"]="6*0*34,8*40*-1$2$6$a,11*0*4l,18*0*59";
w["manual"]="9*3*1n";
w["manually"]="11*10*-1$c,18*0*4l";
w["material"]="6*0*3o";
w["math"]="18*1*ej$n4,19*0*bb";
w["may"]="1*0*1i,2*2*10$4c$53,4*1*s$1a,6*0*1p,8*0*4i,11*0*42,12*0*1p,14*0*10";
w["means"]="17*0*18";
w["memory"]="4*0*16";
w["message"]="17*0*u";
w["messages"]="16*0*34";
w["method"]="1*1*n$6b,9*0*1r,11*12*-1$s$11$4q,14*0*2s,15*11*-1$r$11,18*0*3s";
w["method."]="1*0*6a,11*10*-1$r,14*0*2r,15*10*-1$q";
w["might"]="11*0*1k";
w["min"]="18*1*ek$n5,19*0*bc";
w["model"]="18*0*49";
w["model."]="18*0*48";
w["modified"]="2*10*-1$i";
w["more"]="1*0*s,6*0*3f,8*0*4s,9*0*1a,10*0*d,11*0*4d,14*1*2d$48,16*0*s,17*0*2s,18*13*-1$e$3d$4a$54";
w["most"]="1*0*5r";
w["msg1010"]="18*4*an$bk$d4$e0$g6";
w["much"]="1*0*7d";
w["multiple"]="1*0*6t,5*0*6c,8*10*-1$n,11*0*39,14*0*12,18*0*2k";
w["multithreaded"]="1*0*6i,6*1*1a$3a";
w["must"]="5*0*10,11*10*-1$b,16*0*15,18*0*v";
w["my"]="2*2*3u$4l$5c";
w["mycorporateproxyaddress"]="2*13*-1$o$24";
w["n"]="3*0*3t,8*0*e1,18*0*eh";
w["name"]="1*0*30,2*1*16$4g,4*0*3n,8*1*23$2i,9*2*4q$64$6c,11*2*76$96$9e,12*0*1a,13*2*2d$45$4d,14*3*3b$5q$6d$6l,19*3*25$2n$e1$e3";
w["name."]="8*0*2h";
w["names"]="8*0*g5";
w["ndividually"]="1*0*4n";
w["ndividually."]="1*0*4m";
w["need"]="2*2*12$2l$2q,5*11*-1$g$3d,6*2*n$22$40,18*3*9u";
w["needed"]="11*0*34";
w["needs"]="2*10*-1$h";
w["new"]="0*10*-1$5,1*5*2s,3*15*6j$70$7m,4*0*13,6*0*q,7*2*17$1o$2c,8*36*v$1f$5s$ba$br$hj$hr$i1$i9,9*10*27$5c$5m,11*10*7q$83,13*10*2v$38,14*7*1d$1r$2t$61,16*5*29,18*15*9j$m3$mb,19*20*97$9d$aa$ai";
w["newquery1010"]="18*0*b1";
w["next"]="4*10*-1$e";
w["nokill"]="7*1*11$20,11*0*2f";
w["none"]="19*1*37$3g";
w["not."]="17*0*1k";
w["note"]="2*0*38,9*0*62,11*0*94,13*0*43,14*0*6b,19*0*dn";
w["note:"]="2*0*37,9*0*61,11*0*93,13*0*42,14*0*6a,19*0*dm";
w["noting"]="1*0*5k";
w["null"]="3*1*5b$8l,8*3*8q$8v$b6$em,18*7*df$fm$i1$ia$k2$p7,19*1*6r$de";
w["null;"]="3*0*5a,8*2*8p$8u$b5,18*1*i9$k1,19*0*6q";
w["number"]="1*0*40,2*0*58,8*0*4c,18*3*ed";
w["numrows"]="3*0*2k,8*1*ce$co,11*0*8j,13*0*3o,18*4*73$e6$em$et$n8,19*0*bf";
w["numrows;"]="18*0*72";
w["object"]="1*4*6h$6s$72$74$7n,8*4*11$1c$26$5e$5u,9*2*19$22$29,13*11*-1$l$o,14*1*2o$2v,16*0*1s,18*21*-1$l$13$23$2p$3c$40$4t$74$9h$iq,19*0*34";
w["object-oriented"]="18*16*-1$k$12$22$2o$3v$4s$ip";
w["object."]="1*1*6r$7m,8*0*1b,9*0*18,13*10*-1$k,18*3*3b$9g,19*0*33";
w["objects"]="1*0*7o,5*0*5p,8*0*75,11*1*41$4a,14*0*14,19*2*2g$2k$2v";
w["obtain"]="17*0*1q";
w["occur"]="9*0*r";
w["occurred"]="18*0*ov,19*0*d6";
w["occurred:"]="18*0*ou,19*0*d5";
w["offers"]="16*10*-1$9";
w["once"]="4*0*23";
w["one"]="1*0*77,4*10*-1$j,5*1*58$5f,7*1*1b$1p,14*2*1e$1s$28,17*0*q,18*6*1t$9e$f2,19*10*-1$8";
w["only"]="2*0*4d,14*0*26";
w["operating"]="17*0*1i";
w["ops"]="8*7*8t$9o$ah$aj$bv$g8$hq$i8,9*1*50$5q,11*1*7c$87,13*1*2j$3c";
w["ops;"]="8*0*ai";
w["organization"]="17*0*o";
w["oriented"]="18*16*-1$m$14$24$2q$41$4u$ir";
w["original"]="18*2*r$1k$5g";
w["our"]="6*0*3h";
w["out"]="3*7*29$3f$3k$3q$68$6r$7g$8s,4*10*-1$q,6*0*2d,8*2*bh$c4$gn,9*0*41,11*0*6d,13*0*1k,14*0*51,15*0*1t,18*4*hh$hq$o4$oa$oh,19*3*9t$cb$ch$co";
w["out."]="4*10*-1$p";
w["outfilename"]="3*1*4r$7o";
w["output"]="3*2*7l$7t$81,8*7*9a$9q$al$an$dj$do$du$e3";
w["output1"]="8*0*ht";
w["output2"]="8*0*ib";
w["output;"]="8*1*99$am";
w["overconsumption"]="4*0*18";
w["overconsumption."]="4*0*17";
w["own"]="1*1*6p$7k,8*0*3t,18*0*39";
w["owner"]="4*5*2j$2m$3f$3i$3k$3t,8*15*1i$28$2b$2f$3b$3f$3k$93$9g$a1$a3$bd$gr$h3$hm$i4,12*3*12$15$17$1g,19*7*24$26$28$3s$41$4p$87$e0";
w["owner's"]="4*0*2l,8*0*2e";
w["owner;"]="8*0*a2";
w["p"]="14*0*4r";
w["parallel"]="1*0*75";
w["parallelism"]="8*10*-1$p";
w["parameter"]="8*3*2a$2c$2k$2u,16*0*1p";
w["parameter."]="8*0*29,16*0*1o";
w["parameters"]="4*0*39,8*0*1v,12*0*q,14*0*34,19*0*1s";
w["parameters."]="8*0*1u";
w["parameters:"]="4*0*38,12*0*p,14*0*33,19*0*1r";
w["parent"]="3*0*k";
w["particular"]="14*10*-1$9";
w["particularly"]="6*0*1q,11*0*35";
w["pass"]="9*10*-1$d";
w["password"]="2*4*1a$1d$22,3*0*5q,4*2*3l$3m$3u,8*11*1j$3h$3l$94$9i$a9$ab$be$gs$h8$hn$i5,9*2*4u$66$6e,11*2*7a$98$9g,12*2*18$19$1h,13*2*2h$47$4f,14*3*3f$5u$6f$6n,17*0*27,18*7*6p$7j$7s$9q$ju$kh$kq$m7,19*11*29$2a$3t$42$4r$6k$7d$7n$9n$ae$dq$e5";
w["password."]="9*0*6d,11*0*9f,13*0*4e,14*0*6m,17*0*26,19*0*e4";
w["password;"]="8*0*aa,18*1*6o$jt,19*0*6j";
w["path"]="1*0*31,5*3*3m$3p$41$44,9*2*2c$2n$5p,11*0*86,13*0*3b,16*0*2j,19*1*39$3n";
w["path."]="5*0*3l";
w["path1"]="8*1*fh$hp";
w["path2"]="8*1*ft$i7";
w["peak"]="8*0*4n";
w["perform"]="1*0*5t";
w["performance"]="1*1*1e$7f";
w["periodically"]="4*0*u";
w["placeholders"]="9*0*67,11*0*99,13*0*48,14*0*6g,19*0*dr";
w["platform"]="1*1*4d$7u,2*10*-1$a,5*11*-1$e$4l,6*12*-1$d$1i$3l,7*10*-1$e,8*14*-1$t$35$3u$48$50,9*12*-1$c$1h$6a,11*15*-1$h$1i$1v$2n$30$9c,13*11*-1$a$4b,14*25*-1$-1$6$d$u$1a$29$39$6j,15*10*-1$o,19*12*-1$j$55$du";
w["platform."]="1*0*4c,8*11*-1$s$47,19*0*54";
w["points"]="1*0*51";
w["pool"]="4*32*3$7$29$2h,6*0*38,8*9*13$1o$2p$3q$4a$54$5i$68$6j$79,11*4*2k$2t$3c$3f$44,12*41*-1$3$7$g$1s,18*0*4k,19*43*-1$3$7$m$11$18$5h";
w["pool."]="4*0*28,8*0*78,11*0*2s";
w["pools"]="6*0*1m,8*10*-1$c,11*0*4h";
w["pools."]="6*0*1l";
w["poorly"]="1*0*5v";
w["poorly."]="1*0*5u";
w["port"]="2*5*1e$1f$26$57,17*0*22";
w["possess"]="1*0*83,3*0*6p,7*1*r$15,8*0*1m,9*0*5i,11*1*1u$80,13*0*35,14*1*3m$67,16*0*2f,18*0*m9,19*0*ag";
w["possessed"]="14*0*1p";
w["possible"]="16*0*3u";
w["postal"]="19*0*8p";
w["practice"]="1*1*28$3o,9*0*37,18*0*33";
w["practices"]="1*40*-1$1$3$7,18*0*3h";
w["prepquery1010"]="18*0*c7";
w["prevents"]="1*1*4o$54,8*0*6h";
w["print"]="18*2*hi$o5$ob,19*1*cc$ci";
w["println"]="3*5*5m$69$6s$7h$8b$8t,8*3*bi$c5$ec$go,9*0*42,11*0*6e,13*0*1l,14*0*52,15*0*1u,18*11*7f$82$ag$b8$co$dj$fq$hr$kd$l0$oi$or,19*4*79$81$9u$cp$d2";
w["printrows"]="9*0*3s,11*1*68$8m,13*1*1f$3r,15*2*12$15$1o";
w["prints"]="5*1*55$5c,13*0*v,15*0*1a";
w["printtable"]="18*3*5s$7h$j7$kf,19*0*7b";
w["private"]="8*10*90$97";
w["problems"]="16*0*n";
w["process"]="16*1*2u$3s";
w["process."]="16*0*3r";
w["processed"]="8*0*fp";
w["processing"]="6*10*-1$e";
w["produces"]="1*0*1c";
w["program"]="11*0*3n";
w["programming"]="18*10*-1$o";
w["programming."]="18*10*-1$n";
w["progress"]="1*0*3i";
w["projects"]="5*9*4b";
w["properly"]="11*0*5p";
w["provide"]="19*1*1b$5j";
w["provided"]="6*0*2f";
w["proxy"]="2*68*-1$-1$2$5$c$k$r$18$1c$1g$1t$21$2n$2s$3j$40$4a$4n$51$5e,14*2*3r$42$4d,17*3*l$1g$1t$2c";
w["proxy's"]="17*0*1s";
w["proxy_password"]="2*4*1b$20";
w["proxy_username"]="2*4*17$1s";
w["pub"]="3*0*4l,8*1*fi$fu,18*1*8t$lr,19*0*90";
w["public"]="3*15*1u$22$40,8*32*8g$8m$8r$9b$ap$et$fj$fv,9*15*3l$3p$45,11*15*61$65$6h,13*15*18$1c$1o,14*15*4k$4o$55,15*15*1h$1l$21,18*20*5q$5u$j5$j9,19*11*5r$5v$91";
w["pull"]="18*3*f0";
w["purposes"]="18*3*mt,19*3*b4";
w["purposes."]="18*3*ms,19*3*b3";
w["pwd"]="3*2*54$63$6n,9*1*4s$5g,11*1*78$7u,13*1*2f$33,14*2*3g$5s$65,16*0*2d";
w["pwd;"]="3*0*53";
w["q"]="1*0*2r,9*0*5j";
w["queried"]="9*10*-1$g";
w["queries"]="1*1*20$2g,9*3*u$1d$1v$39,19*7*1c$2h$3k$44$71$96$9b$9p";
w["queries."]="1*0*2f,9*1*t$1u";
w["queries;"]="19*0*70";
w["query"]="0*10*-1$j,1*4*j$2i$2q$2t$3m,3*5*5c$5e$6v$71$7d$7i,8*5*b0$b2$bq$bs$c1$c7,9*50*-1$1$3$e$15$17$1q$21$28$2e$2m$2q$2v$5n,11*1*81$84,13*32*1$4$36$39,18*10*bc$ce$cs$k3$k5$ma$mc$mg$ml,19*8*2r$2u$5l$6s$6u$ah$aj$an$as";
w["query."]="18*3*cd";
w["query:"]="18*1*bb$cr,19*0*5k";
w["query;"]="3*0*5d,8*0*b1,18*0*k4,19*0*6t";
w["querycols1010"]="18*0*dc";
w["queryid"]="18*8*6r$av$b2$c2$c8$ci$dd$e9$fg";
w["querying"]="13*10*-1$7";
w["querymode1010"]="18*0*c1";
w["queryrows1010"]="18*0*e8";
w["queryxml"]="18*5*6g$8i$ca$jl$lg$mf,19*3*6b$8k$9g$am";


// Auto generated index for searching.
w["queryxml;"]="18*1*6f$jk,19*0*6a";
w["questions"]="6*0*3u";
w["questions."]="6*0*3t";
w["queue"]="8*0*5b";
w["queued"]="1*0*7a";
w["queued."]="1*0*79";
w["queuing"]="8*0*5n";
w["r"]="9*1*3u$43,11*1*6a$6f,13*1*1h$1m,14*1*4u$53,15*1*1q$1v";
w["ran"]="8*0*c6";
w["random"]="1*4*21$2c$2l$3d$50";
w["rc1010"]="18*3*bf$cv$dr$g1";
w["re"]="2*0*2h,11*1*26$5l,18*0*1c";
w["read"]="9*0*3f";
w["read."]="9*0*3e";
w["receive"]="9*10*-1$f,17*0*s";
w["receives"]="13*10*-1$d";
w["receiving"]="0*10*-1$k";
w["recommended"]="1*10*-1$5";
w["reduce"]="19*10*-1$a";
w["refer"]="6*0*46,10*0*f";
w["reference"]="6*3*3n$4a,8*0*g3,9*3*1m,10*45*-1$0$1$5$j";
w["relea"]="11*0*2p";
w["release"]="8*0*6b,12*10*-1$a";
w["released"]="4*0*26,19*1*51$57";
w["releases"]="11*0*13";
w["releasing"]="0*10*-1$m";
w["reluid"]="4*10*-1$i";
w["reproduce"]="16*0*1t";
w["reproducible"]="16*0*40";
w["request"]="4*1*v$1d,8*0*60,14*10*-1$q,16*2*10$1c$26,17*11*-1$j$2r";
w["request."]="16*2*v$1b$25,17*11*-1$i$2q";
w["requesting"]="4*0*31";
w["requests"]="1*1*2k$47,16*30*1$3,17*0*30";
w["require"]="2*2*3l$4e$54,18*0*29";
w["required"]="15*10*-1$c,18*4*1p$2h$3l$4g$53";
w["required."]="18*0*52";
w["requirements"]="5*9*m";
w["requires"]="17*1*p$24";
w["reset"]="12*31*1$5$1c";
w["resetpool"]="12*42*-1$0$4$i$l$1e";
w["resolve"]="17*11*-1$e$2j";
w["resources"]="14*10*-1$m";
w["respond"]="16*0*r";
w["result"]="11*0*43,13*0*q";
w["results"]="0*10*-1$l,1*1*1g$48,3*4*27$2j$2p$7c$7s,8*4*av$c0$cd$cn$ct,9*0*5s,11*2*89$8i$8n,13*43*-1$2$5$f$3e$3n$3s,18*2*mk$n7$nd,19*14*-1$f$1k$ar$be$bk";
w["results."]="1*0*1f";
w["results;"]="8*0*au";
w["resultset"]="3*1*26$7b,8*0*at,9*0*5r,11*0*88,13*13*-1$j$n$11$3d,18*0*mj,19*0*aq";
w["retries"]="8*0*5v";
w["retrieval"]="6*10*-1$f,19*0*1j";
w["retrieve"]="1*0*4i,19*10*-1$e";
w["retrieves"]="5*0*5q,8*0*7b,15*0*17";
w["retrieving"]="1*0*4r";
w["return"]="18*3*a0";
w["returned"]="8*0*cb";
w["rintrows"]="14*0*4s";
w["roup"]="8*0*3e";
w["row"]="1*0*4k,3*3*2n$2o$2q$2u,8*3*cr$cs$cu$d2,9*0*3t,11*1*69$8o,13*1*1g$3t,14*0*4t,15*1*19$1p,18*10*gh$h3$nb$nc$ne$ni,19*3*bi$bj$bl$bp";
w["row..."]="18*3*h2";
w["rowoutput"]="3*0*4s";
w["rows"]="1*0*41,5*0*5d,8*0*cg,9*0*33,13*0*10,18*6*ee$mp,19*3*b0";
w["rowsperblock"]="18*0*70";
w["rowsperblock;"]="18*0*6v";
w["rowwriter"]="3*1*20$5o,5*0*5b";
w["run"]="3*1*7e$7j,8*1*ar$c2,9*3*1s$1t$2g$5u,11*1*23$8b,13*1*s$3g,16*0*44,18*5*cc$cq$mm,19*2*1d$48$at";
w["run."]="11*0*22";
w["running"]="9*30*0$2";
w["runquery1010"]="18*0*cg";
w["runtime"]="11*0*3h,16*0*32";
w["s"]="8*0*1e,16*0*28";
w["sam"]="4*31*2$6$2g,6*2*1k$35$37,8*49*-1$3$7$b$12$1n$2o$3p$49$53$5h$67$77,11*3*2j$3e$4g$4m,12*41*-1$2$6$f$1r,18*1*4j$5a,19*42*-1$2$6$l$10$5g";
w["same"]="1*0*7s,8*0*6r";
w["sample"]="1*0*2p";
w["sample:"]="1*0*2o";
w["saved"]="9*0*3b";
w["scoping"]="9*0*n";
w["scroll"]="1*0*13";
w["scrolling"]="1*1*1a$1o";
w["sdk"]="0*10*-1$9,1*11*-1$d$46,3*11*-1$5$p,5*14*-1$j$p$1i$29$3a,6*16*-1$4$11$13$27$49,8*0*59,10*15*-1$4$i,11*10*-1$a,15*10*-1$h,16*1*j$1d,17*10*-1$b,18*19*-1$9$u$18$1o$27$2t$43$51$5k$iv";
w["sdk."]="1*10*-1$c,6*1*10$26,17*10*-1$a,18*0*17";
w["sdk:"]="18*2*1n$5j$iu";
w["sdks"]="6*0*3i,16*10*-1$7";
w["seconds"]="8*0*64";
w["seconds."]="8*0*63";
w["section"]="17*0*2d";
w["sections"]="6*0*1o";
w["sed"]="11*0*2q";
w["see"]="1*0*u,9*0*1k,11*0*4i,14*1*2i$4a,17*0*2u,18*2*3f$4c$56";
w["sel"]="1*0*33,3*0*75,8*0*ga,9*0*52,11*0*7e,13*0*2l,18*1*8k$li,19*0*8m";
w["selects"]="9*0*30";
w["send"]="16*0*3v";
w["sending"]="16*0*3l";
w["separate"]="5*0*62,8*1*73$7k,9*0*3c";
w["sequential"]="1*0*1r";
w["sequentially"]="1*1*16$1b";
w["serve"]="14*10*-1$o";
w["server"]="2*26*-1$-1$d$m$t$1i$2o$2t$4b$52,5*0*69,14*1*3s$43,17*0*17";
w["server's"]="2*12*-1$l$s$1h";
w["servers"]="2*0*3k";
w["session"]="0*20*-1$-1$h$o,1*6*11$2v$6g$6q$71$7l$7v,3*7*58$59$6i$6k$6t$72$8k$8n,5*0*5o,7*15*-1$g$1a$1m$24$2a$2d,8*18*10$1a$1d$1g$1q$25$2q$5d$5t$74$b3$b4$b9$bb$bj$bo$bt$el$eo,9*1*5a$5d,11*45*2$5$t$14$1d$1j$20$2g$2o$31$40$49$4v$51$5n$7o$7r,13*1*2t$30,14*60*-1$-1$1$3$7$i$v$13$1b$1h$1o$2a$2n$2u$5v$62,16*2*1k$27$2a,18*8*3a$3j$jv$k0$m2$m4$md$p6$p9,19*9*32$4d$6o$6p$9j$a9$ab$ak$dd$dg";
w["session."]="0*10*-1$n,11*0*4u";
w["session_name"]="1*0*2u";
w["sessions"]="1*1*h$r,4*2*14$22$2f,6*0*1j,8*0*6c,11*10*-1$i";
w["sessions."]="1*0*q,4*0*2e";
w["set"]="1*3*23$3e$3p$53,2*0*3n,4*0*1j,8*11*-1$f$44,13*11*-1$e$p,14*10*-1$l,18*3*hv";
w["sets"]="1*0*4t,4*10*-1$8,9*0*l";
w["setting"]="0*10*-1$b,1*0*4e,15*30*0$3";
w["setup"]="15*10*-1$8";
w["setwindowsize"]="1*0*69,18*0*mh,19*0*ao";
w["several"]="1*10*-1$4";
w["share"]="1*0*6v,8*0*42";
w["shared"]="6*0*32,8*50*-1$-1$0$4$8$h,11*0*4j,18*0*57";
w["sharing"]="11*0*3b";
w["should"]="1*1*2a$6m,9*12*-1$6$q$3a,18*0*36,19*0*50";
w["side"]="8*10*-1$k";
w["sign"]="5*9*31";
w["sign-on"]="5*9*30";
w["similar"]="8*0*15";
w["simplethread"]="5*0*5i,8*4*8i$9c$gq$hk$i2";
w["single"]="1*0*70,3*0*6a,5*9*2v,6*3*18$1f$2r$2u,8*12*-1$e$18$43,11*1*1p$27,14*11*-1$p$t,18*1*83$l1";
w["single-threaded"]="6*0*17";
w["single-user"]="6*1*1e$2t,8*0*17";
w["situation"]="8*0*4q";
w["size"]="1*3*25$3s$5n$65";
w["so"]="4*10*-1$d";
w["sociated"]="14*0*2b";
w["some"]="2*0*3i";
w["soon"]="8*0*6d";
w["sort"]="1*0*1l";
w["source"]="16*12*k$3b$3g$3m$41";
w["specific"]="6*0*43,9*0*k,12*10*-1$e";
w["specifically"]="18*10*-1$j";
w["specified"]="9*0*16";
w["specifies"]="16*0*2i";
w["specify"]="2*0*2m,16*0*1q";
w["speed"]="19*0*1i";
w["square"]="2*1*1p$3c";
w["sso"]="5*11*32$37$3h";
w["sso_login"]="5*0*3g";
w["stack"]="16*0*35";
w["staff"]="17*0*1p";
w["standalone"]="16*0*3f";
w["start"]="4*0*12,5*10*-1$6,6*1*o$23,8*1*ie$ig,18*3*f7";
w["started"]="5*30*1$3,6*0*1u,7*0*2f,8*0*bk,14*0*1t";
w["started."]="7*0*2e";
w["starts"]="7*0*1n";
w["state"]="19*0*8q";
w["statement"]="11*0*58";
w["static"]="3*10*23$41,8*5*eu,9*10*3q$46,11*10*66$6i,13*10*1d$1p,14*10*4p$56,15*10*1m$22,18*10*5v$ja,19*5*60";
w["stations"]="3*0*4o";
w["still"]="17*0*2h";
w["string"]="2*11*-1$g$1n,3*6*44$49$4j$4q$4v$52$55,8*16*8n$8s$91$9d$9f$9h$9j$9l$9n$f1$f6$fg$fs$g7$h2$h7$hc,9*5*2i$49$4c$4n$4r$4v,11*4*6l$6o$73$77$7b,13*4*1s$1v$2a$2e$2i,14*4*3v$59$5c$5n$5r,18*11*62$6b$6e$6h$6k$6n$jd$jg$jj$jm$jp$js,19*6*63$66$69$6c$6f$6i$6l";
w["style"]="18*10*-1$d";
w["submit"]="16*0*16,17*0*2o";
w["submitting"]="0*10*-1$i";
w["succeeded"]="18*3*a9";
w["succeeded."]="18*3*a8";
w["support"]="2*0*33,16*45*-1$0$2$c$q$1a$24$2t$3q,17*42*-1$1$3$h$2p$2v";
w["support."]="16*10*-1$b";
w["sure"]="18*3*a7";
w["synchronization"]="18*0*31";
w["synchronization."]="18*0*30";
w["synchronize"]="18*0*2j";
w["system"]="1*0*4p,3*6*5k$5r$67$6q$7f$89$8r,5*0*1a,8*4*bg$c3$ea$gm$gu,9*0*40,11*0*6c,13*0*1j,14*0*50,15*0*1s,17*0*1j,18*21*7d$7k$80$ae$ap$b6$bm$cm$d6$dh$e2$fo$g8$hg$hp$kb$ki$ku$o3$o9$og$op,19*8*77$7f$7v$9s$a4$ca$cg$cn$d0";
w["system."]="5*0*19";
w["t"]="1*0*5d,3*0*3i,8*0*dm,17*0*15,18*1*hn$o7,19*0*ce";
w["table"]="4*0*1i,5*1*57$5e,8*5*8o$9m$ad$af$bu$c9,9*2*2d$2o$35,18*9*77$fd$fl$gr$ha$hj$i0$i8,19*0*2m";
w["table."]="9*0*34";
w["table.length;"]="18*0*gq";
w["table;"]="8*0*ae,18*0*76";
w["tablename"]="3*1*4k$73";
w["tablepath"]="18*5*6j$8s$c9$jo$lq$me,19*3*6e$8v$9f$al";
w["tablepath;"]="18*1*6i$jn,19*0*6d";
w["tables"]="4*0*1k,5*0*5u,8*1*7g$fq";
w["take"]="19*0*2l";
w["takes"]="4*0*36,8*2*1r$27$2s,9*0*2b,12*0*n,14*0*31,19*11*-1$d$1p";
w["team"]="2*0*34";
w["technology"]="2*0*32";
w["ten"]="8*0*62";
w["tentendata"]="3*0*1s,8*0*8e,9*0*3j,11*0*5v,13*0*16,14*0*4i,15*0*1f,18*1*5o$j3,19*0*5p";
w["tentenexception"]="3*13*-1$7$a$i$86,18*0*om,19*0*ct";
w["terminate"]="4*0*2a";
w["terminated"]="11*1*2i$3p";
w["terminated."]="11*1*2h$3o";
w["terminates"]="4*0*20";
w["tested"]="16*0*46";
w["tested."]="16*0*45";
w["testing"]="18*6*eb$mr,19*3*b2";
w["testsession"]="9*1*5b$5o,11*3*5a$7p$85$8u,13*1*2u$3a,14*1*30$60";
w["text"]="5*0*4o";
w["than"]="8*0*51";
w["them"]="8*0*7i,19*0*3p";
w["them:"]="19*0*3o";
w["those"]="4*10*-1$k";
w["thread"]="1*2*6e$6l$7h,8*5*5p$6f$8a$8k$hh$hv,18*1*2v$35";
w["thread1"]="8*2*hi$id$ih";
w["thread2"]="8*2*i0$if$ij";
w["threaded"]="6*1*19$2s";
w["threads"]="1*1*6u$78,5*1*5l$6e,8*23*-1$-1$l$o$4t$6n$70,18*0*2l";
w["threads."]="5*0*6d";
w["three"]="5*0*20,7*0*k";
w["through"]="1*1*14$1p,6*0*2n,13*10*-1$i";
w["throws"]="3*10*2a$46,8*5*f3";
w["time"]="4*10*-1$f,5*3*4t$4v$5a$5h,6*0*u,8*1*5o$6t,12*0*1v,14*10*-1$s,18*3*f5,19*20*-1$-1$c$r";
w["time."]="5*1*59$5g,8*0*6s,12*0*1u,14*10*-1$r,18*3*f4,19*10*-1$q";
w["tools"]="5*0*1q";
w["tostring"]="3*0*3o,8*0*ds,18*1*oe$p2,19*1*cl$d9";
w["traces"]="16*0*36";
w["transfer"]="1*0*56";
w["transport"]="17*0*v";
w["troubleshoot"]="17*10*-1$7";
w["troubleshooting"]="17*30*0$2";
w["try"]="3*11*e$v$6g$7p,6*0*2c,8*5*b7,11*6*55$5d$7m,17*10*-1$6,18*10*at$m0,19*5*a7";
w["trying"]="8*1*4u$5q";
w["ttpager"]="5*0*65";
w["turn"]="16*0*1g";
w["two"]="5*3*5k$5n$5t$61,8*4*1s$6v$72$7e$7j";
w["txt"]="3*0*4t,8*1*hu$ic";
w["type"]="7*10*-1$5,11*0*1a,14*2*1m$2h$3i";
w["types"]="5*1*4n$53,7*31*1$3$o,14*0*2k";
w["types."]="5*0*52";
w["types:"]="7*0*n";
w["typically"]="9*0*v";
w["typing"]="5*0*3u";
w["uery"]="9*0*5k";
w["uid"]="18*0*4n";
w["uids"]="19*0*a3";
w["umrows"]="18*0*ei";
w["unnecessary"]="1*0*55";
w["unneeded"]="1*0*4u";
w["unsure"]="2*0*2i";
w["unzip"]="5*0*2f";
w["up"]="0*10*-1$c,1*2*f$p$10,11*45*-1$1$4$e$1n$2v$3v$4t$53,15*30*1$4";
w["up."]="11*0*1m";
w["update"]="4*0*1f";
w["updated"]="4*0*1o,18*0*10";
w["updated."]="4*0*1n";
w["updating"]="18*31*0$3$1d";
w["upon"]="1*0*i";
w["uri"]="8*2*1h$31$36,14*0*35";
w["url"]="3*1*4a$6l,4*2*3a$3d$3s,8*7*92$9e$9t$9v$bc$f7$hl$i3,12*2*r$10$1f,17*1*m$1u,19*3*1t$22$3r$4o";
w["url;"]="8*0*9u";
w["us"]="19*0*8o";
w["usage"]="0*30*1$3,3*0*5n,6*0*2k,8*0*gp,18*1*7g$ke,19*0*7a";
w["use"]="1*2*l$1u$68,2*0*3b,4*0*1s,5*0*36,8*1*4e$6q,9*0*1p,11*20*-1$-1$7$l,12*1*v$20,16*0*1j,17*0*1d,18*0*11,19*0*21";
w["use."]="11*10*-1$k";
w["used"]="11*1*1b$4r,19*0*4u";
w["user"]="2*1*15$4f,3*5*51$5v$6b$6d$6f$6m,4*0*3g,6*1*1g$2v,8*5*14$19$1p$22$24$2g,9*6*4o$4p$4t$5f$63$65$6b,11*9*1q$28$2m$74$75$79$7t$95$97$9d,12*12*-1$c$13$1n,13*6*2b$2c$2g$32$44$46$4c,14*23*-1$a$18$1k$23$3a$3c$3e$5o$5p$5t$64$6c$6e$6k,16*0*2c,18*3*84$86$l2$l4,19*0*12";
w["user's"]="11*0*2l,14*0*3d";
w["user;"]="3*0*50";
w["username"]="2*4*19$1u,3*0*5p,8*0*bm,17*0*25,18*9*6m$7i$7o$88$9p$jr$kg$km$l6$m6,19*6*6h$7c$7j$89$9m$ad$do";
w["username:"]="8*0*bl";
w["username;"]="18*1*6l$jq,19*0*6g";
w["users"]="1*2*29$3a$7b,8*1*3n$41,9*10*-1$b,11*0*3a,19*0*1m";
w["users."]="19*0*1l";
w["uses"]="3*0*u,5*0*5j,18*0*44,19*0*5d";
w["using"]="1*11*-1$9$3l,5*10*2u$6b,6*0*24,8*0*3o,9*0*1e,11*10*-1$p,12*11*-1$h$1j,16*10*-1$5,18*3*4i$i6";
w["utilization"]="8*0*4o";
w["utilized"]="8*1*5l$6a,11*0*47";
w["utilized."]="8*0*5k";
w["valid"]="7*0*l,9*0*68,11*0*9a,13*0*49,14*1*37$6h,19*0*ds";
w["value"]="1*0*34,3*0*76,8*0*gb,9*0*53,11*0*7f,13*0*2m,18*1*8l$lj,19*2*36$3f$8n";
w["variable"]="5*0*3r";
w["variables"]="2*0*3g,8*0*30";
w["variables."]="2*0*3f";
w["variables:"]="8*0*2v";
w["via"]="1*1*2e$80,2*40*-1$1$4$b,14*1*3q$4c,17*0*2b,18*0*4o";
w["view"]="4*0*1g";
w["void"]="3*10*24$42,8*10*aq$ev,9*10*3r$47,11*10*67$6j,13*10*1e$1q,14*10*4q$57,15*10*1n$23,18*10*60$jb,19*5*61";
w["w"]="19*1*9i$a1";
w["wa"]="19*0*8s";
w["waiting"]="11*1*3r$48";
w["walks"]="6*0*2l";
w["want"]="1*1*1j$62,4*1*t$1c,5*0*35";
w["warm"]="19*41*-1$1$5$k$5f";
w["warmed"]="19*1*58$9v";
w["warming"]="19*1*17$5t";
w["warmlog"]="19*0*9q";
w["warmpool"]="19*37*0$4$s$1n$3q$3v$49$5e$9k";
w["way"]="19*10*-1$9";
w["ways"]="11*0*1g";
w["we"]="18*3*9t";
w["weather"]="3*0*4n";
w["web"]="5*0*68,17*0*1a";
w["what"]="7*10*-1$7,17*0*21";
w["when"]="1*12*-1$8$17$44,7*13*-1$9$1d$1q$26,8*0*5g,11*3*15$32$3m$5i,14*0*17,18*3*19$i3,19*1*16$56";
w["where"]="8*2*4m$4r$6l,16*0*2k";
w["wherever"]="16*0*3t";
w["whether"]="2*0*2j";
w["which"]="9*0*2p,11*0*12,13*10*-1$g,14*0*3j";
w["while"]="18*5*eq";
w["who"]="6*0*3e,12*0*1o";
w["window"]="1*6*24$3r$3v$4f$5i$5m$64";
w["window."]="1*0*5h";
w["windows"]="5*1*r$42";
w["windowsize"]="18*2*6t$92$c3";
w["within"]="9*0*s,14*10*-1$e";
w["without"]="1*0*3k,19*1*3j$4a";
w["won"]="1*0*5c";
w["won't"]="1*0*5b";
w["work"]="9*0*11";
w["workflow"]="6*0*2p";
w["working"]="5*0*2r,6*1*v$1c,12*0*1q";
w["worry"]="1*0*5f";
w["worth"]="1*0*5j";
w["write"]="3*2*3g$3l$3r,8*2*dk$dp$dv,16*0*2l";
w["writer"]="3*1*1j$28,8*2*80$98$9p";
w["writerows"]="3*1*25$7r";
w["writing"]="5*10*-1$7,8*0*ef,9*0*1c";
w["writing:"]="8*0*ee";
w["written"]="18*1*5f$io";
w["www2"]="2*3*29$44$4r$5h,3*0*4c,5*0*1m,8*0*f9,9*0*4f,11*0*6r,13*0*22,14*0*5f,18*1*8b$l9,19*0*8c";
w["xml"]="1*0*1v";
w["year"]="18*1*8m$lk";
w["you"]="1*4*1h$4h$5a$60$66,2*3*v$2g$2k$2p,4*13*-1$g$r$19$1q,5*24*-1$-1$5$f$v$1c$34$3b,6*3*m$21$2m$3v,8*0*4h,11*14*-1$m$1o$25$37$5k,12*10*-1$8,14*0*3o,16*11*-1$4$14,17*23*-1$-1$4$c$r$2g$2m,18*7*1b$1q$2a$2i$3m$i4,19*3*19$46$4g$4j";
w["you're"]="2*0*2f,11*1*24$5j,18*0*1a";
w["your"]="0*10*-1$d,1*2*g$39$3q,2*23*-1$-1$6$e$q$2r$30,4*0*10,5*14*-1$8$18$2n$2q$3o,6*1*s$3s,8*3*3m$52$66$6e,9*21*-1$-1$4$a$o,13*10*-1$b,14*1*3t$41,15*10*-1$6,16*2*u$19$23,17*6*n$19$1e$1h$1m$1r$2k,18*0*1e,19*0*4m";
w["yourself"]="18*0*2n";
w["yourself."]="18*0*2m";
w["zip"]="5*2*1f$1t$2h";
w["{"]="3*12*21$2c$2m$3a$3e$48$5j$6h$7q$80$88$8j$8m,8*11*8l$9r$as$b8$cq$de$di$e9$ek$en$f5$gl,9*2*3o$3v$4b,11*5*64$6b$6n$7n$8l$8t,13*3*1b$1i$1u$3q,14*2*4n$4v$5b,15*2*1k$1r$25,18*22*5t$64$7c$ad$au$b5$cl$dg$eu$fn$gu$hf$ie$j8$jf$ka$m1$na$nu$o2$oo$p5$p8,19*9*5u$65$76$a8$bh$c5$c9$cv$dc$df";
w["}"]="3*12*3j$3p$3u$3v$5u$7u$83$84$8h$8p$8q$8v$90,8*11*ao$dn$dt$e2$e5$ei$eq$er$es$h1$il$im,9*2*44$5v$60,11*5*6g$8q$8r$90$91$92,13*3*1n$3v$40$41,14*2*54$68$69,15*2*20$26$27,18*22*7n$as$bp$d9$e5$gb$ho$hs$ib$ic$ii$ij$ik$kl$o8$of$oj$ok$p3$pb$pc$pd$pe,19*9*7i$cf$cm$cq$cr$da$di$dj$dk$dl";

return {'fil': fil, 'w': w}})();
