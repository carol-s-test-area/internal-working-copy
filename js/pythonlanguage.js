window['pythonlanguage'] = (function() {
var w = new Object();
fil = new Array();
fil["0"]= "DataTypeMapping.html@@@Appendix: Data type mapping@@@The following table maps data types from 1010data to Python...";
fil["1"]= "DistApply.html@@@ten.dist_apply(columns,fun,args)@@@Return a list of the return values for each call to the function object. This is the Python function used to disseminate Python code to all sub-processes...";
fil["2"]= "ExamplesLanguagePython.html@@@Macro Language examples@@@The following examples show how to implement various Python applications within Macro Language code with the functions ten.GetData(),\n    ten.MetaData(), ten.rows(), and ten.rebase()...";
fil["3"]= "Get.html@@@ten.get(x)@@@Return column data at the sub-processes. In order for ten.dist_apply to retrieve column data at the sub-procs, the fun argument in ten.dist_apply must have access to ten.get...";
fil["4"]= "GetDataClass.html@@@ten.GetData(ops,table,rows,cols)@@@Return a GetData object...";
fil["5"]= "Introduction.html@@@Introduction@@@This guide contains the information you need to start developing with the Python language in the 1010data Insights Platform...";
fil["6"]= "MakeTable.html@@@ten.make_table(data,labels=None)@@@Prepares and sets the temporary table (worksheet) as well as its labels...";
fil["7"]= "MetaDataClass.html@@@ten.MetaData()@@@Return a MetaData object...";
fil["8"]= "PythonTabulationFunctions.html@@@Python tabulation functions@@@The following section contains the 1010data-supplied functions that can be placed within a tabulation (&lt;tabu&gt;) in Macro Language code...";
fil["9"]= "PythoninTRS.html@@@Using the Python language in TRS@@@The Trillion-Row Spreadsheet (TRS) timeline makes it easy to insert Python code into your analysis...";
fil["10"]= "ReBaseFunction.html@@@ten.rebase(data,metadata)@@@Set a new temporary table (worksheet) and returns a base op...";
fil["11"]= "RowsFunction.html@@@ten.rows(ops,table)@@@A method that returns the number of rows relative to ops,\n            table...";
fil["12"]= "SuppliedPythonFunctions.html@@@Macro Language Python functions@@@The following section contains 1010data-supplied functions for interfacing with the Insights Platform...";
fil["13"]= "TabulationFunctionExamples.html@@@Tabulation function examples@@@The following examples show how to implement various Python applications within Macro Language code with the tabulation functions ten.dist_apply(),\n            ten.make_table(), and ten.get()...";
fil["14"]= "UserGroupFunctions.html@@@Defining user and group functions@@@You can use the Macro Language tag &lt;resource&gt; to define a resource for Macro Language using Python code. You can also use\n            &lt;def_ufun&gt; and &lt;def_gfun&gt; to define your own Python functions within the Macro Language Workshop...";
fil["15"]= "codeLanguagePython.html@@@Using &lt;code language_=&quot;python&quot;&gt; in Macro Language@@@You can now use Python to build query operations or ops in the 1010data Macro Language...";
var doStem = false;searchLoaded = true;// Porter stemmer in Javascript. Few comments, but it's easy to follow against the rules in the original
// paper, in
//
//  Porter, 1980, An algorithm for suffix stripping, Program, Vol. 14,
//  no. 3, pp 130-137,
//
// see also http://www.tartarus.org/~martin/PorterStemmer

// Release 1 be 'andargor', Jul 2004
// Release 2 (substantially revised) by Christopher McKenzie, Aug 2009


var stemmer = (function(){
	var step2list = {
			"ational" : "ate",
			"tional" : "tion",
			"enci" : "ence",
			"anci" : "ance",
			"izer" : "ize",
			"bli" : "ble",
			"alli" : "al",
			"entli" : "ent",
			"eli" : "e",
			"ousli" : "ous",
			"ization" : "ize",
			"ation" : "ate",
			"ator" : "ate",
			"alism" : "al",
			"iveness" : "ive",
			"fulness" : "ful",
			"ousness" : "ous",
			"aliti" : "al",
			"iviti" : "ive",
			"biliti" : "ble",
			"logi" : "log"
		},

		step3list = {
			"icate" : "ic",
			"ative" : "",
			"alize" : "al",
			"iciti" : "ic",
			"ical" : "ic",
			"ful" : "",
			"ness" : ""
		},

		c = "[^aeiou]",          // consonant
		v = "[aeiouy]",          // vowel
		C = c + "[^aeiouy]*",    // consonant sequence
		V = v + "[aeiou]*",      // vowel sequence

		mgr0 = "^(" + C + ")?" + V + C,               // [C]VC... is m>0
		meq1 = "^(" + C + ")?" + V + C + "(" + V + ")?$",  // [C]VC[V] is m=1
		mgr1 = "^(" + C + ")?" + V + C + V + C,       // [C]VCVC... is m>1
		s_v = "^(" + C + ")?" + v;                   // vowel in stem

	return function (w) {
		var 	stem,
			suffix,
			firstch,
			re,
			re2,
			re3,
			re4,
			origword = w;

		if (w.length < 3) { return w; }

		firstch = w.substr(0,1);
		if (firstch == "y") {
			w = firstch.toUpperCase() + w.substr(1);
		}

		// Step 1a
		re = /^(.+?)(ss|i)es$/;
		re2 = /^(.+?)([^s])s$/;

		if (re.test(w)) { w = w.replace(re,"$1$2"); }
		else if (re2.test(w)) {	w = w.replace(re2,"$1$2"); }

		// Step 1b
		re = /^(.+?)eed$/;
		re2 = /^(.+?)(ed|ing)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			re = new RegExp(mgr0);
			if (re.test(fp[1])) {
				re = /.$/;
				w = w.replace(re,"");
			}
		} else if (re2.test(w)) {
			var fp = re2.exec(w);
			stem = fp[1];
			re2 = new RegExp(s_v);
			if (re2.test(stem)) {
				w = stem;
				re2 = /(at|bl|iz)$/;
				re3 = new RegExp("([^aeiouylsz])\\1$");
				re4 = new RegExp("^" + C + v + "[^aeiouwxy]$");
				if (re2.test(w)) {	w = w + "e"; }
				else if (re3.test(w)) { re = /.$/; w = w.replace(re,""); }
				else if (re4.test(w)) { w = w + "e"; }
			}
		}

		// Step 1c
		re = /^(.+?)y$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(s_v);
			if (re.test(stem)) { w = stem + "i"; }
		}

		// Step 2
		re = /^(.+?)(ational|tional|enci|anci|izer|bli|alli|entli|eli|ousli|ization|ation|ator|alism|iveness|fulness|ousness|aliti|iviti|biliti|logi)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			suffix = fp[2];
			re = new RegExp(mgr0);
			if (re.test(stem)) {
				w = stem + step2list[suffix];
			}
		}

		// Step 3
		re = /^(.+?)(icate|ative|alize|iciti|ical|ful|ness)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			suffix = fp[2];
			re = new RegExp(mgr0);
			if (re.test(stem)) {
				w = stem + step3list[suffix];
			}
		}

		// Step 4
		re = /^(.+?)(al|ance|ence|er|ic|able|ible|ant|ement|ment|ent|ou|ism|ate|iti|ous|ive|ize)$/;
		re2 = /^(.+?)(s|t)(ion)$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(mgr1);
			if (re.test(stem)) {
				w = stem;
			}
		} else if (re2.test(w)) {
			var fp = re2.exec(w);
			stem = fp[1] + fp[2];
			re2 = new RegExp(mgr1);
			if (re2.test(stem)) {
				w = stem;
			}
		}

		// Step 5
		re = /^(.+?)e$/;
		if (re.test(w)) {
			var fp = re.exec(w);
			stem = fp[1];
			re = new RegExp(mgr1);
			re2 = new RegExp(meq1);
			re3 = new RegExp("^" + C + v + "[^aeiouwxy]$");
			if (re.test(stem) || (re2.test(stem) && !(re3.test(stem)))) {
				w = stem;
			}
		}

		re = /ll$/;
		re2 = new RegExp(mgr1);
		if (re.test(w) && re2.test(w)) {
			re = /.$/;
			w = w.replace(re,"");
		}

		// and turn initial Y back to y

		if (firstch == "y") {
			w = firstch.toLowerCase() + w.substr(1);
		}

		return w;
	}
})();// Auto generated list of analyzer stop words that must be ignored by search.
stopWords = new Array();
stopWords[0]= "but";
stopWords[1]= "be";
stopWords[2]= "with";
stopWords[3]= "such";
stopWords[4]= "then";
stopWords[5]= "for";
stopWords[6]= "no";
stopWords[7]= "will";
stopWords[8]= "not";
stopWords[9]= "are";
stopWords[10]= "and";
stopWords[11]= "their";
stopWords[12]= "if";
stopWords[13]= "this";
stopWords[14]= "on";
stopWords[15]= "into";
stopWords[16]= "a";
stopWords[17]= "or";
stopWords[18]= "there";
stopWords[19]= "in";
stopWords[20]= "that";
stopWords[21]= "they";
stopWords[22]= "was";
stopWords[23]= "is";
stopWords[24]= "it";
stopWords[25]= "an";
stopWords[26]= "the";
stopWords[27]= "as";
stopWords[28]= "at";
stopWords[29]= "these";
stopWords[30]= "by";
stopWords[31]= "to";
stopWords[32]= "of";

var indexerLanguage="en";

// Auto generated index for searching.
w["!--"]="2*3*50";
w["!--perform"]="2*3*37";
w["#"]="2*19*5g$67$6n$81$8g$pg$re$t6$tg$tm$tt$u4$ua$ub$ui$19d$19r$1a5$1ah$1bg,7*0*6q,13*0*2r,14*3*h4$ha$hf$k3";
w["#loop"]="2*0*19h";
w["#out"]="2*0*14k";
w["'general"]="7*1*5f$6m";
w["'labels'"]="4*1*6h$70";
w["'names'"]="4*2*6f$6u$75";
w["'ops'"]="2*0*1bv";
w["'state'"]="2*0*74";
w["'total'"]="2*0*79";
w["+"]="14*1*j2$km";
w["+'"]="7*0*75";
w["-"]="2*10*qp$vp$10l$11j$11l$12i$12l$134$136$146$1dh";
w["--"]="2*3*56";
w["."]="1*1*27$2m,2*8*r$ag$b1$ce$d8$e0$ff$fr$l1,3*0*12,4*10*18$36$4s$6j$72$77$7q$94$9e$9p$a5,6*1*11$1p,7*2*2v$3f$4l,8*0*18,9*0*3o,10*1*1g$1n,11*2*f$12$1k,12*1*v$1e,13*0*s,14*1*51$dd,15*5*2u$35$4a$4v$63$b5";
w[".5"]="14*0*mb";
w["/"]="2*105*3k$49$4p$m2$vr$10b$10q$11d$11n$128$12q$138$13m$140$14a$190$1ep$1fg$1g1$1gn$1h1,4*21*cf$do$fb$fm$g2,7*5*5s,13*10*23$63,14*20*bo$c5$g8$lv";
w["/code"]="2*25*8p$rr$v8$152$1e8,4*5*ep,7*5*7g,13*5*59,14*25*8g$9s$i0$jb$le,15*1*8k$ab";
w["/def_ufun"]="14*20*8k$a0$i4$jf";
w["/defop"]="2*10*rv$vc,13*5*5h";
w["/library"]="2*5*vg,14*10*a9$jk";
w["/link"]="2*10*12t$1g8";
w["/resource"]="14*10*73$a5";
w["/table"]="14*5*ba";
w["/tabu"]="2*25*4s$10e$12b$1es$1g4,4*5*g5,13*5*5d";
w["0"]="2*10*73$o4$oh$q6$19q$1a4$1bt$1cm$1cv$1da$1de,13*0*49,14*4*ht$j7$ki$kr$lt";
w["0088"]="4*0*dl";
w["1"]="2*11*78$o3$q5$112$16e$199$1ag$1c3$1c9$1cd$1co$1f5,4*2*7g$8q$e3,7*0*6l,13*2*3h$4m$56,14*0*ai";
w["10"]="2*0*3u,14*2*f2$k2$ls";
w["100"]="2*0*3v";
w["1000"]="2*0*41";
w["1010"]="0*0*1r,2*0*jc,8*0*q,12*0*m";
w["1010data"]="0*11*-1$g$j,2*3*b4$ln,5*14*-1$b$i$3r$41$48,8*10*-1$a,9*0*1u,12*11*-1$c$1u,14*3*3d,15*15*-1$p$24$38$84";
w["1010data-supplied"]="2*0*b3,8*10*-1$9,12*10*-1$b,15*0*37";
w["2"]="2*4*70$7d$7p$qt$1dk,4*1*7h$8r,14*0*am";
w["27363"]="15*0*9i";
w["3"]="4*1*7i$8s,14*0*ar";
w["4"]="14*0*b2";
w["450"]="4*0*dn";
w["5"]="14*4*f0$fm$k1$lu$mc";
w["500"]="2*0*40";
w["6371"]="4*0*dk";
w["6371.0088"]="4*0*dj";
w["6943"]="15*0*9k";
w["7685674"]="15*0*9h";
w["7996"]="15*0*9j";
w["8"]="0*1*1j$2o,4*1*1g$3c,7*0*s,11*0*16";
w[":"]="2*2*7k$fq$1c4";
w["<"]="2*304*13$36$3f$3m$4b$4j$4r$4v$58$5e$6e$8o$ab$ao$bd$c4$cg$hq$jm$kj$kp$ku$l4$ld$lk$lt$m4$m7$me$mm$rq$ru$s2$s8$sg$v7$vb$vf$vj$vt$102$10d$10h$10s$114$11f$11p$11u$12a$12e$12s$130$13a$13o$142$14c$14i$151$16c$16g$17e$182$18r$192$19b$1e7$1eb$1eg$1er$1ev$1f7$1fi$1fn$1g3$1g7$1gb$1gp,4*46*15$bd$ca$ch$cn$eo$es$f2$fd$fo$g4,6*0*1v,7*20*5n$5u$64$7f,8*3*h$v$15$38,11*0*v,12*0*q,13*45*1r$25$2d$2g$2m$58$5c$5g$5k,14*201*e$t$12$1h$1r$2e$30$33$38$4i$5o$6d$6g$6m$72$76$7c$7q$80$8f$8j$8o$96$9c$9r$9v$a4$a8$ac$b9$bd$bq$fb$g3$ga$gd$gs$h2$hv$i3$i8$io$iu$ja$je$jj$jn$jt$ld$lh,15*46*1$9$t$21$2a$2i$41$4j$51$71$7d$89$8e$8j$91$97$9c$aa";
w["<def_gfun>"]="14*9*-1";
w["<def_ufun>"]="14*9*-1";
w["<resource>"]="14*9*-1";
w[">"]="2*303*17$3e$3l$4a$4i$4q$4u$57$5d$6g$8f$8r$af$aq$bh$c6$hs$jo$kl$kt$l0$l8$lf$lm$m3$m6$md$ml$rp$rt$s1$s7$sf$v6$va$ve$vi$vs$101$10c$10g$10r$113$11e$11o$11t$129$12d$12r$12v$139$13n$141$14b$14h$150$154$16f$16i$17g$184$191$19a$1e6$1ea$1ef$1eq$1eu$1f6$1fh$1fm$1g2$1g6$1ga$1go$1h2,4*46*17$bf$cg$cm$en$er$f1$fc$fn$g3$g7,6*0*21,7*20*5t$63$7e$7i,8*3*j$13$17$3a,11*0*11,12*0*u,13*45*24$2c$2f$2l$57$5b$5f$5j$64,14*201*g$11$16$1j$1v$2i$32$37$3c$4m$5q$6f$6l$71$75$7b$7p$7v$8e$8i$8n$95$9b$9q$9u$a3$a7$ab$ah$bc$bp$c6$fd$g9$gc$gr$h1$hu$i2$i7$in$it$j9$jd$ji$jm$js$lc$lg$m0,15*46*5$d$v$23$2c$2m$43$4l$53$73$7h$8d$8i$8m$96$9b$a9$ad";
w["@staticmethod"]="2*0*ov";
w["about"]="5*0*2e,15*0*29";
w["above"]="5*0*45";
w["abs"]="2*1*13g$1gh";
w["access"]="1*0*23,3*10*-1$u,8*0*1b";
w["accessed"]="1*0*1f";
w["accum"]="6*0*1s,8*0*36,13*0*1n,15*0*1v";
w["accum."]="15*0*1u";
w["across"]="13*0*30,14*10*ch$cr";
w["activity"]="2*0*15a";
w["ad"]="15*0*bm";
w["add"]="2*0*83,9*1*1a$1f";
w["adding"]="2*0*18i,5*0*f,7*0*5j,9*0*u";
w["adds"]="10*0*t";
w["advantage"]="2*0*17t";
w["affected"]="2*0*15i";
w["after"]="2*1*cf$i1,4*0*ba,9*0*3b";
w["ak"]="2*1*11c$1ff";
w["algorithm"]="4*2*af$am$e4";
w["all"]="1*10*-1$r,2*0*187,4*3*28$2m$42$5n,8*1*1f$23,12*0*1f";
w["allowing"]="15*0*18";
w["allows"]="14*1*21$2k,15*0*5m";
w["along"]="14*0*hi";
w["alpha"]="0*0*1g";
w["also"]="2*3*dj$fu$h6$167,5*0*1v,14*10*-1$r";
w["alternate"]="2*9*163";
w["alues"]="4*0*3s";
w["alues."]="4*0*3r";
w["amount"]="2*0*15r";
w["amounts"]="5*0*1s";
w["analysis"]="9*11*-1$l$15,15*0*1a";
w["analysis."]="9*11*-1$k$14";
w["another"]="2*0*17s";
w["anual"]="14*3*3g";
w["any"]="2*0*al,4*0*5f";
w["appears"]="9*1*3r$4d";
w["appears:"]="9*0*4c";
w["append"]="2*16*np$o1$o8$og$ol$or$pr$q3$qa$qi$qn$r2$1ci$1cs$1d5$1d9$1do";
w["appendix"]="0*30*1$6";
w["appendix:"]="0*30*0$5";
w["appends"]="2*0*pn";
w["applications"]="2*11*-1$d$11,13*10*-1$d";
w["applied"]="1*0*1n";
w["applies"]="14*0*cq";
w["apply"]="1*35*2$8$13,3*20*-1$-1$g$r,8*1*1c$1u,13*11*-1$m$3s,14*0*do";
w["args"]="1*32*5$b$16$28,14*3*7k$90$gk$ig";
w["argument"]="1*0*29,3*10*-1$n,14*0*56,15*0*b2";
w["arguments"]="1*9*17,3*9*18,4*29*s$38$4u$6l$8j,6*9*t,7*15*o$1p$3b,10*9*1d,11*9*m,13*0*19,14*1*dj$ec";
w["arguments:"]="4*20*37$4t$6k$8i,7*15*n$1o$3a";
w["array"]="0*0*1m,3*0*1j,4*5*1v$5b$7f$7n$8p$91,14*0*hp,15*1*9g$ag";
w["array."]="3*0*1i";
w["array/cstring"]="0*0*1l";
w["arrays"]="0*0*22,4*9*3o$79$7d$7u$7v$84$87$8f$8k$8n,7*3*1e$1f$1q$1t,14*2*e2$h9$l1";
w["assign"]="2*0*do";
w["attempt"]="9*0*2b";
w["attribute"]="15*0*15";
w["attributes"]="2*1*hp$1a8,14*0*3o";
w["attributes."]="14*0*3n";
w["automatically"]="2*1*ar$bo,15*1*2o$45";
w["autosales"]="7*1*5r$7m";
w["average"]="4*1*br$bt";
w["avg"]="2*13*105$107$10o$121$123$12o$13h$13k$1ej$1el$1fq$1fs$1gi$1gl,4*3*f5$fa$fg$fl";
w["axis"]="13*1*3g$48,14*0*hs";
w["b"]="4*1*7o$92";
w["back"]="2*0*h8,5*0*1e";
w["ball"]="4*0*e5";
w["base"]="2*17*3g$96$hr$lu$18s,4*6*1j$3f$cb,7*5*v$5o,10*12*-1$e$14$29,11*0*19,13*5*1s,14*5*g4,15*1*4p$92";
w["based"]="4*0*ad";
w["basic"]="15*0*7v";
w["because"]="2*0*an";
w["become"]="15*1*6u$76";
w["been"]="2*0*18a";
w["before"]="2*1*a1$c3,8*0*2q";
w["begin"]="2*0*cl";
w["beginning"]="2*1*la$17k";
w["below"]="15*0*bg";
w["below:"]="15*0*bf";
w["big"]="2*3*125$13j$1fu$1gk";
w["bigint"]="0*0*12";
w["boolean"]="7*0*3j";
w["both"]="2*0*dd";
w["break"]="2*1*10m$12m";
w["breaks"]="2*5*4d$9i$vv$11r$1ed$1fk,4*0*eu";
w["brown"]="14*0*ak";
w["bu"]="2*0*12k";
w["build"]="2*0*j2,15*10*-1$l";
w["building"]="2*9*is";
w["builds"]="2*0*168";
w["business"]="5*0*3i";
w["byte"]="0*0*23";
w["bytes"]="0*1*1q$2i";
w["c"]="4*1*7p$93";
w["c1"]="7*0*2e";
w["c2"]="7*0*2f";
w["ca"]="2*1*118$1fb";
w["cache"]="2*1*hl$k2,10*0*u";
w["cache."]="2*1*hk$k1";
w["cached"]="2*0*18b";
w["caches"]="2*0*k8";
w["call"]="1*11*-1$h$2k,10*0*1r";
w["called"]="14*0*46";
w["can"]="2*6*ck$ep$gg$js$k4$u5$17b,4*0*6s,5*2*m$2l$38,8*11*-1$d$t,12*0*o,13*0*1l,14*20*-1$-1$9$q,15*12*-1$h$1m$69";
w["cannot"]="4*0*5e";
w["case"]="2*1*bt$172";
w["cbreaks"]="2*0*4f";
w["cdata"]="2*26*5f$ch$mn$sh$14j$19c,4*5*co,7*5*65,9*0*1g,12*0*1o,13*5*2n,14*30*6n$81$9d$h3$iv$ju,15*2*7r$8f$9d";
w["change"]="2*1*er$f2";
w["changed"]="2*2*fc$fj$fv";
w["changes"]="7*0*7u";
w["chartreuse"]="15*0*9s";
w["class"]="2*0*n0,8*0*1j,12*0*10";
w["classes"]="12*0*18";
w["clean"]="14*5*4q$4u$7h$8t$bk$c1";
w["clean_lemmatizer_to_csl"]="14*0*4t";
w["clean_tokenizer_to_csl"]="14*0*4p";
w["cleaner"]="2*0*uh";
w["click"]="9*3*2n$31$3m$4h";
w["close"]="2*0*jj";
w["cloud"]="5*0*4e,9*0*24,12*0*24";
w["cloud."]="5*0*4d";
w["cluster"]="4*0*cs";
w["clustering"]="4*0*ae";
w["cnt"]="2*0*4o,4*0*g1";
w["cnts"]="4*0*fs";
w["code"]="1*10*-1$q,2*77*-1$h$14$59$6f$8q$ac$ap$be$c5$cp$cr$f1$ih$ip$jn$kh$mf$rs$s9$v9$14d$153$16h$170$17f$183$193$1e9,4*12*16$a7$aj$ci$eq,5*2*1a$22$2g,7*11*5v$7h$7t,8*12*-1$n$10$22,9*22*-1$i$m$12$18$1d$1n$1o$2l$2u$3n$3p$4b$4g,11*0*10,12*1*r$1h,13*20*-1$h$2h$5a,14*60*-1$o$7r$8h$97$9t$gt$i1$ip$jc$jo$lf,15*47*2$a$u$1e$1s$22$2b$2j$42$4k$52$68$72$7e$8a$8h$8l$98$ac";
w["code."]="2*1*co$io,8*10*-1$m,14*10*-1$n,15*0*1d";
w["code:"]="9*0*2k";
w["col"]="13*0*4u,15*2*9p$a7$al";
w["col_names"]="15*1*9o$ak";
w["color"]="15*1*a6$bd";
w["colors"]="15*0*aq";
w["colors."]="15*0*ap";
w["cols"]="4*38*5$b$q$2b$30$45$4m$5q$9o$d8,13*2*2o$3t$42,14*0*ae";
w["column"]="1*0*1t,2*6*6q$9c$fa$fh$fs$gb$ij,3*22*-1$-1$7$i$1b$1f,4*2*2e$49$5u,6*0*15,7*2*24$2c$2l,14*3*ep$fq$fs$m8";
w["column."]="14*0*fp";
w["columns"]="0*0*1s,1*33*3$9$14$18$1e,2*0*f5,4*1*2o$9b,13*3*2b$2p$4e$5p,14*2*5n$dm$f3,15*0*ba";
w["columns."]="4*0*2n";
w["comma"]="14*0*el";
w["comma-separated"]="14*0*ek";
w["common"]="6*0*1g";
w["completely"]="15*0*5v";
w["compute"]="5*0*16";
w["consists"]="14*0*1l";
w["contain"]="0*0*1t,15*0*1b";
w["containing"]="14*0*fk,15*2*ah$ao$b8";
w["contains"]="2*2*t$ic$158,4*0*9j,5*10*-1$3,7*0*4g,8*10*-1$8,12*11*-1$a$17,13*0*18,14*2*ds$ft$ma";
w["context"]="14*0*2c";
w["continue"]="2*0*8l";
w["control"]="2*0*u0,5*1*14$1d";
w["convert"]="4*0*8e";
w["coords"]="4*1*dc$ec";
w["copy"]="7*0*2s";
w["core"]="14*0*6u";
w["count"]="4*0*c3";
w["cover"]="2*2*sl$tc$v1";
w["create"]="2*9*2k$5i$87$8v$9b$9g$dk$k5$km$l3,6*1*1n$1u,8*1*2d$37,13*0*1o,14*5*23$2m$5h$5l$eu$fn";
w["created"]="14*0*ff";
w["creates"]="7*2*j$1k$36";
w["creating"]="10*0*21,14*0*k8";
w["csl"]="14*9*4s$50$5t$60$7j$8v$bg$bm$bt$c3";
w["csl_lems"]="14*0*5v";
w["csl_toks"]="14*0*5s";
w["cstring"]="0*0*1n";
w["current"]="2*2*ec$em$16v,4*1*14$b6,7*0*55,11*0*u,15*1*4d$5i";
w["currently"]="9*0*3c";
w["dat"]="7*1*6d$7c,14*2*kb$l2$la";
w["data"]="0*42*-1$2$7$d$m$24,1*0*1u,2*1*dh$ka,3*21*-1$-1$8$j$1g,4*1*a0$b3,5*2*17$1j$1u,6*32*3$9$q$u,8*0*2m,10*32*2$6$l$1e,14*12*cj$cv$er$f7,15*2*3i$6n$6s";
w["data."]="2*0*dg,5*0*1t,14*2*cu$eq$f6";
w["dataframe"]="2*6*5p$84$dm$gj$14m$14r$1e4,4*2*6b$83$9d,6*0*10,7*2*3e$3v$49,10*1*s$1f,13*1*1c$4c,15*3*6p$74$a3$b1";
w["date"]="2*4*nj$op$r0$1b7$1dm";
w["datetime"]="2*5*mt$mv$p5$1al$1an$1as";
w["db"]="4*1*dt$eh";
w["db.labels_"]="4*0*eg";
w["dbscan"]="4*6*aa$b1$cu$du$ee$ev$fu";
w["dbscan_labels"]="4*0*b0";
w["dec"]="0*0*q";
w["decimal"]="0*0*p";
w["def"]="2*6*n3$p1$p9$r6$si$1ao$1ba,13*0*32,14*65*-1$-1$v$14$1t$2g$35$3a$4k$7e$8l$8q$a1$gf$i5$ia$jg";
w["def_gfun"]="14*2*13$2f$39";
w["def_ufun"]="14*23*u$1s$34$4j$7d$8p$ge$i9";
w["default"]="0*0*1b,2*0*cc,4*8*19$1m$23$2h$3t$4c$5i$61$73,7*3*18$27$2o$3m,14*0*g6,15*1*4t$94";
w["default.lonely"]="2*0*cb,15*0*4s";
w["define"]="14*22*-1$-1$h$17$4n$d2";
w["defined"]="2*0*15o";
w["defines"]="14*1*42$cm";
w["defining"]="14*39*0$4$3r";
w["definitions"]="14*0*1m";
w["defop"]="2*21*kk$ll$m8$s0$s3$vd,13*10*26$5i";
w["delta"]="2*5*nh$of$qm$1b5$1d8$1dd";
w["demonstrates"]="2*0*2i";
w["demos"]="2*7*3i$99$c1$m0$10v$15d$18u$1f2,4*1*a3$cd,7*1*5q$7l";
w["demos.autosales"]="7*0*7k";
w["demos.stations"]="2*2*98$c0$15c,4*0*a2";
w["density"]="4*5*ac$al$ao$bo$c0$c7";
w["density-based"]="4*0*ab";
w["density."]="4*0*bn";
w["description"]="8*9*o,10*9*n,12*9*j,14*0*3i";
w["detailed"]="14*0*3h";
w["determine"]="2*0*1bh,4*0*an";
w["developing"]="5*10*-1$8";
w["df"]="2*3*5q$61$8d$dn,4*3*d4$dd$ed$em,7*1*33$3c,13*2*1a$4a$4r";
w["dic"]="2*0*pm";
w["dict"]="0*0*2a";
w["dictionary"]="2*2*hc$hm$1a6";
w["dictionary."]="2*0*hb";
w["did"]="2*0*ai";
w["different"]="2*0*24";
w["difficult"]="2*0*18o";
w["directly"]="5*0*2u";
w["discussed"]="2*0*bb";
w["disseminate"]="1*10*-1$o,8*0*20";
w["dist"]="1*35*1$7$12,3*20*-1$-1$f$q,8*0*1t,13*17*-1$l$28$3r$5m";
w["dist_matrix_mul"]="13*5*5l";
w["distribute"]="13*0*2v";
w["distributed"]="13*9*v";
w["doesn"]="2*0*jh";
w["doesn't"]="2*0*jg";
w["dog"]="14*2*aq$b0$b7";
w["dog's"]="14*1*av$b6";
w["don"]="5*0*2a";
w["don't"]="5*0*29";
w["done"]="15*0*1o";
w["dot"]="13*0*3l";
w["down"]="9*0*45";
w["drop"]="9*0*44";
w["drop-down"]="9*0*43";
w["drops"]="2*0*16n";
w["dtype"]="4*1*7j$8t,7*0*20,15*0*9l";
w["dtype."]="7*0*1v";
w["e"]="2*0*2g";
w["each"]="1*11*-1$g$2j,2*2*9k$15t$19n,4*1*bv$c6,7*0*1u,10*0*1q,14*3*52$cs$g0$mf";
w["ears"]="14*0*b1";
w["easy"]="9*10*-1$f";
w["edit"]="9*0*4e";
w["educational"]="2*0*uf";
w["effect"]="4*0*12,10*0*20,11*0*s,15*0*4h";
w["effectively"]="15*0*75";
w["either"]="4*0*6t";
w["elev"]="2*26*3p$3t$4g$7g$7i$7s$9e$fl$fo$108$10a$10p$124$127$12p$13e$13i$13l$13v$1em$1eo$1ft$1g0$1gf$1gj$1gm$1h0";
w["elev."]="2*0*fn";
w["elev_level"]="2*0*9d";
w["elevation"]="2*0*9o";
w["else"]="2*0*1df";
w["empty"]="4*0*5g";
w["en"]="14*0*6t";
w["encode"]="13*0*51";
w["end"]="2*0*jk,15*0*65";
w["enter"]="2*4*1i$2o$52";
w["entering"]="2*0*cm,15*0*3s";
w["entire"]="2*7*16d$16l$16o$177$17v$198$19f$1bk";
w["entire_"]="2*0*197";
w["entirely"]="15*0*5u";
w["eps"]="4*0*dv";


// Auto generated index for searching.
w["epsilon"]="4*1*dm$e0";
w["equal"]="1*0*2p";
w["error"]="7*1*3q$45,9*0*27";
w["especially"]="5*0*1m";
w["ession"]="15*0*3v";
w["etc"]="2*1*g8$ts,7*0*2h";
w["etc."]="7*0*2g";
w["eval"]="2*2*sk$tb$v0";
w["evaluation"]="2*1*tk$u2";
w["examine"]="15*0*55";
w["example"]="2*33*1f$21$2a$2h$8s$ir$iv$ue$162$166,4*10*9q$9s,7*10*4m$4o,13*10*u$13,14*20*3q$41$ce$cl,15*12*64$6m$6r$8n$8q";
w["example:"]="2*27*29$iq$161,13*9*t,14*18*3p$cd";
w["example_data_frame"]="15*0*6q";
w["examples"]="2*42*-1$2$5$7$v$175,13*40*-1$2$5$7,15*1*bk$bp";
w["examples."]="2*0*174,15*0*bo";
w["exist"]="5*0*4c,8*0*32,9*0*23,12*0*23";
w["exit"]="2*2*1s$32$8i";
w["exiting"]="8*0*2r,15*1*50$70";
w["expand"]="2*2*111$137$1f4";
w["extract"]="2*0*dc,15*0*5b";
w["f"]="0*0*r,14*1*go$gq";
w["false"]="7*0*3o";
w["finally"]="2*0*ge,14*1*5f$f8";
w["finished"]="2*1*jq$186,5*0*1b";
w["first"]="2*2*8u$f8$1a0,14*1*d0$hb";
w["first1"]="2*2*13s$149$1gt";
w["fit"]="4*0*e9";
w["fl"]="2*1*11b$1fe";
w["flag"]="2*1*16m$180";
w["float"]="0*0*s,14*1*dr$dv";
w["folder"]="13*0*21";
w["follow"]="9*0*30";
w["follow."]="9*0*2v";
w["following"]="0*10*-1$a,2*16*-1$6$2e$a8$i8$iu$165$1h8,4*0*9r,7*2*4n$7r$83,8*10*-1$6,9*0*48,12*11*-1$8$k,13*12*-1$6$12$6a,14*3*40$cc$ck$m6,15*0*8o";
w["following:"]="2*2*a7$i7$1h7,7*1*7q$82,13*0*69,14*1*cb$m5";
w["follows"]="15*1*6i$88";
w["follows:"]="15*1*6h$87";
w["format"]="13*0*4v";
w["found"]="5*0*2q";
w["fox"]="14*0*al";
w["frame"]="15*1*6o$6t";
w["from"]="0*10*-1$f,2*11*1l$63$6j$ds$e6$g0$ie$im$ms$tf$uo$1ak,4*5*2r$4h$7t$a1$cp$cv,7*4*e$1d$31$51$6a,9*0*41,10*0*1k,14*0*l0,15*0*40";
w["fromtimestamp"]="2*1*p6$1at";
w["fun"]="1*35*4$a$15$1j$21$2b$2l,2*4*4n$104$120$1ei$1fp,3*10*-1$m,4*2*f9$fk$g0";
w["function"]="1*22*-1$-1$i$m$1k$1m,2*1*t8$u9,4*0*8d,8*3*1e$1q$27$2g,13*32*1$4$1h$2u,14*11*53$cg$co$fj";
w["function."]="2*0*u8";
w["functionality"]="5*0*2p";
w["functions"]="2*10*-1$i,8*42*-1$2$5$c$r$19,12*41*-1$3$7$e$n,13*10*-1$j,14*55*-1$3$7$1b$26$2s$3v$4o$5a$66$d5,15*0*7n";
w["functions."]="14*0*2r";
w["functions:"]="14*0*d4";
w["g"]="2*2*13r$148$1gs";
w["g_first1"]="2*0*147";
w["gd"]="2*4*5o$5u$62$d5$dr,4*1*d3$d5";
w["general"]="7*1*5g$6n";
w["generally"]="0*0*18";
w["generate"]="7*0*2b";
w["generated"]="2*0*g3";
w["get"]="1*0*26,2*1*r7$14u,3*45*-1$1$4$11$16,8*1*2j$2l,9*0*26,13*11*-1$r$3c,15*0*3k";
w["getdata"]="2*15*-1$k$5m$60$d3$d7$kc,4*54*-1$1$7$d$j$2q$35$4g$4r$65$78$7r$9g$d2,7*1*4u$6f,12*0*19";
w["gets"]="15*0*46";
w["gfun"]="14*12*-1$15$2h$3b";
w["given"]="14*0*e3";
w["gm"]="7*1*5e$6r";
w["good"]="15*0*7j";
w["gram"]="13*5*36$41$43$4d$4k$54";
w["grams"]="13*1*3p$47";
w["group"]="14*31*2$6$2p";
w["grouped"]="4*0*bk";
w["guide"]="5*10*-1$2";
w["hands"]="5*1*13$1c";
w["has"]="1*0*22,2*0*hn,10*0*1u";
w["have"]="2*3*ei$hv$tu$189,3*10*-1$t,8*0*1a";
w["have."]="2*0*hu";
w["haversine"]="4*0*e8";
w["having"]="2*0*176";
w["helper"]="4*0*8c";
w["here"]="2*3*55";
w["high"]="2*3*46$48$9u$a0";
w["hist"]="2*1*1bf$1cf";
w["history"]="2*24*nb$nn$nv$o6$oe$oj$oo$pp$q1$q8$qg$ql$qr$qv$rd$1b0$1cg$1cq$1d3$1d7$1dc$1di$1dl$1dv$1e5";
w["how"]="2*13*-1$9$1h$2j$j1,13*10*-1$9";
w["however"]="2*0*18h";
w["i"]="0*0*v,2*7*1a3$1af$1br$1c2$1c8$1cc$1cn$1cu,13*3*4h$4i$50$52,14*6*im$j4$j5$k6$kg$ko$kp";
w["i'm"]="14*0*k5";
w["icon"]="9*1*35$4l";
w["icon."]="9*1*34$4k";
w["id"]="15*2*9e$a5$ae";
w["ids"]="15*2*a4$aj$bc";
w["ids."]="15*0*ai";
w["ignore"]="7*1*35$3i";
w["implement"]="2*10*-1$a,13*10*-1$a";
w["implementations"]="2*0*25";
w["import"]="2*5*ak$mo$mq$mu$1ai$1am,4*1*ct$d1,14*0*6o,15*0*3h";
w["imported"]="4*0*ah,15*1*30$33";
w["imported."]="4*0*ag";
w["imports"]="2*0*as,15*0*2p";
w["includes"]="0*0*1o,4*1*av$bq";
w["including"]="2*0*15f,14*0*3k";
w["incorporate"]="5*0*2t";
w["ind"]="14*0*af";
w["index"]="2*1*1a1$1ad,7*0*48";
w["indexed"]="7*0*3t";
w["information"]="2*0*ls,5*10*-1$4,15*0*28";
w["information."]="2*0*lr";
w["init"]="2*1*n4$vo";
w["initialize"]="2*7*kr$l6$ma$vl";
w["initialize_logger"]="2*6*kq$l5$vk";
w["initializing"]="2*0*rf";
w["initially"]="14*0*h6";
w["insert"]="9*12*-1$g$2i$32";
w["inserts"]="9*0*37";
w["inside"]="12*0*1n,15*0*7q";
w["insights"]="5*18*-1$c$u$11$1f$1o$25$35$42$49,9*1*q$1v,12*11*-1$g$1v";
w["instance"]="5*0*47,9*0*21,12*0*21";
w["instantiate"]="7*0*4q";
w["instantiating"]="2*0*ct";
w["int"]="0*0*t";
w["int32"]="0*0*10,4*1*7l$8v,15*0*9n";
w["int64"]="0*2*11$14$1a";
w["integer"]="0*1*u$1c,11*0*1d,14*0*eg";
w["integration"]="5*0*34";
w["interfacing"]="5*0*3c,12*10*-1$f";
w["introduction"]="5*30*0$1";
w["invoke"]="2*0*aa,15*0*2h";
w["iris"]="13*0*22";
w["isoformat"]="2*6*ot$p2$p8$r4$1ap$1av$1dp";
w["it."]="15*0*5c";
w["iterator"]="4*2*1t$3m$59";
w["ith"]="2*1*19t$1a9";
w["its"]="6*10*-1$i,14*1*di$eb";
w["j"]="0*0*13";
w["join"]="14*1*83$9f";
w["json"]="0*3*1p$1u$2f$2p";
w["jumped"]="14*2*an$as$b3";
w["just"]="2*0*16s";
w["k"]="2*3*nc$nd$1b1$1b2";
w["kms"]="4*1*dg$dq";
w["kms_per_radian"]="4*1*df$dp";
w["label"]="4*0*fr,13*0*1e";
w["labels"]="2*8*6r$6v$72$77$7c$f4$fb$fi$ik,4*9*2g$4b$60$6i$71$b2$ef$ei$f0$fv,6*43*-1$4$a$k$r$12$17,7*10*1i$2i$2n$2u$4j$5b$5m$6k$6u$78$7v,13*0*4s,14*0*l5";
w["labels."]="6*11*-1$j$16,7*1*2m$5l";
w["labs"]="2*2*6t$7m$80";
w["lambda"]="7*0*71";
w["language"]="2*59*-1$1$4$g$15$1b$1n$2m$3a$5b$8n$93$ad$bf$bj$ig$kg$mh$sb$14f$195,4*0*ck,5*12*-1$a$k$3t,7*0*61,8*11*-1$l$11,9*33*2$6$11$1c$42,12*33*1$5$s$1k$29,13*11*-1$g$2j,14*47*-1$-1$-1$c$k$1e$1q$3t$44$5k$7t$99$gv$ir$jq,15*81*-1$3$7$b$f$s$12$16$2k$7f$82$86$8b$8s$90$99$bj";
w["language."]="2*1*1a$92,12*0*28,14*0*1p,15*10*-1$r";
w["language:"]="15*0*8v";
w["language_"]="2*4*5a$mg$sa$14e$194,4*0*cj,7*0*60,13*0*2i,14*4*7s$98$gu$iq$jp";
w["large"]="5*0*1r";
w["last"]="14*0*m7";
w["lat"]="4*2*d9$f6$f8";
w["latency"]="5*0*1l";
w["latitude"]="4*1*9u$bs";
w["lavender"]="15*0*9t";
w["lazy"]="14*2*ap$au$b5";
w["lemma"]="14*0*9h";
w["lemmatize"]="14*0*68";
w["lemmatizer"]="14*2*4v$8u$c2";
w["lems"]="14*1*61$bu";
w["len"]="2*1*1bp$1db";
w["length"]="1*0*2o,13*1*5r$5v";
w["less"]="5*0*3a";
w["level"]="2*10*3q$43$4h$7h$7j$7t$9f$9p$9r$fm$fp,4*1*c1$c9";
w["level."]="4*0*c8";
w["libraries"]="0*0*2g,2*0*am,5*0*2i";
w["libraries."]="5*0*2h";
w["library"]="2*10*m5$vh,14*20*4d$6e$aa$gb$jl";
w["like"]="2*2*a6$i6$1h6,7*1*7p$81,13*0*68,14*0*ca";
w["lines"]="2*0*f0";
w["link"]="2*23*tq$10t$11i$12h$12u$133$1f0$1g9";
w["list"]="1*14*-1$d$19$1d$2g$2n,2*0*h9,4*13*u$1r$2c$3k$47$50$57$5h$5s$7b$85$89$8l$96,6*0*14,7*4*13$1r$22$2j$6v,9*0*47,10*0*17,11*0*o,13*2*1d$3o$46,14*3*3m$dl$en$h7,15*0*an";
w["list."]="9*0*46";
w["list_of_grams"]="13*0*3n";
w["ln"]="14*0*gp";
w["load"]="14*0*6s";
w["loaded"]="2*1*b7$bn,15*0*3b";
w["log"]="2*49*kv$le$n2$pa$pl$r8$rk$s5$sp$sr$ss$t5$up$uu$v5$10i$11g$12f$131$143$14t$14v$159$15p$17m$18j$1bb$1c5$1dr$1h4";
w["log_dic"]="2*0*pk";
w["logger"]="2*29*26$it$j3$ks$l7$mb$rh$vm$164$169";
w["logging"]="2*0*17i";
w["lon"]="4*2*da$fh$fj";
w["lonely"]="2*0*cd,14*0*g7,15*1*4u$95";
w["longitude"]="4*1*9v$bu";
w["look"]="7*0*80";
w["looks"]="2*2*a5$i5$1h5,7*0*7o,13*0*67,14*1*c9$m3";
w["loop"]="2*0*19i";
w["low"]="2*1*44$9s";
w["lst"]="14*1*db$e8";
w["m"]="7*1*5k$76,13*0*4f,14*4*3f$k7";
w["m0"]="2*0*g6";
w["m1"]="2*0*g7";
w["macro"]="2*51*-1$0$3$f$19$1m$2l$39$8m$91$bi$if$kf,5*1*j$3s,8*10*-1$k,12*31*0$4$1j,13*10*-1$f,14*32*-1$-1$-1$b$j$1d$43$5j,15*44*-1$6$e$q$11$85$8u$bi";
w["make"]="5*0*2m,6*37*1$7$o$1a$1l,8*0*2a,12*0*25,13*12*-1$o$16$4p,14*0*63";
w["make_table"]="6*0*19";
w["makes"]="2*0*16a,4*0*a8,9*10*-1$e";
w["manual"]="2*3*lp,15*3*26";
w["many"]="2*0*17o";
w["map"]="7*0*70";
w["mapping"]="0*30*4$9";
w["maps"]="0*10*-1$c";
w["mat"]="13*4*34$38$3j$3m$3v";
w["matrix"]="13*16*10$29$4t$5n,14*1*he$ka";
w["md"]="2*10*5l$5r$6i$6u$71$76$7b$7n$8e$d0$e5,7*7*4s$50$66$69$6j$6t$77$7d,14*1*kt$lb";
w["md.labels"]="7*0*6s";
w["mdb"]="13*0*1u,14*0*78";
w["meaningful"]="2*0*ga,5*0*3h";
w["means"]="15*0*1f";
w["measurements"]="2*0*17n";
w["med"]="14*3*fo$fr$lk$m9";
w["median"]="14*1*fu$me";
w["merge"]="2*0*tr";
w["message"]="2*4*n8$o9$pf$qb$15q,9*0*28";
w["messages"]="2*0*18k";
w["metadata"]="2*23*-1$m$5j$5t$6a$6s$85$cu$d2$de$eb$ej$es$f3$gk,7*56*-1$1$3$5$b$d$k$1c$1l$30$37$4d$4r$54$5c$68,10*34*3$7$m$1h$1i$1m,12*0*1a,14*0*kv,15*1*1k$3j";
w["method"]="2*4*dq$e4$gp$h5$18q,7*0*43,11*10*-1$8,15*0*6c";
w["method."]="2*1*go$18p";
w["methods"]="2*0*ke,4*9*2p,7*9*c,12*0*1b";
w["methods."]="2*0*kd";
w["metric"]="4*0*e7";
w["mid"]="2*1*45$9t";
w["min"]="4*0*e1";
w["minimizes"]="5*0*1i";
w["mk"]="14*3*da$e7$id$lo";
w["mk_lst_nms"]="14*1*d9$e6";
w["model"]="0*0*29";
w["modifications"]="2*1*1p$2s";
w["modified"]="2*0*ii";
w["modifies"]="4*1*32$4o";
w["modify"]="15*0*5o";
w["modifying"]="15*0*5q";
w["module"]="2*0*b6,15*0*3a";
w["more"]="2*2*g9$lq$18n,5*0*3e,15*1*27$bl";
w["motors"]="7*1*5i$6p";
w["motors'"]="7*1*5h$6o";
w["msg"]="2*16*mc$mj$mk$ng$o7$q9$ro$s6$sd$se$t0$vn$10j$11h$12g$132$144";
w["mul"]="13*7*2a$35$40$5o";
w["multi"]="7*1*3s$47";
w["multi-index"]="7*0*46";
w["multi-indexed"]="7*0*3r";
w["multiindex"]="7*1*34$3h";
w["multiindex_ignore"]="7*0*3g";
w["multiplication"]="13*9*11";
w["must"]="3*10*-1$s,5*1*3v$4b,8*0*31,9*0*22,12*0*22";
w["n"]="14*2*ef$ii$j8";
w["name"]="2*9*3o$m9$s4$106$122$13c$19s$1ek$1fr$1gd,3*0*1a,4*5*67$6d$6n$f4$ff$fq,13*0*27,14*8*6j$79$7g$8s$bf$bs$gh$ic$lj";
w["name_type"]="4*1*6c$6m";
w["named"]="8*0*2v,15*1*59$bb";
w["names"]="2*4*7o$ft$g5$gd$il,4*9*2f$4a$5v$6g$6v$76$80$88$95$9a,7*5*1g$21$26$2d$2t$4i,13*0*1g,14*1*eo$l3,15*2*9q$a8$am";
w["names."]="2*0*gc,7*0*25,13*0*1f";
w["natural"]="14*9*3s";
w["nc"]="14*2*k0$kj$ks";
w["necessary"]="9*0*1e";
w["need"]="2*1*aj$18d,5*11*-1$6$2c";
w["nested"]="12*0*1m";
w["new"]="2*0*hf,9*1*38$3h,10*10*-1$9,14*0*65,15*2*5d$60$77";
w["newly"]="14*0*fe";
w["next"]="2*1*e1$ev";
w["nlp"]="14*7*47$6k$6q$7a$87$9j$bj$c0";
w["nms"]="14*6*dc$e9$if$kk$l4$l6$lq";
w["none"]="4*15*l$n$p$r$1c$1p$25$2j$2v$31$3v$4f$4l$4n$5k$64,6*31*5$b$s,7*5*i$1b$1h$1j$29$2q";
w["none."]="4*3*1b$1o$4e$63,7*0*1a";
w["nops"]="2*1*1bo$1bu";
w["normally"]="15*0*4r";
w["note"]="0*0*2c,2*2*i9$ph$ud,5*0*3m,9*0*1s,10*0*1p,12*0*1s,14*1*2u$k4";
w["note:"]="0*0*2b,2*0*uc,5*0*3l,9*0*1r,10*0*1o,12*0*1r,14*0*2t";
w["notes"]="0*0*o";
w["now"]="2*1*eq$i4,4*1*au$b4,5*0*n,9*0*s,15*13*-1$i$13$1n$5g";
w["np"]="4*6*7e$7k$7m$8o$8u$90$ea,13*1*39$44,14*2*hm$ho$kc,15*2*31$9f$9m";
w["nr"]="14*1*jv$kf";
w["nrows"]="2*4*ne$no$pq$1b3$1ch";
w["num"]="14*1*ie$lp";
w["number"]="1*0*2q,2*1*15g$1bi,11*11*-1$a$1f,15*0*3l";
w["numeric"]="4*2*20$3p$5c";
w["numpy"]="0*0*16,2*0*at,3*0*1h,4*6*1u$3n$5a$7c$86$8m$de,7*0*1s,14*1*h8$hd,15*2*2q$2v$af";
w["ny"]="2*1*11a$1fd";
w["object"]="1*11*-1$k$1l,2*4*5k$5n$cv$d4$ek,4*11*-1$f$9h,7*14*-1$7$m$1n$39$4e,10*0*1j";
w["object."]="1*10*-1$j,4*10*-1$e,7*13*-1$6$l$1m$38";
w["objects"]="2*0*db";
w["occurs"]="2*0*tn";
w["once"]="2*0*181,5*0*18";
w["only"]="5*0*2r";
w["op"]="2*8*k6$nf$o0$q2$19u$1a7$1aa$1b4$1cr,10*11*-1$g$2b";
w["op."]="10*11*-1$f$2a";
w["open"]="14*0*4b";
w["operation"]="2*4*28$j4$15j$15l$15u,9*8*n$19$2e$2o$33$39$3e$3i$4j";
w["operation."]="2*0*27,9*0*2d";
w["operations"]="15*13*-1$n$1j$4g$7p";
w["ops"]="2*44*64$65$6b$6m$8a$bl$bs$dt$du$e9$gv$ko$n6$ns$o2$pd$pu$q4$rm$sm$su$t2$tp$uk$ur$v2$14n$16q$16t$179$18m$1a2$1ae$1bd$1bj$1bq$1c0$1c1$1c7$1ca$1cb$1cl$1ct$1dt$1e0,4*43*2$8$k$t$11$4i$4j$4p$4v$54$9l$as$bb$d6$ej,7*4*h$12$17$6g$79,10*0*15,11*44*-1$2$6$d$k$n$r$1i,14*0*l7,15*21*-1$o$48$4b$5a$5e$5r$62$6g$6j$9v$b4$b6";
w["ops."]="4*0*53,7*0*16";
w["optimized"]="2*0*th";
w["optional"]="4*3*3j$46$56$5r,6*0*13";
w["order"]="1*0*1r,2*0*pi,3*10*-1$c,5*0*3n,6*0*1t,8*0*35";
w["original"]="7*0*7j";
w["orjson"]="0*0*2j";
w["others"]="0*0*2k";
w["our"]="2*4*he$hh$ju$rg$17d";
w["out"]="2*1*9j$14l";
w["output"]="2*0*2v";
w["over"]="2*0*19j,14*2*ao$at$b4";
w["own"]="14*12*-1$19$25$2o";
w["p"]="14*2*dq$gm$hr";
w["package"]="0*0*28";
w["package/model"]="0*0*27";
w["pandas"]="0*0*17,2*3*66$au$dl$dv,4*6*66$6a$7s$82$8h$9c$db,6*0*v,7*3*32$3d$3u$6i,13*0*1b,15*1*2r$32";
w["pandas."]="4*0*8g";
w["panel"]="9*3*1q$3a$3j$3q";
w["panel."]="9*0*1p";
w["particular"]="14*0*1o";


// Auto generated index for searching.
w["pass"]="2*1*gh$uj";
w["passed"]="1*0*2a,7*1*41$4b";
w["passed."]="7*1*40$4a";
w["path"]="2*3*6k$e7$ha$hd,4*2*1l$2s$3h,7*3*f$11$52$6b,11*0*1b";
w["path."]="4*1*1k$3g,7*0*10,11*0*1a";
w["pattern"]="2*0*tl";
w["pd"]="2*1*14q$1e3,13*0*4b,15*2*34$a2$b0";
w["pd.dataframe"]="15*0*av";
w["pe"]="13*0*60";
w["per"]="4*1*dh$dr";
w["perform"]="2*4*1o$2r$38";
w["performs"]="4*1*ak$bg";
w["periwinkle"]="15*0*9u";
w["petal"]="13*0*5u";
w["place"]="2*0*17c";
w["placed"]="8*10*-1$e";
w["platform"]="5*18*-1$e$10$12$1h$1p$26$36$43$4a,9*1*r$20,12*11*-1$i$20,15*1*2n$54";
w["platform."]="5*12*-1$d$v$1g,12*10*-1$h";
w["populated"]="14*0*f4";
w["practice"]="6*0*1h,15*0*7k";
w["preloaded"]="8*0*1l,12*0*12";
w["prepares"]="6*10*-1$c";
w["previous"]="2*0*173";
w["problems"]="5*0*3k";
w["problems."]="5*0*3j";
w["process"]="2*1*jb$jd";
w["processes"]="1*13*-1$u$1i$20$2e,3*10*-1$b,5*1*r$1q,8*1*26$2p";
w["processing"]="14*9*3u";
w["procs"]="3*10*-1$l";
w["produce"]="0*1*2h$2l";
w["protects"]="2*0*td";
w["put"]="15*0*7l";
w["python"]="0*14*-1$i$l$15$26$2e,1*20*-1$-1$l$p,2*50*-1$c$10$16$1j$1r$1t$2c$2p$2u$33$53$5c$8j$a2$ae$b8$bg$bp$cn$cq$i2$in$j6$j8$je$jv$k7$mi$sc$14g$196,4*2*a6$ai$cl,5*18*-1$9$g$l$15$19$21$2s$33$3p,7*1*62$7s,8*37*0$3$12$1d$1m$1p$21$2s$33,9*48*-1$1$5$h$10$1b$1m$2j$2t$40$4a$4f,12*34*2$6$t$13$1g$27,13*11*-1$c$2k,14*30*-1$-1$m$1a$49$5e$6i$7u$9a$cp$es$h0$is$jr,15*56*-1$4$c$k$17$1c$1q$1r$2l$3c$3t$56$67$7g$7m$81$8c$8g$8r$9a";
w["python."]="0*11*-1$h$25,2*0*j5,14*0*5d,15*0*1p";
w["q"]="2*6*rj$so$sq$t4$ut$v4$14s";
w["q_log"]="2*0*ri";
w["quantile"]="14*22*cf$cn$d8$dg$dp$dt$e5$fi$fl$gj$hh$hn$ln$md";
w["quantile."]="14*0*e4";
w["query"]="2*20*2n$3b$6d$90$a4$ef$en$lb$li$n1$16p$178$17p$18g$19l$1bc$1bl$1c6$1ds,4*0*b9,5*2*t$27$30,7*0*58,15*16*-1$m$1i$4f$5l$5p$5t$7a";
w["query."]="2*2*ee$lh$18f,4*0*b8,7*0*57,15*1*5k$79";
w["querylog"]="2*0*rl";
w["quickest"]="14*0*aj";
w["r"]="14*8*82$9e$d7$df$fh$gi$hl$j0$lm";
w["r_quantile"]="14*2*d6$de$fg";
w["radian"]="4*1*di$ds";
w["radians"]="4*0*eb";
w["ragged"]="0*1*1k$21";
w["randn"]="14*0*ke";
w["random"]="14*1*f5$kd";
w["range"]="2*0*1bs,4*2*1s$3l$58,13*1*4j$53,14*2*j6$kh$kq";
w["range2list"]="2*0*3s";
w["raw"]="2*0*df";
w["rebase"]="2*17*-1$q$86$8c$gn$gu$h4$14p$1e2,4*0*el,7*0*7b,10*38*1$5$k$q$1t$27,12*0*1d,14*0*l9,15*3*6b$6l$a1$at";
w["recursion"]="2*0*19g";
w["reference"]="2*3*lo,14*3*3e,15*3*25";
w["references"]="10*0*18";
w["regression"]="13*0*1v";
w["related"]="15*0*7o";
w["relative"]="11*11*-1$c$1h";
w["relevant"]="1*0*1o,14*0*1n";
w["rename"]="2*0*6p";
w["replace"]="2*3*7f$7r$7u$fk,15*0*5s";
w["replacing"]="7*0*5d";
w["represen"]="1*0*1b";
w["represent"]="15*0*5h";
w["representing"]="4*3*v$1i$3e$51,7*1*u$14,11*2*p$18$1e";
w["represents"]="4*0*b5,15*0*4c";
w["required"]="6*0*1r";
w["rerun"]="2*0*18e";
w["resource"]="14*43*-1$-1$f$i$1i$31$45$6h$74$77$a6";
w["respectively"]="14*0*6c";
w["respectively."]="14*0*6b";
w["rest"]="2*0*fg";
w["resulting"]="2*2*gi$155$1h3,13*0*65,14*1*c7$m1";
w["results"]="2*0*188";
w["retrieve"]="1*0*1s,3*10*-1$h";
w["retrieved"]="3*0*1d";
w["retrieved."]="3*0*1c";
w["retrieves"]="4*0*9t";
w["return"]="1*21*-1$-1$c$e$2h,2*5*gq$p4$ra$t1$um$1ar,3*10*-1$6,4*10*-1$c,7*10*-1$4,13*0*3i";
w["returned"]="4*0*bc";
w["returns"]="1*9*2f,2*0*h7,3*9*1e,4*15*27$2l$41$5m$69$7a$81$9f,6*10*18$1c,7*9*4c,10*21*-1$d$13$25$28,11*19*-1$9$1c,13*0*1i,14*2*57$du$eh,15*0*b3";
w["row"]="9*10*-1$a,14*13*29$2a$ci$ct$g2$mh";
w["row-by-row"]="14*0*28";
w["row."]="14*1*g1$mg";
w["rows"]="2*14*-1$o$nr$pt$15h$1ck,4*40*4$a$o$1q$2a$2u$3i$44$4k$55$5p$9n,11*46*-1$1$5$b$j$1g,12*0*1c,14*1*f1$hk,15*0*3m";
w["rows."]="4*2*29$43$5o,14*0*hj";
w["run"]="5*0*o,15*0*1t";
w["running"]="5*0*s";
w["s"]="2*3*7e$7l$7q$7v,14*12*7n$7o$88$8c$93$94$9k$9o$ed$ih$ik$il$j1,15*0*3u";
w["same"]="2*1*ho$ja,5*0*q";
w["sample"]="9*0*49";
w["samples"]="4*0*e2";
w["scalar"]="14*0*2b";
w["scatter"]="2*0*lc";
w["scope"]="2*1*u1$1bn";
w["scope!"]="2*0*1bm";
w["sea"]="2*1*42$9q";
w["second"]="2*1*20$1ac";
w["section"]="2*0*s,8*11*-1$7$s,12*10*-1$9";
w["see"]="2*0*lj,14*0*2v,15*1*20$bh";
w["segments"]="1*1*1q$2s,8*0*1g";
w["segments."]="1*1*1p$2r";
w["segs"]="13*0*31";
w["sel"]="2*22*115$11k$13p$145$1f8$1gq";
w["select"]="9*0*3v";
w["selected"]="9*0*3d";
w["self"]="2*19*n5$na$nm$nu$o5$od$oi$on$os$pc$po$q0$q7$qf$qk$qq$qu$r3$r9$rc";
w["self.history"]="2*1*n9$rb";
w["sent"]="14*3*6a$ag$bn$c4";
w["sepal"]="13*1*5q$5s";
w["separate"]="5*0*2f";
w["separated"]="14*0*em";
w["session"]="2*23*1k$1v$2d$2q$35$54$8k$a3$ba$br$i3$j9$jf$k0,8*2*1o$2t$34,10*1*10$1a,12*0*15,15*2*3e$44$57";
w["session."]="2*3*1u$34$b9$bq,8*0*1n,12*0*14,15*0*3d";
w["set"]="2*2*69$ea$el,4*2*10$52$99,7*1*15$53,10*10*-1$8,11*0*q,15*2*3n$61$6e";
w["sets"]="6*10*-1$d";
w["shape"]="13*1*4l$55";
w["should"]="12*0*1l";
w["show"]="2*10*-1$8,13*10*-1$8";
w["shown"]="15*0*be";
w["shows"]="2*2*1g$22$j0";
w["side"]="10*0*1v";
w["similar"]="2*3*13d$13u$1ge$1gv,14*0*m4";
w["simpl"]="2*0*2f";
w["simple"]="2*9*1e$2b,15*0*8p";
w["simply"]="2*0*ca,9*0*1j";
w["since"]="2*1*eg$j7,5*0*20";
w["singleton"]="10*0*16";
w["skipping"]="2*0*18l";
w["sklearn"]="2*0*av,4*0*cr,15*0*2s";
w["sklearn.cluster"]="4*0*cq";
w["slices"]="2*2*17r$19k$19o";
w["slices."]="2*0*17q";
w["sm"]="14*0*70";
w["snippet"]="9*0*v";
w["solving"]="5*0*3g";
w["some"]="0*0*2d,2*1*k9$to";
w["sometimes"]="2*0*ti";
w["source"]="2*4*4l$109$126$1en$1fv,4*2*f7$fi$ft,14*0*4c";
w["spacy"]="14*2*4f$6p$6r";
w["spacy."]="14*0*4e";
w["specific"]="5*0*2o";
w["specified"]="5*0*23";
w["spend"]="5*0*39";
w["split"]="13*0*2q";
w["spreadsheet"]="9*10*-1$b";
w["stack"]="13*0*3a";
w["start"]="2*1*1d$95,5*10*-1$7";
w["starts"]="2*1*cs$19p";
w["state"]="2*19*4e$4m$6c$75$9m$bu$ed$fd$pb$st$uq$100$117$11m$11s$13t$1ee$1fa$1fl$1gu,4*0*b7,7*0*56,15*2*4e$5j$78";
w["state's"]="2*0*9l";
w["states"]="2*1*10n$12n";
w["staticmethod"]="2*0*p0";
w["station"]="4*0*bm";
w["stations"]="2*10*3j$9a$9n$c2$m1$vq$110$135$15e$18v$1f3,4*4*a4$ar$bj$c5$ce";
w["stations."]="4*0*aq";
w["stop"]="14*1*8b$9n";
w["stored"]="0*0*1v,2*1*19v$1ab";
w["str"]="13*0*4g,14*1*j3$kn";
w["string"]="4*1*1h$3d,7*0*t,11*0*17,14*4*55$59$69$ee$ei";
w["string."]="14*0*58";
w["strings"]="1*0*1a,4*1*8b$98";
w["sub"]="1*13*-1$t$1h$1v$2d,3*20*-1$-1$a$k,8*1*25$2o";
w["sub-processes."]="1*12*-1$s$1g$2c,3*10*-1$9,8*1*24$2n";
w["submit"]="9*1*2c$4i";
w["subp"]="13*1*33$3u";
w["sum"]="13*0*45";
w["supplementary"]="8*0*p,12*0*l";
w["supplied"]="2*0*b5,8*10*-1$b,12*10*-1$d,13*0*2t,15*0*39";
w["support"]="5*0*h";
w["supports"]="9*0*t,15*0*14";
w["syntax"]="1*9*v,3*9*13,4*9*g,6*9*l,7*9*8,9*0*1i,10*9*h,11*9*g,14*0*3l,15*10*7u$80";
w["syntax."]="9*0*1h";
w["system"]="2*0*g2";
w["system-generated"]="2*0*g1";
w["t"]="2*0*ji,5*0*2b,13*0*3k";
w["t0"]="2*10*oa$om$ou$qc$qj$qo$r5$1d0$1d6$1dg$1dq";
w["ta"]="2*0*12j";
w["table"]="0*10*-1$b,2*34*1q$2t$31$3h$6l$89$97$bm$bv$c9$e8$eu$f7$h3$hj$i0$ib$lv$n7$nt$pe$pv$rn$sn$sv$t3$ul$us$v3$157$15b$18t$1be$1cp$1du,4*38*3$9$m$1d$2t$33$39$9m$cc$d7,6*48*-1$2$8$f$p$1b$1e$1m,7*5*g$p$5p$6c$6h$7n,8*1*1i$2b,10*12*-1$b$1c$24,11*43*-1$3$7$e$l$13$1j,13*13*-1$p$17$1t$4q,14*15*5i$ad$bb$c8$ev$g5$k9$m2,15*4*1g$49$4o$4q$93";
w["table."]="2*2*et$f6$h2,8*0*1h,10*1*1b$23,11*9*-1";
w["table2"]="2*2*10u$1ce$1f1";
w["tables"]="10*0*12,15*0*3p";
w["tables."]="10*0*11";
w["tabu"]="2*51*4c$4t$vu$10f$10k$11q$12c$1ec$1et$1fj$1g5,4*10*be$et$g6,6*2*1o$1q$20,8*13*-1$i$16$30$39,13*11*1j$2e$4n$5e";
w["tabulation"]="2*6*3d$9h$g4$id,4*1*bh$bp,8*40*-1$1$4$g,13*41*-1$0$3$i$66,14*0*2q";
w["tabulation--"]="2*3*3c";
w["tag"]="2*8*18$6h$c8$ci$jp$16k$171$17h$185,12*0*1q,14*13*-1$d$1k$20$2j,15*4*10$2e$4n$7i$7t";
w["tag."]="2*1*c7$16j,12*0*1p,15*2*2d$4m$7s";
w["tags"]="14*0*3j";
w["tail"]="14*0*b8";
w["take"]="2*0*17l,14*0*hg";
w["takes"]="10*0*r,14*2*54$dh$ea,15*0*au";
w["tal"]="13*0*61";
w["tcol"]="2*25*4k$103$11v$1eh$1fo,4*15*f3$fe$fp";
w["technology"]="5*0*3d";
w["temp"]="15*0*3o";
w["temporary"]="2*5*30$88$h1$hi$ia$156,6*11*-1$e$1d,10*13*-1$a$v$19$22";
w["ten"]="1*36*0$6$11$25,2*55*-1$-1$-1$-1$j$l$n$p$5s$5v$8b$b0$b2$d1$d6$gm$gt$mr$nq$ps$14o$1cj$1e1,3*65*-1$-1$-1$0$3$e$p$10$15,4*37*0$6$i$d0$ek,6*36*0$6$n$1k,7*39*0$2$a$4t$67$6e$7a,8*3*1k$1s$29$2i,10*39*0$4$j$p$1l$1s$26,11*35*0$4$i,12*1*11$16,13*34*-1$-1$-1$k$n$q$15$3b$3q$4o,14*1*ku$l8,15*6*2t$36$3f$6d$6k$a0$as";
w["ten.dist_apply"]="1*5*10,3*20*-1$-1$d$o,8*0*1r";
w["ten.get"]="1*0*24,3*5*v$14,8*0*2h";
w["ten.get."]="3*9*-1";
w["ten.getdata"]="4*5*h";
w["ten.make_table"]="6*6*m$1j,8*0*28,13*0*14";
w["ten.metadata"]="7*5*9";
w["ten.rebase"]="2*1*gl$gs,10*6*i$o,15*0*ar";
w["ten.rows"]="11*5*h";
w["tenten"]="2*2*sj$ta$uv";
w["tenten_eval_cover"]="2*0*t9";
w["test"]="13*0*20";
w["text"]="0*1*1f$20,14*0*85";
w["themselves"]="14*0*5b";
w["therefore"]="5*0*2j";
w["throughout"]="2*0*lg";
w["throw"]="7*1*3p$44";
w["time"]="2*13*mp$nk$ob$oc$oq$qd$qe$r1$15s$1aj$1b8$1d1$1d2$1dn,5*1*3b$3f";
w["timeline"]="9*16*-1$d$p$2h$2p$3g$3l$3u";
w["timeline."]="9*1*3f$3t";
w["timestamp"]="2*6*ni$ok$qh$qs$1b6$1d4$1dj";
w["tokenize"]="14*0*67";
w["tokenizer"]="14*2*4r$7i$bl";
w["toks"]="14*1*5u$bh";
w["took"]="2*0*160";
w["took."]="2*0*15v";
w["total"]="2*2*7a$fe$tv";
w["transfer"]="5*0*1k";
w["transformation"]="15*0*1l";
w["transformations"]="15*0*1h";
w["treat"]="2*0*jt";
w["tree"]="4*0*e6";
w["trillion"]="9*10*-1$9";
w["trillion-row"]="9*10*-1$8";
w["trs"]="9*46*-1$3$7$c$o$2g$2m$36$3k$3s";
w["true"]="7*0*42";
w["ts"]="1*0*1c";
w["turn"]="14*0*hc";
w["turquoise"]="15*0*9r";
w["two"]="2*2*23$f9$kn,14*1*5m$d3,15*0*b9";
w["tx"]="2*1*119$1fc";
w["type"]="0*33*3$8$k$n$1e,2*0*15k,4*2*68$6e$6o";
w["type."]="0*0*1d";
w["types"]="0*10*-1$e,7*0*4k,14*3*7m$92$gn$ij";
w["ufun"]="14*53*-1$10$1u$36$4l$7f$8m$8r$a2$gg$i6$ib$jh";
w["ultrajson"]="0*0*2q";
w["unicode"]="4*5*2d$48$5t$6p$8a$97,7*1*23$2k";
w["unintuitive"]="2*0*tj";
w["up"]="2*0*16u,4*0*13,11*0*t,15*0*4i";
w["update"]="7*0*5a";
w["us"]="2*0*te";
w["use"]="0*0*19,2*3*da$e3$u6$16b,4*0*a9,5*1*2n$3o,6*0*1i,7*0*4v,9*1*17$2f,12*0*26,14*23*-1$-1$a$s$4h$64$fa,15*11*-1$j$6a";
w["used"]="1*10*-1$n,8*3*u$1v$2c$2k,12*0*p,13*0*1m,15*0*3g";
w["user"]="2*0*15n,13*0*2s,14*30*1$5,15*0*5n";
w["user-defined"]="2*0*15m";
w["uses"]="2*0*ki";
w["using"]="2*2*bc$17u$19e,5*0*40,9*30*0$4,14*11*-1$l$4a,15*31*0$8$7c";
w["utf"]="0*1*1i$2n,4*1*1f$3b,7*0*r,11*0*15";
w["utf-8"]="0*1*1h$2m,4*1*1e$3a,7*0*q,11*0*14";
w["v"]="4*0*3q";
w["v16"]="5*0*44";
w["value"]="2*10*3r$dp$gr$un$116$13f$13q$16r$1f9$1gg$1gr,4*9*1a$1n$24$2i$3u$4d$5j$62$6q$74,7*4*19$28$2p$3l$3n,8*0*2u,14*3*bi$bv$e1$ll,15*0*6f";
w["value."]="7*0*3k";
w["values"]="1*11*-1$f$2i,4*1*22$5d,14*0*fv";
w["values."]="4*0*21";
w["vanced"]="15*0*bn";
w["variable"]="4*1*34$4q,15*1*58$5f";
w["variables"]="2*0*bk,4*0*9k,7*0*4h,15*0*47";
w["various"]="2*12*-1$b$u$kb,13*10*-1$b";
w["very"]="2*3*47$9v$l9$17j";
w["w"]="14*3*dk$gl$h5$hq";
w["want"]="9*0*2s";
w["we"]="2*23*1c$51$5h$68$6o$82$8h$8t$94$a9$ah$d9$di$e2$eh$eo$gf$jr$k3$l2$17a$18c,7*1*4p$59,14*4*4g$5g$d1$et$f9";
w["weather"]="4*3*ap$bi$bl$c4";
w["web"]="14*0*6v";
w["well"]="4*0*c2,6*10*-1$h";
w["when"]="2*0*jl,5*0*1n,9*0*29,15*1*2f$7b";
w["where"]="2*0*19m,9*0*2q";
w["which"]="2*1*h0$pj,4*6*26$2k$40$5l$6r$9i$at,7*2*2a$2r$4f,13*0*1k,14*2*dn$e0$ej";
w["while"]="14*0*2d";
w["width"]="13*1*5t$62";
w["willbe"]="2*15*3n$13b$1gc,14*16*5p$be$br$fc$li";
w["within"]="2*11*-1$e$12,5*2*p$24$3q,8*11*-1$f$14,12*0*1i,13*10*-1$e,14*10*-1$1c,15*1*83$8t";
w["work"]="14*0*27";
w["workflow"]="5*0*32";
w["workflow."]="5*0*31";
w["worksheet"]="2*0*hg,6*12*-1$g$1f$23,8*1*2f$3c,10*10*-1$c,13*0*1q,15*1*6v$b7";
w["worksheet."]="6*0*22,8*1*2e$3b,13*0*1p";
w["worksheets"]="15*0*3r";
w["worksheets."]="15*0*3q";
w["workshop"]="14*10*-1$1g";
w["workshop."]="14*10*-1$1f";
w["worry"]="5*0*2d";
w["would"]="2*1*ht$ug";
w["wrapper"]="2*1*t7$u7";
w["write"]="9*0*1k";
w["written"]="14*1*48$5c";
w["x"]="2*3*p3$p7$1aq$1au,3*32*2$5$17$19,7*1*73$74,13*1*37$3f,14*11*7l$84$86$8a$8d$91$9g$9i$9m$9p$kl$lr";
w["x.is_stop"]="14*1*89$9l";
w["x:"]="7*0*72";
w["you"]="2*1*cj$u3,5*14*-1$5$28$2k$37$3u,9*3*16$25$2a$2r,14*22*-1$-1$8$p$22$2l,15*11*-1$g$2g";
w["your"]="5*1*2v$46,9*13*-1$j$13$1l$1t,12*0*1t,14*12*-1$18$24$2n,15*1*19$66";
w["z"]="13*1*3d$3e";
w["}"]="2*1*nl$1b9";
w["—"]="14*0*5r";
w["—that"]="14*0*62";

return {'fil': fil, 'w': w}})();
